<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('side-menu'); ?>
    <?php echo $__env->make('admin.side_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content_add_form'); ?>
    <div class="form-group<?php echo e($errors->has('mobile_number') ? ' has-error' : ''); ?>">
        <label for="mobile_number" class="col-md-2 pull-right control-label">شماره موبایل:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="mobile_number" value="<?php echo e(old('mobile_number',isset($u->mobile_number) ? $u->mobile_number : '')); ?>" autocomplete="off">
            <?php if($errors->has('mobile_number')): ?><span class="help-block"><strong><?php echo e($errors->first('mobile_number')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('fullname') ? ' has-error' : ''); ?>">
        <label for="fullname" class="col-md-2 pull-right control-label">نام و نام خانوادگی:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="fullname" value="<?php echo e(old('fullname',isset($u->fullname) ? $u->fullname : '')); ?>" autocomplete="off">
            <?php if($errors->has('fullname')): ?><span class="help-block"><strong><?php echo e($errors->first('fullname')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
        <label for="password" class="col-md-2 pull-right control-label">کد اعتبارسنجی:</label>
        <div class="col-md-6 pull-right">
            <input type="password" class="form-control" name="password" value="<?php echo e(old('password',isset($u->password) ? '**||password-no-changed' : '')); ?>" autocomplete="off">
            <?php if($errors->has('password')): ?><span class="help-block"><strong><?php echo e($errors->first('password')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('avatar_dir') ? ' has-error' : ''); ?>">
        <label for="avatar_dir" class="col-md-2 pull-right control-label">تصویر پروفایل:</label>
        <div class="col-md-4 pull-right">
            <input type="file" onchange="readURL(this,'','img_preview')" name="avatar_dir" id="avatar_dir" value="<?php echo e(old('avatar_dir',isset($u->avatar_dir) ? $u->avatar_dir : '')); ?>" autocomplete="off">
            <?php if($errors->has('avatar_dir')): ?> <span class="help-block"><strong><?php echo e($errors->first('avatar_dir')); ?></strong></span> <?php endif; ?>
        </div>
        <div class="col-md-4 pull-right">
            <img id="img_preview" class="<?php echo e(isset($u->avatar_dir) ? '' : 'hide'); ?>" src="<?php echo e(isset($u->avatar_dir)?url($u->avatar_dir) : ''); ?>" alt="تصویر پروفایل" autocomplete="off" />
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('user_status') ? ' has-error' : ''); ?>">
        <label for="user_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='user_status' class='selectpicker form-control pull-right' autocomplete="off">
                <option <?php if(old('user_status' ,isset($u->user_status) ? $u->user_status : '' )==1): ?> selected <?php endif; ?> value="1" >فعال</option>
                <option <?php if(old('user_status' , isset($u->user_status) ? $u->user_status : '')==0): ?> selected <?php endif; ?> value="0" >غیر فعال</option>
            </select>
            <?php if($errors->has('user_status')): ?><span class="help-block"><strong><?php echo e($errors->first('user_status')); ?></strong></span><?php endif; ?>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('jsCustom'); ?>
    <script type="text/javascript">
        function readURL(input,img_id,img_preview_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+img_preview_id+img_id)
                        .attr('src', e.target.result)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
                $('#'+img_preview_id+img_id).removeClass('hide');
            }
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-add', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
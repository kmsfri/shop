<?php $__env->startSection('main'); ?>

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="<?php echo e(url('/')); ?>">خانه</a></li>/
                <li>اطلاعات کاربری</li>
            </ul>
        </div>
    </div>



    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <div class="rowContainer">
                    <h1 class="tt-title-subpages noborder">اطلاعات کاربری</h1>
                </div>

                <div class="rowContainer">
                    <?php echo $__env->make('landing.panel.common.sideMenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <div class="customColumn pull-left-custom panel-contentCont">
                        <div class="panel rowPanel">



                            <div class="customContainer1">
                                <form action="<?php echo e(Route('saveUserInfo')); ?>" method="POST">
                                    <?php echo e(csrf_field()); ?>

                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <input name="fullname" class="customInput1" type="text" id="fname" placeholder="نام و نام خانوادگی شما..." value="<?php echo e(old('fullname',isset($user->fullname) ? $user->fullname : '')); ?>">
                                            <?php if($errors->has('fullname')): ?><span class="help-block"><strong><?php echo e($errors->first('fullname')); ?></strong></span><?php endif; ?>
                                        </div>
                                        <div class="custom-col-25">
                                            <label class="customLabel" for="fname">نام و نام خانوادگی</label>
                                        </div>
                                    </div>

                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <textarea name="address" class="customInput1" id="subject"  style="height:200px"><?php echo e(old('address',isset($user->address) ? Helpers::br2nl($user->address) : '')); ?></textarea>
                                            <?php if($errors->has('addres')): ?><span class="help-block"><strong><?php echo e($errors->first('addres')); ?></strong></span><?php endif; ?>
                                        </div>
                                        <div class="custom-col-25">
                                            <label class="customLabel" for="subject">آدرس</label>
                                        </div>
                                    </div>
                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <input class="customSubmit1" type="submit" value="ذخیره">
                                        </div>
                                        <div class="custom-col-25">
                                        </div>

                                    </div>
                                </form>
                            </div>







                        </div>

                    </div>

                </div>

            </div>
        </div>



        <?php $__env->stopSection(); ?>
        <?php $__env->startSection('js'); ?>
            <script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
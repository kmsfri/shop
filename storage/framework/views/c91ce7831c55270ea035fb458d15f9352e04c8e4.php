<!DOCTYPE html>
<html lang="en">

<head>
    <head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="<?php echo e($masterData['website_logo']); ?>">
    <title>
        <?php echo e($masterData['title']); ?>

        <?php echo e((!empty($meta) && $meta->title!=Null)?' - '.$meta->title:''); ?>

    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php if(!empty($meta)): ?>
        <?php $__currentLoopData = $meta; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <meta name="<?php echo e($key); ?>" content="<?php echo e($m); ?>">
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
    <?php echo (isset($canonical_url))?'<link rel="canonical" href="'.$canonical_url.'">':''; ?>

    <?php if(!empty($openGraph)): ?>
        <?php $__currentLoopData = $openGraph; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$og): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <meta property="og:<?php echo e($key); ?>" content="<?php echo e($og); ?>">
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
	<link rel="stylesheet" href="<?php echo e(asset('Web/css/theme.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('Web/css/rtl.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('Web/css/owl.carousel.min.css')); ?>">
    <?php echo $__env->yieldContent('css'); ?>
</head>
<body>
<div id="loader-wrapper">
	<div id="loader">
		<div class="dot"></div>
		<div class="dot"></div>
		<div class="dot"></div>
		<div class="dot"></div>
		<div class="dot"></div>
		<div class="dot"></div>
		<div class="dot"></div>
	</div>
</div>
<div id="tt-boxedbutton">
	<div class="rtlbutton boxbutton-js">
		<div class="box-btn">نمایش</div>
		<div class="box-description">
			تغییر حالت نمایش به<strong>باکس</strong>
		</div>
		<div class="box-disable">
			<span class="tt-disable">غیر فعال</span>
		</div>
	</div>
</div>
<header>
        <div class="tt-color-scheme-01">
            <div class="container">
                <div class="tt-header-row tt-top-row">
                    <div class="tt-col-left tt-col-large2">
                        <div class="tt-box-info">
                            <ul>
                                <li><i class="icon-h-35"></i><a href="tel:+77723457885">+98123456789</a></li>
                                <li><i class="icon-h-38"></i>از 8 صبح تا 5 بعد از ظهر</li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <nav class="panel-menu">
            <ul>

                <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if(count($menu->subMenu()->where('c_status','=',1)->get())): ?>
                        <li><a href="#"><?php echo e($menu->m_title); ?></a>

                                        <ul>
                                            <?php $__currentLoopData = $menu->subMenu()->where('c_status','=',1)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_mnu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><a href="<?php echo e($sub_mnu->m_link); ?>"><?php echo e($sub_mnu->m_title); ?></a></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                        </li>
                    <?php else: ?>
                        <li><a href="<?php echo e($menu->m_link); ?>"><?php echo e($menu->m_title); ?></a></li>
                    <?php endif; ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                        </ul>




</nav>
        <div class="tt-mobile-header">
            <div class="container-fluid">
                <div class="tt-header-row">
                    <div class="tt-mobile-parent-menu">
                        <div class="tt-menu-toggle" style="display: none;"><i class="icon-h-27"></i></div>
                    </div>
                    <div class="tt-mobile-parent-search tt-parent-box"></div>
                    <div class="tt-mobile-parent-cart tt-parent-box"></div>
                    <div class="tt-mobile-parent-account tt-parent-box"></div>
                    <div class="tt-mobile-parent-multi tt-parent-box"></div>
                </div>
            </div>
            <div class="container-fluid tt-top-line">
                <div class="row">
                    <div class="tt-logo-container">
                        <a class="tt-logo tt-logo-alignment" href="<?php echo e(url('/')); ?>"><img class="tt-retina" src="<?php echo e(asset('Web/images/custom/logo.png')); ?>" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tt-desktop-header">
            <div class="container">
                <div class="tt-header-holder">
                    <div class="tt-col-obj tt-obj-logo">
                        <a class="tt-logo tt-logo-alignment" href="index.html"><img class="tt-retina loading" src="<?php echo e(asset('Web/images/custom/logo.png')); ?>" alt="" data-was-processed="true"></a>
                    </div>
                    <div class="tt-col-obj tt-obj-menu">
                        <div class="tt-desctop-parent-menu tt-parent-box">
                            <div class="tt-desctop-menu">
                                <nav>
                                    <ul class="menupadd">
                                        <li><a href="<?php echo e(url('/')); ?>">صفحه اصلی</a></li>
                                        <li><a href="<?php echo e(route('gotocompare')); ?>">مقایسه کالا</a></li>
                                        <li><a href="<?php echo e(route('products')); ?>">لیست محصولات</a></li>
                                        <li><a href="<?php echo e(route('showblog')); ?>">لیست مطالب</a></li>
                                            <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(count($menu->subMenu()->where('c_status','=',1)->get())): ?>
                                                <li class="dropdown tt-megamenu-col-01"><a href="#"><?php echo e($menu->m_title); ?></a>
                                                    <div class="dropdown-menu">
                                                        <div class="row tt-col-list">
                                                            <div class="col">
                                                                <ul class="tt-megamenu-submenu">
                                                                    <?php $__currentLoopData = $menu->subMenu()->where('c_status','=',1)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_mnu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <li><a href="<?php echo e($sub_mnu->m_link); ?>"><?php echo e($sub_mnu->m_title); ?></a></li>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php else: ?>
                                                <li><a href="<?php echo e($menu->m_link); ?>"><?php echo e($menu->m_title); ?></a></li>
                                            <?php endif; ?>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                        
                                       
                                    </ul>
                                </nav>
                                </div>
                            </div>
                        </div>
                        <div class="tt-col-obj tt-obj-options obj-move-right">
                            
                            <div class="tt-desctop-parent-cart tt-parent-box">
                                
                            <div class="tt-cart tt-dropdown-obj">
                                    <button class="tt-dropdown-toggle"><i class="icon-g-46"></i></button>
                                    <div class="tt-dropdown-menu">
                                        <div class="tt-mobile-add">
                                            <h6 class="tt-title">سبد</h6>
                                            <button class="tt-close">بستن</button>
                                        </div>
                                        <div class="tt-dropdown-inner">
                                            <div class="tt-cart-layout">
                                                <!-- <a href="empty-cart.html" class="tt-cart-empty"><i class="icon-g-46"></i><p>No Products in the Cart</p></a> -->
                                                <div class="tt-cart-content">
                                                    <div class="tt-cart-list">

                                                    </div>

                                                    <div class="tt-cart-btn">
                                                        <div class="tt-item"><a href="<?php echo e(route('showCart')); ?>" class="btn">نمایش سبد خرید</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div></div>
                            <div class="tt-desctop-parent-account tt-parent-box">
                                
                            <div class="tt-account tt-dropdown-obj">
                                    <button class="tt-dropdown-toggle"><i class="icon-h-36"></i></button>
                                    <div class="tt-dropdown-menu">
                                        <div class="tt-mobile-add">
                                            <button class="tt-close">بستن</button>
                                        </div>
                                        <div class="tt-dropdown-inner">
                                            <ul>
                                                <?php if(\Illuminate\Support\Facades\Auth::guard('user')->check()): ?>
                                                <li><a href="<?php echo e(route('showUserDashboard')); ?>"><i class="icon-g-91"></i>داشبورد</a></li>
                                                <li><a href="<?php echo e(route('showUserPayments')); ?>"><i class="icon-g-20"></i>پرداختی ها</a></li>
                                                <li><a href="<?php echo e(route('showUserOrderHistory')); ?>"><i class="icon-g-55"></i>لیست سفارشات</a></li>
                                                <li><a href="<?php echo e(route('do-user-logout')); ?>"><i class="icon-g-93"></i>خروج</a></li>
                                                <?php else: ?>
                                                    <li><a href="<?php echo e(route('user-login')); ?>"><i class="icon-g-92"></i>ورود</a></li>
                                                <li><a href="<?php echo e(route('user-register')); ?>"><i class="icon-h-22"></i>ثبت نام</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div></div>
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="tt-stuck-nav">
                <div class="container">
                    <div class="tt-header-row ">
                        <div class="tt-stuck-parent-menu"></div>
                        <div class="tt-stuck-parent-search tt-parent-box"></div>
                        <div class="tt-stuck-parent-cart tt-parent-box"></div>
                        <div class="tt-stuck-parent-account tt-parent-box"></div>
                        <div class="tt-stuck-parent-multi tt-parent-box"></div>
                    </div>
                </div>
            </div>
    </header>




    <?php if(!empty(session('messages'))): ?>
        <?php $__currentLoopData = session('messages'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="alert danger-red text-center">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                <?php echo e($message); ?>

            </div>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>


    <?php echo $__env->yieldContent('main'); ?>
























<footer>
        <div class="tt-footer-col tt-color-scheme-03">
            <div class="container">
                <div class="row">

                    <?php $__currentLoopData = $footerLinks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fL): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-6 col-lg-2 col-xl-3">
                        <div class="tt-mobile-collapse">
                            <h4 class="tt-collapse-title"><?php echo e($fL->m_title); ?></h4>
                            <div class="tt-collapse-content">
                                <ul class="tt-list">
                                    <?php $__currentLoopData = $fL->subMenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fLS): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(url($fLS->m_link)); ?>"><?php echo e($fLS->m_title); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <div class="col-md-6 col-lg-4 col-xl-3">
                        <div class="tt-mobile-collapse">
                            <h4 class="tt-collapse-title">تماس با ما</h4>
                            <div class="tt-collapse-content"><address><p><span>آدرس:</span> متن آدرس خیابان و کوچه</p><p><span>تلفن:</span> 123</p><p><span>ایمیل:</span> <a href="info@.com">info@mydomain.com</a>
                                    <p><span>
                                    <a href="<?php echo e(Route('showContactUsForm')); ?>">فرم تماس با ما</a>
                                        </span></p>

                                    </p></address></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xl-3">
                        <div class="tt-newsletter">
                            <div class="tt-mobile-collapse">
                                <h4 class="tt-collapse-title">عنوان</h4>
                                <div class="tt-collapse-content">
                                    <p>متن</p>
                                    <form id="newsletterform" class="form-inline form-default" method="post" novalidate="novalidate" action="#">
                                        <div class="form-group">
                                            <input type="text" name="email" class="form-control" placeholder="ایمیل را وارد کنید">
                                            <button type="submit" class="btn" style="height: 40px !important">ارسال</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <ul class="tt-social-icon">
                            <li>
                                <a class="icon-g-64" target="_blank" href="http://www.facebook.com/"></a>
                            </li>
                            <li>
                                <a class="icon-h-58" target="_blank" href="http://www.facebook.com/"></a>
                            </li>
                            <li>
                                <a class="icon-g-66" target="_blank" href="http://www.twitter.com/"></a>
                            </li>
                            <li>
                                <a class="icon-g-67" target="_blank" href="http://www.google.com/"></a>
                            </li>
                            <li>
                                <a class="icon-g-70" target="_blank" href="https://instagram.com/"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="tt-footer-custom tt-color-scheme-04">
            <div class="container">
                <div class="tt-row">
                    <div class="tt-col-left">
                        <div class="tt-col-item tt-logo-col">
                            <a class="tt-logo tt-logo-alignment" href="index.html"><img class="tt-retina loading" src="images/custom/logo-white.png" alt="" data-was-processed="true"></a>
                        </div>
                        <div class="tt-col-item">
                            <div class="tt-box-copyright">© تمامی حقوق محفوظ است.1397</div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </footer>
<a href="#" class="tt-back-to-top">بازگشت به بالا</a>



<?php echo $__env->yieldContent('js'); ?>

</body>

</html>
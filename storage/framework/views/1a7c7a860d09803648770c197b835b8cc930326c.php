<?php $__env->startSection('content_add_form'); ?>

    <div class="form-group<?php echo e($errors->has('phone_number') ? ' has-error' : ''); ?>">
        <label for="phone_number" class="col-md-2 pull-right control-label">ایمیل یا شماره تماس:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="phone_number" value="<?php echo e(old('phone_number',isset($contact->phone_number) ? $contact->phone_number : '')); ?>">
            <?php if($errors->has('phone_number')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('phone_number')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('contact_title') ? ' has-error' : ''); ?>">
        <label for="contact_title" class="col-md-2 pull-right control-label">عنوان پیام:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="contact_title" value="<?php echo e(old('contact_title',isset($contact->contact_title) ? $contact->contact_title : '')); ?>">
            <?php if($errors->has('contact_title')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('contact_title')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('contact_text') ? ' has-error' : ''); ?>">
        <label for="contact_text" class="col-md-2 pull-right control-label">متن پیام:</label>
        <div class="col-md-6 pull-right">
            <textarea class="form-control" name="contact_text"><?php echo e(old('contact_text',isset($contact->contact_text) ? Helpers::br2nl($contact->contact_text) : '')); ?></textarea>
            <?php if($errors->has('contact_text')): ?><span class="help-block"><strong><?php echo e($errors->first('contact_text')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('viewed') ? ' has-error' : ''); ?>">
        <label for="viewed" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='viewed' class='selectpicker form-control pull-right'>
                <option <?php if(old('viewed' , isset($contact->viewed) ? $contact->viewed : '')==0): ?> selected <?php endif; ?> value="0" >جدید</option>
                <option <?php if(old('viewed' , isset($contact->viewed) ? $contact->viewed : '')==1): ?> selected <?php endif; ?> value="1" >بررسی شده</option>
            </select>
            <?php if($errors->has('viewed')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('viewed')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>




<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-add', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
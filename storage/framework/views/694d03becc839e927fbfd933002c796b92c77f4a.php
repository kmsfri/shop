<div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">

            <ul class="sidebar-nav">

                <li class="sidebar-brand col-md-12">
                      <div><img class="img img-thumbnail img-responsive" src="<?php echo e(url( '/uploads/users/admin/'.Auth::guard('admin')->user()->avatar_dir)); ?>" id="side-menu-avatar"></div>
                      <div><?php echo e(Auth::guard('admin')->user()->user_title); ?></div>
                      <div><?php echo e(Auth::guard('admin')->user()->description); ?></div>
                      <hr class="hr1">
                </li>

                <li><a class="master-el side-el-dash" href="<?php echo e(url('admin/dashboard')); ?>">پیشخوان</a></li>
                <li>
                    <a class="master-el" href="javascript::void();">کاربران<img src="<?php echo e(url('uploads/static/admin/arrow.png')); ?>"></a>
                    <ul class="inner-ul">
                        <li><a class="master-el" href="<?php echo e(Route('userList')); ?>">کاربران وبسایت<span></span></a></li>
                        <li><a class="master-el" href="<?php echo e(Route('adminList')); ?>">کاربران بخش مدیریت<span></span></a></li>
                    </ul>
                </li>
                <li><a class="master-el" href="<?php echo e(Route('adminCategories')); ?>">دسته بندیها</a></li>
                <li><a class="master-el" href="<?php echo e(Route('adminBrands')); ?>">برندها</a></li>
                <li><a class="master-el" href="<?php echo e(Route('adminLists')); ?>">لیستها</a></li>
                <li><a class="master-el" href="<?php echo e(Route('advertisesList')); ?>">محصولات</a></li>

                <li><a class="master-el" href="<?php echo e(Route('adminpCategories')); ?>">دسته بندی مطالب</a></li>
                <li><a class="master-el" href="<?php echo e(Route('adminComments')); ?>">نظرات محصولات</a></li>
                <li><a class="master-el" href="<?php echo e(Route('adminPostComments')); ?>">نظرات مطالب</a></li>
                <li><a class="master-el" href="<?php echo e(Route('adminContacts')); ?>">تماس با ما</a></li>
                <li><a class="master-el" href="<?php echo e(Route('adminDiscounts')); ?>">تخفیف ها</a></li>
                <li><a class="master-el" href="<?php echo e(Route('properties')); ?>">خصوصیات و مقادیر</a></li>
                <li><a class="master-el" href="#">پرداخت ها</a></li>
                <li>
                    <a class="master-el" href="javascript::void();">محتوا<img src="<?php echo e(url('uploads/static/admin/arrow.png')); ?>"></a>
                    <ul class="inner-ul">
                        <li><a class="master-el" href="<?php echo e(Route('PostsList')); ?>">مطالب</a></li>
                        <li><a class="master-el" href="<?php echo e(Route('notesList')); ?>">یادداشت ها<span></span></a></li>
                        <li><a class="master-el" href="<?php echo e(Route('adminSlides')); ?>">اسلایدر اصلی<span></span></a></li>
                        <li><a class="master-el" href="<?php echo e(Route('adminSlides2')); ?>">اسلایدر 2<span></span></a></li>
                        <li><a class="master-el" href="<?php echo e(Route('menus')); ?>">منوها<span></span></a></li>
                        <li><a class="master-el" href="<?php echo e(Route('footerLinks')); ?>">لینک های فوتر<span></span></a></li>
                        <li><a class="master-el" href="<?php echo e(Route('editConfig')); ?>">پیکربندی وبسایت<span></span></a></li>
                    </ul>
                </li>


            </ul>
        </div>
        <!-- /#sidebar-wrapper -->


</div>
    <!-- /#wrapper -->

    <!-- Menu Toggle Script -->
    <script>

    $(".master-el").click(function() {
      $('.inner-ul').hide('fast' , 'swing');
      $(this).siblings('.inner-ul').slideDown('fast');
      //$(this).siblings('.inner-ul').slideToggle();
    });


    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('side-menu'); ?>
    <?php echo $__env->make('admin.side_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content_add_form'); ?>
    <div class="form-group<?php echo e($errors->has('post_title') ? ' has-error' : ''); ?>">
        <label for="post_title" class="col-md-2 pull-right control-label">عنوان مطلب:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="post_title" value="<?php echo e(old('post_title',isset($post->post_title) ? $post->post_title : '')); ?>" autocomplete="off">
            <?php if($errors->has('post_title')): ?><span class="help-block"><strong><?php echo e($errors->first('post_title')); ?></strong></span><?php endif; ?>
        </div>
    </div>
    <div class="form-group<?php echo e($errors->has('short_text') ? ' has-error' : ''); ?>">
        <label for="short_text" class="col-md-2 pull-right control-label">توضیحات کوتاه مطلب:</label>
        <div class="col-md-6 pull-right">
            <textarea class="form-control" name="short_text"><?php echo e(old('short_text',isset($post->short_text) ? Helpers::br2nl($post->short_text) : '')); ?></textarea>
            <?php if($errors->has('short_text')): ?><span class="help-block"><strong><?php echo e($errors->first('short_text')); ?></strong></span><?php endif; ?>
        </div>
    </div>
    <div class="form-group<?php echo e($errors->has('main_text') ? ' has-error' : ''); ?>">
        <label for="main_text" class="col-md-2 pull-right control-label">توضیحات مطلب:</label>
        <div class="col-md-6 pull-right">
            <textarea class="form-control" name="main_text"><?php echo e(old('main_text',isset($post->main_text) ? Helpers::br2nl($post->main_text) : '')); ?></textarea>
            <?php if($errors->has('main_text')): ?><span class="help-block"><strong><?php echo e($errors->first('main_text')); ?></strong></span><?php endif; ?>
        </div>
    </div>
    <div class="form-group<?php echo e($errors->has('post_tags') ? ' has-error' : ''); ?>">
        <label for="post_tags" class="col-md-2 pull-right control-label">برچسب ها(با خط تیره جدا کنید):</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="post_tags" value="<?php echo e(old('post_tags',isset($post->post_tags) ? $post->post_tags : '')); ?>" autocomplete="off">
            <?php if($errors->has('post_tags')): ?><span class="help-block"><strong><?php echo e($errors->first('post_tags')); ?></strong></span><?php endif; ?>
        </div>
    </div>
    <div class="form-group<?php echo e($errors->has('meta_keywords') ? ' has-error' : ''); ?>">
        <label for="meta_keywords" class="col-md-2 pull-right control-label">کلمات کلیدی سئو:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="meta_keywords" value="<?php echo e(old('meta_keywords',isset($post->meta_keywords) ? $post->meta_keywords : '')); ?>" autocomplete="off">
            <?php if($errors->has('meta_keywords')): ?><span class="help-block"><strong><?php echo e($errors->first('meta_keywords')); ?></strong></span><?php endif; ?>
        </div>
    </div>
    <div class="form-group<?php echo e($errors->has('post_slug') ? ' has-error' : ''); ?>">
        <label for="post_slug" class="col-md-2 pull-right control-label">آدرس url مطلب:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="post_slug" value="<?php echo e(old('post_slug',isset($post->post_slug) ? $post->post_slug : '')); ?>" autocomplete="off">
            <?php if($errors->has('post_slug')): ?><span class="help-block"><strong><?php echo e($errors->first('post_slug')); ?></strong></span><?php endif; ?>
        </div>
    </div>
    <div class="form-group<?php echo e($errors->has('post_status') ? ' has-error' : ''); ?>">
        <label for="post_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='post_status' class='selectpicker form-control pull-right' autocomplete="off">
                <option <?php if(old('post_status' ,isset($post->post_status) ? $post->post_status : '' )==1): ?> selected <?php endif; ?> value="1" >فعال</option>
                <option <?php if(old('post_status' , isset($post->post_status) ? $post->post_status : '')==0): ?> selected <?php endif; ?> value="0" >غیر فعال</option>
            </select>
            <?php if($errors->has('post_status')): ?><span class="help-block"><strong><?php echo e($errors->first('post_status')); ?></strong></span><?php endif; ?>
        </div>
    </div>





    <div class="form-group">
        <label for="files" class="col-md-2 pull-right control-label">تصاویر مطلب:</label>
        <div class="col-md-10 pull-right">
            <div class="box-form">
                <input id="files" type="file" name="newImg[]" multiple="multiple" autocomplete="off" accept="image/jpg, image/jpeg, image/png" /><br>
                <output id="result">
                    <?php if(isset($post) && $post!=Null): ?>
                        <?php $__currentLoopData = $post->PostImages()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $simg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div>
                                <img class="thumbnail" src="<?php echo e(url($simg->image_dir)); ?>">
                                <input name="oldImg[]" type="hidden" value="<?php echo e($simg->id); ?>">
                                <a href="javascript:void()" onclick="$(this).closest('div').remove()" style="display: block">حذف</a>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </output>
                <?php if($errors->has('newImg.*')): ?> <span class="help-block"><strong><?php echo e($errors->first('newImg.*')); ?></strong></span> <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('jsCustom'); ?>
    <script type="text/javascript">
        window.onload = function(){
            //Check File API support
            if(window.File && window.FileList && window.FileReader) {
                var filesInput = document.getElementById("files");
                filesInput.addEventListener("change", function(event){
                    var files = event.target.files; //FileList object
                    var output = document.getElementById("result");
                    $( ".newImgThumb" ).remove();
                    for(var i = 0; i< files.length; i++) {
                        var file = files[i];
                        //Only pics
                        if(!file.type.match('image'))
                            continue;

                        var picReader = new FileReader();

                        picReader.addEventListener("load",function(event){

                            var picFile = event.target;
                            var div = document.createElement("div");

                            div.innerHTML = "" +
                                "<img class='thumbnail newImgThumb' src='" + picFile.result + "'" + "title='" + picFile.name + "'/>" +
                                "<span class='newImgThumb' style=\"display: block\">تصویر جدید</span>"
                            ;
                            $(output).append(div);

                        });

                        //Read the image
                        picReader.readAsDataURL(file);
                    }

                });
            }
            else
            {
                console.log("Your browser does not support File API");
            }
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-add', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
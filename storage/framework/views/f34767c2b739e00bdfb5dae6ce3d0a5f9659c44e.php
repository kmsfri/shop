<?php $__env->startSection('main'); ?>
    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="index.html">خانه</a></li>
                <li>عنوان</li>
            </ul>
        </div>
    </div>
    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <h1 class="tt-title-subpages noborder"><?php echo e($note->note_title); ?></h1>
                <p>
                    <?php echo $note->note_body; ?>

                </p>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('js'); ?>


    <script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/slick/slick.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
    <!-- form validation and sending to mail -->
    <script src="<?php echo e(asset('Web/external/form/jquery.form.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/form/jquery.validate.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/form/jquery.form-init.js')); ?>"></script>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
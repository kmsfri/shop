<?php $__env->startSection('content_list'); ?>
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">محصول</td>
        <td class="text-center">کاربر</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>
    </thead>
    <tbody>
    <?php $c=1; ?>
    <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="<?php echo e($comment->id); ?>" type="checkbox">
            </td>
            <td class="text-center">
                <?php echo e($c); ?> <?php $c++; ?>
            </td>
            <td class="text-center">
                <?php echo e($comment->Post->post_title); ?>

            </td>
            <td class="text-center">
                <?php echo e(($comment->User->fullname==Null)?'نامشخص':$comment->User->fullname); ?>

            </td>
            <td class="text-center">
                <?php echo e(($comment->comment_status==1)?'فعال':'غیرفعال'); ?>

            </td>
            <td class="text-center">
                <a href="<?php echo e(url(Route('edit-post-comment-form',$comment->id))); ?>">
                    ویرایش
                </a>|<a href="<?php echo e(url(Route('editPost',$comment->post_id))); ?>">
                    مطلب
                </a>|<a href="<?php echo e(url(Route('editUser',$comment->user_id))); ?>">
                    کاربر
                </a>
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-lists', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
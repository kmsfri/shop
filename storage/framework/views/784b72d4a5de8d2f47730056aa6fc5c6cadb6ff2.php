<?php $__env->startSection('main'); ?>
        
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="<?php echo e(url('/')); ?>">خانه</a></li>/
			<li>ورود</li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="tt-title-subpages noborder">ورود</h1>
			<div class="tt-login-form">

				<div class="row justify-content-center">
					<div class="col-xs-12 col-md-6">
						<div class="tt-item">
							<h2 class="tt-title">ورود</h2>
							<div class="form-default form-top">
								<form id="customer_login" method="post" action="<?php echo e(Route('do-user-login')); ?>" novalidate="novalidate">
									<?php echo e(csrf_field()); ?>

									<div class="form-group">
										<label for="loginInputName">شماره موبایل *</label><br>
										<div class="tt-required">
											<?php if( Session::has('data') ): ?>
												<?php echo e(Session::get('data')); ?>

											<?php endif; ?>
										</div>
										<input type="text" name="mobile" class="form-control" id="loginInputName" placeholder="شماره موبایل خود را وارد نمایید">
									</div>
									<div class="form-group">
										<label for="loginInputEmail">رمز عبور *</label>
										<input type="password" name="password" class="form-control" id="loginInputEmail" placeholder="رمز عبور خود را وارد نمایید">
									</div>
									<div class="row">
										<div class="col-auto mr-auto">
											<div class="form-group">
												<button class="btn btn-border" type="submit">ورود</button>
											</div>
										</div>
										<div class="col-auto align-self-end">
											<div class="form-group">
												<ul class="additional-links">
													<li><a href="#">رمز عبور خود را فراموش کرده اید?</a></li>
												</ul>
											</div>
										</div>
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
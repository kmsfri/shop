<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('side-menu'); ?>
    <?php echo $__env->make('admin.side_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content_add_form'); ?>
    <input type='hidden' name='product_id' value="<?php echo e(old('product_id',isset($product_id) ? $product_id : '')); ?>">
    <div class="form-group<?php echo e($errors->has('title1') ? ' has-error' : ''); ?>">
        <label for="title1" class="col-md-2 pull-right control-label">عنوان:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="title1" value="<?php echo e(old('title1',isset($slide->title1) ? $slide->title1 : '')); ?>" autocomplete="off">
            <?php if($errors->has('title1')): ?><span class="help-block"><strong><?php echo e($errors->first('title1')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('img_link') ? ' has-error' : ''); ?>">
        <label for="img_link" class="col-md-2 pull-right control-label">لینک:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="img_link" value="<?php echo e(old('img_link',isset($slide->img_link) ? $slide->img_link : '')); ?>" autocomplete="off">
            <?php if($errors->has('img_link')): ?><span class="help-block"><strong><?php echo e($errors->first('img_link')); ?></strong></span><?php endif; ?>
        </div>
    </div>



    <div class="form-group<?php echo e($errors->has('img_dir') ? ' has-error' : ''); ?>">
        <label for="img_dir" class="col-md-2 pull-right control-label">تصویر اسلاید:</label>
        <div class="col-md-4 pull-right">
            <input type="file" onchange="readURL(this,'','img_dir_preview')" name="img_dir" id="img_dir" value="<?php echo e(old('img_dir',isset($slide->img_dir) ? $slide->img_dir : '')); ?>" autocomplete="off">
            <?php if($errors->has('img_dir')): ?> <span class="help-block"><strong><?php echo e($errors->first('avatar_dir')); ?></strong></span> <?php endif; ?>
        </div>
        <div class="col-md-4 pull-right">
            <img id="img_dir_preview" class="<?php echo e(isset($slide->img_dir) ? '' : 'hide'); ?>" src="<?php echo e(isset($slide->img_dir) ? url($slide->img_dir) : '#'); ?>" alt="تصویر اسلاید" autocomplete="off" />
        </div>
    </div>


    <div class="form-group<?php echo e($errors->has('img_order') ? ' has-error' : ''); ?>">
        <label for="img_order" class="col-md-2 pull-right control-label">ترتیب:</label>
        <div class="col-md-6 pull-right">
            <select  name='img_order' class='selectpicker form-control pull-right'>
                <?php for($i=1; $i<=20; $i++): ?>
                    <option <?php if(old('img_order' , isset($slide->img_order) ? $slide->img_order : '')==$i): ?> selected <?php endif; ?> value="<?php echo e($i); ?>" ><?php echo e($i); ?></option>
                <?php endfor; ?>
            </select>
            <?php if($errors->has('img_order')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('img_order')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>



    <div class="form-group<?php echo e($errors->has('img_status') ? ' has-error' : ''); ?>">
        <label for="img_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='img_status' class='selectpicker form-control pull-right' autocomplete="off">
                <option <?php if(old('img_status' ,isset($slide->img_status) ? $slide->img_status : '' )==1): ?> selected <?php endif; ?> value="1" >فعال</option>
                <option <?php if(old('img_status' , isset($slide->img_status) ? $slide->img_status : '')==0): ?> selected <?php endif; ?> value="0" >غیر فعال</option>
            </select>
            <?php if($errors->has('img_status')): ?><span class="help-block"><strong><?php echo e($errors->first('img_status')); ?></strong></span><?php endif; ?>
        </div>
    </div>






<?php $__env->stopSection(); ?>
<?php $__env->startSection('jsCustom'); ?>
    <script type="text/javascript">
        function readURL(input,img_id,img_preview_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+img_preview_id+img_id)
                        .attr('src', e.target.result)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
                $('#'+img_preview_id+img_id).removeClass('hide');
            }
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-add', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
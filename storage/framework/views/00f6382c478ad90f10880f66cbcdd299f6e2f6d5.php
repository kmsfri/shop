<?php $__env->startSection('content_add_form'); ?>
    <div class="form-group<?php echo e($errors->has('brand_title') ? ' has-error' : ''); ?>">
        <label for="brand_title" class="col-md-2 pull-right control-label">عنوان:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="brand_title" value="<?php echo e(old('brand_title',isset($brand->brand_title) ? $brand->brand_title : '')); ?>">
            <?php if($errors->has('brand_title')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('brand_title')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>



    <div class="form-group<?php echo e($errors->has('brand_img') ? ' has-error' : ''); ?>">
        <label for="brand_img" class="col-md-2 pull-right control-label">تصویر برند:</label>
        <div class="col-md-4 pull-right">
            <input type="file" onchange="readURL(this,'','img_preview')" name="brand_img" id="brand_img"  autocomplete="off">
            <?php if($errors->has('brand_img')): ?> <span class="help-block"><strong><?php echo e($errors->first('brand_img')); ?></strong></span> <?php endif; ?>
        </div>
        <div class="col-md-4 pull-right">
            <img id="img_preview" class="<?php echo e(isset($brand->brand_img) ? '' : 'hide'); ?>" src="<?php echo e(isset($brand->brand_img)?url($brand->brand_img) : ''); ?>" alt="تصویر برند" autocomplete="off" />
        </div>
    </div>


    <div class="form-group<?php echo e($errors->has('brand_slug_corrected') ? ' has-error' : ''); ?>">
        <label for="brand_slug" class="col-md-2 pull-right control-label">کلمات کلیدی آدرس:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="brand_slug" value="<?php echo e(old('brand_slug',isset($brand->brand_slug) ? $brand->brand_slug : '')); ?>">
            <?php if($errors->has('brand_slug_corrected')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('brand_slug_corrected')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('brand_order') ? ' has-error' : ''); ?>">
        <label for="brand_order" class="col-md-2 pull-right control-label">ترتیب:</label>
        <div class="col-md-6 pull-right">
            <select  name='brand_order' class='selectpicker form-control pull-right'>
                <?php for($i=1; $i<=40; $i++): ?>
                    <option <?php if(old('brand_order' , isset($brand->brand_order) ? $brand->brand_order : '')==$i): ?> selected <?php endif; ?> value="<?php echo e($i); ?>" ><?php echo e($i); ?></option>
                <?php endfor; ?>
            </select>
            <?php if($errors->has('brand_order')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('brand_order')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>


    <div class="form-group<?php echo e($errors->has('brand_status') ? ' has-error' : ''); ?>">
        <label for="brand_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='brand_status' class='selectpicker form-control pull-right'>
                <option <?php if(old('brand_status' , isset($brand->brand_status) ? $brand->brand_status : '')==1): ?> selected <?php endif; ?> value="1" >فعال</option>
                <option <?php if(old('brand_status' , isset($brand->brand_status) ? $brand->brand_status : '')==0): ?> selected <?php endif; ?> value="0" >غیر فعال</option>
            </select>
            <?php if($errors->has('brand_status')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('brand_status')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('jsCustom'); ?>
    <script type="text/javascript">
        function readURL(input,img_id,img_preview_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+img_preview_id+img_id)
                        .attr('src', e.target.result)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
                $('#'+img_preview_id+img_id).removeClass('hide');
            }
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-add', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('side-menu'); ?>
    <?php echo $__env->make('admin.side_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content_add_form'); ?>
<input type="hidden" name="parent_id" value="<?php echo e(old('parent_id',isset($parent_id) ? $parent_id : Null)); ?>">


<div class="form-group<?php echo e($errors->has('m_title') ? ' has-error' : ''); ?>">
    <label for="m_title" class="col-md-2 pull-right control-label">عنوان:</label>
    <div class="col-md-6 pull-right">
        <input type="text" class="form-control" name="m_title" value="<?php echo e(old('m_title',isset($data->m_title) ? $data->m_title : '')); ?>">
        <?php if($errors->has('m_title')): ?> <span class="help-block"><strong><?php echo e($errors->first('m_title')); ?></strong></span> <?php endif; ?>
    </div>
</div>

<div class="form-group<?php echo e($errors->has('m_link') ? ' has-error' : ''); ?>">
    <label for="m_link" class="col-md-2 pull-right control-label">لینک:</label>
    <div class="col-md-6 pull-right">
        <input type="text" class="form-control" name="m_link" value="<?php echo e(old('m_link',isset($data->m_link) ? $data->m_link : '')); ?>">
        <?php if($errors->has('m_link')): ?> <span class="help-block"><strong><?php echo e($errors->first('m_link')); ?></strong></span> <?php endif; ?>
    </div>
</div>


<div class="form-group<?php echo e($errors->has('c_order') ? ' has-error' : ''); ?>">
    <label for="c_order" class="col-md-2 pull-right control-label">ترتیب:</label>
    <div class="col-md-6 pull-right">
        <select  name='c_order' class='selectpicker form-control pull-right'>
            <?php for($i=1; $i<=40; $i++): ?>
                <option <?php if(old('c_order' , isset($data->c_order) ? $data->c_order : '')==$i): ?> selected <?php endif; ?> value="<?php echo e($i); ?>" ><?php echo e($i); ?></option>
            <?php endfor; ?>
        </select>
        <?php if($errors->has('c_order')): ?> <span class="help-block"><strong><?php echo e($errors->first('c_order')); ?></strong></span> <?php endif; ?>
    </div>
</div>


<div class="form-group<?php echo e($errors->has('c_status') ? ' has-error' : ''); ?>">
    <label for="c_status" class="col-md-2 pull-right control-label">وضعیت:</label>
    <div class="col-md-6 pull-right">
        <select  name='c_status' class='selectpicker form-control pull-right'>
            <option <?php if(old('c_status' , isset($data->c_status) ? $data->c_status : '')==1): ?> selected <?php endif; ?> value="1" >فعال</option>
            <option <?php if(old('c_status' , isset($data->c_status) ? $data->c_status : '')==0): ?> selected <?php endif; ?> value="0" >غیر فعال</option>
        </select>
        <?php if($errors->has('c_status')): ?> <span class="help-block"><strong><?php echo e($errors->first('c_status')); ?></strong></span> <?php endif; ?>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-add', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
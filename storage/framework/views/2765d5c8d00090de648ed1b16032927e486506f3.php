<?php $__env->startSection('main'); ?>

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="<?php echo e(url('/')); ?>">خانه</a></li>/
                <li>پرداختی ها</li>
            </ul>
        </div>
    </div>



    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <div class="rowContainer">
                    <h1 class="tt-title-subpages noborder">پرداختی ها</h1>
                </div>

                <div class="rowContainer">
                    <?php echo $__env->make('landing.panel.common.sideMenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <div class="customColumn pull-left-custom panel-contentCont">
                        <div class="panel rowPanel">
                            <table class="customTable1">
                                <tr>
                                    <th>مبلغ</th>
                                    <th>کد پرداخت</th>
                                    <th>وضعیت پرداخت</th>
                                    <th>زمان پرداخت</th>
                                </tr>
                                <?php $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($payment->paid_amount_text); ?></td>
                                        <td><?php echo e($payment->ref_id); ?></td>
                                        <td><?php echo e($payment->paid_status_text); ?></td>
                                        <td><?php echo e($payment->time_text); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </table>
                        </div>

                    </div>

                </div>

            </div>
        </div>



        <?php $__env->stopSection(); ?>
        <?php $__env->startSection('js'); ?>
            <script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('side-menu'); ?>
    <?php echo $__env->make('admin.side_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content_add_form'); ?>
    <div class="form-group<?php echo e($errors->has('m_title') ? ' has-error' : ''); ?>">
        <label for="m_title" class="col-md-2 pull-right control-label">عنوان وبسایت:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="m_title" value="<?php echo e(old('m_title',isset($m_title) ? $m_title : '')); ?>" autocomplete="off">
            <?php if($errors->has('m_title')): ?><span class="help-block"><strong><?php echo e($errors->first('m_title')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('m_website_logo') ? ' has-error' : ''); ?>">
        <label for="m_website_logo" class="col-md-2 pull-right control-label">لوگوی وب سایت:</label>
        <div class="col-md-4 pull-right">
            <input type="file" onchange="readURL(this,'','admin_img_preview')" name="m_website_logo" id="m_website_logo" value="<?php echo e(old('m_website_logo',isset($m_website_logo) ? $m_website_logo : '')); ?>" autocomplete="off">
            <?php if($errors->has('m_website_logo')): ?> <span class="help-block"><strong><?php echo e($errors->first('m_website_logo')); ?></strong></span> <?php endif; ?>
        </div>
        <div class="col-md-4 pull-right">
            <img id="admin_img_preview" class="<?php echo e((isset($m_website_logo)&&$m_website_logo!="") ? '' : 'hide'); ?>" src="<?php echo e(isset($m_website_logo) ? url($m_website_logo) : '#'); ?>" alt="تصویر لوگو" autocomplete="off" />
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('footer_about_us') ? ' has-error' : ''); ?>">
        <label for="footer_about_us" class="col-md-2 pull-right control-label">فوتر(درباره ما):</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="footer_about_us" value="<?php echo e(old('footer_about_us',isset($footer_about_us) ? $footer_about_us : '')); ?>" autocomplete="off">
            <?php if($errors->has('footer_about_us')): ?><span class="help-block"><strong><?php echo e($errors->first('footer_about_us')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('footer_support_text') ? ' has-error' : ''); ?>">
        <label for="footer_support_text" class="col-md-2 pull-right control-label">فوتر(پشتیبانی):</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="footer_support_text" value="<?php echo e(old('footer_support_text',isset($footer_support_text) ? $footer_support_text : '')); ?>" autocomplete="off">
            <?php if($errors->has('footer_support_text')): ?><span class="help-block"><strong><?php echo e($errors->first('footer_support_text')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('footer_contact_us_title') ? ' has-error' : ''); ?>">
        <label for="footer_contact_us_title" class="col-md-2 pull-right control-label">فوتر(تماس با ما):</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="footer_contact_us_title" value="<?php echo e(old('footer_contact_us_title',isset($footer_contact_us_title) ? $footer_contact_us_title : '')); ?>" autocomplete="off">
            <?php if($errors->has('footer_contact_us_title')): ?><span class="help-block"><strong><?php echo e($errors->first('footer_contact_us_title')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('delivery_price') ? ' has-error' : ''); ?>">
        <label for="delivery_price" class="col-md-2 pull-right control-label">هزینه ارسال(تومان):</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="delivery_price" value="<?php echo e(old('delivery_price',isset($delivery_price) ? $delivery_price : '')); ?>" autocomplete="off">
            <?php if($errors->has('delivery_price')): ?><span class="help-block"><strong><?php echo e($errors->first('delivery_price')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('website_description') ? ' has-error' : ''); ?>">
        <label for="website_description" class="col-md-2 pull-right control-label">توضیحات وبسایت:</label>
        <div class="col-md-6 pull-right">
            <textarea class="form-control" name="website_description"><?php echo e(Helpers::br2nl(old('website_description',isset($website_description) ? $website_description : ''))); ?></textarea>
            <?php if($errors->has('website_description')): ?><span class="help-block"><strong><?php echo e($errors->first('website_description')); ?></strong></span><?php endif; ?>
        </div>
    </div>

    <div class="form-group<?php echo e($errors->has('website_keywords') ? ' has-error' : ''); ?>">
        <label for="website_keywords" class="col-md-2 pull-right control-label">کلمات کلیدی وبسایت:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="website_keywords" value="<?php echo e(old('website_keywords',isset($website_keywords) ? $website_keywords : '')); ?>" autocomplete="off">
            <?php if($errors->has('website_keywords')): ?><span class="help-block"><strong><?php echo e($errors->first('website_keywords')); ?></strong></span><?php endif; ?>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('jsCustom'); ?>
    <script type="text/javascript">
        function readURL(input,img_id,img_preview_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+img_preview_id+img_id)
                        .attr('src', e.target.result)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
                $('#'+img_preview_id+img_id).removeClass('hide');
            }
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-add', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
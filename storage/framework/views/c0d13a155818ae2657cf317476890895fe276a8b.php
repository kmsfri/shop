<?php $__env->startSection('main'); ?>

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="<?php echo e(url('/')); ?>">خانه</a></li>/
                <li>مقایسه کالا</li>
            </ul>
        </div>
    </div>
    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <h1 class="tt-title-subpages noborder">مقایسه کالا</h1>
                <div class="tt-login-form">

                    <div class="row justify-content-center">
                        <div class="col-xs-12 col-md-6">
                            <div class="tt-item">
                                <h2 class="tt-title">انتخاب محصولات جهت مقایسه</h2>
                                <div class="form-default form-top">
                                    <form id="customer_login" method="get" action="<?php echo e(Route('productscompare')); ?>" novalidate="novalidate">
                                        <div class="form-group">
                                            <label for="loginInputName">محصول اول</label><br>
                                            <div class="tt-required">
                                                <?php if( Session::has('data') ): ?>
                                                    <?php echo e(Session::get('data')); ?>

                                                <?php endif; ?>
                                            </div>
                                            <select class="form-control" name="p1" id="loginInputName">
                                                <?php if(count($products)): ?>
                                                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($product->id); ?>"><?php echo e($product->product_title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="loginInputEmail">محصول دوم *</label>
                                            <select class="form-control" name="p2" id="loginInputEmail">
                                                <?php if(count($products)): ?>
                                                    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($product->id); ?>"><?php echo e($product->product_title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                        <div class="row">
                                            <div class="col-auto mr-auto">
                                                <div class="form-group">
                                                    <button class="btn btn-border" type="submit">مقایسه</button>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('main'); ?>
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="<?php echo e(url('/')); ?>">خانه</a></li>
			<li>لیست محصولات</li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
	<div class="container-indent">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-lg-3 col-xl-3 leftColumn aside">
					<div class="tt-btn-col-close">
						<a href="#">بستن</a>
					</div>
					<div class="tt-collapse open tt-filter-detach-option">
						<div class="tt-collapse-content">
							<div class="filters-mobile">
								<div class="filters-row-select">

								</div>
							</div>
						</div>
					</div>

					<div class="tt-collapse open">
						<h3 class="tt-collapse-title">دسته بندی ها</h3>
						<div class="tt-collapse-content">
							<ul class="tt-list-row">
								<?php if(count($cats)): ?>
									<?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li><a href="<?php echo e(route('productscat',$cat->category_slug)); ?>"><?php echo e($cat->category_title); ?></a></li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
							</ul>
						</div>
					</div>
					<div class="tt-collapse open">
						<h3 class="tt-collapse-title">قیمت(از - تا)</h3>
						<div class="tt-collapse-content">
							<ul class="tt-list-row">
								<form method="get" action="<?php echo e(route('productsprice')); ?>"
								<li>
									<div class="form-group">
										<input type="number" name="from" class="form-control" placeholder="از قیمت(تومان)">
									</div>
								</li>
								<li>
									<div class="form-group">
										<input type="number" name="to" class="form-control" placeholder="تا قیمت(تومان)">
									</div>
								</li>
								<li>
									<div class="form-group">
										<button type="submit" class="btn" style="height: 40px !important">فیلتر</button>
									</div>
								</li>
								</form>
							</ul>
						</div>
					</div>
					<div class="tt-collapse open">
						<h3 class="tt-collapse-title">برندها</h3>
						<div class="tt-collapse-content">


								<?php if(count($brands)): ?>
									<?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<a href="<?php echo e(route('productsbrand',$brand->brand_slug)); ?>"><img src="<?php echo e(asset($brand->brand_img)); ?>" width="40px" height="40px"></a>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>

						</div>
					</div>
					<div class="tt-collapse open">
						<h3 class="tt-collapse-title">وضعیت</h3>
						<div class="tt-collapse-content">
							<ul class="tt-list-row">
								<li><a href="<?php echo e(route('productsslug','discounted-time')); ?>">تخفیفات زمان دار</a></li>
								<li><a href="<?php echo e(route('productsslug','discounted-price')); ?>">تخفیفات معمولی</a></li>
								<li><a href="<?php echo e(route('productsslug','fixed-price')); ?>">قیمت مقطوع</a></li>
							</ul>

						</div>
					</div>
					<div class="tt-collapse open">
						<h3 class="tt-collapse-title">محصولات تصادفی</h3>
						<div class="tt-collapse-content">
							<div class="tt-aside">
								<?php if($randProducts): ?>
									<?php $__currentLoopData = $randProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $randProduct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<a class="tt-item" href="<?php echo e(route('showProductPage',$randProduct->product_slug)); ?>">
									<div class="tt-img">
										<span class="tt-img-default"><img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php echo e(asset($randProduct->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($randProduct->product_title); ?>"></span>
									</div>
									<div class="tt-content">
										<h6 class="tt-title"><?php echo e($randProduct->product_title); ?></h6>
										<div class="tt-price">
											<?php if($randProduct->discount_percent == 0): ?>
												<span class="new-price"><?php echo e(number_format($randProduct->product_price)); ?> تومان</span>
											<?php else: ?>
												<span class="sale-price"><?php echo e(number_format($randProduct->product_price - ($randProduct->product_price * $randProduct->discount_percent) / 100)); ?> تومان</span>
												<span class="old-price"><?php echo e(number_format($randProduct->product_price)); ?> تومان</span>
											<?php endif; ?>
										</div>
									</div>
								</a>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<?php if(isset($randpost[0])): ?>
					<div class="tt-content-aside">
						<a href="<?php echo e(route('showsingleblog',$randpost[0]->post_slug)); ?>" class="tt-promo-03">
							<img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php if($randpost[0]->PostImages()->count() > 0): ?> <?php echo e(asset($randpost[0]->PostImages()->first()->image_dir)); ?> <?php endif; ?>" alt="<?php echo e($randpost[0]->post_title); ?>">
							<div class="tt-description tt-point-v-t">
								<div class="tt-description-wrapper">
									<div class="tt-title-large"><?php echo e($randpost[0]->post_title); ?></div>
									<p>
										<?php echo e($randpost[0]->short_text); ?>

									</p>
								</div>
							</div>
						</a>
					</div>
						<?php endif; ?>
				</div>
				<div class="col-md-12 col-lg-9 col-xl-9">
					<div class="content-indent container-fluid-custom-mobile-padding-02">
						<div class="tt-filters-options">
							<h1 class="tt-title">
								لیست محصولات
							</h1>
							<div class="tt-btn-toggle">
								<a href="#">فیلتر</a>
							</div>
							<div class="tt-quantity">
								<a href="#" class="tt-col-one" data-value="tt-col-one"></a>
								<a href="#" class="tt-col-two" data-value="tt-col-two"></a>
								<a href="#" class="tt-col-three" data-value="tt-col-three"></a>
								<a href="#" class="tt-col-four" data-value="tt-col-four"></a>
								<a href="#" class="tt-col-six" data-value="tt-col-six"></a>
							</div>
						</div>
						<div class="tt-product-listing row">
							<form id="orderFrm" method="POST" action="<?php echo e(Route('addProductToCart')); ?>">
								<?php echo e(csrf_field()); ?>

								<input type="hidden" id="prd_id" name="product_id" value="" autocomplete="off">
								<input name="order_count" type="hidden" value="1" size="5"/>
							</form>
							<?php if(count($products)): ?>
								<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="col-sm-12 col-md-4 tt-col-item">
								<div class="tt-product">
									<div class="tt-image-box">
										<a href="<?php echo e(route('showProductPage',$product->product_slug)); ?>">
											<span class="tt-img-default"><img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php echo e(asset($product->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($product->product_title); ?>"></span>
										</a>
										<?php if($product->countdown_to != null && $product->countdown_to >= date('Y-m-d').'00:00:00'): ?> )
										<div class="tt-countdown_box">
											<div class="tt-countdown_inner">
												<div class="tt-countdown"
													 data-date="<?php echo e($product->countdown_to); ?>"
													 data-year="سال"
													 data-month="ماه"
													 data-week="هفته"
													 data-day="روز"
													 data-hour="ساعت"
													 data-minute="دقیقه"
													 data-second="ثانیه"></div>
											</div>
										</div>
											<?php endif; ?>
									</div>
									<div class="tt-description">
										<div class="tt-row">
											<ul class="tt-add-info">
												<li><a href="#">برند</a></li>
											</ul>
											<div class="tt-rating">
												<i class="icon-star"></i>
												<i class="icon-star"></i>
												<i class="icon-star"></i>
												<i class="icon-star-half"></i>
												<i class="icon-star-empty"></i>
											</div>
										</div>
										<h2 class="tt-title"><a href="<?php echo e(route('showProductPage',$product->product_slug)); ?>"><?php echo e($product->product_title); ?></a></h2>
										<div class="tt-price">

											<?php if($product->discount_percent == 0): ?>
												<span class="new-price"><?php echo e(number_format($product->product_price)); ?> تومان</span>
											<?php else: ?>
												<span class="new-price"><?php echo e(number_format($product->product_price - ($product->product_price * $product->discount_percent) / 100)); ?> تومان</span>
												<span class="old-price"><?php echo e(number_format($product->product_price)); ?> تومان</span>
											<?php endif; ?>
										</div>
										<div class="tt-product-inside-hover">
											<a onclick="addToCart(this.className)" class="<?php echo e($product->id); ?> tt-btn-addtocart">افزودن به سبد خرید</a>
										</div>
									</div>
								</div>
							</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php else: ?>
								<p>محصولی پیدا نشد.</p>
							<?php endif; ?>



							
						</div>
						<div class="text-center tt_product_showmore">
							<div class="tt-pagination"><?php echo str_replace('/?', '?', $products->render()); ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/slick/slick.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/perfect-scrollbar/perfect-scrollbar.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/countdown/jquery.plugin.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/countdown/jquery.countdown.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<!-- form validation and sending to mail -->
<script src="<?php echo e(asset('Web/external/form/jquery.form.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/form/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/form/jquery.form-init.js')); ?>"></script>
<script>
    function addToCart(classname) {
        var res = classname.split(" ");
        $('#prd_id').val(res[0]);
        document.getElementById('orderFrm').submit();
    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
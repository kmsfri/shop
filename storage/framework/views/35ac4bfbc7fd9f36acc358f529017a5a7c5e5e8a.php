<?php $__env->startSection('main'); ?>
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="<?php echo e(url('/')); ?>">خانه</a></li>/
			<li><a href="<?php echo e(route('showblog')); ?>">لیست مطالب</a></li>
			<li><?php echo e($post->post_title); ?></li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
	<div class="container-indent">
		<div class="container container-fluid-custom-mobile-padding">
			<div class="row justify-content-center">
				<div class="col-xs-12 col-md-10 col-lg-8 col-md-auto">
					<div class="tt-post-single" style="text-align: right;">
						<h1 class="tt-title">
							<?php echo e($post->post_title); ?>

						</h1>
						<div class="tt-autor">
							توسط:  <span> <?php echo e($post->Author()->first()->user_title); ?> </span>
							<br>
							<span><?php echo e(Helpers::convert_date_g_to_j($post->created_at,true)); ?></span>
						</div>
						<div class="tt-post-content">
							<!-- slider -->
							<?php if(count($post->PostImages()->get())): ?>
							<div class="tt-slider-blog-single slick-animated-show-js">
								<?php $__currentLoopData = $post->PostImages()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div><img src="<?php echo e(asset($img->image_dir)); ?>" alt=""></div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>

							<div class="tt-slick-row">
								<div class="item">
									<div class="tt-slick-quantity">
										<span class="account-number">1</span> / <span class="total">1</span>
									</div>
								</div>
								<div class="item">
									<div class="tt-slick-button">
										<button type="button" class="slick-arrow slick-prev">قبلی</button>
										<button type="button" class="slick-arrow slick-next">بعدی</button>
									</div>
								</div>
							</div>
						<?php endif; ?>
							<!-- /slider -->
							<h2 class="tt-title text-right" dir="rtl"><?php echo e($post->short_text); ?></h2>
							<p class="text-right">
								<?php echo e($post->main_text); ?>


							</p>
							<p>
								
							</p>
						</div>
						<div class="post-meta">
							<span class="item">
								<span>تگ:</span>
								<?php
								$tags = explode('-',$post->post_tags)


								?>
								<?php if(count($tags)): ?>
									<?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<span><a href="#"><?php echo e($tag); ?></a></span>
								,
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-indent wrapper-social-icon">
		<div class="container">
			<ul class="tt-social-icon justify-content-center">
				<li><a class="icon-g-64" href="http://www.facebook.com/"></a></li>
				<li><a class="icon-h-58" href="http://www.facebook.com/"></a></li>
				<li><a class="icon-g-66" href="http://www.twitter.com/"></a></li>
				<li><a class="icon-g-67" href="http://www.google.com/"></a></li>
				<li><a class="icon-g-70" href="https://instagram.com/"></a></li>
			</ul>
		</div>
	</div>
	<div class="container-indent">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xs-12 col-md-10 col-lg-8 col-md-auto">
					<div class="comments-single-post">
						<h6 class="tt-title-border">پست های اخیر</h6>
						<div class="tt-blog-thumb-list">
							<div class="row">
								<?php if(count($recentposts)): ?>
									<?php $__currentLoopData = $recentposts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recentpost): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="col-sm-6">
									<div class="tt-blog-thumb">
										<div class="tt-img"><a href="<?php echo e(route('showsingleblog',$recentpost->post_slug)); ?>" target="_blank"><img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php if($recentpost->PostImages()->count() > 0): ?> <?php echo e(asset($recentpost->PostImages()->first()->image_dir)); ?> <?php endif; ?>" alt=""></a></div>
										<div class="tt-title-description">
											<div class="tt-background"></div>
											<div class="tt-tag">
												<a href="#"><?php echo e($recentpost->post_title); ?></a>
											</div>
												<p><?php echo e(str_limit($recentpost->short_text,100)); ?></p>
										</div>
									</div>
								</div>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php else: ?>
									<p class="text-center">مطلبی وجود ندارد</p>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-indent">
		<div class="container container-fluid-custom-mobile-padding">
			<div class="row justify-content-center">
				<div class="col-xs-12 col-md-10 col-lg-8 col-md-auto">
					<div class="form-single-post">
						<hr>
						<?php if(count($post->Comments()->where('comment_status',1)->get())): ?>
						<?php $__currentLoopData = $post->Comments()->where('comment_status','=',1)->withPivot('comment_text')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="tt-item">
							<div class="tt-avatar">
								<a href="#"><img src="<?php echo e(asset($cm->avatar_dir)); ?>" alt="<?php echo e($cm->fullname); ?>" class="loading" data-was-processed="true"></a>
							</div>
							<div class="tt-content">
								<div class="tt-comments-info">
									<span class="username"> توسط :<span><?php echo e($cm->fullname); ?></span></span>| در تاریخ :
									<span class="time"><?php echo e(Helpers::convert_date_g_to_j($cm->pivot->created_at,true)); ?></span>
								</div>
								<p>
									<?php echo e($cm->pivot->comment_text); ?>

								</p>
							</div>
						</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif; ?>
						<hr>
						<h6 class="tt-title-border">دیدگاه شما</h6>
						<div class="form-default">
							<?php if(\Illuminate\Support\Facades\Auth::guard('user')->check()): ?>
								<?php if( Session::has('data') ): ?>
									<div class="alert">
										<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
										<?php echo e(Session::get('data')); ?>

									</div>
								<?php endif; ?>
								<form method="post" action="<?php echo e(route('submit_comment',$post->id)); ?>">
									<?php echo e(csrf_field()); ?>

									<div class="form-group">
										<label for="textarea" class="control-label">توضیحات *</label>
										<textarea class="form-control"  id="textarea" placeholder="توضیحات" name="comment_text" rows="8"></textarea>
									</div>
									<div class="form-group">
										<button type="submit" class="btn"> ارسال</button>
									</div>
								</form>
							<?php else: ?>
								<p>برای ثبت نظر باید وارد حساب کاربری خود شوید</p>
							<?php endif; ?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <?php $__env->stopSection(); ?>


 <?php $__env->startSection('js'); ?>

<script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/slick/slick.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<?php if( Session::has('data') ): ?>
	<?php echo e(Session::get('data')); ?>

<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('main'); ?>

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="<?php echo e(url('/')); ?>">خانه</a></li>/
                <li>مقایسه کالا</li>
            </ul>
        </div>
    </div>
    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <h1 class="tt-title-subpages noborder">مقایسه محصولات</h1>
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="tt-modal-quickview desctope">
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <div class="arrow-location-center slick-initialized slick-slider">
                                            <div>
                                                <div class="slick-slide slick-current slick-active">
                                                    <img src="<?php echo e(asset($product1->ProductImages()->first()->image_dir)); ?>" data-src="" alt="<?php echo e(asset($product1->ProductImages()->first()->image_dir)); ?>" class="loaded" data-was-processed="true">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <div class="tt-product-single-info">

                                        <h2 class="tt-title"><a href="<?php echo e(route('showProductPage',$product1->product_slug)); ?>"><?php echo e($product1->product_title); ?></a></h2>
                                        <div class="tt-price">
                                            <?php if($product1->discount_percent == 0): ?>
                                                <span class="new-price"><?php echo e(number_format($product1->product_price)); ?> تومان</span>
                                            <?php else: ?>
                                                <span class="new-price"><?php echo e(number_format($product1->product_price - ($product1->product_price * $product1->discount_percent) / 100)); ?> تومان</span>
                                                <span class="old-price"><?php echo e(number_format($product1->product_price)); ?> تومان</span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="tt-review">
                                            <div class="tt-rating">
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star-half"></i>
                                                <i class="icon-star-empty"></i>
                                            </div>
                                        </div>
                                        <hr>
                                        <table class="tt-table-03">
                                            <tbody>

                                            <?php $__currentLoopData = $product1->properties; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($pr['propTitle']); ?>:</td>
                                                    <td><?php echo e($pr['propValue']); ?></td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </tbody>
                                        </table>
                                        <hr>
                                        <div class="tt-wrapper">
                                            <?php echo e($product1->product_description); ?>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="tt-modal-quickview desctope">
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <div class="arrow-location-center slick-initialized slick-slider">
                                        <div>
                                            <div class="slick-slide slick-current slick-active">
                                                <img src="<?php echo e(asset($product2->ProductImages()->first()->image_dir)); ?>" data-src="<?php echo e(asset($product2->ProductImages()->first()->image_dir)); ?>" alt="" class="loaded" data-was-processed="true">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <div class="tt-product-single-info">

                                        <h2 class="tt-title"><a href="<?php echo e(route('showProductPage',$product2->product_slug)); ?>"><?php echo e($product2->product_title); ?></a></h2>
                                        <div class="tt-price">
                                            <?php if($product2->discount_percent == 0): ?>
                                                <span class="new-price"><?php echo e(number_format($product2->product_price)); ?> تومان</span>
                                            <?php else: ?>
                                                <span class="new-price"><?php echo e(number_format($product2->product_price - ($product2->product_price * $product2->discount_percent) / 100)); ?> تومان</span>
                                                <span class="old-price"><?php echo e(number_format($product2->product_price)); ?> تومان</span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="tt-review">
                                            <div class="tt-rating">
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star-half"></i>
                                                <i class="icon-star-empty"></i>
                                            </div>
                                        </div>
                                        <hr>
                                        <table class="tt-table-03">
                                            <tbody>

                                            <?php $__currentLoopData = $product2->properties; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($pr['propTitle']); ?>:</td>
                                                    <td><?php echo e($pr['propValue']); ?></td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </tbody>
                                        </table>
                                        <hr>
                                        <div class="tt-wrapper">
                                            <?php echo e($product2->product_description); ?>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('side-menu'); ?>
    <?php echo $__env->make('admin.side_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div id="dashboard-cont">
    <div class="row">
        <div class="col-lg-3 pull-right">
            <div class="row">
                <div class="pull-right dash-row1-sec1">0</div>
                <div class="pull-right dash-row1-sec2">فروشگاه ها</div>
            </div>
        </div>
        <div class="col-lg-3 pull-right">
            <div class="row">
                <div class="pull-right dash-row1-sec1">0</div>
                <div class="pull-right dash-row1-sec2">کاربران تایید شده</div>
            </div>
        </div>
        <div class="col-lg-3 pull-right">
            <div class="row">
                <div class="pull-right dash-row1-sec1">0</div>
                <div class="pull-right dash-row1-sec2">عنوان تست</div>
            </div>
        </div>
        <div class="col-lg-3 pull-right">
            <div class="row">
                <div class="pull-right dash-row1-sec1">0</div>
                <div class="pull-right dash-row1-sec2">عنوان تست</div>
            </div>
        </div>
    </div>
    <div class="row dash-row">
        <div class="col-lg-6 pull-right out-col">
            <div class="row">

                <div class="col-lg-6">
                    <div class="row dash-row2-right-sec1 dash-paid-amount"></br>0 تومان</div>
                    <div class="row dash-row2-right-sec2">کل واریزی</div>
                </div>
                <div class="col-lg-6">
                    <div class="row dash-row2-right-sec1">0</div>
                    <div class="row dash-row2-right-sec2">کل آگهی ها</div>
                </div>

            </div>
        </div>
        <div class="col-lg-6 pull-right out-col">
            <div class="row">

                <div class="col-lg-6 pull-right dash-row2-left-el">
                    <div class="row">
                        <div class="pull-right dash-row2-left-sec1">0</div>
                        <div class="pull-right dash-row2-left-sec2">تماس با ما</div>
                    </div>
                </div>
                <div class="col-lg-6 pull-right dash-row2-left-el">
                    <div class="row">
                        <div class="pull-right dash-row2-left-sec1">0</div>
                        <div class="pull-right dash-row2-left-sec2">مدیران وب سایت</div>
                    </div>
                </div>
                <div class="col-lg-6 pull-right dash-row2-left-el">
                    <div class="row">
                        <div class="pull-right dash-row2-left-sec1">0</div>
                        <div class="pull-right dash-row2-left-sec2">عنوان تست</div>
                    </div>
                </div>
                <div class="col-lg-6 pull-right dash-row2-left-el">
                    <div class="row">
                        <div class="pull-right dash-row2-left-sec1">0</div>
                        <div class="pull-right dash-row2-left-sec2">عنوان تست</div>
                    </div>
                </div>
                <div class="col-lg-6 pull-right dash-row2-left-el">
                    <div class="row">
                        <div class="pull-right dash-row2-left-sec1">0</div>
                        <div class="pull-right dash-row2-left-sec2">عنوان تست</div>
                    </div>
                </div>
                <div class="col-lg-6 pull-right dash-row2-left-el">
                    <div class="row">
                        <div class="pull-right dash-row2-left-sec1">0</div>
                        <div class="pull-right dash-row2-left-sec2">sms ارسالی</div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="row dash-row">
        <div class="col-lg-3 pull-right">
            <div class="row dash-row3-right-sec1"></div>
            <div class="row dash-row3-right-sec2">
                <span class="row3-title1">بازدید امروز: </span><span class="row3-title2">0 بازدید</span>
            </div>
            <div class="row dash-row3-right-sec2">
                <span class="row3-title1">بازدید هفته: </span><span class="row3-title2">0 بازدید</span>
            </div>
            <div class="row dash-row3-right-sec2">
                <span class="row3-title1">بازدید ماه: </span><span class="row3-title2">0 بازدید</span>
            </div>
        </div>
        <div class="col-lg-9 pull-right out-col">
            <div class="row">
                <div class="col-lg-12">
                    <textarea id="noteTextArea" class="form-control" placeholder="یادداشت"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 pull-right">
                    <div class="hour-cont" id="time-cont-up">
                        <?php echo e($nowTime); ?>

                    </div>
                </div>
                <div class="col-lg-4 pull-right">
                    <div class="date-cont">
                        <?php echo e($nowDate); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript">
        $(document).ready (function (){
            startCount();
        });

        function startCount(){
            timer = setInterval(count,1000);
        }
        function count(){
            var time_shown = $("#time-cont-up").text();
            var time_chunks = time_shown.split(":");
            var hour, mins, secs;
            hour=Number(time_chunks[0]);
            mins=Number(time_chunks[1]);
            secs=Number(time_chunks[2]);

            secs++;
            if (secs==60){
                secs = 0;
                mins=mins + 1;
            }
            if (mins==60){
                mins=0;
                hour=hour + 1;
            }
            if (hour==13){
                hour=1;
            }
            $("#time-cont-up").text(hour +":" + plz(mins) + ":" + plz(secs));
        }

        function plz(digit){
            var zpad = digit + '';
            if (digit < 10) {
                zpad = "0" + zpad;
            }
            return zpad;
        }
    </script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
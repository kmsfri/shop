<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('side-menu'); ?>
    <?php echo $__env->make('admin.side_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-default add-data-panel">
    <div class="panel-heading-cs1">
        <div class="pull-left">
            <a href="<?php echo e($backward_url); ?>" class="btn btn-back-cs1">بازگشت</a>
        </div>
        <div class="panel-heading-cs1-title"><?php echo $title; ?></div>
    </div>
    <div class="panel-body panel-body-cs1">
        <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="<?php echo e(isset($post_add_url)?$post_add_url:$post_edit_url); ?>" autocomplete="off">
        <?php if($request_type=='edit'): ?>
            <input type="hidden" name="edit_id" value="<?php echo e(old('edit_id',isset($edit_id) ? $edit_id : '')); ?>">
        <?php endif; ?>
            <?php echo e(csrf_field()); ?>

            <?php echo $__env->yieldContent('content_add_form'); ?>
            <div class="form-group">
                <div class="col-md-3 pull-left">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-sign-in"></i> ذخیره
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('css'); ?>
	<link rel="stylesheet" href="<?php echo e(asset('Web/css/star-rating-svg.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main'); ?>
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="<?php echo e(url('/')); ?>">خانه</a></li>
			<li><a href="">خرید</a></li>
			<li><?php echo e($product->product_title); ?></li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
	<div class="container-indent">
		<!-- mobile product slider  -->
		<div class="tt-mobile-product-layout visible-xs">
			<div class="tt-mobile-product-slider arrow-location-center slick-animated-show-js">
				<?php $__currentLoopData = $product->ProductImages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div><img src="<?php echo e(asset($image->image_dir)); ?>" alt=""></div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</div>
		<!-- /mobile product slider  -->
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-6 hidden-xs">
					<div class="tt-product-single-img">
						<div>
							<button class="tt-btn-zomm tt-top-right"><i class="icon-h-06"></i></button>
							<img class="zoom-product" src="<?php echo e(asset($product->ProductImages->first()->image_dir)); ?>" data-zoom-image="<?php echo e(asset($product->ProductImages->first()->image_dir)); ?>" alt="">
						</div>
					</div>
					<div class="product-images-carousel">
						<ul id="smallGallery" class="arrow-location-02  slick-animated-show-js">
							<?php $__currentLoopData = $product->ProductImages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li><a class="<?php echo e(($product->ProductImages->first()->id==$image->id)?'zoomGalleryActive':''); ?>" href="#" data-image="<?php echo e(asset($image->image_dir)); ?>" data-zoom-image="<?php echo e(asset($image->image_dir)); ?>"><img src="<?php echo e(asset($image->image_dir)); ?>" alt="" /></a></li>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

						</ul>
					</div>
				</div>
				<div class="col-md-12 col-lg-6">
					<div class="tt-product-single-info">
						<div class="tt-add-info">
							<ul>
								<li><span>عنوان:</span><?php echo e($product->product_title); ?></li>
							</ul>
						</div>
						<h1 class="tt-title"><?php echo e($product->product_title); ?></h1>
						<div class="tt-price">
							<span class="new-price"><?php echo e(number_format($product->finalPrice)); ?> تومان</span>
							<span class="old-price" style="color: red"><?php echo e(number_format($product->product_price)); ?> تومان</span>
						</div>
						<div class="tt-review">
							<div class="tt-rating">
								<?php
								$scores = $product->Ratings()->count();
								if ($scores > 0){
                                            $sum = $product->Ratings()->sum('score_value') / $scores;
                                        }
                                        else{
                                            $sum = 0;
                                        }

								?>
								<div class="my-rating-all pull-left" data-score="<?php echo e($sum); ?>"></div>
							</div>


							<a href="<?php echo e(route('add_to_wish',$product->id)); ?>">افزودن به لیست علاقه مندی 💕 </a>
						</div>

						<div class="tt-wrapper">
							<?php echo e($product->product_description); ?>

						</div>
						<?php if($product->countdown_to != null && $product->countdown_to >= date('Y-m-d').'00:00:00'): ?>
						<div class="tt-wrapper">
							<div class="tt-countdown_box_02">
								<div class="tt-countdown_inner">
									<div class="tt-countdown"
										data-date="<?php echo e($product->countdown_to); ?>"
										data-year="سال"
										data-month="ماه"
										data-week="هفته"
										data-day="روز"
										data-hour="ساعت"
										data-minute="دقیقه"
										data-second="ثانیه"></div>
								</div>
							</div>
						</div>
						<?php endif; ?>
						<form id="orderFrm" method="POST" action="<?php echo e(Route('addProductToCart')); ?>">
							<?php echo e(csrf_field()); ?>

							<input type="hidden" name="product_id" value="<?php echo e($product->id); ?>" autocomplete="off">
						</form>
						<div class="tt-wrapper">
							<div class="tt-row-custom-01">
								<div class="col-item">
									<div class="tt-input-counter style-01">
										<span class="minus-btn"></span>
										<input form="orderFrm" name="order_count" type="text" value="1" size="5"/>
										<span class="plus-btn"></span>
									</div>
								</div>
								<div class="col-item">
									<button type="submit" form="orderFrm" class="btn btn-lg"><i class="icon-g-46"></i>افزودن به سبد خرید</button>

								</div>
							</div>
						</div>
						<div class="tt-wrapper">
							<div class="tt-add-info">
								<ul>
									<li><span>دسته بندی:</span>
										<?php $__currentLoopData = $product->Categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ctg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php echo e(($product->Categories->first()->id!=$ctg->id)?' , ':''); ?>

											<a href="<?php echo e(route('productscat',$ctg->category_slug)); ?>"><?php echo e($ctg->category_title); ?></a>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</li>
								</ul>
							</div>
						</div>
						<div class="tt-collapse-block">
							<div class="tt-item">
								<div class="tt-collapse-title">توضیحات</div>
								<div class="tt-collapse-content">
									<?php echo e($product->product_description); ?>

								</div>
							</div>
							<div class="tt-item">
								<div class="tt-collapse-title">مشخصات محصول</div>
								<div class="tt-collapse-content">
									<table class="tt-table-03">
										<tbody>

											<?php $__currentLoopData = $product->properties; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<tr>
													<td><?php echo e($pr['propTitle']); ?>:</td>
													<td><?php echo e($pr['propValue']); ?></td>
												</tr>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</tbody>
									</table>
								</div>


								<?php $__currentLoopData = $product->selectables; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<div class="selecs clearfix">
										<div class="row">
											<div class="col-6"><span class="title"><?php echo e($sl['propTitle']); ?></span></div>
											<div class="col-6">
												<select form="orderFrm" name="selectables[<?php echo e($sl['propID']); ?>]">
													<option data-display="انتخاب <?php echo e($sl['propTitle']); ?>">انتخاب <?php echo e($sl['propTitle']); ?></option>
													<?php $__currentLoopData = $sl['propValue']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<option  value="<?php echo e($slp['property_id']); ?>"><?php echo e($slp['property_title']); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</select>
											</div>
										</div>
									</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

								<?php $__currentLoopData = $product->fillables; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<div class="selecs clearfix">
										<div class="row">
											<div class="col-6"><span class="title"><?php echo e($fl['propTitle']); ?></span></div>
											<div class="col-6 form-group">
												<input form="orderFrm" type="text" name="fillables[<?php echo e($fl['propID']); ?>]" class="form-control" placeholder="توضیحات <?php echo e($fl['propTitle']); ?>">
											</div>
										</div>
									</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

							</div>
							<div class="tt-item">
								<div class="tt-collapse-title">نظرات - (<?php echo e(count($product->Comments()->where('comment_status',1)->get())); ?> نظر )</div>
								<div class="tt-collapse-content">
									<div class="tt-review-block">
										
										<div class="tt-review-comments">
											<?php if(count($product->Comments()->where('comment_status',1)->get())): ?>
												<?php $__currentLoopData = $product->Comments()->where('comment_status','=',1)->withPivot('comment_text')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<div class="tt-item">
														<div class="tt-avatar">
															<a href="#"><img src="<?php echo e(asset($cm->avatar_dir)); ?>" alt="<?php echo e($cm->fullname); ?>" class="loading" data-was-processed="true"></a>
														</div>
														<div class="tt-content">
															<div class="tt-comments-info">
																<span class="username"> توسط :<span><?php echo e($cm->fullname); ?></span></span>| در تاریخ :
																<span class="time"><?php echo e(Helpers::convert_date_g_to_j($cm->pivot->created_at,true)); ?></span>
															</div>
															<p>
																<?php echo e($cm->pivot->comment_text); ?>

															</p>
														</div>
													</div>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											<?php endif; ?>
											<hr>
										</div>
										<div class="tt-review-form">
											<div class="tt-message-info">
												<span>شما نیز میتوانید دیدگاه خود را در رابطه با این محصول بنویسید:</span>
											</div>
											<?php if(\Illuminate\Support\Facades\Auth::guard('user')->check()): ?>
												<?php if( Session::has('data') ): ?>
													<div class="alert">
														<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
														<?php echo e(Session::get('data')); ?>

													</div>
												<?php endif; ?>
												<form method="post" action="<?php echo e(route('submit_comment_product',$product->id)); ?>">
													<div class="my-rating <?php echo e($product->id); ?>"></div>
													<?php echo e(csrf_field()); ?>

													<div class="form-group">
														<label for="textarea" class="control-label">توضیحات *</label>
														<textarea class="form-control"  id="textarea" placeholder="توضیحات" name="comment_text" rows="8"></textarea>
													</div>
													<div class="form-group">
														<button type="submit" class="btn"> ارسال</button>
													</div>
												</form>
											<?php else: ?>
												<p>برای ثبت نظر باید وارد حساب کاربری خود شوید</p>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-indent wrapper-social-icon">
		<div class="container">
			<ul class="tt-social-icon justify-content-center">
				<li><a class="icon-g-64" href="http://www.facebook.com/"></a></li>
				<li><a class="icon-h-58" href="http://www.facebook.com/"></a></li>
				<li><a class="icon-g-66" href="http://www.twitter.com/"></a></li>
				<li><a class="icon-g-67" href="http://www.google.com/"></a></li>
				<li><a class="icon-g-70" href="https://instagram.com/"></a></li>
			</ul>
		</div>
	</div>
	<?php if(count($related_products)): ?>
	<div class="container-indent">
		<div class="container container-fluid-custom-mobile-padding">
			<div class="tt-block-title text-left">
				<h3 class="tt-title-small">محصولات مشابه</h3>
			</div>
			<div class="tt-carousel-products row arrow-location-right-top tt-alignment-img tt-layout-product-item slick-animated-show-js">
				<?php $__currentLoopData = $related_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $related_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="col-2 col-md-4 col-lg-3">
					<div class="tt-product">
						<div class="tt-image-box">
							<a href="<?php echo e(route('showProductPage',$related_product->product_slug)); ?>">
								<span class="tt-img"><img src="<?php echo e(asset($related_product->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($related_product->product_title); ?>"></span>
							</a>
						</div>
						<div class="tt-description">
							<div class="tt-row">
								<ul class="tt-add-info">
									<li><a href="#">ّبرند</a></li>
								</ul>
							</div>
							<h2 class="tt-title"><a href="<?php echo e(route('showProductPage',$related_product->product_slug)); ?>"><?php echo e($related_product->product_title); ?></a></h2>
							<div class="tt-price">
								قیمت
							</div>
							<div class="tt-product-inside-hover">
								<a href="#" class="tt-btn-addtocart" >افزودن به سبد خرید</a>
							</div>
						</div>
					</div>
				</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</div>
	</div>
		<?php endif; ?>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('js'); ?>


<script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/slick/slick.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/perfect-scrollbar/perfect-scrollbar.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/rs-plugin/js/jquery.themepunch.tools.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/js/owl.carousel.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/js/jquery.star-rating-svg.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/countdown/jquery.plugin.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/countdown/jquery.countdown.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>


<script src="<?php echo e(asset('Web/external/elevatezoom/jquery.elevatezoom.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
<script>
    function product_rate(s_value,vid) {
        $.post("<?php echo e(route('rate_product',$product->id)); ?>",
            {
                s_value: s_value,
                product_id:vid
            } ,
            function(data){
                if(data != "ok"){
                    alert('مشکلی در ثبت امتیاز به وجود آمده است، در صورت بروز مجدد، صفحه را دوباره رفرش کنید');
                }

            })
            .fail(function(data) {
                console.log(data);
                alert( "شما باید وارد حساب کاربری خود شوید" );
                window.location = "<?php echo e(route('user-login')); ?>";
            })
    }
    $(".my-rating").starRating({
        starSize: 20,
        useFullStars: true,
        callback: function(currentRating, el){
            product_rate(currentRating,el[0].classList[1]);
        }
    });
    function rating(){
        $('.my-rating-all').each(function(i, obj) {
            $(obj).starRating({
                useFullStars: true,
                starSize: 15,
                readOnly: true,
                initialRating: Math.ceil($(obj).data('score')),
            });
        });
    }

    $(document).ready(function () {
        rating();
    });


</script>
<?php if( Session::has('wish') ): ?>
		<?php echo Session::get('wish'); ?>

<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content_list'); ?>
<thead>
<tr>
    <td style="width: 1px;" class="text-center"></td>
    <td class="text-left">
        <center>
            ردیف
        </center>
    </td>
    <td class="text-left">
        <center>
            عنوان
        </center>
    </td>
    <td class="text-right">
        <center>
            وضعیت
        </center>
    </td>
    <td class="text-right">
        <center>
            لینک دسترسی
        </center>
    </td>
    <td class="text-right">
        <center>
            عملیات
        </center>
    </td>

</tr>

</thead>
<tbody>
<?php $c=1; ?>
<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    <tr>
        <td class="text-center">
            <input form="delForm" name="remove_val[]" value="<?php echo e($d->id); ?>" type="checkbox">
        </td>
        <td class="text-center">

                <?php echo e($c); ?> <?php $c++; ?>

        </td>
        <td class="text-center">

            <?php if($canHasSubmenu): ?>
                <a href="<?php echo e(Route('menus',$d->id)); ?>">
            <?php endif; ?>
                <?php echo e($d->m_title); ?>

            <?php if($canHasSubmenu): ?>
                </a>
            <?php endif; ?>


        </td>
        <td class="text-center">

                <?php if($d->c_status==1): ?>
                    فعال
                <?php else: ?>
                    غیر فعال
                <?php endif; ?>

        </td>

        <td class="text-center">
            <a href="<?php echo $d->m_link; ?>">
                <?php echo $d->m_link; ?>

            </a>
        </td>

        <td class="text-center">
            <a href="<?php echo e(url('admin/menu/edit/'.$d->id)); ?>">
                ویرایش
            </a>
        </td>

    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</tbody>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-lists', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('main'); ?>

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="<?php echo e(url('/')); ?>">خانه</a></li>/
                <li>تماس با ما</li>
            </ul>
        </div>
    </div>
    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <h1 class="tt-title-subpages noborder">تماس با ما</h1>
                <div class="tt-login-form">
                    <div class="row justify-content-center">
                        <div class="col-xs-12 col-md-8">
                            <div class="tt-item">
                                <h2 class="tt-title">فیلدهای زیر را پر کنید</h2>
                                <p>
                                <form method="POST" action="<?php echo e(Route('saveContactUs')); ?>">
                                    <?php echo e(csrf_field()); ?>

                                    <div class="form-default form-top">
                                        <div class="form-group">
                                            <label>شماره تماس یا ایمیل</label><br>
                                            <input type="text" name="phone_number" class="form-control" placeholder="" value="<?php echo e(old('phone_number')); ?>">
                                            <?php if($errors->has('phone_number')): ?><span class="help-block"><strong><?php echo e($errors->first('phone_number')); ?></strong></span><?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-default form-top">
                                        <div class="form-group">
                                            <label>عنوان پیام</label><br>
                                            <input type="text" name="contact_title" class="form-control" placeholder="" value="<?php echo e(old('contact_title')); ?>">
                                            <?php if($errors->has('contact_title')): ?><span class="help-block"><strong><?php echo e($errors->first('contact_title')); ?></strong></span><?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-default form-top">
                                        <div class="form-group">
                                            <label>متن پیام</label><br>
                                            <textarea name="contact_text" class="form-control"><?php echo e(old('contact_text')); ?></textarea>
                                            <?php if($errors->has('contact_text')): ?><span class="help-block"><strong><?php echo e($errors->first('contact_text')); ?></strong></span><?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-default form-top">
                                        <div class="form-group">
                                            <button class="btn btn-border" type="submit">ارسال</button>
                                        </div>
                                    </div>
                                </form>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
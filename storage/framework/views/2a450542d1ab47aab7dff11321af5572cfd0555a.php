<?php $__env->startSection('main'); ?>

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="<?php echo e(url('/')); ?>">خانه</a></li>/
                <li>پیشخوان</li>
            </ul>
        </div>
    </div>



    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <div class="rowContainer">
                    <h1 class="tt-title-subpages noborder">پیشخوان</h1>
                </div>

                <div class="rowContainer">
                    <?php echo $__env->make('landing.panel.common.sideMenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <div class="customColumn pull-left-custom panel-contentCont">
                        <div class="panel rowPanel">



                            <div class="customContainer1">

                            </div>







                        </div>

                    </div>

                </div>

            </div>
        </div>



        <?php $__env->stopSection(); ?>
        <?php $__env->startSection('js'); ?>
            <script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
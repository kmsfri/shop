<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('side-menu'); ?>
    <?php echo $__env->make('admin.side_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel-heading-cs1">
    <div class="pull-left">
        <a href="<?php echo e($backward_url); ?>" class="btn btn-back-cs1">بازگشت</a>
    </div>
    <div class="pull-left">
        <?php if($add_url!=Null): ?>
            <a href="<?php echo e($add_url); ?>" class="btn btn-btn1-cs1">افزودن</a>
        <?php endif; ?>
    </div>
    <div class="pull-left">
        <form id="delForm" action="<?php echo e($delete_url); ?>" method="post" onsubmit="return confirm('آیا از حذف همه ی موارد انتخاب شده مطمئنید؟');">
            <?php echo e(csrf_field()); ?>

            <button type="submit" class="btn btn-btn1-cs1">حذف</button>
        </form>
    </div>
    <div class="panel-heading-cs1-title"><?php echo $title; ?></div>
</div>
<div class="panel-body panel-body-cs1">
    <div class="table-responsive table_posts_container">
        <table class="table table-striped table-hover">
            <?php echo $__env->yieldContent('content_list'); ?>
        </table>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
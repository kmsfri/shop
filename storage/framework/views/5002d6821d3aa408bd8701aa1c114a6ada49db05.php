<div class="vertical-menu customColumn pull-right-custom">
    <a href="<?php echo e(Route('showUserDashboard')); ?>" class="<?php echo e(($activeTab==1)?'active':''); ?>">پیشخوان</a>
    <a href="<?php echo e(Route('showUserInfoForm')); ?>" class="<?php echo e(($activeTab==2)?'active':''); ?>">اطلاعات کاربری</a>
    <a href="<?php echo e(Route('showUserOrderHistory')); ?>" class="<?php echo e(($activeTab==3)?'active':''); ?>">سوابق خرید</a>
    <a href="<?php echo e(Route('showUserPayments')); ?>" class="<?php echo e(($activeTab==4)?'active':''); ?>">پرداخت ها</a>
    <a href="<?php echo e(Route('showWishProductList')); ?>" class="<?php echo e(($activeTab==6)?'active':''); ?>">لیست علاقه مندیها</a>
    <a href="<?php echo e(Route('showChangePasswordForm')); ?>" class="<?php echo e(($activeTab==5)?'active':''); ?>">تغییر کلمه عبور</a>
    <a href="<?php echo e(Route('do-user-logout')); ?>">خروج از حساب کاربری</a>
</div>


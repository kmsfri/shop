<!DOCTYPE html>
<html lang="fa">
<head>
  <link rel="icon" href="<?php echo asset('uploads/static/fav.png'); ?>" type="image/x-icon" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">



  <title>پنل مدیریت</title>
  <script type="text/javascript" src="<?php echo asset('bootstrap-3.3.7/js/jquery.min.js'); ?>"></script>
  <link href="<?php echo asset('bootstrap-3.3.7/css/bootstrap-theme.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<?php echo asset('bootstrap-3.3.7/js/bootstrap.min.js'); ?>"></script>
  <link href="<?php echo asset('bootstrap-3.3.7/css/bootstrap.min.css'); ?>" media="all" rel="stylesheet" type="text/css" />

  <link href="<?php echo asset('css/custom.css'); ?>" media="all" rel="stylesheet" type="text/css" />

  <link href="<?php echo asset('css/adminSidebar.css'); ?>" media="all" rel="stylesheet" type="text/css" />




  <style>
    body *{
      border-radius:0 !important;
      direction: rtl;
    }
    ol, ul {
      list-style: none;
    }
    blockquote, q {
      quotes: none;
    }

    .sidebar-brand{
      text-align: center !important;
      padding-right: 65px !important;
    }
    .sidebar-brand a,
    .sidebar-brand span{
      float: right;
      color:white;
    }



  </style>


    <?php echo $__env->yieldContent('cssFiles'); ?>
    <?php echo $__env->yieldContent('jsFiles'); ?>
</head>
<body id="app-layout " >
    <div class="row">
        <div class="col-lg-12">
            <div class="admin-content-sec">
              <?php echo $__env->make('admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


                <style>
                    thead th{
                        text-align: right;
                    }
                </style>

                <div class="admin-content-cont">

                    <div class="row">
                        <div class="col-lg-12">
                            <?php if(session('messages')): ?>
                                <ul>
                                    <?php $__currentLoopData = session('messages'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $msg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($msg); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            <?php endif; ?>
                            <div class="panel">

                                <?php echo $__env->yieldContent('content'); ?>


                            </div>

                        </div>
                    </div>
                </div>




            </div>
            <?php echo $__env->yieldContent('side-menu'); ?>
        </div>

    </div>
    <footer class="row">
        <?php echo $__env->yieldContent('footer'); ?>
    </footer>

</body>
</html>
<?php echo $__env->yieldContent('jsCustom'); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('Web/css/star-rating-svg.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main'); ?>
<div id="tt-pageContent">
	<div class="container-indent nomargin">
		<div class="container-fluid">
			<div class="row">
                <div class="col-sm-12" style="background: linear-gradient(rgba(0, 0, 0,0.52),rgba(0, 0, 0,52)),url(<?php echo e(asset('Web/images/testimonials.jpg')); ?>) 50% 87%;
    background-size: 145%;
    background-attachment: fixed;">
                <form class="search-paralex" method="get" action="<?php echo e(route('productssearch')); ?>">
                    <input type="hidden" id="brandinput" name="brand_id" value="0">
                    <div class="input-group" style="margin-bottom: 25px;">
                                    <input type="text" class="form-control height50" name="search" placeholder="عبارت مورد نظر را برای جستجو وارد نمایید...">
                                    <button type="submit" class="btn height50">جستجو</button>
                                </div>
                                <br>
                    <?php if(count($brands)): ?>
                         <ul class="brand-list">
                            <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><img src="<?php echo e(asset($brand->brand_img)); ?>" width="50px" onclick="selectBrand(this.className);" class="<?php echo e($brand->id); ?>" height="50px" alt="<?php echo e($brand->brand_title); ?>"></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    <?php endif; ?>
                </form>
                
                </div>         
            </div>
		</div>
	</div>
	<div class="container-indent nomargin">
		<div class="container-fluid-custom">
			<div class="row">
				<div class="col-sm-12 col-md-6">
                    <div class="row">

                        <div class="col-sm-12">
                            <a href="<?php echo e(Route('showProductPage',$randomProducts1[0]->product_slug)); ?>" class="tt-promo-box tt-one-child">
                                <img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php echo e(asset($randomProducts1[0]->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($randomProducts1[0]->product_title); ?>">
                                <div class="tt-description">
                                    <div class="tt-description-wrapper">
                                        <div class="tt-background"></div>
                                        <div class="tt-title-small">
                                            <p><?php echo e($randomProducts1[0]->product_title); ?></p>
                                            <span class="new-price"><?php echo e(number_format($randomProducts1[0]->product_price - ($randomProducts1[0]->product_price * $randomProducts1[0]->discount_percent) / 100)); ?> تومان</span>
                                            <span class="old-price"><?php echo e(number_format($randomProducts1[0]->product_price)); ?> تومان</span>
                                        </div>
                                    </div>
                                    <?php if($randomProducts1[0]->countdown_to != null && $randomProducts1[0]->countdown_to >= date('Y-m-d').'00:00:00'): ?> )
                                    <div class="tt-countdown_box">
                                        <div class="tt-countdown_inner">
                                            <div class="tt-countdown"
                                                 data-date="<?php echo e($randomProducts1[0]->countdown_to); ?>"
                                                 data-year="سال"
                                                 data-month="ماه"
                                                 data-week="هفته"
                                                 data-day="روز"
                                                 data-hour="ساعت"
                                                 data-minute="دقیقه"
                                                 data-second="ثانیه"></div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a href="<?php echo e(Route('showProductPage',$randomProducts1[1]->product_slug)); ?>" class="tt-promo-box tt-one-child">
                                <img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php echo e(asset($randomProducts1[1]->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($randomProducts1[1]->product_title); ?>">
                                <div class="tt-description">
                                    <div class="tt-description-wrapper">
                                        <div class="tt-background"></div>
                                        <div class="tt-title-small">
                                            <p><?php echo e($randomProducts1[1]->product_title); ?></p>
                                            <span class="new-price"><?php echo e(number_format($randomProducts1[1]->product_price - ($randomProducts1[1]->product_price * $randomProducts1[1]->discount_percent) / 100)); ?> تومان</span>
                                            <span class="old-price"><?php echo e(number_format($randomProducts1[1]->product_price)); ?> تومان</span>
                                        </div>
                                    </div>

                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a href="<?php echo e(Route('showProductPage',$randomProducts1[2]->product_slug)); ?>" class="tt-promo-box tt-one-child">
                                <img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php echo e(asset($randomProducts1[2]->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($randomProducts1[2]->product_title); ?>">
                                <div class="tt-description">
                                    <div class="tt-description-wrapper">
                                        <div class="tt-background"></div>
                                        <div class="tt-title-small">
                                            <p><?php echo e($randomProducts1[2]->product_title); ?></p>
                                            <span class="new-price"><?php echo e(number_format($randomProducts1[2]->product_price - ($randomProducts1[2]->product_price * $randomProducts1[2]->discount_percent) / 100)); ?> تومان</span>
                                            <span class="old-price"><?php echo e(number_format($randomProducts1[2]->product_price)); ?> تومان</span>
                                        </div>
                                    </div>

                                </div>
                            </a>
                        </div>
                    </div>
                </div>

				<div class="col-sm-12 col-md-6">
					<div class="row">
						<div class="col-sm-6">
							<a href="<?php echo e(Route('showProductPage',$randomProducts1[3]->product_slug)); ?>" class="tt-promo-box tt-one-child">
								<img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php echo e(asset($randomProducts1[3]->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($randomProducts1[3]->product_title); ?>">
								<div class="tt-description">
									<div class="tt-description-wrapper">
										<div class="tt-background"></div>
                                        <div class="tt-title-small">
                                            <p><?php echo e($randomProducts1[3]->product_title); ?></p>
                                            <span class="new-price"><?php echo e(number_format($randomProducts1[3]->product_price - ($randomProducts1[3]->product_price * $randomProducts1[3]->discount_percent) / 100)); ?> تومان</span>
                                            <span class="old-price"><?php echo e(number_format($randomProducts1[3]->product_price)); ?> تومان</span>
                                        </div>

									</div>

								</div>
							</a>
						</div>
						<div class="col-sm-6">
							<a href="<?php echo e(Route('showProductPage',$randomProducts1[4]->product_slug)); ?>" class="tt-promo-box tt-one-child">
								<img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php echo e(asset($randomProducts1[4]->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($randomProducts1[4]->product_title); ?>">
								<div class="tt-description">
									<div class="tt-description-wrapper">
										<div class="tt-background"></div>
										<div class="tt-title-small">
                                            <p><?php echo e($randomProducts1[4]->product_title); ?></p>
                                            <span class="new-price"><?php echo e(number_format($randomProducts1[4]->product_price - ($randomProducts1[4]->product_price * $randomProducts1[4]->discount_percent) / 100)); ?> تومان</span>
                                            <span class="old-price"><?php echo e(number_format($randomProducts1[4]->product_price)); ?> تومان</span>
                                        </div>
									</div>

								</div>
							</a>
						</div>
						<div class="col-sm-12">
							<a href="<?php echo e(Route('showProductPage',$randomProducts1[5]->product_slug)); ?>" class="tt-promo-box tt-one-child">
								<img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php echo e(asset($randomProducts1[5]->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($randomProducts1[5]->product_title); ?>">
								<div class="tt-description">
									<div class="tt-description-wrapper">
										<div class="tt-background"></div>
										<div class="tt-title-small">
                                            <p><?php echo e($randomProducts1[5]->product_title); ?></p>
                                            <span class="new-price"><?php echo e(number_format($randomProducts1[5]->product_price - ($randomProducts1[5]->product_price * $randomProducts1[5]->discount_percent) / 100)); ?> تومان</span>
                                            <span class="old-price"><?php echo e(number_format($randomProducts1[5]->product_price)); ?> تومان</span>
                                        </div>
									</div>
                                    <?php if($randomProducts1[5]->countdown_to != null && $randomProducts1[5]->countdown_to >= date('Y-m-d').'00:00:00'): ?> )
                                    <div class="tt-countdown_box">
                                        <div class="tt-countdown_inner">
                                            <div class="tt-countdown"
                                                 data-date="<?php echo e($randomProducts1[5]->countdown_to); ?>"
                                                 data-year="سال"
                                                 data-month="ماه"
                                                 data-week="هفته"
                                                 data-day="روز"
                                                 data-hour="ساعت"
                                                 data-minute="دقیقه"
                                                 data-second="ثانیه"></div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <form id="orderFrm" method="POST" action="<?php echo e(Route('addProductToCart')); ?>">
        <?php echo e(csrf_field()); ?>

        <input type="hidden" id="prd_id" name="product_id" value="" autocomplete="off">
        <input name="order_count" type="hidden" value="1" size="5"/>
    </form>
	<div class="container-indent">
		<div class="container container-fluid-custom-mobile-padding">
			<div class="tt-block-title">
				<h1 class="tt-title">عنوان</h1>
				<div class="tt-description">نمایش تا آخر هفته</div>
			</div>
			<div class="row tt-layout-product-item">
                <?php $__currentLoopData = $randProducts2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-6 col-md-4 col-lg-3">
                    <div class="tt-product">
                        <div class="tt-image-box">
                            <a href="<?php echo e(Route('showProductPage',$product->product_slug)); ?>">
                                <span class="tt-img"><img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php echo e(asset($product->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($product->product_title); ?>"></span>
                            </span>
                            </a>
                        </div>
                        <div class="tt-description">
                            <div class="tt-row">
                                <ul class="tt-add-info">
                                    <li><a href="#"></a></li>
                                </ul>
                                <div class="tt-rating">
                                    <?php
                                        $scores = $product->Ratings()->count();
                                        if ($scores > 0){
                                            $sum = $product->Ratings()->sum('score_value') / $scores;
                                        }
                                        else{
                                            $sum = 0;
                                        }

                                    ?>
                                    <div class="my-rating-all pull-left" data-score="<?php echo e($sum); ?>"></div>
                                </div>
                            </div>
                            <h2 class="tt-title"><a  href="<?php echo e(Route('showProductPage',$product->product_slug)); ?>"><?php echo e($product->product_title); ?></a></h2>
                            <div class="tt-product-inside-hover">
                                <?php if($product->selectable_properties_count==0 && $product->fillable_properties_count==0): ?>
                                <a onclick="addToCart(this.className)" class="<?php echo e($product->id); ?> tt-btn-addtocart">افزودن به سبد خرید</a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


				
			
			</div>
		</div>
	</div>
	<div class="container-indent">
		<div class="container-fluid-custom">
			<div class="row tt-layout-promo-box">
				<div class="col-sm-12" style="background: url(<?php echo e(asset('Web/images/testimonials.jpg')); ?>) 50% 87%;
    background-size: 145%;
    background-attachment: fixed;height: 200px">
                    <p class="text-center gift-text"><?php echo e($oneRandomDiscount->discount_title1); ?>-<?php echo e($oneRandomDiscount->discount_code); ?></p>
                    <div class="col-sm-6 gift-countdown">
                        <div class="tt-countdown_box">
                                <div class="tt-countdown_inner">
                                    <div class="tt-countdown"
                                        data-date="<?php echo e($oneRandomDiscount->is_valid_to); ?>"
                                        data-year="سال"
                                        data-month="ماه"
                                        data-week="هفته"
                                        data-day="روز"
                                        data-hour="ساعت"
                                        data-minute="دقیقه"
                                        data-second="ثانیه"></div>
                                </div>
                            </div>
                    </div>
</div>
			</div>
		</div>
	</div>
	
	<div class="container-indent">
		<div class="container">
			<div class="tt-block-title">
				<h2 class="tt-title">آخرین مطالب وبسایت</h2>
				<div class="tt-description"><a href="<?php echo e(route('showblog')); ?>">بخش وبلاگ</a></div>
			</div>
			<div class="tt-blog-thumb-list">
				<div class="row">
                    <?php if($posts): ?>
                        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="col-12 col-sm-6 col-md-6 col-lg-4">
						<div class="tt-blog-thumb">
                            <div class="tt-img"><a href="<?php echo e(route('showsingleblog',$post->post_slug)); ?>"><img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php if($post->PostImages()->count() > 0): ?> <?php echo e(asset($post->PostImages()->first()->image_dir)); ?> <?php endif; ?>" alt=""></a></div>
                            <div class="tt-title-description">
                                <div class="tt-background"></div>
                                <div class="tt-title">
                                    <a href="<?php echo e(route('showsingleblog',$post->post_slug)); ?>"><?php echo e($post->post_title); ?></a>
                                </div>
                                <p>
                                    متن
                                </p>
                                <div class="tt-meta">
                                    <div class="tt-autor">
                                        توسط:  <span> <?php echo e($post->Author()->first()->user_title); ?> </span>
                                        <br>
                                        <span><?php echo e(Helpers::convert_date_g_to_j($post->created_at,true)); ?></span>
                                    </div>
                                    <div class="tt-comments">|
                                        <a href="#"><i class="tt-icon icon-h-11"></i><?php echo e($post->Comments()->count()); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container-indent">
		<div class="container-fluid">
			<div class="tt-block-title">
				<h2 class="tt-title">محصولات تصادفی</h2>
			</div>
			<div class="row">
				<div class="col-sm-12">
                    <div class="owl-carousel owl-theme">

                    <?php $__currentLoopData = $randProducts3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item">
                    <div class="tt-product">
                        <div class="tt-image-box">
                            <a href="<?php echo e(Route('showProductPage',$product->product_slug)); ?>">
                                <span class="tt-img"><img src="<?php echo e(asset($product->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($product->product_title); ?>"></span>

                            </a>
                        </div>
                        <div class="tt-description">
                            <div class="tt-row">
                                <ul class="tt-add-info">
                                    <li><a href="#"><?php echo e($product->product_title); ?></a></li>
                                </ul>
                                <div class="tt-rating">
                                    <?php
                                        $scores = $product->Ratings()->count();
                                        if ($scores > 0){
                                            $sum = $product->Ratings()->sum('score_value') / $scores;
                                        }
                                        else{
                                            $sum = 0;
                                        }

                                    ?>
                                    <div class="my-rating-all pull-left" data-score="<?php echo e($sum); ?>"></div>
                                </div>
                            </div>
                            <h2 class="tt-title"><a href="<?php echo e(Route('showProductPage',$product->product_slug)); ?>">نوع محصول</a></h2>
                            <div class="tt-product-inside-hover">
                                <?php if($product->selectable_properties_count==0 && $product->fillable_properties_count==0): ?>
                                <a href="#" class="tt-btn-addtocart" data-toggle="modal" data-target="#modalAddToCartProduct">افزودن به سبد خرید</a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div>            
                </div>
			</div>
		</div>
	</div>
	<div class="container-indent">
		<div class="container">
			<div class="row tt-services-listing tt-services-listing-aligment">
				<div class="col-xs-12 col-md-6 col-lg-4">
					<a href="#" class="tt-services-block">
						<div class="tt-col-icon">
							<i class="icon-f-48"></i>
						</div>
						<div class="tt-col-description">
							<h4 class="tt-title">متن</h4>
						</div>
					</a>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-4">
					<a href="#" class="tt-services-block">
						<div class="tt-col-icon">
							<i class="icon-f-35"></i>
						</div>
						<div class="tt-col-description">
							<h4 class="tt-title">متن</h4>
						</div>
					</a>
				</div>
				<div class="col-xs-12 col-md-6 col-lg-4">
					<a href="#" class="tt-services-block">
						<div class="tt-col-icon">
							<i class="icon-e-09"></i>
						</div>
						<div class="tt-col-description">
							<h4 class="tt-title">بازگشتن بعد از 30 روز</h4>
							<p>متن</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

<script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/slick/slick.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/perfect-scrollbar/perfect-scrollbar.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/rs-plugin/js/jquery.themepunch.tools.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/js/owl.carousel.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/countdown/jquery.plugin.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/countdown/jquery.countdown.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
<script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<script src="<?php echo e(asset('Web/js/jquery.star-rating-svg.js')); ?>"></script>

<script type="text/javascript">
    $('.owl-carousel').owlCarousel({
    rtl:true,
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});
    function selectBrand(classname) {
        var res = classname.split(" ");
        $('.'+res[0]).css( "opacity", "0.5" );
        if($('#brandinput').val() != 0){
            $('.'+$('#brandinput').val()).css( "opacity", "1" );
        }
       $('#brandinput').val(res[0]);
    }
    function addToCart(classname) {
        var res = classname.split(" ");
        $('#prd_id').val(res[0]);
        document.getElementById('orderFrm').submit();
    }
    function rating(){
        $('.my-rating-all').each(function(i, obj) {
            $(obj).starRating({
                useFullStars: true,
                starSize: 15,
                readOnly: true,
                initialRating: Math.ceil($(obj).data('score')),
            });
        });
    }

    $(document).ready(function () {
        rating();
    });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('main'); ?>

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="<?php echo e(url('/')); ?>">خانه</a></li>/
                <li>کلمه عبور</li>
            </ul>
        </div>
    </div>



    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <div class="rowContainer">
                    <h1 class="tt-title-subpages noborder">تغییر رمز عبور</h1>
                </div>

                <div class="rowContainer">
                    <?php echo $__env->make('landing.panel.common.sideMenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <div class="customColumn pull-left-custom panel-contentCont">
                        <div class="panel rowPanel">



                            <div class="customContainer1">
                                <form action="<?php echo e(Route('changePassword')); ?>" method="POST">
                                    <?php echo e(csrf_field()); ?>

                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <input name="password" class="customInput1" type="password" placeholder="رمز عبور" value="">
                                            <?php if($errors->has('password')): ?><span class="help-block"><strong><?php echo e($errors->first('password')); ?></strong></span><?php endif; ?>
                                        </div>
                                        <div class="custom-col-25">
                                            <label class="customLabel" for="password">رمز عبور</label>
                                        </div>
                                    </div>


                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <input name="repassword" class="customInput1" type="password" placeholder="تکرار رمز عبور" value="">
                                            <?php if($errors->has('repassword')): ?><span class="help-block"><strong><?php echo e($errors->first('repassword')); ?></strong></span><?php endif; ?>
                                        </div>
                                        <div class="custom-col-25">
                                            <label class="customLabel" for="repassword">تکرار رمز عبور</label>
                                        </div>
                                    </div>


                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <input class="customSubmit1" type="submit" value="ذخیره">
                                        </div>
                                        <div class="custom-col-25">
                                        </div>

                                    </div>
                                </form>
                            </div>







                        </div>

                    </div>

                </div>

            </div>
        </div>



        <?php $__env->stopSection(); ?>
        <?php $__env->startSection('js'); ?>
            <script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
            <script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
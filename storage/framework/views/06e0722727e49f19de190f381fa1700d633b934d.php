<?php $__env->startSection('header'); ?>
    <?php echo $__env->make('admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('side-menu'); ?>
    <?php echo $__env->make('admin.side_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content_list'); ?>
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">تصویر</td>
        <td class="text-center">عنوان</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>

    </thead>
    <tbody>
    <?php $c=1; ?>
    <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="<?php echo e($post->id); ?>" type="checkbox">
            </td>
            <td class="text-center">
                <?php echo e($c); ?> <?php $c++; ?>
            </td>
            <td class="text-center">
                <?php if($post->PostImages()->first()!=Null && trim($post->PostImages()->first()->image_dir)!='' && file_exists( public_path().'/'.$post->PostImages()->first()->image_dir)): ?>
                    <img src="<?php echo e(url( $post->PostImages()->first()->image_dir)); ?>" class="shop-list-avatar">
                <?php else: ?>
                    <span>بدون تصویر</span>
                <?php endif; ?>
            </td>
            <td class="text-center" style="width: 40%;">
                <?php echo e($post->post_title); ?>

            </td>
            <td class="text-center">
                <?php echo e(($post->post_status==1)?'فعال':'غیرفعال'); ?>

            </td>
            <td class="text-center">
                <a href="<?php echo e(Route('editPost',$post->id)); ?>" data-toggle="tooltip" title="ویرایش">
                    ویرایش
                </a>|
                <a href="<?php echo e(Route('adminPostComments',$post->id)); ?>" data-toggle="tooltip" title="نظرات">
                    نظرات
                </a>


            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-lists', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
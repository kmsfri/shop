<?php $__env->startSection('main'); ?>

<div class="tt-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="<?php echo e(url('/')); ?>">خانه</a></li>/
            <li>کلمه عبور</li>
        </ul>
    </div>
</div>
<div id="tt-pageContent">
    <div class="container-indent">
        <div class="container">
            <div class="rowContainer">
                <h1 class="tt-title-subpages noborder">لیست علاقه مندیها</h1>
            </div>

            <div class="rowContainer">
                <?php echo $__env->make('landing.panel.common.sideMenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="customColumn pull-left-custom panel-contentCont">
                    <div class="panel rowPanel">
                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-sm-6" style="float: left;">
                                <a href="<?php echo e(Route('showProductPage',$product->product_slug)); ?>" class="tt-promo-box tt-one-child">
                                    <img src="<?php echo e(asset('Web/images/loader.svg')); ?>" data-src="<?php echo e(asset($product->ProductImages()->first()->image_dir)); ?>" alt="<?php echo e($product->product_title); ?>">
                                    <div class="tt-description">
                                        <div class="tt-description-wrapper">
                                            <div class="tt-background"></div>
                                            <div class="tt-title-small">
                                                <p><?php echo e($product->product_title); ?></p>
                                                <span class="new-price"><?php echo e(number_format($product->product_price - ($product->product_price * $product->discount_percent) / 100)); ?> تومان</span>
                                                <span class="old-price"><?php echo e(number_format($product->product_price)); ?> تومان</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('Web/external/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/panelmenu/panelmenu.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/external/lazyLoad/lazyload.min.js')); ?>"></script>
    <script src="<?php echo e(asset('Web/js/main.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('landing.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
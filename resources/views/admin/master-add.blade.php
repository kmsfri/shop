@extends('admin.master')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content')
<div class="panel panel-default add-data-panel">
    <div class="panel-heading-cs1">
        <div class="pull-left">
            <a href="{{$backward_url}}" class="btn btn-back-cs1">بازگشت</a>
        </div>
        <div class="panel-heading-cs1-title">{!! $title !!}</div>
    </div>
    <div class="panel-body panel-body-cs1">
        <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ isset($post_add_url)?$post_add_url:$post_edit_url }}" autocomplete="off">
        @if($request_type=='edit')
            <input type="hidden" name="edit_id" value="{{ old('edit_id',isset($edit_id) ? $edit_id : '') }}">
        @endif
            {{ csrf_field() }}
            @yield('content_add_form')
            <div class="form-group">
                <div class="col-md-3 pull-left">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-sign-in"></i> ذخیره
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
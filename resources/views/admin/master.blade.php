<!DOCTYPE html>
<html lang="fa">
<head>
  <link rel="icon" href="{!! asset('uploads/static/fav.png') !!}" type="image/x-icon" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">



  <title>پنل مدیریت</title>
  <script type="text/javascript" src="{!! asset('bootstrap-3.3.7/js/jquery.min.js') !!}"></script>
  <link href="{!! asset('bootstrap-3.3.7/css/bootstrap-theme.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="{!! asset('bootstrap-3.3.7/js/bootstrap.min.js') !!}"></script>
  <link href="{!! asset('bootstrap-3.3.7/css/bootstrap.min.css') !!}" media="all" rel="stylesheet" type="text/css" />

  <link href="{!! asset('css/custom.css') !!}" media="all" rel="stylesheet" type="text/css" />

  <link href="{!! asset('css/adminSidebar.css') !!}" media="all" rel="stylesheet" type="text/css" />




  <style>
    body *{
      border-radius:0 !important;
      direction: rtl;
    }
    ol, ul {
      list-style: none;
    }
    blockquote, q {
      quotes: none;
    }

    .sidebar-brand{
      text-align: center !important;
      padding-right: 65px !important;
    }
    .sidebar-brand a,
    .sidebar-brand span{
      float: right;
      color:white;
    }



  </style>


    @yield('cssFiles')
    @yield('jsFiles')
</head>
<body id="app-layout " >
    <div class="row">
        <div class="col-lg-12">
            <div class="admin-content-sec">
              @include('admin.header')


                <style>
                    thead th{
                        text-align: right;
                    }
                </style>

                <div class="admin-content-cont">

                    <div class="row">
                        <div class="col-lg-12">
                            @if(session('messages'))
                                <ul>
                                    @foreach(session('messages') as $msg)
                                        <li>{{$msg}}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="panel">

                                @yield('content')


                            </div>

                        </div>
                    </div>
                </div>




            </div>
            @yield('side-menu')
        </div>

    </div>
    <footer class="row">
        @yield('footer')
    </footer>

</body>
</html>
@yield('jsCustom')

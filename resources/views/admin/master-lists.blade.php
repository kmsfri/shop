@extends('admin.master')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content')
<div class="panel-heading-cs1">
    <div class="pull-left">
        <a href="{{ $backward_url }}" class="btn btn-back-cs1">بازگشت</a>
    </div>
    <div class="pull-left">
        @if($add_url!=Null)
            <a href="{{$add_url}}" class="btn btn-btn1-cs1">افزودن</a>
        @endif
    </div>
    <div class="pull-left">
        <form id="delForm" action="{{$delete_url}}" method="post" onsubmit="return confirm('آیا از حذف همه ی موارد انتخاب شده مطمئنید؟');">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-btn1-cs1">حذف</button>
        </form>
    </div>
    <div class="panel-heading-cs1-title">{!! $title  !!}</div>
</div>
<div class="panel-body panel-body-cs1">
    <div class="table-responsive table_posts_container">
        <table class="table table-striped table-hover">
            @yield('content_list')
        </table>
    </div>
</div>
@stop
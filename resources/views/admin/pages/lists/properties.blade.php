@extends('admin.master-lists')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_list')

<thead>
<tr>
    <td style="width: 1px;" class="text-center"></td>
    <td class="text-left">
        <center>
            ردیف
        </center>
    </td>
    <td class="text-left">
        <center>
            @if($parent_id==Null)
                عنوان خصوصیت
            @else
                مقدار خصوصیت
            @endif
        </center>
    </td>
    <td class="text-right">
        <center>
            وضعیت
        </center>
    </td>
    <td class="text-right">
        <center>
            عملیات
        </center>
    </td>

</tr>

</thead>
<tbody>
@php $c=1; @endphp
@foreach($properties as $prop)
    <tr>
        <td class="text-center">
            <input form="delForm" name="remove_val[]" value="{{$prop->id}}" type="checkbox">
        </td>
        <td class="text-center">
            <center>
                {{$c}} @php $c++; @endphp
            </center>
        </td>
        <td class="text-center">
            <center>
                @if($parent_id==Null)
                    <a href="{{ url('admin/property/'.$prop->id)}}">
                        @endif
                        {{$prop->property_title}}
                        @if($parent_id==Null)
                    </a>
                @endif


            </center>
        </td>
        <td class="text-center">
            <center>
                @if($prop->property_status==1)
                    فعال
                @else
                    غیر فعال
                @endif
            </center>
        </td>

        <td class="text-center">
            <a href="{{url('admin/property/edit/'.$prop->id)}}">
                ویرایش
            </a>
        </td>

    </tr>
@endforeach
</tbody>
@stop
@extends('admin.master-lists')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_list')
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">تصویر</td>
        <td class="text-center">عنوان</td>
        <td class="text-center">محصول</td>
        <td class="text-center">ترتیب</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>

    </thead>
    <tbody>
    @php $c=1; @endphp
    @foreach($slides as $sl)
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="{{$sl->id}}" type="checkbox">
            </td>
            <td class="text-center">
                {{$c}} @php $c++; @endphp
            </td>
            <td class="text-center">
                @if($sl->img_dir!=Null && trim($sl->img_dir)!='' && file_exists( public_path().'/'.$sl->img_dir))
                    <img src="{{url( $sl->img_dir)}}" class="shop-list-avatar">
                @else
                    <span>بدون تصویر</span>
                @endif
            </td>
            <td class="text-center">
                {{$sl->title1}}
            </td>
            <td class="text-center">
                {{($sl->Product()->first()!=Null)?$sl->Product()->first()->product_title:'-'}}
            </td>
            <td class="text-center">
                {{$sl->img_order}}
            </td>
            <td class="text-center">
                {{($sl->img_status==1)?'فعال':'غیرفعال'}}
            </td>
            <td class="text-center">
                <a href="{{Route('adminEditSlide2',$sl->id)}}" data-toggle="tooltip" title="ویرایش">
                    ویرایش
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
@stop

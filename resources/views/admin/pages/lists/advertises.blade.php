@extends('admin.master-lists')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_list')
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">تصویر</td>
        <td class="text-center">عنوان</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>

    </thead>
    <tbody>
    @php $c=1; @endphp
    @foreach($advertises as $ad)
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="{{$ad->id}}" type="checkbox">
            </td>
            <td class="text-center">
                {{$c}} @php $c++; @endphp
            </td>
            <td class="text-center">
                @if($ad->ProductImages()->first()!=Null && trim($ad->ProductImages()->first()->image_dir)!='' && file_exists( public_path().'/'.$ad->ProductImages()->first()->image_dir))
                    <img src="{{url( $ad->ProductImages()->first()->image_dir)}}" class="shop-list-avatar">
                @else
                    <span>بدون تصویر</span>
                @endif
            </td>
            <td class="text-center" style="width: 40%;">
                {{$ad->product_title}}
            </td>
            <td class="text-center">
                {{($ad->product_status==1)?'فعال':'غیرفعال'}}
            </td>
            <td class="text-center">
                <a href="{{Route('editAdvertise',$ad->id)}}" data-toggle="tooltip" title="ویرایش">
                    ویرایش
                </a>|
                <a href="{{Route('adminAddSlideFrm',$ad->id)}}" data-toggle="tooltip" title="افزودن اسلاید">
                    افزودن اسلاید
                </a>|
                <a href="{{Route('adminComments',$ad->id)}}" data-toggle="tooltip" title="نظرات">
نظرات
                </a>|
                <a href="{{Route('adminEditAdvertiseList',$ad->id)}}" data-toggle="tooltip" title="لیستها">
لیستها
                </a>

            </td>
        </tr>
    @endforeach
    </tbody>
@stop

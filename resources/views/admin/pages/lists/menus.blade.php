@extends('admin.master-lists')
@section('content_list')
<thead>
<tr>
    <td style="width: 1px;" class="text-center"></td>
    <td class="text-left">
        <center>
            ردیف
        </center>
    </td>
    <td class="text-left">
        <center>
            عنوان
        </center>
    </td>
    <td class="text-right">
        <center>
            وضعیت
        </center>
    </td>
    <td class="text-right">
        <center>
            لینک دسترسی
        </center>
    </td>
    <td class="text-right">
        <center>
            عملیات
        </center>
    </td>

</tr>

</thead>
<tbody>
@php $c=1; @endphp
@foreach($data as $d)

    <tr>
        <td class="text-center">
            <input form="delForm" name="remove_val[]" value="{{$d->id}}" type="checkbox">
        </td>
        <td class="text-center">

                {{$c}} @php $c++; @endphp

        </td>
        <td class="text-center">

            @if($canHasSubmenu)
                <a href="{{Route('menus',$d->id)}}">
            @endif
                {{$d->m_title}}
            @if($canHasSubmenu)
                </a>
            @endif


        </td>
        <td class="text-center">

                @if($d->c_status==1)
                    فعال
                @else
                    غیر فعال
                @endif

        </td>

        <td class="text-center">
            <a href="{!! $d->m_link !!}">
                {!! $d->m_link !!}
            </a>
        </td>

        <td class="text-center">
            <a href="{{url('admin/menu/edit/'.$d->id)}}">
                ویرایش
            </a>
        </td>

    </tr>
@endforeach
</tbody>
@stop
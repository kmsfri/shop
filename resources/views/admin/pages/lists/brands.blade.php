@extends('admin.master-lists')
@section('content_list')
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">عنوان</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>
    </thead>
    <tbody>
    @php $c=1; @endphp
    @foreach($brands as $brand)
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="{{$brand->id}}" type="checkbox">
            </td>
            <td class="text-center">
                {{$c}} @php $c++; @endphp
            </td>
            <td class="text-center">
                {{$brand->brand_title}}
            </td>
            <td class="text-center">
                {{($brand->brand_status==1)?'فعال':'غیرفعال'}}
            </td>
            <td class="text-center">
                <a href="{{url(Route('edit-brand-form',$brand->id))}}">
                    ویرایش
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>

@stop
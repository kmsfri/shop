@extends('admin.master-lists')
@section('content_list')
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">ایمیل یا شماره تماس</td>
        <td class="text-center">عنوان</td>
        <td class="text-center">تاریخ</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>
    </thead>
    <tbody>
    @php $c=1; @endphp
    @foreach($contacts as $contact)
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="{{$contact->id}}" type="checkbox">
            </td>
            <td class="text-center">
                {{$c}} @php $c++; @endphp
            </td>
            <td class="text-center">
                {{$contact->phone_number}}
            </td>
            <td class="text-center">
                {{$contact->contact_title}}
            </td>
            <td class="text-center">
                {{Helpers::convert_date_g_to_j($contact->created_at,true).' - '.$contact->created_at->format('H:i:s')}}
            </td>
            <td class="text-center">
                {{($contact->viewed==1)?'بررسی شده':'جدید'}}
            </td>
            <td class="text-center">
                <a href="{{url(Route('edit-contact-form',$contact->id))}}">
                    ویرایش
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>

@stop
@extends('admin.master-lists')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_list')
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">تصویر</td>
        <td class="text-center">عنوان</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>

    </thead>
    <tbody>
    @php $c=1; @endphp
    @foreach($posts as $post)
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="{{$post->id}}" type="checkbox">
            </td>
            <td class="text-center">
                {{$c}} @php $c++; @endphp
            </td>
            <td class="text-center">
                @if($post->PostImages()->first()!=Null && trim($post->PostImages()->first()->image_dir)!='' && file_exists( public_path().'/'.$post->PostImages()->first()->image_dir))
                    <img src="{{url( $post->PostImages()->first()->image_dir)}}" class="shop-list-avatar">
                @else
                    <span>بدون تصویر</span>
                @endif
            </td>
            <td class="text-center" style="width: 40%;">
                {{$post->post_title}}
            </td>
            <td class="text-center">
                {{($post->post_status==1)?'فعال':'غیرفعال'}}
            </td>
            <td class="text-center">
                <a href="{{Route('editPost',$post->id)}}" data-toggle="tooltip" title="ویرایش">
                    ویرایش
                </a>|
                <a href="{{Route('adminPostComments',$post->id)}}" data-toggle="tooltip" title="نظرات">
                    نظرات
                </a>


            </td>
        </tr>
    @endforeach
    </tbody>
@stop

@extends('admin.master-lists')
@section('content_list')
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">عنوان 1</td>
        <td class="text-center">درصد تخفیف</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>
    </thead>
    <tbody>
    @php $c=1; @endphp
    @foreach($discounts as $dc)
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="{{$dc->id}}" type="checkbox">
            </td>
            <td class="text-center">
                {{$c}} @php $c++; @endphp
            </td>
            <td class="text-center">
                {{$dc->discount_title1}}
            </td>
            <td class="text-center">
                {{$dc->discount_percent}}%
            </td>
            <td class="text-center">
                {{($dc->discount_status==1)?'فعال':'غیرفعال'}}
            </td>
            <td class="text-center">
                <a href="{{url(Route('adminEditDiscount',$dc->id))}}">
                    ویرایش
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>

@stop
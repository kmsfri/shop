@extends('admin.master-lists')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_list')
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">عنوان</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>

    </thead>
    <tbody>
    @php $c=1; @endphp
    @foreach($notes as $note)
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="{{$note->id}}" type="checkbox">
            </td>
            <td class="text-center">
                {{$c}} @php $c++; @endphp
            </td>
            <td class="text-center" style="width: 40%;">
                {{$note->note_title}}
            </td>
            <td class="text-center">
                {{($note->note_status==1)?'فعال':'غیرفعال'}}
            </td>
            <td class="text-center">
                <a href="{{Route('editNote',$note->id)}}" data-toggle="tooltip" title="ویرایش">
                    ویرایش
                </a>

            </td>
        </tr>
    @endforeach
    </tbody>
@stop

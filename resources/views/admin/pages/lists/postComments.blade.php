@extends('admin.master-lists')
@section('content_list')
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">محصول</td>
        <td class="text-center">کاربر</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>
    </thead>
    <tbody>
    @php $c=1; @endphp
    @foreach($comments as $comment)
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="{{$comment->id}}" type="checkbox">
            </td>
            <td class="text-center">
                {{$c}} @php $c++; @endphp
            </td>
            <td class="text-center">
                {{$comment->Post->post_title}}
            </td>
            <td class="text-center">
                {{($comment->User->fullname==Null)?'نامشخص':$comment->User->fullname}}
            </td>
            <td class="text-center">
                {{($comment->comment_status==1)?'فعال':'غیرفعال'}}
            </td>
            <td class="text-center">
                <a href="{{url(Route('edit-post-comment-form',$comment->id))}}">
                    ویرایش
                </a>|<a href="{{url(Route('editPost',$comment->post_id))}}">
                    مطلب
                </a>|<a href="{{url(Route('editUser',$comment->user_id))}}">
                    کاربر
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>

@stop
@extends('admin.master-lists')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_list')
<thead>
<tr>
    <td style="width: 1px;" class="text-center"></td>
    <td class="text-center">ردیف</td>
    <td class="text-center">نام کاربری</td>
    <td class="text-center">نام و نام خانوادگی</td>
    <td class="text-center">تصویر</td>
    <td class="text-center">وضعیت</td>
    <td class="text-center">عملیات</td>
</tr>

</thead>
<tbody>
@php $c=1; @endphp
@foreach($admins as $u)
    <tr>
        <td class="text-center">
            <input form="delForm" name="remove_val[]" value="{{$u->id}}" type="checkbox">
        </td>
        <td class="text-center">
                {{$c}} @php $c++; @endphp
        </td>
        <td class="text-center">
                {{$u->user_name}}
        </td>
        <td class="text-center">
                {{$u->user_title}}
        </td>
        <td class="text-center">
                @if(trim($u->avatar_dir)!='' && file_exists( public_path().'/uploads/users/admin/'.$u->avatar_dir))
                    <img src="{{url( '/uploads/users/admin/'.$u->avatar_dir)}}" class="shop-list-avatar">
                @else
                    <span>بدون تصویر</span>
                @endif
        </td>
        <td class="text-center">
            {{($u->user_status==1)?'فعال':'غیرفعال'}}
        </td>
        <td class="text-center">
            <a href="{{url('admin/user/admin/edit/'.$u->id)}}" data-toggle="tooltip" title="ویرایش کاربر">
                ویرایش کاربر
            </a></br>
            <a href="{{url('admin/user/admin/edit_sec_permit/'.$u->id)}}" data-toggle="tooltip" title="ویرایش دسترسی به بخشها">
                ویرایش دسترسیها
            </a>
        </td>
    </tr>
@endforeach
</tbody>
@stop

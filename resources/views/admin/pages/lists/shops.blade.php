@extends('admin.master-lists')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_list')
    <thead>
    <tr>
        <td style="width: 1px;" class="text-center"></td>
        <td class="text-center">ردیف</td>
        <td class="text-center">تصویر</td>
        <td class="text-center">عنوان</td>
        <td class="text-center">وضعیت</td>
        <td class="text-center">عملیات</td>
    </tr>

    </thead>
    <tbody>
    @php $c=1; @endphp
    @foreach($shops as $shop)
        <tr>
            <td class="text-center">
                <input form="delForm" name="remove_val[]" value="{{$shop->id}}" type="checkbox">
            </td>
            <td class="text-center">
                {{$c}} @php $c++; @endphp
            </td>
            <td class="text-center">
                @if($shop->ShopImages()->first()!=Null && trim($shop->ShopImages()->first()->image_dir)!='' && file_exists( public_path().'/'.$shop->ShopImages()->first()->image_dir))
                    <img src="{{url( $shop->ShopImages()->first()->image_dir)}}" class="shop-list-avatar">
                @else
                    <span>بدون تصویر</span>
                @endif
            </td>
            <td class="text-right" style="width: 40%;">
                <a href="#">
                    {{$shop->shop_title}}
                </a>
            </td>
            <td class="text-center">
                {{($shop->shop_status==1)?'فعال':'غیرفعال'}}
            </td>
            <td class="text-center">
                <a href="{{Route('editShop',$shop->id)}}" data-toggle="tooltip" title="ویرایش">
                    ویرایش
                </a>|
                <a href="{{Route('advertisesList',$shop->id)}}" data-toggle="tooltip" title="مشاهده آگهی ها">
                    آگهی ها
                </a>|
                <a href="{{Route('adminEditShopCategory',$shop->id)}}" data-toggle="tooltip" title="مشاهده آگهی ها">
                    دسته بندیها
                </a>|
                <a href="{{Route('adminEditShopProperty',$shop->id)}}" data-toggle="tooltip" title="خصوصیات">
                    خصوصیات
                </a>

            </td>
        </tr>
    @endforeach
    </tbody>
@stop

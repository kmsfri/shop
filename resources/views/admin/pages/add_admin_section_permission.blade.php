@extends('admin.master')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content')
    <div class="panel panel-default add-data-panel">
        <div class="panel-heading-cs1">
            <div class="pull-left">
                <a href="{{$backward_url}}" class="btn btn-back-cs1">بازگشت</a>
            </div>
            <div class="panel-heading-cs1-title">{!! $title !!}</div>
        </div>
        <div class="panel-body panel-body-cs1">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/user/admin/edit_sec_permit') }}">
                {{ csrf_field() }}
                <input type="hidden" name="u_id" value="{{ old('u_id',$u_id) }}">
                @foreach($routes as $route)
                    <div class="col-lg-4 pull-right">
                        <div class="checkbox">
                            <label ><input type="checkbox" name="routes_assign_to_user[]"  value="{{$route->id}}" {{($route->permited==1)? 'checked' : ''}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$route->route_title}}</label>
                        </div>
                    </div>
                @endforeach
                <div class="row form-group">
                    <div class="col-md-12">
                        </br></br>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-sign-in"></i> ذخیره
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop

@extends('admin.master-add')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_add_form')
<input type='hidden' name='user_id' value="{{ old('user_id',isset($user_id) ? $user_id : '') }}">
<div class="form-group{{ $errors->has('shop_title') ? ' has-error' : '' }}">
    <label for="shop_title" class="col-md-2 pull-right control-label">عنوان فروشگاه:</label>
    <div class="col-md-6 pull-right">
        <input type="text" class="form-control" name="shop_title" value="{{ old('shop_title',isset($shop->shop_title) ? $shop->shop_title : '') }}" autocomplete="off">
        @if ($errors->has('shop_title'))<span class="help-block"><strong>{{ $errors->first('shop_title') }}</strong></span>@endif
    </div>
</div>
<div class="form-group{{ $errors->has('shop_address') ? ' has-error' : '' }}">
    <label for="shop_address" class="col-md-2 pull-right control-label">آدرس فروشگاه:</label>
    <div class="col-md-6 pull-right">
        <textarea class="form-control" name="shop_address">{{ old('shop_address',isset($shop->shop_address) ? Helpers::br2nl($shop->shop_address) : '') }}</textarea>
        @if ($errors->has('shop_address'))<span class="help-block"><strong>{{ $errors->first('shop_address') }}</strong></span>@endif
    </div>
</div>
<div class="form-group{{ $errors->has('shop_description') ? ' has-error' : '' }}">
    <label for="shop_description" class="col-md-2 pull-right control-label">توضیحات فروشگاه:</label>
    <div class="col-md-6 pull-right">
        <textarea class="form-control" name="shop_description">{{ old('shop_description',isset($shop->shop_description) ? Helpers::br2nl($shop->shop_description) : '') }}</textarea>
        @if ($errors->has('shop_description'))<span class="help-block"><strong>{{ $errors->first('shop_description') }}</strong></span>@endif
    </div>
</div>

<div class="form-group{{ $errors->has('shop_status') ? ' has-error' : '' }}">
    <label for="shop_status" class="col-md-2 pull-right control-label">وضعیت:</label>
    <div class="col-md-6 pull-right">
        <select  name='shop_status' class='selectpicker form-control pull-right' autocomplete="off">
            <option @if(old('shop_status' ,isset($shop->shop_status) ? $shop->shop_status : '' )==1) selected @endif value="1" >فعال</option>
            <option @if(old('shop_status' , isset($shop->shop_status) ? $shop->shop_status : '')==0) selected @endif value="0" >غیر فعال</option>
        </select>
        @if ($errors->has('shop_status'))<span class="help-block"><strong>{{ $errors->first('shop_status') }}</strong></span>@endif
    </div>
</div>
<div class="form-group">
    <label for="files" class="col-md-2 pull-right control-label">تصاویر فروشگاه:</label>
    <div class="col-md-10 pull-right">
        <div class="box-form">
            <input id="files" type="file" name="newImg[]" multiple="multiple" autocomplete="off" accept="image/jpg, image/jpeg, image/png" /><br>
            <output id="result">
                @if(isset($shop) && $shop!=Null)
                    @foreach($shop->ShopImages()->get() as $simg)
                        <div>
                            <img class="thumbnail" src="{{url($simg->image_dir)}}">
                            <input name="oldImg[]" type="hidden" value="{{$simg->id}}">
                            <a href="javascript:void()" onclick="$(this).closest('div').remove()" style="display: block">حذف</a>
                        </div>
                    @endforeach
                @endif
            </output>
            @if ($errors->has('newImg.*')) <span class="help-block"><strong>{{ $errors->first('newImg.*') }}</strong></span> @endif
        </div>
    </div>
</div>
@stop
@section('jsCustom')
<script type="text/javascript">
    window.onload = function(){
        //Check File API support
        if(window.File && window.FileList && window.FileReader) {
            var filesInput = document.getElementById("files");
            filesInput.addEventListener("change", function(event){
                var files = event.target.files; //FileList object
                var output = document.getElementById("result");
                $( ".newImgThumb" ).remove();
                for(var i = 0; i< files.length; i++) {
                    var file = files[i];
                    //Only pics
                    if(!file.type.match('image'))
                        continue;

                    var picReader = new FileReader();

                    picReader.addEventListener("load",function(event){

                        var picFile = event.target;
                        var div = document.createElement("div");

                        div.innerHTML = "" +
                            "<img class='thumbnail newImgThumb' src='" + picFile.result + "'" + "title='" + picFile.name + "'/>" +
                            "<span class='newImgThumb' style=\"display: block\">تصویر جدید</span>"
                        ;
                        $(output).append(div);

                    });

                    //Read the image
                    picReader.readAsDataURL(file);
                }

            });
        }
        else
        {
            console.log("Your browser does not support File API");
        }
    }
</script>
@stop
@extends('admin.master-add')
@section('content_add_form')

    <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
        <label for="phone_number" class="col-md-2 pull-right control-label">ایمیل یا شماره تماس:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="phone_number" value="{{ old('phone_number',isset($contact->phone_number) ? $contact->phone_number : '') }}">
            @if ($errors->has('phone_number'))
                <span class="help-block"><strong>{{ $errors->first('phone_number') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('contact_title') ? ' has-error' : '' }}">
        <label for="contact_title" class="col-md-2 pull-right control-label">عنوان پیام:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="contact_title" value="{{ old('contact_title',isset($contact->contact_title) ? $contact->contact_title : '') }}">
            @if ($errors->has('contact_title'))
                <span class="help-block"><strong>{{ $errors->first('contact_title') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('contact_text') ? ' has-error' : '' }}">
        <label for="contact_text" class="col-md-2 pull-right control-label">متن پیام:</label>
        <div class="col-md-6 pull-right">
            <textarea class="form-control" name="contact_text">{{ old('contact_text',isset($contact->contact_text) ? Helpers::br2nl($contact->contact_text) : '') }}</textarea>
            @if ($errors->has('contact_text'))<span class="help-block"><strong>{{ $errors->first('contact_text') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('viewed') ? ' has-error' : '' }}">
        <label for="viewed" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='viewed' class='selectpicker form-control pull-right'>
                <option @if(old('viewed' , isset($contact->viewed) ? $contact->viewed : '')==0) selected @endif value="0" >جدید</option>
                <option @if(old('viewed' , isset($contact->viewed) ? $contact->viewed : '')==1) selected @endif value="1" >بررسی شده</option>
            </select>
            @if ($errors->has('viewed'))
                <span class="help-block"><strong>{{ $errors->first('viewed') }}</strong></span>
            @endif
        </div>
    </div>




@stop

@extends('admin.master-add')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('jsFiles')
    <script type="text/javascript" src="{!! asset('j-calender/scripts/jquery.ui.datepicker-cc.all.min.js') !!}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#is_valid_to').datepicker({
                dateFormat: 'yy/mm/dd',
                inline: true,
                onSelect: function(dateText, inst) {
                    var date = $(this).datepicker('getDate'),
                        day  = date.getDate(),
                        month = date.getMonth() + 1,
                        year =  date.getFullYear();
                }
            });
        });
    </script>
@stop
@section('cssFiles')
    <link href="{{ asset('j-calender/styles/jquery-ui-1.8.14.css') }}" media="all" rel="stylesheet" type="text/css" />
@stop
@section('content_add_form')

    <div class="form-group{{ $errors->has('discount_code') ? ' has-error' : '' }}">
        <label for="discount_code" class="col-md-2 pull-right control-label">کد تخفیف:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="discount_code" value="{{ old('discount_code',isset($discount->discount_code) ? $discount->discount_code : '') }}" autocomplete="off">
            @if ($errors->has('discount_code'))<span class="help-block"><strong>{{ $errors->first('discount_code') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('discount_percent') ? ' has-error' : '' }}">
        <label for="discount_percent" class="col-md-2 pull-right control-label">درصد تخفیف:</label>
        <div class="col-md-6 pull-right">
            <select  name='discount_percent' class='selectpicker form-control pull-right'>
                @for($i=1; $i<=100; $i++)
                    <option @if(old('discount_percent' , isset($discount->discount_percent) ? $discount->discount_percent : '')==$i) selected @endif value="{{$i}}" >{{$i}}</option>
                @endfor
            </select>
            @if ($errors->has('discount_percent'))
                <span class="help-block"><strong>{{ $errors->first('discount_percent') }}</strong></span>
            @endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('discount_title1') ? ' has-error' : '' }}">
        <label for="discount_title1" class="col-md-2 pull-right control-label">عنوان 1:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="discount_title1" value="{{ old('discount_title1',isset($discount->discount_title1) ? $discount->discount_title1 : '') }}" autocomplete="off">
            @if ($errors->has('discount_title1'))<span class="help-block"><strong>{{ $errors->first('discount_title1') }}</strong></span>@endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('discount_title2') ? ' has-error' : '' }}">
        <label for="discount_title2" class="col-md-2 pull-right control-label">عنوان 2:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="discount_title2" value="{{ old('discount_title2',isset($discount->discount_title2) ? $discount->discount_title2 : '') }}" autocomplete="off">
            @if ($errors->has('discount_title2'))<span class="help-block"><strong>{{ $errors->first('discount_title2') }}</strong></span>@endif
        </div>
    </div>



    <div class="form-group{{ $errors->has('is_valid_to') ? ' has-error' : '' }}">
        <label for="is_valid_to" class="col-md-2 pull-right control-label">تاریخ انقضاء:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" id="is_valid_to" name="is_valid_to" value="{{ old('is_valid_to',isset($discount->is_valid_to_j) ? $discount->is_valid_to_j : '') }}" autocomplete="off">
            @if ($errors->has('is_valid_to'))<span class="help-block"><strong>{{ $errors->first('is_valid_to') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('total_count') ? ' has-error' : '' }}">
        <label for="total_count" class="col-md-2 pull-right control-label">تعداد کل:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="total_count" value="{{ old('total_count',isset($discount->total_count) ? $discount->total_count : '') }}" autocomplete="off">
            @if ($errors->has('total_count'))<span class="help-block"><strong>{{ $errors->first('total_count') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('used_count') ? ' has-error' : '' }}">
        <label for="used_count" class="col-md-2 pull-right control-label">تعداد استفاده شده:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="used_count" value="{{ old('used_count',isset($discount->used_count) ? $discount->used_count : '') }}" autocomplete="off">
            @if ($errors->has('used_count'))<span class="help-block"><strong>{{ $errors->first('used_count') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('up_to') ? ' has-error' : '' }}">
        <label for="up_to" class="col-md-2 pull-right control-label">حداکثر مبلغ:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="up_to" value="{{ old('up_to',isset($discount->up_to) ? $discount->up_to : '') }}" autocomplete="off">
            @if ($errors->has('up_to'))<span class="help-block"><strong>{{ $errors->first('up_to') }}</strong></span>@endif
        </div>
    </div>



    <div class="form-group{{ $errors->has('discount_status') ? ' has-error' : '' }}">
        <label for="discount_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='discount_status' class='selectpicker form-control pull-right' autocomplete="off">
                <option @if(old('discount_status' ,isset($app->discount_status) ? $app->discount_status : '' )==1) selected @endif value="1" >فعال</option>
                <option @if(old('discount_status' , isset($app->discount_status) ? $app->discount_status : '')==0) selected @endif value="0" >غیر فعال</option>
            </select>
            @if ($errors->has('discount_status'))<span class="help-block"><strong>{{ $errors->first('discount_status') }}</strong></span>@endif
        </div>
    </div>


@stop

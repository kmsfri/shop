@extends('admin.master-add')
@section('content_add_form')
    <div class="form-group{{ $errors->has('brand_title') ? ' has-error' : '' }}">
        <label for="brand_title" class="col-md-2 pull-right control-label">عنوان:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="brand_title" value="{{ old('brand_title',isset($brand->brand_title) ? $brand->brand_title : '') }}">
            @if ($errors->has('brand_title'))
                <span class="help-block"><strong>{{ $errors->first('brand_title') }}</strong></span>
            @endif
        </div>
    </div>



    <div class="form-group{{ $errors->has('brand_img') ? ' has-error' : '' }}">
        <label for="brand_img" class="col-md-2 pull-right control-label">تصویر برند:</label>
        <div class="col-md-4 pull-right">
            <input type="file" onchange="readURL(this,'','img_preview')" name="brand_img" id="brand_img"  autocomplete="off">
            @if ($errors->has('brand_img')) <span class="help-block"><strong>{{ $errors->first('brand_img') }}</strong></span> @endif
        </div>
        <div class="col-md-4 pull-right">
            <img id="img_preview" class="{{ isset($brand->brand_img) ? '' : 'hide' }}" src="{{ isset($brand->brand_img)?url($brand->brand_img) : '' }}" alt="تصویر برند" autocomplete="off" />
        </div>
    </div>


    <div class="form-group{{ $errors->has('brand_slug_corrected') ? ' has-error' : '' }}">
        <label for="brand_slug" class="col-md-2 pull-right control-label">کلمات کلیدی آدرس:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="brand_slug" value="{{ old('brand_slug',isset($brand->brand_slug) ? $brand->brand_slug : '') }}">
            @if ($errors->has('brand_slug_corrected'))
                <span class="help-block"><strong>{{ $errors->first('brand_slug_corrected') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('brand_order') ? ' has-error' : '' }}">
        <label for="brand_order" class="col-md-2 pull-right control-label">ترتیب:</label>
        <div class="col-md-6 pull-right">
            <select  name='brand_order' class='selectpicker form-control pull-right'>
                @for($i=1; $i<=40; $i++)
                    <option @if(old('brand_order' , isset($brand->brand_order) ? $brand->brand_order : '')==$i) selected @endif value="{{$i}}" >{{$i}}</option>
                @endfor
            </select>
            @if ($errors->has('brand_order'))
                <span class="help-block"><strong>{{ $errors->first('brand_order') }}</strong></span>
            @endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('brand_status') ? ' has-error' : '' }}">
        <label for="brand_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='brand_status' class='selectpicker form-control pull-right'>
                <option @if(old('brand_status' , isset($brand->brand_status) ? $brand->brand_status : '')==1) selected @endif value="1" >فعال</option>
                <option @if(old('brand_status' , isset($brand->brand_status) ? $brand->brand_status : '')==0) selected @endif value="0" >غیر فعال</option>
            </select>
            @if ($errors->has('brand_status'))
                <span class="help-block"><strong>{{ $errors->first('brand_status') }}</strong></span>
            @endif
        </div>
    </div>
@stop
@section('jsCustom')
    <script type="text/javascript">
        function readURL(input,img_id,img_preview_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+img_preview_id+img_id)
                        .attr('src', e.target.result)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
                $('#'+img_preview_id+img_id).removeClass('hide');
            }
        }
    </script>
@stop

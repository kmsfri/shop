@extends('admin.master-add')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('jsFiles')
    <script type="text/javascript" src="{!! asset('j-calender/scripts/jquery.ui.datepicker-cc.all.min.js') !!}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#countdown').datepicker({
                dateFormat: 'yy/mm/dd',
                inline: true,
                onSelect: function(dateText, inst) {
                    var date = $(this).datepicker('getDate'),
                        day  = date.getDate(),
                        month = date.getMonth() + 1,
                        year =  date.getFullYear();
                }
            });
        });
    </script>
@stop
@section('cssFiles')
    <link href="{{ asset('j-calender/styles/jquery-ui-1.8.14.css') }}" media="all" rel="stylesheet" type="text/css" />
@stop
@section('content_add_form')


    <div class="form-group{{ $errors->has('brand_id') ? ' has-error' : '' }}">
        <label for="brand_id" class="col-md-2 pull-right control-label">برند:</label>
        <div class="col-md-6 pull-right">
            <select  name='brand_id' class='selectpicker form-control pull-right'>
                <option disabled selected value="">انتخاب کنید</option>
                @foreach($brands as $brand)
                    <option @if(old('brand_id' , isset($advertise->brand_id) ? $advertise->brand_id : '')==$brand->id) selected @endif value="{{$brand->id}}" >{{$brand->brand_title}}</option>
                @endforeach
            </select>
            @if ($errors->has('brand_id')) <span class="help-block"><strong>{{ $errors->first('brand_id') }}</strong></span> @endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('product_slug') ? ' has-error' : '' }}">
        <label for="product_slug" class="col-md-2 pull-right control-label">کلمات کلیدی آدرس:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="product_slug" value="{{ old('product_slug',isset($advertise->product_slug) ? $advertise->product_slug : '') }}" autocomplete="off">
            @if ($errors->has('product_slug'))<span class="help-block"><strong>{{ $errors->first('product_slug') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('countdown') ? ' has-error' : '' }}">
        <label for="countdown" class="col-md-2 pull-right control-label">شمارنده تا تاریخ:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" id="countdown" name="countdown" value="{{ old('countdown',isset($advertise->countdownTo) ? $advertise->countdownTo : '') }}" autocomplete="off">
            @if ($errors->has('countdown'))<span class="help-block"><strong>{{ $errors->first('countdown') }}</strong></span>@endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('product_title') ? ' has-error' : '' }}">
        <label for="product_title" class="col-md-2 pull-right control-label">عنوان محصول:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="product_title" value="{{ old('product_title',isset($advertise->product_title) ? $advertise->product_title : '') }}" autocomplete="off">
            @if ($errors->has('product_title'))<span class="help-block"><strong>{{ $errors->first('product_title') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('product_description') ? ' has-error' : '' }}">
        <label for="product_description" class="col-md-2 pull-right control-label">توضیحات محصول:</label>
        <div class="col-md-6 pull-right">
            <textarea class="form-control" name="product_description">{{ old('product_description',isset($advertise->product_description) ? Helpers::br2nl($advertise->product_description) : '') }}</textarea>
            @if ($errors->has('product_description'))<span class="help-block"><strong>{{ $errors->first('product_description') }}</strong></span>@endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('product_price') ? ' has-error' : '' }}">
        <label for="product_price" class="col-md-2 pull-right control-label">قیمت(تومان):</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="product_price" value="{{ old('product_price',isset($advertise->product_price) ? $advertise->product_price : '') }}" autocomplete="off">
            @if ($errors->has('product_price'))<span class="help-block"><strong>{{ $errors->first('product_price') }}</strong></span>@endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('discount_percent') ? ' has-error' : '' }}">
        <label for="discount_percent" class="col-md-2 pull-right control-label">تخفیف(درصد):</label>
        <div class="col-md-6 pull-right">
            <select  name='discount_percent' class='selectpicker form-control pull-right'>
                @for($i=0; $i<=100; $i++)
                    <option @if(old('discount_percent' , isset($advertise->discount_percent) ? $advertise->discount_percent : '')==$i) selected @endif value="{{$i}}" >{{$i}} درصد</option>
                @endfor
            </select>
            @if ($errors->has('discount_percent')) <span class="help-block"><strong>{{ $errors->first('discount_percent') }}</strong></span> @endif
        </div>
    </div>





    <div class="form-group{{ $errors->has('product_status') ? ' has-error' : '' }}">
        <label for="product_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='product_status' class='selectpicker form-control pull-right' autocomplete="off">
                <option @if(old('product_status' ,isset($advertise->product_status) ? $advertise->product_status : '' )==1) selected @endif value="1" >فعال</option>
                <option @if(old('product_status' , isset($advertise->product_status) ? $advertise->product_status : '')==0) selected @endif value="0" >غیر فعال</option>
            </select>
            @if ($errors->has('product_status'))<span class="help-block"><strong>{{ $errors->first('product_status') }}</strong></span>@endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('in_stock_count') ? ' has-error' : '' }}">
        <label for="in_stock_count" class="col-md-2 pull-right control-label">موجودی:</label>
        <div class="col-md-6 pull-right">
            <select  name='in_stock_count' class='selectpicker form-control pull-right'>
                @for($i=0; $i<=240; $i++)
                    <option @if(old('in_stock_count' , isset($advertise->in_stock_count) ? $advertise->in_stock_count : '')==$i) selected @endif value="{{$i}}" >{{$i}}</option>
                @endfor
            </select>
            @if ($errors->has('in_stock_count'))
                <span class="help-block"><strong>{{ $errors->first('in_stock_count') }}</strong></span>
            @endif
        </div>
    </div>


    @foreach($props as $pr)
        <div class="form-group{{ $errors->has('props.'.$pr->id) ? ' has-error' : '' }}">
            <label for="props[{{$pr->id}}]" class="col-md-2 pull-right control-label">{{$pr->property_title}}:</label>
            <div class="col-md-6 pull-right">


                <select  name='props[{{$pr->id}}]' id='props[{{$pr->id}}]' class='selectpicker form-control pull-right' autocomplete="off">
                    <option  value="" >انتخاب کنید</option>
                    @foreach($pr->propEnabledValues()->get() as $prv)
                        <option @if(old('props.'.$pr->id , (isset($advertise)&&isset($advertise->specProperty($pr->id)->id)) ? $advertise->specProperty($pr->id)->id : '')==$prv->id) selected @endif value="{{$prv->id}}" >{{$prv->property_title}}</option>
                    @endforeach
                    <option {{(old('props.'.$pr->id , (isset($advertise)&&isset($advertise->Property()->where('property_id',$pr->id)->first()->id)) ? $advertise->Property($pr->id)->where('property_id',$pr->id)->first()->id : '')==$pr->id && $advertise->Property($pr->id)->where('property_id',$pr->id)->withPivot('select_status')->first()->pivot->select_status=='1')?'selected':''}}  value="selectable_by_buyer" >انتخاب توسط کاربر</option>
                    <option {{(old('props.'.$pr->id , (isset($advertise)&&isset($advertise->Property()->where('property_id',$pr->id)->first()->id)) ? $advertise->Property($pr->id)->where('property_id',$pr->id)->first()->id : '')==$pr->id && $advertise->Property($pr->id)->where('property_id',$pr->id)->withPivot('select_status')->first()->pivot->select_status=='2')?'selected':''}}  value="fillable_by_buyer" >وارد کردن توسط کاربر</option>
                    <option {{(old('props.'.$pr->id , (isset($advertise)&&isset($advertise->Property()->where('property_id',$pr->id)->first()->id)) ? $advertise->Property($pr->id)->where('property_id',$pr->id)->first()->id : '')==$pr->id && $advertise->Property($pr->id)->where('property_id',$pr->id)->withPivot('select_status')->first()->pivot->select_status=='3')?'selected':''}}  value="select_color_by_buyer" >انتخاب از لیست رنگ بندی</option>
                </select>

                @if($errors->has('props.'.$pr->id)) <span class="help-block"><strong>{{ $errors->first('props.'.$pr->id) }}</strong></span> @endif
            </div>
        </div>
    @endforeach


    <div class="form-group">
        <label for="files" class="col-md-2 pull-right control-label">تصاویر محصول:</label>
        <div class="col-md-10 pull-right">
            <div class="box-form">
                <input id="files" type="file" name="newImg[]" multiple="multiple" autocomplete="off" accept="image/jpg, image/jpeg, image/png" /><br>
                <output id="result">
                    @if(isset($advertise) && $advertise!=Null)
                        @foreach($advertise->ProductImages()->get() as $simg)
                            <div>
                                <img class="thumbnail" src="{{url($simg->image_dir)}}">
                                <input name="oldImg[]" type="hidden" value="{{$simg->id}}">
                                <a href="javascript:void()" onclick="$(this).closest('div').remove()" style="display: block">حذف</a>
                            </div>
                        @endforeach
                    @endif
                </output>
                @if ($errors->has('newImg.*')) <span class="help-block"><strong>{{ $errors->first('newImg.*') }}</strong></span> @endif
            </div>
        </div>
    </div>
@stop
@section('jsCustom')
    <script type="text/javascript">
        window.onload = function(){
            //Check File API support
            if(window.File && window.FileList && window.FileReader) {
                var filesInput = document.getElementById("files");
                filesInput.addEventListener("change", function(event){
                    var files = event.target.files; //FileList object
                    var output = document.getElementById("result");
                    $( ".newImgThumb" ).remove();
                    for(var i = 0; i< files.length; i++) {
                        var file = files[i];
                        //Only pics
                        if(!file.type.match('image'))
                            continue;

                        var picReader = new FileReader();

                        picReader.addEventListener("load",function(event){

                            var picFile = event.target;
                            var div = document.createElement("div");

                            div.innerHTML = "" +
                                "<img class='thumbnail newImgThumb' src='" + picFile.result + "'" + "title='" + picFile.name + "'/>" +
                                "<span class='newImgThumb' style=\"display: block\">تصویر جدید</span>"
                            ;
                            $(output).append(div);

                        });

                        //Read the image
                        picReader.readAsDataURL(file);
                    }

                });
            }
            else
            {
                console.log("Your browser does not support File API");
            }
        }
    </script>
@stop
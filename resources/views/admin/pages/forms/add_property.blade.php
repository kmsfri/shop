@extends('admin.master-add')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_add_form')
    <input type="hidden" name="parent_id" value="{{ old('parent_id',isset($parent_id) ? $parent_id : Null) }}">

    <div class="form-group{{ $errors->has('property_title') ? ' has-error' : '' }}">
        <label for="property_title" class="col-md-2 pull-right control-label">{{($parent_id==Null)?'عنوان خصوصیت:' : 'مقدار:'}}</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="property_title" value="{{ old('property_title',isset($property->property_title) ? $property->property_title : '') }}">
            @if ($errors->has('property_title')) <span class="help-block"><strong>{{ $errors->first('property_title') }}</strong></span> @endif
        </div>
    </div>



    <div class="form-group{{ $errors->has('property_order') ? ' has-error' : '' }}">
        <label for="property_order" class="col-md-2 pull-right control-label">ترتیب:</label>
        <div class="col-md-6 pull-right">
            <select  name='property_order' class='selectpicker form-control pull-right'>
                @for($i=1; $i<=20; $i++)
                    <option @if(old('property_order' , isset($property->property_order) ? $property->property_order : '')==$i) selected @endif value="{{$i}}" >{{$i}}</option>
                @endfor
            </select>
            @if ($errors->has('property_order')) <span class="help-block"><strong>{{ $errors->first('property_order') }}</strong></span> @endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('property_status') ? ' has-error' : '' }}">
        <label for="property_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='property_status' class='selectpicker form-control pull-right'>
                <option @if(old('property_status' , isset($property->property_status) ? $property->property_status : '')==1) selected @endif value="1" >فعال</option>
                <option @if(old('property_status' , isset($property->property_status) ? $property->property_status : '')==0) selected @endif value="0" >غیر فعال</option>
            </select>
            @if ($errors->has('property_status')) <span class="help-block"><strong>{{ $errors->first('property_status') }}</strong></span> @endif
        </div>
    </div>


@stop
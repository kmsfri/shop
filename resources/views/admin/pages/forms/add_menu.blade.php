@extends('admin.master-add')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_add_form')
<input type="hidden" name="parent_id" value="{{ old('parent_id',isset($parent_id) ? $parent_id : Null) }}">


<div class="form-group{{ $errors->has('m_title') ? ' has-error' : '' }}">
    <label for="m_title" class="col-md-2 pull-right control-label">عنوان:</label>
    <div class="col-md-6 pull-right">
        <input type="text" class="form-control" name="m_title" value="{{ old('m_title',isset($data->m_title) ? $data->m_title : '') }}">
        @if ($errors->has('m_title')) <span class="help-block"><strong>{{ $errors->first('m_title') }}</strong></span> @endif
    </div>
</div>

<div class="form-group{{ $errors->has('m_link') ? ' has-error' : '' }}">
    <label for="m_link" class="col-md-2 pull-right control-label">لینک:</label>
    <div class="col-md-6 pull-right">
        <input type="text" class="form-control" name="m_link" value="{{ old('m_link',isset($data->m_link) ? $data->m_link : '') }}">
        @if ($errors->has('m_link')) <span class="help-block"><strong>{{ $errors->first('m_link') }}</strong></span> @endif
    </div>
</div>


<div class="form-group{{ $errors->has('c_order') ? ' has-error' : '' }}">
    <label for="c_order" class="col-md-2 pull-right control-label">ترتیب:</label>
    <div class="col-md-6 pull-right">
        <select  name='c_order' class='selectpicker form-control pull-right'>
            @for($i=1; $i<=40; $i++)
                <option @if(old('c_order' , isset($data->c_order) ? $data->c_order : '')==$i) selected @endif value="{{$i}}" >{{$i}}</option>
            @endfor
        </select>
        @if ($errors->has('c_order')) <span class="help-block"><strong>{{ $errors->first('c_order') }}</strong></span> @endif
    </div>
</div>


<div class="form-group{{ $errors->has('c_status') ? ' has-error' : '' }}">
    <label for="c_status" class="col-md-2 pull-right control-label">وضعیت:</label>
    <div class="col-md-6 pull-right">
        <select  name='c_status' class='selectpicker form-control pull-right'>
            <option @if(old('c_status' , isset($data->c_status) ? $data->c_status : '')==1) selected @endif value="1" >فعال</option>
            <option @if(old('c_status' , isset($data->c_status) ? $data->c_status : '')==0) selected @endif value="0" >غیر فعال</option>
        </select>
        @if ($errors->has('c_status')) <span class="help-block"><strong>{{ $errors->first('c_status') }}</strong></span> @endif
    </div>
</div>
@stop

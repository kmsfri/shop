@extends('admin.master-add')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('jsFiles')
    <script type="text/javascript" src="{!! asset('j-calender/scripts/jquery.ui.datepicker-cc.all.min.js') !!}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#released_at').datepicker({
                dateFormat: 'yy/mm/dd',
                inline: true,
                onSelect: function(dateText, inst) {
                    var date = $(this).datepicker('getDate'),
                        day  = date.getDate(),
                        month = date.getMonth() + 1,
                        year =  date.getFullYear();
                }
            });
        });
    </script>
@stop
@section('cssFiles')
    <link href="{{ asset('j-calender/styles/jquery-ui-1.8.14.css') }}" media="all" rel="stylesheet" type="text/css" />
@stop
@section('content_add_form')

    <div class="form-group{{ $errors->has('version_number') ? ' has-error' : '' }}">
        <label for="version_number" class="col-md-2 pull-right control-label">ورژن:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="version_number" value="{{ old('version_number',isset($app->version_number) ? $app->version_number : '') }}" autocomplete="off">
            @if ($errors->has('version_number'))<span class="help-block"><strong>{{ $errors->first('version_number') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('version_name') ? ' has-error' : '' }}">
        <label for="version_name" class="col-md-2 pull-right control-label">عنوان ورژن:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="version_name" value="{{ old('version_name',isset($app->version_name) ? $app->version_name : '') }}" autocomplete="off">
            @if ($errors->has('version_name'))<span class="help-block"><strong>{{ $errors->first('version_name') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('app_file') ? ' has-error' : '' }}">
        <label for="app_file" class="col-md-2 pull-right control-label">فایل اپلیکیشن:</label>
        <div class="col-md-4 pull-right">
            <input type="file"  name="app_file" id="app_file" value="{{ old('app_file',isset($app->download_dir) ? $app->download_dir : '') }}" autocomplete="off">
            @if ($errors->has('app_file')) <span class="help-block"><strong>{{ $errors->first('app_file') }}</strong></span> @endif
        </div>
        <div class="col-md-4 pull-right">
            {{ isset($app->download_dir)?url($app->download_dir) : '' }}
        </div>
    </div>

    <div class="form-group{{ $errors->has('released_at') ? ' has-error' : '' }}">
        <label for="released_at" class="col-md-2 pull-right control-label">تاریخ انتشار:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" id="released_at" name="released_at" value="{{ old('released_at',isset($app->released_at) ? $app->released_at : '') }}" autocomplete="off">
            @if ($errors->has('released_at'))<span class="help-block"><strong>{{ $errors->first('released_at') }}</strong></span>@endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('version_description') ? ' has-error' : '' }}">
        <label for="version_description" class="col-md-2 pull-right control-label">توضیحات ورژن:</label>
        <div class="col-md-6 pull-right">
            <textarea class="form-control" name="version_description">{{ old('version_description',isset($app->version_description) ? Helpers::br2nl($app->version_description) : '') }}</textarea>
            @if ($errors->has('version_description'))<span class="help-block"><strong>{{ $errors->first('version_description') }}</strong></span>@endif
        </div>
    </div>



    <div class="form-group{{ $errors->has('version_status') ? ' has-error' : '' }}">
        <label for="version_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='version_status' class='selectpicker form-control pull-right' autocomplete="off">
                <option @if(old('version_status' ,isset($app->version_status) ? $app->version_status : '' )==1) selected @endif value="1" >فعال</option>
                <option @if(old('version_status' , isset($app->version_status) ? $app->version_status : '')==0) selected @endif value="0" >غیر فعال</option>
            </select>
            @if ($errors->has('version_status'))<span class="help-block"><strong>{{ $errors->first('version_status') }}</strong></span>@endif
        </div>
    </div>

@stop

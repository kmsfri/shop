@extends('admin.master-add')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_add_form')
    <div class="form-group{{ $errors->has('post_title') ? ' has-error' : '' }}">
        <label for="post_title" class="col-md-2 pull-right control-label">عنوان مطلب:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="post_title" value="{{ old('post_title',isset($post->post_title) ? $post->post_title : '') }}" autocomplete="off">
            @if ($errors->has('post_title'))<span class="help-block"><strong>{{ $errors->first('post_title') }}</strong></span>@endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('short_text') ? ' has-error' : '' }}">
        <label for="short_text" class="col-md-2 pull-right control-label">توضیحات کوتاه مطلب:</label>
        <div class="col-md-6 pull-right">
            <textarea class="form-control" name="short_text">{{ old('short_text',isset($post->short_text) ? Helpers::br2nl($post->short_text) : '') }}</textarea>
            @if ($errors->has('short_text'))<span class="help-block"><strong>{{ $errors->first('short_text') }}</strong></span>@endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('main_text') ? ' has-error' : '' }}">
        <label for="main_text" class="col-md-2 pull-right control-label">توضیحات مطلب:</label>
        <div class="col-md-6 pull-right">
            <textarea class="form-control" name="main_text">{{ old('main_text',isset($post->main_text) ? Helpers::br2nl($post->main_text) : '') }}</textarea>
            @if ($errors->has('main_text'))<span class="help-block"><strong>{{ $errors->first('main_text') }}</strong></span>@endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('post_tags') ? ' has-error' : '' }}">
        <label for="post_tags" class="col-md-2 pull-right control-label">برچسب ها(با خط تیره جدا کنید):</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="post_tags" value="{{ old('post_tags',isset($post->post_tags) ? $post->post_tags : '') }}" autocomplete="off">
            @if ($errors->has('post_tags'))<span class="help-block"><strong>{{ $errors->first('post_tags') }}</strong></span>@endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('meta_keywords') ? ' has-error' : '' }}">
        <label for="meta_keywords" class="col-md-2 pull-right control-label">کلمات کلیدی سئو:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="meta_keywords" value="{{ old('meta_keywords',isset($post->meta_keywords) ? $post->meta_keywords : '') }}" autocomplete="off">
            @if ($errors->has('meta_keywords'))<span class="help-block"><strong>{{ $errors->first('meta_keywords') }}</strong></span>@endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('post_slug') ? ' has-error' : '' }}">
        <label for="post_slug" class="col-md-2 pull-right control-label">آدرس url مطلب:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="post_slug" value="{{ old('post_slug',isset($post->post_slug) ? $post->post_slug : '') }}" autocomplete="off">
            @if ($errors->has('post_slug'))<span class="help-block"><strong>{{ $errors->first('post_slug') }}</strong></span>@endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('post_status') ? ' has-error' : '' }}">
        <label for="post_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='post_status' class='selectpicker form-control pull-right' autocomplete="off">
                <option @if(old('post_status' ,isset($post->post_status) ? $post->post_status : '' )==1) selected @endif value="1" >فعال</option>
                <option @if(old('post_status' , isset($post->post_status) ? $post->post_status : '')==0) selected @endif value="0" >غیر فعال</option>
            </select>
            @if ($errors->has('post_status'))<span class="help-block"><strong>{{ $errors->first('post_status') }}</strong></span>@endif
        </div>
    </div>





    <div class="form-group">
        <label for="files" class="col-md-2 pull-right control-label">تصاویر مطلب:</label>
        <div class="col-md-10 pull-right">
            <div class="box-form">
                <input id="files" type="file" name="newImg[]" multiple="multiple" autocomplete="off" accept="image/jpg, image/jpeg, image/png" /><br>
                <output id="result">
                    @if(isset($post) && $post!=Null)
                        @foreach($post->PostImages()->get() as $simg)
                            <div>
                                <img class="thumbnail" src="{{url($simg->image_dir)}}">
                                <input name="oldImg[]" type="hidden" value="{{$simg->id}}">
                                <a href="javascript:void()" onclick="$(this).closest('div').remove()" style="display: block">حذف</a>
                            </div>
                        @endforeach
                    @endif
                </output>
                @if ($errors->has('newImg.*')) <span class="help-block"><strong>{{ $errors->first('newImg.*') }}</strong></span> @endif
            </div>
        </div>
    </div>
@stop
@section('jsCustom')
    <script type="text/javascript">
        window.onload = function(){
            //Check File API support
            if(window.File && window.FileList && window.FileReader) {
                var filesInput = document.getElementById("files");
                filesInput.addEventListener("change", function(event){
                    var files = event.target.files; //FileList object
                    var output = document.getElementById("result");
                    $( ".newImgThumb" ).remove();
                    for(var i = 0; i< files.length; i++) {
                        var file = files[i];
                        //Only pics
                        if(!file.type.match('image'))
                            continue;

                        var picReader = new FileReader();

                        picReader.addEventListener("load",function(event){

                            var picFile = event.target;
                            var div = document.createElement("div");

                            div.innerHTML = "" +
                                "<img class='thumbnail newImgThumb' src='" + picFile.result + "'" + "title='" + picFile.name + "'/>" +
                                "<span class='newImgThumb' style=\"display: block\">تصویر جدید</span>"
                            ;
                            $(output).append(div);

                        });

                        //Read the image
                        picReader.readAsDataURL(file);
                    }

                });
            }
            else
            {
                console.log("Your browser does not support File API");
            }
        }
    </script>
@stop
@extends('admin.master-add')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_add_form')
    <input type='hidden' name='product_id' value="{{ old('product_id',isset($product_id) ? $product_id : '') }}">
    <div class="form-group{{ $errors->has('title1') ? ' has-error' : '' }}">
        <label for="title1" class="col-md-2 pull-right control-label">عنوان:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="title1" value="{{ old('title1',isset($slide->title1) ? $slide->title1 : '') }}" autocomplete="off">
            @if ($errors->has('title1'))<span class="help-block"><strong>{{ $errors->first('title1') }}</strong></span>@endif
        </div>
    </div>
    
    
    <div class="form-group{{ $errors->has('slide2_text') ? ' has-error' : '' }}">
    <label for="slide2_text" class="col-md-2 pull-right control-label">متن:</label>
    <div class="col-md-6 pull-right">
        <textarea class="form-control" name="slide2_text">{{ old('slide2_text',isset($slide->slide2_text) ? Helpers::br2nl($slide->slide2_text) : '') }}</textarea>
        @if ($errors->has('slide2_text'))<span class="help-block"><strong>{{ $errors->first('slide2_text') }}</strong></span>@endif
    </div>
    </div>
    
    <div class="form-group{{ $errors->has('slide2_color') ? ' has-error' : '' }}">
        <label for="slide2_color" class="col-md-2 pull-right control-label">کد رنگ:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="slide2_color" value="{{ old('slide2_color',isset($slide->slide2_color) ? $slide->slide2_color : '') }}" autocomplete="off">
            @if ($errors->has('slide2_color'))<span class="help-block"><strong>{{ $errors->first('slide2_color') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('img_link') ? ' has-error' : '' }}">
        <label for="img_link" class="col-md-2 pull-right control-label">لینک:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="img_link" value="{{ old('img_link',isset($slide->img_link) ? $slide->img_link : '') }}" autocomplete="off">
            @if ($errors->has('img_link'))<span class="help-block"><strong>{{ $errors->first('img_link') }}</strong></span>@endif
        </div>
    </div>



    <div class="form-group{{ $errors->has('img_dir') ? ' has-error' : '' }}">
        <label for="img_dir" class="col-md-2 pull-right control-label">تصویر اسلاید:</label>
        <div class="col-md-4 pull-right">
            <input type="file" onchange="readURL(this,'','img_dir_preview')" name="img_dir" id="img_dir" value="{{ old('img_dir',isset($slide->img_dir) ? $slide->img_dir : '') }}" autocomplete="off">
            @if ($errors->has('img_dir')) <span class="help-block"><strong>{{ $errors->first('avatar_dir') }}</strong></span> @endif
        </div>
        <div class="col-md-4 pull-right">
            <img id="img_dir_preview" class="{{ isset($slide->img_dir) ? '' : 'hide' }}" src="{{ isset($slide->img_dir) ? url($slide->img_dir) : '#' }}" alt="تصویر اسلاید" autocomplete="off" />
        </div>
    </div>


    <div class="form-group{{ $errors->has('img_order') ? ' has-error' : '' }}">
        <label for="img_order" class="col-md-2 pull-right control-label">ترتیب:</label>
        <div class="col-md-6 pull-right">
            <select  name='img_order' class='selectpicker form-control pull-right'>
                @for($i=1; $i<=20; $i++)
                    <option @if(old('img_order' , isset($slide->img_order) ? $slide->img_order : '')==$i) selected @endif value="{{$i}}" >{{$i}}</option>
                @endfor
            </select>
            @if ($errors->has('img_order'))
                <span class="help-block"><strong>{{ $errors->first('img_order') }}</strong></span>
            @endif
        </div>
    </div>



    <div class="form-group{{ $errors->has('img_status') ? ' has-error' : '' }}">
        <label for="img_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='img_status' class='selectpicker form-control pull-right' autocomplete="off">
                <option @if(old('img_status' ,isset($slide->img_status) ? $slide->img_status : '' )==1) selected @endif value="1" >فعال</option>
                <option @if(old('img_status' , isset($slide->img_status) ? $slide->img_status : '')==0) selected @endif value="0" >غیر فعال</option>
            </select>
            @if ($errors->has('img_status'))<span class="help-block"><strong>{{ $errors->first('img_status') }}</strong></span>@endif
        </div>
    </div>






@stop
@section('jsCustom')
    <script type="text/javascript">
        function readURL(input,img_id,img_preview_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+img_preview_id+img_id)
                        .attr('src', e.target.result)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
                $('#'+img_preview_id+img_id).removeClass('hide');
            }
        }
    </script>
@stop
@extends('admin.master-add')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_add_form')
    <div class="form-group{{ $errors->has('note_title') ? ' has-error' : '' }}">
        <label for="note_title" class="col-md-2 pull-right control-label">عنوان یادداشت:</label>
        <div class="col-md-12 pull-right">
            <input type="text" class="form-control" name="note_title" value="{{ old('note_title',isset($note->note_title) ? $note->note_title : '') }}" autocomplete="off">
            @if ($errors->has('note_title'))<span class="help-block"><strong>{{ $errors->first('note_title') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('note_slug') ? ' has-error' : '' }}">
        <label for="note_slug" class="col-md-2 pull-right control-label">کلمات کلیدی آدرس:</label>
        <div class="col-md-12 pull-right">
            <input type="text" class="form-control" name="note_slug" value="{{ old('note_slug',isset($note->note_slug) ? $note->note_slug : '') }}" autocomplete="off">
            @if ($errors->has('note_slug'))<span class="help-block"><strong>{{ $errors->first('note_slug') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('note_body') ? ' has-error' : '' }}">
        <label for="note_body" class="col-md-2 pull-right control-label">متن:</label>
        <div class="col-md-12 pull-right">
            <textarea id="cktext" rows="10" class="form-control" name="note_body">{{ old('note_body',isset($note->note_body) ? Helpers::br2nl($note->note_body) : '') }}</textarea>
            @if ($errors->has('note_body'))<span class="help-block"><strong>{{ $errors->first('note_body') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('note_short_desc') ? ' has-error' : '' }}">
        <label for="note_short_desc" class="col-md-2 pull-right control-label">متن خلاصه:</label>
        <div class="col-md-12 pull-right">
            <textarea class="form-control" name="note_short_desc">{{ old('note_short_desc',isset($note->note_short_desc) ? Helpers::br2nl($note->note_short_desc) : '') }}</textarea>
            @if ($errors->has('note_short_desc'))<span class="help-block"><strong>{{ $errors->first('note_short_desc') }}</strong></span>@endif
        </div>
    </div>




    <div class="form-group{{ $errors->has('note_order') ? ' has-error' : '' }}">
        <label for="note_order" class="col-md-2 pull-right control-label">ترتیب:</label>
        <div class="col-md-12 pull-right">
            <select  name='note_order' class='selectpicker form-control pull-right'>
                @for($i=1; $i<=20; $i++)
                    <option @if(old('note_order' , isset($note->note_order) ? $note->note_order : '')==$i) selected @endif value="{{$i}}" >{{$i}}</option>
                @endfor
            </select>
            @if ($errors->has('note_order')) <span class="help-block"><strong>{{ $errors->first('note_order') }}</strong></span> @endif
        </div>
    </div>





    <div class="form-group{{ $errors->has('note_status') ? ' has-error' : '' }}">
        <label for="note_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-12 pull-right">
            <select  name='note_status' class='selectpicker form-control pull-right' autocomplete="off">
                <option @if(old('note_status' ,isset($note->note_status) ? $note->note_status : '' )==1) selected @endif value="1" >فعال</option>
                <option @if(old('note_status' , isset($note->note_status) ? $note->note_status : '')==0) selected @endif value="0" >غیر فعال</option>
            </select>
            @if ($errors->has('note_status'))<span class="help-block"><strong>{{ $errors->first('note_status') }}</strong></span>@endif
        </div>
    </div>


@stop
@section('jsCustom')
    <script src="{{ asset('landing/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('cktext');
    </script>
@stop



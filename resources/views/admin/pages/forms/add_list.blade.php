@extends('admin.master-add')
@section('header')
    @include('admin.header')
@stop
@section('side-menu')
    @include('admin.side_menu')
@stop
@section('content_add_form')
    <div class="form-group{{ $errors->has('list_title') ? ' has-error' : '' }}">
        <label for="list_title" class="col-md-2 pull-right control-label">عنوان لیست:</label>
        <div class="col-md-6 pull-right">
            <input type="text" class="form-control" name="list_title" value="{{ old('list_title',isset($list->list_title) ? $list->list_title : '') }}" autocomplete="off">
            @if ($errors->has('list_title'))<span class="help-block"><strong>{{ $errors->first('list_title') }}</strong></span>@endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('list_description') ? ' has-error' : '' }}">
        <label for="list_description" class="col-md-2 pull-right control-label">توضیحات:</label>
        <div class="col-md-6 pull-right">
            <textarea class="form-control" name="list_description">{{ old('list_description',isset($list->list_description) ? Helpers::br2nl($list->list_description) : '') }}</textarea>
            @if ($errors->has('list_description'))<span class="help-block"><strong>{{ $errors->first('list_description') }}</strong></span>@endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('list_order') ? ' has-error' : '' }}">
        <label for="list_order" class="col-md-2 pull-right control-label">ترتیب:</label>
        <div class="col-md-6 pull-right">
            <select  name='list_order' class='selectpicker form-control pull-right'>
                @for($i=1; $i<=20; $i++)
                    <option @if(old('list_order' , isset($list->list_order) ? $list->list_order : '')==$i) selected @endif value="{{$i}}" >{{$i}}</option>
                @endfor
            </select>
            @if ($errors->has('list_order'))
                <span class="help-block"><strong>{{ $errors->first('list_order') }}</strong></span>
            @endif
        </div>
    </div>



    <div class="form-group{{ $errors->has('list_status') ? ' has-error' : '' }}">
        <label for="list_status" class="col-md-2 pull-right control-label">وضعیت:</label>
        <div class="col-md-6 pull-right">
            <select  name='list_status' class='selectpicker form-control pull-right' autocomplete="off">
                <option @if(old('list_status' ,isset($list->list_status) ? $list->list_status : '' )==1) selected @endif value="1" >فعال</option>
                <option @if(old('list_status' , isset($list->list_status) ? $list->list_status : '')==0) selected @endif value="0" >غیر فعال</option>
            </select>
            @if ($errors->has('list_status'))<span class="help-block"><strong>{{ $errors->first('list_status') }}</strong></span>@endif
        </div>
    </div>






@stop

@extends('admin.master-add')
@section('content_add_form')
    @foreach($masterProps as $mprop)
        <div class="box-panel">
            <div class="form-group">
            <div class="header">
                @php
                    $hasSubMenu=false;
                    if($mprop->propValue()->count()>0)$hasSubMenu=true;
                @endphp

                <div class="input-group">
                <input {{($shop->Property()->find($mprop->id)!=Null)?'checked':''}} class="checkbox" {{($hasSubMenu)?'disabled':''}} type="checkbox" value="{{$mprop->id}}"  name="prp[]" autocomplete="off">{{$mprop->property_title}}<br><br>
                </div>
                @if($hasSubMenu) <a onclick="changeCheckBox('{{$mprop->id}}');" href="javascript:void()"  style="color:#d26b6b">نمایش زیرگروهها</a><br> @endif
            </div>
            @if($hasSubMenu)
                <div class="data" style="display: none;" id="checkCollection{{$mprop->id}}">
                    <div class="row">
                        <div class="col-md-12">
                                @foreach($mprop->propEnabledValues()->get() as $prp)
                                <div class="row propRow">
                                    <div class="col-md-2 pull-right">
                                    <input {{($shop->Property()->find($prp->id)!=Null)?'checked':''}} type="checkbox" name="prp[]" value="{{$prp->id}}" autocomplete="off"> {{$prp->property_title}}&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="col-md-3 pull-right">
                                    <input type="text" class="form-control" name="prpPrice[{{$prp->id}}]" value="{{ old('prpPrice.'.$prp->id, ($prpTemp=$shop->Property()->withPivot('property_shop_price')->find($prp->id))!=Null ? $prpTemp->pivot->property_shop_price : '') }}" placeholder="قیمت به تومان">
                                    </div>
                                </div>
                                @endforeach
                        </div>
                    </div>
                </div>
            @endif
            </div>
        </div>
    @endforeach

@stop
@section('jsCustom')
    <script type="text/javascript">
        function changeCheckBox(prp_id){
            $('#checkCollection'+prp_id).slideToggle();
        }
    </script>
@stop

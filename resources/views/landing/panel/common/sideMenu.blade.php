<div class="vertical-menu customColumn pull-right-custom">
    <a href="{{Route('showUserDashboard')}}" class="{{($activeTab==1)?'active':''}}">پیشخوان</a>
    <a href="{{Route('showUserInfoForm')}}" class="{{($activeTab==2)?'active':''}}">اطلاعات کاربری</a>
    <a href="{{Route('showUserOrderHistory')}}" class="{{($activeTab==3)?'active':''}}">سوابق خرید</a>
    <a href="{{Route('showUserPayments')}}" class="{{($activeTab==4)?'active':''}}">پرداخت ها</a>
    <a href="{{Route('showWishProductList')}}" class="{{($activeTab==6)?'active':''}}">لیست علاقه مندیها</a>
    <a href="{{Route('showChangePasswordForm')}}" class="{{($activeTab==5)?'active':''}}">تغییر کلمه عبور</a>
    <a href="{{Route('do-user-logout')}}">خروج از حساب کاربری</a>
</div>


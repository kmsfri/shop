@extends('landing.master')
@section('main')

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">خانه</a></li>/
                <li>پرداختی ها</li>
            </ul>
        </div>
    </div>



    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <div class="rowContainer">
                    <h1 class="tt-title-subpages noborder">پرداختی ها</h1>
                </div>

                <div class="rowContainer">
                    @include('landing.panel.common.sideMenu')

                    <div class="customColumn pull-left-custom panel-contentCont">
                        <div class="panel rowPanel">
                            <table class="customTable1">
                                <tr>
                                    <th>مبلغ</th>
                                    <th>کد پرداخت</th>
                                    <th>وضعیت پرداخت</th>
                                    <th>زمان پرداخت</th>
                                </tr>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{$payment->paid_amount_text}}</td>
                                        <td>{{$payment->ref_id}}</td>
                                        <td>{{$payment->paid_status_text}}</td>
                                        <td>{{$payment->time_text}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>

                    </div>

                </div>

            </div>
        </div>



        @endsection
        @section('js')
            <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
            <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
            <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
            <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
            <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
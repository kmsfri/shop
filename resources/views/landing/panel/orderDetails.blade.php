@extends('landing.master')
@section('main')

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">خانه</a></li>/
                <li>جرئیات سفارش</li>
            </ul>
        </div>
    </div>



    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <div class="rowContainer">
                    <h1 class="tt-title-subpages noborder">جزئیات سفارش</h1>
                </div>

                <div class="rowContainer">
                    @include('landing.panel.common.sideMenu')




                    <div class="customColumn pull-left-custom panel-contentCont">


                        <div class="rowContainer">
                            <div class="panel rowPanel">

                                <h5>مشخصات سفارش</h5>

                                <table class="customTable1">
                                    <tr>
                                        <td>توضیحات خریدار</td>
                                        <td>{{$order->user_order_description}}</td>
                                    </tr>

                                    <tr>
                                        <td>وضعیت سفارش</td>
                                        <td>{{$order->order_status_text}}</td>
                                    </tr>

                                    <tr>
                                        <td>هزینه ی کل</td>
                                        <td>{{$order->paid_amount_text}}</td>
                                    </tr>

                                    <tr>
                                        <td>هزینه ارسال</td>
                                        <td>{{$order->paid_amount_text}}</td>
                                    </tr>

                                    <tr>
                                        <td>آدرس تحویل</td>
                                        <td>{{$order->delivery_address}}</td>
                                    </tr>

                                    <tr>
                                        <td>تخفیف</td>
                                        <td>{{$order->discount_amount_text}}</td>
                                    </tr>
                                    <tr>
                                        <td>مالیات</td>
                                        <td>{{$order->VAT_text}}</td>
                                    </tr>


                                </table>
                            </div>
                        </div>

                        <div class="rowContainer">
                            <div class="panel rowPanel">
                                <h5>محصولات خریداری شده</h5>
                                <table class="customTable1">
                                    <tr>
                                        <th>تصویر</th>
                                        <th>عنوان محصول</th>
                                        <th>تعداد</th>
                                    </tr>

                                    @foreach($order->Products as $orderProduct)
                                        <tr>
                                            <td><img src="{{asset($orderProduct->Product->main_img_dir)}}"></td>
                                            <td><a href="{{Route('showProductPage',$orderProduct->Product->product_slug)}}" target="_blank">{{$orderProduct->Product->product_title}}</a></td>
                                            <td>{{$orderProduct->order_count}} عدد</td>
                                        </tr>
                                    @endforeach


                                </table>

                            </div>
                        </div>


                    </div>

                </div>

            </div>
        </div>



        @endsection
        @section('js')
            <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
            <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
            <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
            <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
            <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
@extends('landing.master')
@section('main')

<div class="tt-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{url('/')}}">خانه</a></li>/
            <li>کلمه عبور</li>
        </ul>
    </div>
</div>
<div id="tt-pageContent">
    <div class="container-indent">
        <div class="container">
            <div class="rowContainer">
                <h1 class="tt-title-subpages noborder">لیست علاقه مندیها</h1>
            </div>

            <div class="rowContainer">
                @include('landing.panel.common.sideMenu')

                <div class="customColumn pull-left-custom panel-contentCont">
                    <div class="panel rowPanel">
                        @foreach($products as $product)
                            <div class="col-sm-6" style="float: left;">
                                <a href="{{Route('showProductPage',$product->product_slug)}}" class="tt-promo-box tt-one-child">
                                    <img src="{{asset('Web/images/loader.svg')}}" data-src="{{asset($product->ProductImages()->first()->image_dir)}}" alt="{{$product->product_title}}">
                                    <div class="tt-description">
                                        <div class="tt-description-wrapper">
                                            <div class="tt-background"></div>
                                            <div class="tt-title-small">
                                                <p>{{$product->product_title}}</p>
                                                <span class="new-price">{{number_format($product->product_price - ($product->product_price * $product->discount_percent) / 100)}} تومان</span>
                                                <span class="old-price">{{number_format($product->product_price)}} تومان</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
    <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
    <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
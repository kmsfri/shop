@extends('landing.master')
@section('main')

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">خانه</a></li>/
                <li>کلمه عبور</li>
            </ul>
        </div>
    </div>



    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <div class="rowContainer">
                    <h1 class="tt-title-subpages noborder">تغییر رمز عبور</h1>
                </div>

                <div class="rowContainer">
                    @include('landing.panel.common.sideMenu')

                    <div class="customColumn pull-left-custom panel-contentCont">
                        <div class="panel rowPanel">



                            <div class="customContainer1">
                                <form action="{{Route('changePassword')}}" method="POST">
                                    {{csrf_field()}}
                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <input name="password" class="customInput1" type="password" placeholder="رمز عبور" value="">
                                            @if ($errors->has('password'))<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>@endif
                                        </div>
                                        <div class="custom-col-25">
                                            <label class="customLabel" for="password">رمز عبور</label>
                                        </div>
                                    </div>


                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <input name="repassword" class="customInput1" type="password" placeholder="تکرار رمز عبور" value="">
                                            @if ($errors->has('repassword'))<span class="help-block"><strong>{{ $errors->first('repassword') }}</strong></span>@endif
                                        </div>
                                        <div class="custom-col-25">
                                            <label class="customLabel" for="repassword">تکرار رمز عبور</label>
                                        </div>
                                    </div>


                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <input class="customSubmit1" type="submit" value="ذخیره">
                                        </div>
                                        <div class="custom-col-25">
                                        </div>

                                    </div>
                                </form>
                            </div>







                        </div>

                    </div>

                </div>

            </div>
        </div>



        @endsection
        @section('js')
            <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
            <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
            <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
            <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
            <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
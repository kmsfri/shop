@extends('landing.master')
@section('main')

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">خانه</a></li>/
                <li>پیشخوان</li>
            </ul>
        </div>
    </div>



    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <div class="rowContainer">
                    <h1 class="tt-title-subpages noborder">پیشخوان</h1>
                </div>

                <div class="rowContainer">
                    @include('landing.panel.common.sideMenu')

                    <div class="customColumn pull-left-custom panel-contentCont">
                        <div class="panel rowPanel">



                            <div class="customContainer1">

                            </div>







                        </div>

                    </div>

                </div>

            </div>
        </div>



        @endsection
        @section('js')
            <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
            <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
            <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
            <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
            <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
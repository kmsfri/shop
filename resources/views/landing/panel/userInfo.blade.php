@extends('landing.master')
@section('main')

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">خانه</a></li>/
                <li>اطلاعات کاربری</li>
            </ul>
        </div>
    </div>



    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <div class="rowContainer">
                    <h1 class="tt-title-subpages noborder">اطلاعات کاربری</h1>
                </div>

                <div class="rowContainer">
                    @include('landing.panel.common.sideMenu')

                    <div class="customColumn pull-left-custom panel-contentCont">
                        <div class="panel rowPanel">



                            <div class="customContainer1">
                                <form action="{{Route('saveUserInfo')}}" method="POST">
                                    {{csrf_field()}}
                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <input name="fullname" class="customInput1" type="text" id="fname" placeholder="نام و نام خانوادگی شما..." value="{{ old('fullname',isset($user->fullname) ? $user->fullname : '') }}">
                                            @if ($errors->has('fullname'))<span class="help-block"><strong>{{ $errors->first('fullname') }}</strong></span>@endif
                                        </div>
                                        <div class="custom-col-25">
                                            <label class="customLabel" for="fname">نام و نام خانوادگی</label>
                                        </div>
                                    </div>

                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <textarea name="address" class="customInput1" id="subject"  style="height:200px">{{ old('address',isset($user->address) ? Helpers::br2nl($user->address) : '') }}</textarea>
                                            @if ($errors->has('addres'))<span class="help-block"><strong>{{ $errors->first('addres') }}</strong></span>@endif
                                        </div>
                                        <div class="custom-col-25">
                                            <label class="customLabel" for="subject">آدرس</label>
                                        </div>
                                    </div>
                                    <div class="customRow">
                                        <div class="custom-col-75">
                                            <input class="customSubmit1" type="submit" value="ذخیره">
                                        </div>
                                        <div class="custom-col-25">
                                        </div>

                                    </div>
                                </form>
                            </div>







                        </div>

                    </div>

                </div>

            </div>
        </div>



        @endsection
        @section('js')
            <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
            <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
            <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
            <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
            <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
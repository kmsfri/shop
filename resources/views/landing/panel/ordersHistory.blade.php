@extends('landing.master')
@section('main')

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">خانه</a></li>/
                <li>سوابق خرید</li>
            </ul>
        </div>
    </div>



        <div id="tt-pageContent">
            <div class="container-indent">
                <div class="container">
                    <div class="rowContainer">
                    <h1 class="tt-title-subpages noborder">سوابق خرید</h1>
                    </div>

                    <div class="rowContainer">
                    @include('landing.panel.common.sideMenu')

                    <div class="customColumn pull-left-custom panel-contentCont">
                        <div class="panel rowPanel">
                        <table class="customTable1">
                            <tr>
                                <th>شماره سفارش</th>
                                <th>هزینه(تومان)</th>
                                <th>وضعیت پرداخت</th>
                                <th>زمان سفارش</th>
                                <th>وضعیت سفارش</th>
                                <th>عملیات</th>
                            </tr>
                            @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->paid_amount_text}}</td>
                                <td>{{$order->paid_text}}</td>
                                <td>{{$order->time_text}}</td>
                                <td>{{$order->order_status_text}}</td>
                                <td><a href="{{Route('showUserOrderDetails',$order->id)}}">جزئیات</a> </td>
                            </tr>
                            @endforeach
                        </table>
                        </div>

                    </div>

                </div>

            </div>
        </div>



@endsection
@section('js')
    <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
    <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
    <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
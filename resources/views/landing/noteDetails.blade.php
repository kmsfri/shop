@extends('landing.master')
@section('main')
    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="index.html">خانه</a></li>
                <li>عنوان</li>
            </ul>
        </div>
    </div>
    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <h1 class="tt-title-subpages noborder">{{$note->note_title}}</h1>
                <p>
                    {!! $note->note_body !!}
                </p>

            </div>
        </div>
    </div>
@endsection


@section('js')


    <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('Web/external/slick/slick.min.js')}}"></script>
    <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
    <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
    <script src="{{asset('Web/js/main.js')}}"></script>
    <!-- form validation and sending to mail -->
    <script src="{{asset('Web/external/form/jquery.form.js')}}"></script>
    <script src="{{asset('Web/external/form/jquery.validate.min.js')}}"></script>
    <script src="{{asset('Web/external/form/jquery.form-init.js')}}"></script>



@endsection
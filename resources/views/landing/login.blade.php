@extends('landing.master')
@section('main')
        
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="{{url('/')}}">خانه</a></li>/
			<li>ورود</li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="tt-title-subpages noborder">ورود</h1>
			<div class="tt-login-form">

				<div class="row justify-content-center">
					<div class="col-xs-12 col-md-6">
						<div class="tt-item">
							<h2 class="tt-title">ورود</h2>
							<div class="form-default form-top">
								<form id="customer_login" method="post" action="{{ Route('do-user-login') }}" novalidate="novalidate">
									{{csrf_field()}}
									<div class="form-group">
										<label for="loginInputName">شماره موبایل *</label><br>
										<div class="tt-required">
											@if( Session::has('data') )
												{{ Session::get('data') }}
											@endif
										</div>
										<input type="text" name="mobile" class="form-control" id="loginInputName" placeholder="شماره موبایل خود را وارد نمایید">
									</div>
									<div class="form-group">
										<label for="loginInputEmail">رمز عبور *</label>
										<input type="password" name="password" class="form-control" id="loginInputEmail" placeholder="رمز عبور خود را وارد نمایید">
									</div>
									<div class="row">
										<div class="col-auto mr-auto">
											<div class="form-group">
												<button class="btn btn-border" type="submit">ورود</button>
											</div>
										</div>
										<div class="col-auto align-self-end">
											<div class="form-group">
												<ul class="additional-links">
													<li><a href="#">رمز عبور خود را فراموش کرده اید?</a></li>
												</ul>
											</div>
										</div>
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
<script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
<script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
<script src="{{asset('Web/js/main.js')}}"></script>
@endsection
@extends('landing.master')
@section('main')
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="{{url('/')}}">خانه</a></li>
			/
			<li>لیست مطالب وبسایت</li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
	<div class="container-indent">
		<div class="container container-fluid-custom-mobile-padding">
			<h1 class="tt-title-subpages noborder">وبلاگ</h1>
			<div class="row flex-sm-row-reverse">
				<div class="col-sm-12 col-md-8 col-lg-9">
					<div class="tt-listing-post">
						@if(count($posts))
							@foreach($posts as $post)
						<div class="tt-post">
							<div class="tt-post-img">
								<a href="{{route('showsingleblog',$post->post_slug)}}"><img src="{{asset('Web/images/loader.svg')}}" data-src="@if($post->PostImages()->count() > 0) {{asset($post->PostImages()->first()->image_dir)}} @endif" alt="{{$post->post_title}}"></a>
							</div>
							<div class="tt-post-content">
								<h2 class="tt-title"><a href="{{route('showsingleblog',$post->post_slug)}}">{{$post->post_title}}</a></h2>
								<div class="tt-description">
									{{str_limit($post->short_text,200)}}
								</div>
								<div class="tt-meta">
									<div class="tt-autor">
										توسط:  <span> {{$post->Author()->first()->user_title}} </span>
										<br>
										<span>{{Helpers::convert_date_g_to_j($post->created_at,true)}}</span>
									</div>
									<div class="tt-comments">|
										<a href="#"><i class="tt-icon icon-h-11"></i>{{$post->Comments()->count()}}</a>
									</div>
								</div>
								<div class="tt-btn">
									<a href="{{route('showsingleblog',$post->post_slug)}}" class="btn">ادامه مطالب</a>
								</div>
							</div>
						</div>
							@endforeach
						
						<div class="tt-pagination">
							<!-- <a href="#" class="btn-pagination btn-prev"></a> -->
							{!! str_replace('/?', '?', $posts->render()) !!}
						</div>
							@else
							<p>مطلبی وجود ندارد</p>
						@endif
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-3 leftColumn">
					<div class="tt-block-aside">
						<h3 class="tt-aside-title">دسته بندی</h3>
						<div class="tt-aside-content">
							<ul class="tt-list-row">
								@if(count($cats))
									@foreach($cats as $cat)
								<li><a href="{{route('blogcat',$cat->category_slug)}}">{{$cat->category_title}}</a></li>
									@endforeach
								@else
								<li>دسته بندی وجود ندارد</li>
									@endif
							</ul>
						</div>
					</div>
					<div class="tt-block-aside">
						<h3 class="tt-aside-title">جستجو</h3>
						<div class="tt-aside-content">
							<form class="form-default" method="get" action="{{route('searchblog')}}">
								<div class="tt-form-search">
									<input type="text" name="search" class="form-control">
									<button type="submit" class="tt-btn-icon icon-h-04"></button>
								</div>
							</form>
						</div>
					</div>
					<div class="tt-block-aside">
						<h3 class="tt-aside-title">پست های اخیر</h3>
						<div class="tt-aside-content">
							<div class="tt-aside-post">

								@if(count($recentposts))
									@foreach($recentposts as $recentpost)
								<div class="item">
									<div class="tt-tag"><a href="{{route('showsingleblog',$recentpost->post_slug)}}">{{$recentpost->post_title}}</a></div>
									<a href="{{route('showsingleblog',$recentpost->post_slug)}}">
										<div class="tt-description">
											{{str_limit($recentpost->short_text,100)}}
										</div>
									</a>
									<div class="tt-info">
										توسط:  <span> {{$recentpost->Author()->first()->user_title}} </span>
										<br>
										<span>{{Helpers::convert_date_g_to_j($recentpost->created_at,true)}}</span>
									</div>
								</div>

									@endforeach
							@endif
							</div>
								</div>
									</a>
									
								</div>
							</div>
						</div>
					</div>
					<div class="tt-block-aside text-center">
						<h3 class="tt-aside-title">ما را در شبکه های اجتماعی دنبال کنید</h3>
						<div class="tt-aside-content">
							<ul class="tt-social-icon justify-content-center">
								<li><a class="icon-g-64" href="http://www.facebook.com/"></a></li>
								<li><a class="icon-h-58" href="http://www.facebook.com/"></a></li>
								<li><a class="icon-g-66" href="http://www.twitter.com/"></a></li>
								<li><a class="icon-g-67" href="http://www.google.com/"></a></li>
								<li><a class="icon-g-70" href="https://instagram.com/"></a></li>
							</ul>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
<script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('Web/external/slick/slick.min.js')}}"></script>
<script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
<script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
<script src="{{asset('Web/js/main.js')}}"></script>
@endsection
@extends('landing.master')
@section('main')

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">خانه</a></li>/
                <li>ثبت نام</li>
            </ul>
        </div>
    </div>
    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <h1 class="tt-title-subpages noborder">ثبت نام</h1>
                <div class="tt-login-form">
<div class="row justify-content-center">
    <div class="col-xs-12 col-md-6">
        <div class="tt-item">
            <h2 class="tt-title">ثبت نام</h2>
            <p>
            <div class="form-default form-top">
                <form id="customer_login" method="post" action="{{ Route('do-user-register') }}" novalidate="novalidate">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="loginInputName">شماره موبایل *</label><br>
                        <div class="tt-required">
                            @if( Session::has('data') )
                                {{ Session::get('data') }}
                            @endif
                        </div>
                        <input type="text" name="mobile" class="form-control" id="loginInputName" placeholder="شماره موبایل خود را وارد نمایید">
                    </div>
                    <div class="row" style="float: left">
                        <div class="col-auto mr-auto">
                            <div class="form-group">
                                <button class="btn btn-border" type="submit">ثبت نام</button>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
            </p>
        </div>
    </div>
</div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
    <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
    <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
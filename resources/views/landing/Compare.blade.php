@extends('landing.master')
@section('main')

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">خانه</a></li>/
                <li>مقایسه کالا</li>
            </ul>
        </div>
    </div>
    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <h1 class="tt-title-subpages noborder">مقایسه محصولات</h1>
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="tt-modal-quickview desctope">
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <div class="arrow-location-center slick-initialized slick-slider">
                                            <div>
                                                <div class="slick-slide slick-current slick-active">
                                                    <img src="{{asset($product1->ProductImages()->first()->image_dir)}}" data-src="" alt="{{asset($product1->ProductImages()->first()->image_dir)}}" class="loaded" data-was-processed="true">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <div class="tt-product-single-info">

                                        <h2 class="tt-title"><a href="{{route('showProductPage',$product1->product_slug)}}">{{$product1->product_title}}</a></h2>
                                        <div class="tt-price">
                                            @if($product1->discount_percent == 0)
                                                <span class="new-price">{{number_format($product1->product_price)}} تومان</span>
                                            @else
                                                <span class="new-price">{{number_format($product1->product_price - ($product1->product_price * $product1->discount_percent) / 100)}} تومان</span>
                                                <span class="old-price">{{number_format($product1->product_price)}} تومان</span>
                                            @endif
                                        </div>
                                        <div class="tt-review">
                                            <div class="tt-rating">
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star-half"></i>
                                                <i class="icon-star-empty"></i>
                                            </div>
                                        </div>
                                        <hr>
                                        <table class="tt-table-03">
                                            <tbody>

                                            @foreach($product1->properties as $pr)
                                                <tr>
                                                    <td>{{$pr['propTitle']}}:</td>
                                                    <td>{{$pr['propValue']}}</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        <hr>
                                        <div class="tt-wrapper">
                                            {{$product1->product_description}}
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="tt-modal-quickview desctope">
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <div class="arrow-location-center slick-initialized slick-slider">
                                        <div>
                                            <div class="slick-slide slick-current slick-active">
                                                <img src="{{asset($product2->ProductImages()->first()->image_dir)}}" data-src="{{asset($product2->ProductImages()->first()->image_dir)}}" alt="" class="loaded" data-was-processed="true">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <div class="tt-product-single-info">

                                        <h2 class="tt-title"><a href="{{route('showProductPage',$product2->product_slug)}}">{{$product2->product_title}}</a></h2>
                                        <div class="tt-price">
                                            @if($product2->discount_percent == 0)
                                                <span class="new-price">{{number_format($product2->product_price)}} تومان</span>
                                            @else
                                                <span class="new-price">{{number_format($product2->product_price - ($product2->product_price * $product2->discount_percent) / 100)}} تومان</span>
                                                <span class="old-price">{{number_format($product2->product_price)}} تومان</span>
                                            @endif
                                        </div>
                                        <div class="tt-review">
                                            <div class="tt-rating">
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star-half"></i>
                                                <i class="icon-star-empty"></i>
                                            </div>
                                        </div>
                                        <hr>
                                        <table class="tt-table-03">
                                            <tbody>

                                            @foreach($product2->properties as $pr)
                                                <tr>
                                                    <td>{{$pr['propTitle']}}:</td>
                                                    <td>{{$pr['propValue']}}</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        <hr>
                                        <div class="tt-wrapper">
                                            {{$product2->product_description}}
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
    <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
    <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
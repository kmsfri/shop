@extends('landing.master')
@section('main')
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="index.html">خانه</a></li>
			<li>عنوان</li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="tt-title-subpages noborder">سبد خرید</h1>
			@if(count($cartElements))
			<div class="tt-shopcart-table-02">

				<table>
					<tbody>


						@foreach($cartElements as $cE)
						<tr>
							<td>
								<div class="tt-product-img">
									<img src="{{asset($cE->Product->main_img_dir)}}" data-src="{{asset($cE->Product->main_img_dir)}}" alt="{{$cE->Product->product_title}}">
								</div>
							</td>
							<td>
								<h2 class="tt-title">
									<a href="{{Route('showProductPage',$cE->Product->product_slug)}}">{{$cE->Product->product_title}}</a>
								</h2>
								<ul class="tt-list-parameters">
									<li>
										<div class="tt-price">
											{{number_format($cE->Product->finalPrice)}} تومان
										</div>
									</li>
									<li>
										<div class="detach-quantity-mobile"></div>
									</li>
									<li>
										<div class="tt-price subtotal">
											{{number_format($cE->Product->finalPrice)}} تومان
										</div>
									</li>
								</ul>
							</td>
							<td>
								<div class="tt-price">
									{{number_format($cE->Product->finalPrice)}} تومان
								</div>
							</td>
							<td>
								<div class="detach-quantity-desctope">
									<div class="tt-input-counter style-01">
										<span class="minus-btn"></span>
										<input form="updateCartForm" name="order_count[{{$cE->id}}]" type="text" value="{{$cE->order_count}}" size="5">
										<span class="plus-btn"></span>
									</div>
								</div>
							</td>
							<td>
								<div class="tt-price subtotal">
									{{number_format($cE->Product->totalPrice)}} تومان
								</div>
							</td>
							<td>
								<a href="{{Route('removeFromCart',$cE->id)}}" class="tt-btn-close"></a>
							</td>
						</tr>
						@endforeach

					</tbody>
				</table>
				<div class="tt-shopcart-btn">
					<div class="col-left">
					</div>
					<div class="col-right">
						<form id="clearCartForm" action="{{Route('clearCart')}}" method="POST">
							{{csrf_field()}}
						</form>
						<form id="updateCartForm" action="{{Route('updateCart')}}" method="POST">
							{{csrf_field()}}
						</form>
						<a class="btn-link" href="javascript::void();" onclick="document.getElementById('clearCartForm').submit();"><i class="icon-h-02"></i>پاک کردن سبد خرید</a>
						<a class="btn-link" href="javascript::void();" onclick="document.getElementById('updateCartForm').submit();"><i class="icon-h-48"></i>به روز رسانی سبد خرید</a>
					</div>
				</div>
			</div>
			<div class="tt-shopcart-col">
				<div class="row">
					<div class="col-md-6 col-lg-4">
						<div class="tt-shopcart-box">
							<h4 class="tt-title">
								آدرس تحویل
							</h4>
							<p>آدرس را به صورت دقیق وارد کنید(استان - شهر - خیابان - کوچه - ساختمان - پلاک)</p>
							<form class="form-default">
								<div class="form-group">
									<label for="address_zip">کد پستی <sup>*</sup></label>
									<input type="text" name="postal_code" class="form-control" id="address_zip" placeholder="کد پستی خود را وارد کنید">
								</div>
								<div class="form-group">
									<label for="address_zip">آدرس <sup>*</sup></label>
									<input type="text" name="address" class="form-control" id="address" value="{{\Illuminate\Support\Facades\Auth::guard('user')->user()->address}}" placeholder="آدرس خود را وارد کنید">
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="tt-shopcart-box">
							<h4 class="tt-title">
								توضیحات سفارش
							</h4>
							<p>در صورت نیاز برای سفارش خود توضیح بنویسید</p>
							<form class="form-default">
								<textarea class="form-control"></textarea>
							</form>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="tt-shopcart-box tt-boredr-large">
							<table class="tt-shopcart-table01">
								<tbody>
									<tr>
										<th>جمع قیمتها</th>
										<td>{{number_format($sumOfPrices)}} تومان</td>
									</tr>
									<tr>
										<th>هزینه ارسال</th>
										<td>{{number_format($deliveryPrice)}} تومان</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th>قابل پرداخت</th>
										<td>{{number_format($finalPrice)}} تومان</td>
									</tr>
								</tfoot>
							</table>
							<button type="submit" class="btn btn-lg"><span class="icon icon-check_circle"></span>پرداخت</button>
						</div>
					</div>
				</div>
			</div>
			@else
				<p class="text-center">سبد خرید شما خالی است</p>
			@endif
		</div>
	</div>
</div>
@endsection


@section('js')


<script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
<script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('Web/external/slick/slick.min.js')}}"></script>
<script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
<script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
<script src="{{asset('Web/js/main.js')}}"></script>
<!-- form validation and sending to mail -->
<script src="{{asset('Web/external/form/jquery.form.js')}}"></script>
<script src="{{asset('Web/external/form/jquery.validate.min.js')}}"></script>
<script src="{{asset('Web/external/form/jquery.form-init.js')}}"></script>



@endsection
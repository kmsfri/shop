@extends('landing.master')
@section('main')
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="{{url('/')}}">خانه</a></li>
			<li>لیست محصولات</li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
	<div class="container-indent">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-lg-3 col-xl-3 leftColumn aside">
					<div class="tt-btn-col-close">
						<a href="#">بستن</a>
					</div>
					<div class="tt-collapse open tt-filter-detach-option">
						<div class="tt-collapse-content">
							<div class="filters-mobile">
								<div class="filters-row-select">

								</div>
							</div>
						</div>
					</div>

					<div class="tt-collapse open">
						<h3 class="tt-collapse-title">دسته بندی ها</h3>
						<div class="tt-collapse-content">
							<ul class="tt-list-row">
								@if(count($cats))
									@foreach($cats as $cat)
								<li><a href="{{route('productscat',$cat->category_slug)}}">{{$cat->category_title}}</a></li>
									@endforeach
								@endif
							</ul>
						</div>
					</div>
					<div class="tt-collapse open">
						<h3 class="tt-collapse-title">قیمت(از - تا)</h3>
						<div class="tt-collapse-content">
							<ul class="tt-list-row">
								<form method="get" action="{{route('productsprice')}}"
								<li>
									<div class="form-group">
										<input type="number" name="from" class="form-control" placeholder="از قیمت(تومان)">
									</div>
								</li>
								<li>
									<div class="form-group">
										<input type="number" name="to" class="form-control" placeholder="تا قیمت(تومان)">
									</div>
								</li>
								<li>
									<div class="form-group">
										<button type="submit" class="btn" style="height: 40px !important">فیلتر</button>
									</div>
								</li>
								</form>
							</ul>
						</div>
					</div>
					<div class="tt-collapse open">
						<h3 class="tt-collapse-title">برندها</h3>
						<div class="tt-collapse-content">


								@if(count($brands))
									@foreach($brands as $brand)
										<a href="{{route('productsbrand',$brand->brand_slug)}}"><img src="{{asset($brand->brand_img)}}" width="40px" height="40px"></a>
									@endforeach
								@endif

						</div>
					</div>
					<div class="tt-collapse open">
						<h3 class="tt-collapse-title">وضعیت</h3>
						<div class="tt-collapse-content">
							<ul class="tt-list-row">
								<li><a href="{{route('productsslug','discounted-time')}}">تخفیفات زمان دار</a></li>
								<li><a href="{{route('productsslug','discounted-price')}}">تخفیفات معمولی</a></li>
								<li><a href="{{route('productsslug','fixed-price')}}">قیمت مقطوع</a></li>
							</ul>

						</div>
					</div>
					<div class="tt-collapse open">
						<h3 class="tt-collapse-title">محصولات تصادفی</h3>
						<div class="tt-collapse-content">
							<div class="tt-aside">
								@if($randProducts)
									@foreach($randProducts as $randProduct)
								<a class="tt-item" href="{{route('showProductPage',$randProduct->product_slug)}}">
									<div class="tt-img">
										<span class="tt-img-default"><img src="{{asset('Web/images/loader.svg')}}" data-src="{{asset($randProduct->ProductImages()->first()->image_dir)}}" alt="{{$randProduct->product_title}}"></span>
									</div>
									<div class="tt-content">
										<h6 class="tt-title">{{$randProduct->product_title}}</h6>
										<div class="tt-price">
											@if($randProduct->discount_percent == 0)
												<span class="new-price">{{number_format($randProduct->product_price)}} تومان</span>
											@else
												<span class="sale-price">{{number_format($randProduct->product_price - ($randProduct->product_price * $randProduct->discount_percent) / 100)}} تومان</span>
												<span class="old-price">{{number_format($randProduct->product_price)}} تومان</span>
											@endif
										</div>
									</div>
								</a>
									@endforeach
								@endif
							</div>
						</div>
					</div>
					@if(isset($randpost[0]))
					<div class="tt-content-aside">
						<a href="{{route('showsingleblog',$randpost[0]->post_slug)}}" class="tt-promo-03">
							<img src="{{asset('Web/images/loader.svg')}}" data-src="@if($randpost[0]->PostImages()->count() > 0) {{asset($randpost[0]->PostImages()->first()->image_dir)}} @endif" alt="{{$randpost[0]->post_title}}">
							<div class="tt-description tt-point-v-t">
								<div class="tt-description-wrapper">
									<div class="tt-title-large">{{$randpost[0]->post_title}}</div>
									<p>
										{{$randpost[0]->short_text}}
									</p>
								</div>
							</div>
						</a>
					</div>
						@endif
				</div>
				<div class="col-md-12 col-lg-9 col-xl-9">
					<div class="content-indent container-fluid-custom-mobile-padding-02">
						<div class="tt-filters-options">
							<h1 class="tt-title">
								لیست محصولات
							</h1>
							<div class="tt-btn-toggle">
								<a href="#">فیلتر</a>
							</div>
							<div class="tt-quantity">
								<a href="#" class="tt-col-one" data-value="tt-col-one"></a>
								<a href="#" class="tt-col-two" data-value="tt-col-two"></a>
								<a href="#" class="tt-col-three" data-value="tt-col-three"></a>
								<a href="#" class="tt-col-four" data-value="tt-col-four"></a>
								<a href="#" class="tt-col-six" data-value="tt-col-six"></a>
							</div>
						</div>
						<div class="tt-product-listing row">
							<form id="orderFrm" method="POST" action="{{Route('addProductToCart')}}">
								{{csrf_field()}}
								<input type="hidden" id="prd_id" name="product_id" value="" autocomplete="off">
								<input name="order_count" type="hidden" value="1" size="5"/>
							</form>
							@if(count($products))
								@foreach($products as $product)
							<div class="col-sm-12 col-md-4 tt-col-item">
								<div class="tt-product">
									<div class="tt-image-box">
										<a href="{{route('showProductPage',$product->product_slug)}}">
											<span class="tt-img-default"><img src="{{asset('Web/images/loader.svg')}}" data-src="{{asset($product->ProductImages()->first()->image_dir)}}" alt="{{$product->product_title}}"></span>
										</a>
										@if($product->countdown_to != null && $product->countdown_to >= date('Y-m-d').'00:00:00') )
										<div class="tt-countdown_box">
											<div class="tt-countdown_inner">
												<div class="tt-countdown"
													 data-date="{{$product->countdown_to}}"
													 data-year="سال"
													 data-month="ماه"
													 data-week="هفته"
													 data-day="روز"
													 data-hour="ساعت"
													 data-minute="دقیقه"
													 data-second="ثانیه"></div>
											</div>
										</div>
											@endif
									</div>
									<div class="tt-description">
										<div class="tt-row">
											<ul class="tt-add-info">
												<li><a href="#">برند</a></li>
											</ul>
											<div class="tt-rating">
												<i class="icon-star"></i>
												<i class="icon-star"></i>
												<i class="icon-star"></i>
												<i class="icon-star-half"></i>
												<i class="icon-star-empty"></i>
											</div>
										</div>
										<h2 class="tt-title"><a href="{{route('showProductPage',$product->product_slug)}}">{{$product->product_title}}</a></h2>
										<div class="tt-price">

											@if($product->discount_percent == 0)
												<span class="new-price">{{number_format($product->product_price)}} تومان</span>
											@else
												<span class="new-price">{{number_format($product->product_price - ($product->product_price * $product->discount_percent) / 100)}} تومان</span>
												<span class="old-price">{{number_format($product->product_price)}} تومان</span>
											@endif
										</div>
										<div class="tt-product-inside-hover">
											<a onclick="addToCart(this.className)" class="{{$product->id}} tt-btn-addtocart">افزودن به سبد خرید</a>
										</div>
									</div>
								</div>
							</div>
								@endforeach
								@else
								<p>محصولی پیدا نشد.</p>
							@endif



							
						</div>
						<div class="text-center tt_product_showmore">
							<div class="tt-pagination">{!! str_replace('/?', '?', $products->render()) !!}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('js')
<script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
<script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('Web/external/slick/slick.min.js')}}"></script>
<script src="{{asset('Web/external/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
<script src="{{asset('Web/external/countdown/jquery.plugin.min.js')}}"></script>
<script src="{{asset('Web/external/countdown/jquery.countdown.min.js')}}"></script>
<script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
<script src="{{asset('Web/js/main.js')}}"></script>
<!-- form validation and sending to mail -->
<script src="{{asset('Web/external/form/jquery.form.js')}}"></script>
<script src="{{asset('Web/external/form/jquery.validate.min.js')}}"></script>
<script src="{{asset('Web/external/form/jquery.form-init.js')}}"></script>
<script>
    function addToCart(classname) {
        var res = classname.split(" ");
        $('#prd_id').val(res[0]);
        document.getElementById('orderFrm').submit();
    }
</script>

@endsection
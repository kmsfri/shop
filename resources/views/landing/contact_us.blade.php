@extends('landing.master')
@section('main')

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">خانه</a></li>/
                <li>تماس با ما</li>
            </ul>
        </div>
    </div>
    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <h1 class="tt-title-subpages noborder">تماس با ما</h1>
                <div class="tt-login-form">
                    <div class="row justify-content-center">
                        <div class="col-xs-12 col-md-8">
                            <div class="tt-item">
                                <h2 class="tt-title">فیلدهای زیر را پر کنید</h2>
                                <p>
                                <form method="POST" action="{{Route('saveContactUs')}}">
                                    {{csrf_field()}}
                                    <div class="form-default form-top">
                                        <div class="form-group">
                                            <label>شماره تماس یا ایمیل</label><br>
                                            <input type="text" name="phone_number" class="form-control" placeholder="" value="{{old('phone_number')}}">
                                            @if ($errors->has('phone_number'))<span class="help-block"><strong>{{ $errors->first('phone_number') }}</strong></span>@endif
                                        </div>
                                    </div>
                                    <div class="form-default form-top">
                                        <div class="form-group">
                                            <label>عنوان پیام</label><br>
                                            <input type="text" name="contact_title" class="form-control" placeholder="" value="{{old('contact_title')}}">
                                            @if ($errors->has('contact_title'))<span class="help-block"><strong>{{ $errors->first('contact_title') }}</strong></span>@endif
                                        </div>
                                    </div>
                                    <div class="form-default form-top">
                                        <div class="form-group">
                                            <label>متن پیام</label><br>
                                            <textarea name="contact_text" class="form-control">{{old('contact_text')}}</textarea>
                                            @if ($errors->has('contact_text'))<span class="help-block"><strong>{{ $errors->first('contact_text') }}</strong></span>@endif
                                        </div>
                                    </div>
                                    <div class="form-default form-top">
                                        <div class="form-group">
                                            <button class="btn btn-border" type="submit">ارسال</button>
                                        </div>
                                    </div>
                                </form>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
    <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
    <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
@extends('landing.master')
@section('main')

    <div class="tt-breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">خانه</a></li>/
                <li>مقایسه کالا</li>
            </ul>
        </div>
    </div>
    <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
                <h1 class="tt-title-subpages noborder">مقایسه کالا</h1>
                <div class="tt-login-form">

                    <div class="row justify-content-center">
                        <div class="col-xs-12 col-md-6">
                            <div class="tt-item">
                                <h2 class="tt-title">انتخاب محصولات جهت مقایسه</h2>
                                <div class="form-default form-top">
                                    <form id="customer_login" method="get" action="{{ Route('productscompare') }}" novalidate="novalidate">
                                        <div class="form-group">
                                            <label for="loginInputName">محصول اول</label><br>
                                            <div class="tt-required">
                                                @if( Session::has('data') )
                                                    {{ Session::get('data') }}
                                                @endif
                                            </div>
                                            <select class="form-control" name="p1" id="loginInputName">
                                                @if(count($products))
                                                    @foreach($products as $product)
                                                        <option value="{{$product->id}}">{{$product->product_title}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="loginInputEmail">محصول دوم *</label>
                                            <select class="form-control" name="p2" id="loginInputEmail">
                                                @if(count($products))
                                                    @foreach($products as $product)
                                                        <option value="{{$product->id}}">{{$product->product_title}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="row">
                                            <div class="col-auto mr-auto">
                                                <div class="form-group">
                                                    <button class="btn btn-border" type="submit">مقایسه</button>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
    <script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
    <script src="{{asset('Web/js/main.js')}}"></script>
@endsection
@extends('landing.master')
@section('main')
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="{{url('/')}}">خانه</a></li>/
			<li><a href="{{route('showblog')}}">لیست مطالب</a></li>
			<li>{{$post->post_title}}</li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
	<div class="container-indent">
		<div class="container container-fluid-custom-mobile-padding">
			<div class="row justify-content-center">
				<div class="col-xs-12 col-md-10 col-lg-8 col-md-auto">
					<div class="tt-post-single" style="text-align: right;">
						<h1 class="tt-title">
							{{$post->post_title}}
						</h1>
						<div class="tt-autor">
							توسط:  <span> {{$post->Author()->first()->user_title}} </span>
							<br>
							<span>{{Helpers::convert_date_g_to_j($post->created_at,true)}}</span>
						</div>
						<div class="tt-post-content">
							<!-- slider -->
							@if(count($post->PostImages()->get()))
							<div class="tt-slider-blog-single slick-animated-show-js">
								@foreach($post->PostImages()->get() as $img)
								<div><img src="{{asset($img->image_dir)}}" alt=""></div>
								@endforeach
							</div>

							<div class="tt-slick-row">
								<div class="item">
									<div class="tt-slick-quantity">
										<span class="account-number">1</span> / <span class="total">1</span>
									</div>
								</div>
								<div class="item">
									<div class="tt-slick-button">
										<button type="button" class="slick-arrow slick-prev">قبلی</button>
										<button type="button" class="slick-arrow slick-next">بعدی</button>
									</div>
								</div>
							</div>
						@endif
							<!-- /slider -->
							<h2 class="tt-title text-right" dir="rtl">{{$post->short_text}}</h2>
							<p class="text-right">
								{{$post->main_text}}

							</p>
							<p>
								
							</p>
						</div>
						<div class="post-meta">
							<span class="item">
								<span>تگ:</span>
								@php
								$tags = explode('-',$post->post_tags)


								@endphp
								@if(count($tags))
									@foreach($tags as $tag)
								<span><a href="#">{{$tag}}</a></span>
								,
									@endforeach
								@endif
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-indent wrapper-social-icon">
		<div class="container">
			<ul class="tt-social-icon justify-content-center">
				<li><a class="icon-g-64" href="http://www.facebook.com/"></a></li>
				<li><a class="icon-h-58" href="http://www.facebook.com/"></a></li>
				<li><a class="icon-g-66" href="http://www.twitter.com/"></a></li>
				<li><a class="icon-g-67" href="http://www.google.com/"></a></li>
				<li><a class="icon-g-70" href="https://instagram.com/"></a></li>
			</ul>
		</div>
	</div>
	<div class="container-indent">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xs-12 col-md-10 col-lg-8 col-md-auto">
					<div class="comments-single-post">
						<h6 class="tt-title-border">پست های اخیر</h6>
						<div class="tt-blog-thumb-list">
							<div class="row">
								@if(count($recentposts))
									@foreach($recentposts as $recentpost)
								<div class="col-sm-6">
									<div class="tt-blog-thumb">
										<div class="tt-img"><a href="{{route('showsingleblog',$recentpost->post_slug)}}" target="_blank"><img src="{{asset('Web/images/loader.svg')}}" data-src="@if($recentpost->PostImages()->count() > 0) {{asset($recentpost->PostImages()->first()->image_dir)}} @endif" alt=""></a></div>
										<div class="tt-title-description">
											<div class="tt-background"></div>
											<div class="tt-tag">
												<a href="#">{{$recentpost->post_title}}</a>
											</div>
												<p>{{str_limit($recentpost->short_text,100)}}</p>
										</div>
									</div>
								</div>
									@endforeach
									@else
									<p class="text-center">مطلبی وجود ندارد</p>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-indent">
		<div class="container container-fluid-custom-mobile-padding">
			<div class="row justify-content-center">
				<div class="col-xs-12 col-md-10 col-lg-8 col-md-auto">
					<div class="form-single-post">
						<hr>
						@if(count($post->Comments()->where('comment_status',1)->get()))
						@foreach($post->Comments()->where('comment_status','=',1)->withPivot('comment_text')->get() as $cm)
						<div class="tt-item">
							<div class="tt-avatar">
								<a href="#"><img src="{{asset($cm->avatar_dir)}}" alt="{{$cm->fullname}}" class="loading" data-was-processed="true"></a>
							</div>
							<div class="tt-content">
								<div class="tt-comments-info">
									<span class="username"> توسط :<span>{{$cm->fullname}}</span></span>| در تاریخ :
									<span class="time">{{Helpers::convert_date_g_to_j($cm->pivot->created_at,true)}}</span>
								</div>
								<p>
									{{$cm->pivot->comment_text}}
								</p>
							</div>
						</div>
							@endforeach
						@endif
						<hr>
						<h6 class="tt-title-border">دیدگاه شما</h6>
						<div class="form-default">
							@if(\Illuminate\Support\Facades\Auth::guard('user')->check())
								@if( Session::has('data') )
									<div class="alert">
										<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
										{{ Session::get('data') }}
									</div>
								@endif
								<form method="post" action="{{route('submit_comment',$post->id)}}">
									{{csrf_field()}}
									<div class="form-group">
										<label for="textarea" class="control-label">توضیحات *</label>
										<textarea class="form-control"  id="textarea" placeholder="توضیحات" name="comment_text" rows="8"></textarea>
									</div>
									<div class="form-group">
										<button type="submit" class="btn"> ارسال</button>
									</div>
								</form>
							@else
								<p>برای ثبت نظر باید وارد حساب کاربری خود شوید</p>
							@endif

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 @endsection


 @section('js')

<script src="{{asset('Web/external/jquery/jquery.min.js')}}"></script>
<script src="{{asset('Web/external/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('Web/external/slick/slick.min.js')}}"></script>
<script src="{{asset('Web/external/panelmenu/panelmenu.js')}}"></script>
<script src="{{asset('Web/external/lazyLoad/lazyload.min.js')}}"></script>
<script src="{{asset('Web/js/main.js')}}"></script>
@if( Session::has('data') )
	{{ Session::get('data') }}
@endif
@endsection
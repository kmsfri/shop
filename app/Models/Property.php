<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    public function propValue()
    {
        return $this->hasMany('App\Models\Property','parent_id');
    }

    public function propEnabledValues()
    {
        return $this->hasMany('App\Models\Property','parent_id')
            ->where('property_status','=',1)
            ->orderBy('property_order','ASC');
    }

    public function Prop()
    {
        return $this->belongsTo('App\Models\Property','parent_id');
    }

    public function Products()
    {
        return $this->belongsToMany('App\Models\Product','product_property_value','property_id','product_id');
    }


}

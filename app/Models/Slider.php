<?php

namespace App\Models;
use Eloquent;


class Slider extends Eloquent
{
    protected $table = 'slider';
    protected $primaryKey = 'id';


    public function Product(){
        return $this->belongsTo('\App\Models\Product','product_id');
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProductReports extends Model
{
    protected $table = 'user_product_reports';
    protected $primaryKey = 'id';


    public function Product(){
        return $this->belongsTo('App\Models\Product');
    }
}

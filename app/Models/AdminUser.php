<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminUser extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table='admin_users';

    protected $fillable = [
        'user_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Routes()
    {
        return $this->belongsToMany('App\Models\AdminRoutes','admin_route_admin_user','admin_user_id','admin_route_id');
    }

    public function CheckRouteID($route_id)
    {
        return $this->belongsToMany('App\Models\AdminRoutes','admin_route_admin_user','admin_user_id','admin_route_id')
            ->where('admin_routes.id','=',$route_id)
            ->count();
    }
    public function Post(){
        return $this->hasMany('\App\Models\Post','created_by');
    }





}

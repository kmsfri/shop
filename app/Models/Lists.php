<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lists extends Model
{
    protected $table = 'lists';
    protected $primaryKey = 'id';


    public function Products()
    {
        return $this->belongsToMany('App\Models\Product','list_product','list_id','product_id');
    }


}

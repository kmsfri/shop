<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    protected $table = 'post_comments';
    protected $primaryKey = 'id';




    public function Post(){
        return $this->belongsTo('\App\Models\Post','post_id');
    }

    public function User(){
        return $this->belongsTo('\App\Models\User','user_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBuyPropertyValue extends Model
{
    protected $table = 'user_buy_property_value';
    protected $primaryKey = 'id';


    public function UserBuy(){
        return $this->belongsTo('\App\Models\UserBuy');
    }

    public function Property(){
        return $this->belongsTo('\App\Models\Property','property_id');
    }

    public function PropertyValue(){
        return $this->belongsTo('\App\Models\Property','property_value_id');
    }
}

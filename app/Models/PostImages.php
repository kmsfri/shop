<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostImages extends Model
{
    protected $table = 'post_images';
    protected $primaryKey = 'id';

    protected $fillable = [
        'post_id','image_dir','image_order'
    ];


    public function Post(){
        return $this->belongsTo('\App\Models\Post','post_id');
    }
}

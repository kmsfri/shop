<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = 'product_discounts';
    protected $primaryKey = 'id';
}

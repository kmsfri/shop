<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'users';
    protected $primaryKey = 'id';


    public function Ratings(){
        return $this->belongsToMany('\App\Models\Product','user_product_ratings','user_id','product_id');
    }

    public function Comments(){
        return $this->belongsToMany('\App\Models\Product','user_product_comments','user_id','product_id');
    }
    public function PostComments(){
        return $this->belongsToMany('\App\Models\Post','post_comments','user_id','post_id');
    }

    public function ProductWish(){
        return $this->belongsToMany('\App\Models\Product','user_product_wish','user_id','product_id');
    }


    public function Boughts()
    {
        return $this->hasMany('App\Models\UserBuy','user_id');
    }



    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function Payments(){
        return $this->hasMany('App\Models\OrderPayment','user_id');
    }

    public function Orders(){
        return $this->hasMany('App\Models\Order','user_id');
    }


    public function OrderProducts(){
        return $this->hasMany('App\Models\OrderProduct','user_id');
    }

}

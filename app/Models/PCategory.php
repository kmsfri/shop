<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PCategory extends Model
{
    protected $table = 'pcategories';
    protected $primaryKey = 'id';

    public function SubCategory()
    {
        return $this->hasMany('App\Models\PCategory','parent_id');
    }

    public function SubCategoryEnabledOrdered()
    {
        return $this->hasMany('App\Models\PCategory','parent_id')
            ->where('category_status','=',1)
            ->orderBy('category_order','ASC');
    }

    public function ParentCategory()
    {
        return $this->belongsTo('App\Models\PCategory','parent_id');
    }


    public function Posts()
    {
        return $this->belongsToMany('App\Models\Post','post_category','pcategory_id','post_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'id';


    public function User(){
        return $this->belongsTo('\App\Models\User');
    }

    /*
    public function Product(){
        return $this->belongsTo('\App\Models\Product');
    }

    public function shortProduct(){
        return $this->Product()->selectRaw('id,product_title,main_img_dir');
    }
    */

    public function Payments(){
        return $this->hasMany('\App\Models\OrderPayment','order_payments','order_id');
    }

    public function Products(){
        return $this->hasMany('\App\Models\OrderProduct','order_id');
    }

    public function Discount(){
        return $this->belongsTo('\App\Models\Discount','product_discount_id');
    }


    /*
    public function Property(){
        return $this->belongsToMany('\App\Models\Property','user_buy_property_value','user_buy_id','property_id');
    }

    public function PropertyValue(){
        return $this->belongsToMany('\App\Models\Property','user_buy_property_value','user_advertise_request_id','property_value_id');
    }

    public function AllPropertyValues(){
        return $this->hasMany('\App\Models\UserBuyPropertyValue','user_buy_id');
    }
    */
    
    

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{
    protected $table = 'user_product_comments';
    protected $primaryKey = 'id';




    public function Product(){
        return $this->belongsTo('\App\Models\Product','product_id');
    }

    public function User(){
        return $this->belongsTo('\App\Models\User','user_id');
    }
}

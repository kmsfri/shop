<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    protected $table = 'order_payments';
    protected $primaryKey = 'id';

    protected $dates = ['pay_time'];


    public function Order(){
        return $this->belongsTo('\App\Models\Order','order_id');
    }

    public function User(){
        return $this->belongsTo('\App\Models\User','user_id');
    }
}

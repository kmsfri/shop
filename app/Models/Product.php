<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';

    public function Categories(){
        return $this->belongsToMany('\App\Models\Category','product_category','product_id','category_id');
    }

    public function ProductImages(){
        return $this->hasMany('\App\Models\ProductImages');
    }

    public function OneProductImage(){
        return $this->hasOne('\App\Models\ProductImages');
    }


    public function Ratings(){
        return $this->belongsToMany('\App\Models\User','user_product_ratings','product_id','user_id')->withPivot('score_value','user_id')->withTimestamps();;
    }

    public function Comments(){
        return $this->belongsToMany('\App\Models\User','user_product_comments','product_id','user_id');
    }

    public function Property()
    {
        return $this->belongsToMany('App\Models\Property','product_property_value','product_id','property_id');
    }

    public function specProperty($prop_id)
    {
        return $this->belongsToMany('App\Models\Property','product_property_value', 'product_id','property_id')
            ->where('parent_id','=',$prop_id)->first();
    }

    public function IsWished(){
        return $this->belongsToMany('\App\Models\User','user_product_wish','product_id','user_id');
    }

    public function Lists()
    {
        return $this->belongsToMany('App\Models\Lists','list_product','product_id','list_id');
    }

    public function Solds()
    {
        return $this->hasMany('App\Models\UserBuy','product_id')
                ->where('paid',1);
    }

    public function Reports(){
        return $this->hasMany('\App\Models\UserProductReport');
    }
    public function related_products($cat_id,$pro_id){
        return $products = Product::whereHas('Categories', function ($q) use ($cat_id) {
            $q->whereIn('category_id', $cat_id);
        })->where('product_status',1)->where('id','!=',$pro_id)->take(4)->get();
    }


    public function selectableProperties(){
        return $this->Property()->where('select_status',1);
    }
    public function fillableProperties(){
        return $this->Property()->where('select_status',1);
    }


}

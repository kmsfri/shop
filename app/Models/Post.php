<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $primaryKey = 'id';

    public function Categories(){
        return $this->belongsToMany('\App\Models\PCategory','post_category','post_id','pcategory_id');
    }

    public function PostImages(){
        return $this->hasMany('\App\Models\PostImages','post_id');
    }
    public function Comments(){
        return $this->belongsToMany('\App\Models\User','post_comments','post_id','user_id')->withTimestamps();;
    }
    public function Author(){
        return $this->belongsTo('\App\Models\AdminUser','created_by');
    }
}

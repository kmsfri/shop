<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    protected $primaryKey = 'id';


    public function Products()
    {
        return $this->hasMany('App\Models\Product','brand_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $table = 'configuration';
    protected $primaryKey = 'id';


    public function MasterTitle(){
        return $this->where('config_key','=','master_title');
    }

    public function WebsiteLogo(){
        return $this->where('config_key','=','website_logo');
    }

    public function FooterAboutUs(){
        return $this->where('config_key','=','footer_about_us');
    }

    public function footerSupportText(){
        return $this->where('config_key','=','footerSupport_text');
    }

    public function FooterContactTitle(){
        return $this->where('config_key','=','footer_contact_us_title');
    }

    public function DeliveryPrice(){
        return $this->where('config_key','=','delivery_price');
    }

    public function WebsiteDescription(){
        return $this->where('config_key','=','website_description');
    }

    public function WebsiteKeywords(){
        return $this->where('config_key','=','website_keywords');
    }





}

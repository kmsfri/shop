<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'id';

    public function SubCategory()
    {
        return $this->hasMany('App\Models\Category','parent_id');
    }

    public function SubCategoryEnabledOrdered()
    {
        return $this->hasMany('App\Models\Category','parent_id')
            ->where('category_status','=',1)
            ->orderBy('category_order','ASC');
    }

    public function ParentCategory()
    {
        return $this->belongsTo('App\Models\Category','parent_id');
    }


    public function Products()
    {
        return $this->belongsToMany('App\Models\Product','product_category','category_id','product_id');
    }



    public function Property()
    {
        return $this->belongsToMany('App\Models\Property','property_category','category_id','property_id');
    }



}

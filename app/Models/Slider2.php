<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider2 extends Model
{
    protected $table = 'slider2';
    protected $primaryKey = 'id';


    public function Product(){
        return $this->belongsTo('\App\Models\Product','product_id');
    }
}

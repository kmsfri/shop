<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'order_products';
    protected $primaryKey = 'id';


    public function Order(){
        return $this->belongsTo('\App\Models\Order','order_id');
    }

    public function Product(){
        return $this->belongsTo('\App\Models\Product','product_id');
    }


    public function Property(){
        return $this->belongsToMany('\App\Models\Property','order_product_property_value','order_product_id','property_id');
    }

    public function PropertyValue(){
        return $this->belongsToMany('\App\Models\Property','order_product_property_value','order_product_id','property_value_id');
    }


}

<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class checkUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::guard('api')->user()->user_status==0){
            return response()->json(['result' => ['banned_user']])->setStatusCode(200);
        }

        return $next($request);
    }
}

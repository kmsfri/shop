<?php

namespace App\Http\Middleware;

use Closure;

class initShopCommonData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $now_datetime= \Carbon\Carbon::now()->toDateTimeString();
        $now_datetime_arr=explode(' ',$now_datetime);

        $commonData['nowTime']=$now_datetime_arr[1];

        $t_date=\Helpers::convert_date_g_to_j($now_datetime_arr[0],true);
        $t_date=explode('/',$t_date);
        $month=\Helpers::get_equal_str_month($t_date[1]);
        $commonData['nowDate']=$t_date[2].' '.$month.' '.$t_date[0];


        \View::share( $commonData);

        return $next($request);
    }
}

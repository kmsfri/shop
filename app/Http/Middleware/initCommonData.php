<?php

namespace App\Http\Middleware;

use App\Models\Configuration;
use Closure;

class initCommonData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $menus=\App\Models\Menu::where('c_status',1)
                                ->where('menu_location',1)
                                ->where('parent_id',null)
                                ->orderBy('c_order','ASC')
                                ->with('subMenu')
                                ->get();

        $footerLinks=\App\Models\Menu::where('c_status',1)
            ->where('menu_location',2)
            ->where('parent_id',null)
            ->orderBy('c_order','ASC')
            ->with('subMenu')
            ->get();

        $master_title=\App\Models\Configuration::first()->MasterTitle()->first()->value1;
        $master_website_logo=\App\Models\Configuration::first()->WebsiteLogo()->first()->value1;
        $footer_about_us=\App\Models\Configuration::first()->FooterAboutUs()->first()->value1;
        $footer_support_text=\App\Models\Configuration::first()->footerSupportText()->first()->value1;
        $footer_contact_us_title=\App\Models\Configuration::first()->FooterContactTitle()->first()->value1;
        $website_description=\App\Models\Configuration::first()->WebsiteDescription()->first()->value1;
        $website_keywords=\App\Models\Configuration::first()->WebsiteKeywords()->first()->value1;

        //begin set seo tags
        $meta=(object)[
            'title'=>Null,
            'keywords'=>$website_keywords,
            'description'=>$website_description,
        ];
        //$canonical_url=urldecode(url(Route('showvilla',$villa->villa_slug)));
        $canonical_url=urldecode(\URL::full());
        $openGraph=(object)[
            'locale'=>'fa_IR',
            'type'=>'website',
            'title'=>Null,
            'description'=>$website_description,
            'url'=>urldecode(\URL::full()),
            'site_name'=>$master_title,
        ];
        /*
        if($villa->VillaImages()->count()>0){
            $openGraph->image=asset('tmp').'/'.$villa->VillaImages()->first()->image_dir;
        }
        */
        //end set seo tags


        \View::Share([
            'menus'=>$menus,
            'footerLinks'=>$footerLinks,
            'masterData'=>[
                'title'=>$master_title,
                'website_logo'=>$master_website_logo,
                'footer_abount_us'=>$footer_about_us,
                'footer_support_text'=>$footer_support_text,
                'footer_contact_us_title'=>$footer_contact_us_title,
            ],
            'meta'=>$meta,
            'openGraph'=>$openGraph,
            'canonical_url'=>$canonical_url,


        ]);

        $request->meta=$meta;
        $request->openGraph=$openGraph;
        $request->canonical_url=$canonical_url;
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //product_rate
        '/user/rate/product/*',

        //end_product_rate


        'api/verifyCodeRequest',
        'api/authenticate',
        'api/categories',
        'api/logout',
        'api/shops',
        'api/shop',
        'api/shop/rating/set',
        'api/shop/comment/set',
        'api/shop/comment/get',
        'api/advertises',
        'api/advertise',
        'api/advertise/order',
        'api/profileInfo/checkCompletion',
        'api/profileInfo/completion',
        'api/profileInfo/get',
        'api/advertise/rating/set',
        'api/advertise/comment/set',
        'api/advertise/comment/get',
        'api/advertise/wish/set',
        'api/advertise/wish/get',
        'api/shop/wish/set',
        'api/shop/wish/get',
        'api/tickets/get',
        'api/ticket',
        'api/ticket/save',
        'api/cart/get',
        'api/cart/removeItem',
        'api/cart/bill/get',
        'api/slider/get',
        'api/slider2/get',
        'api/lists/dynamic/get',
        'api/report/advertise',
        'api/report/shop',
        'api/ticket/seen',
        'api/ticket/unseen/count',
        'api/app/android/latest',
        'api/cart/bill/checkout',
        'api/cart/bill/verify',
        'api/bills/history',
        'api/bill/details',
        'api/cart/get/count'




    ];
}

<?php

namespace App\Http\Controllers\Landing\Panel;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Helpers;
use Validator;

class OrderController extends Controller
{
    public function showOrderHistory(){

        $user=Auth::guard('user')->user();
        $orders=$user->Orders()
                    ->where('paid',1)
                    ->orderBy('created_at','DESC')
                    ->get();

        $orderStatus=[
            0=>'جدید',
            1=>'بررسی شده',
            2=>'انجام شده',
            3=>'ارسال شده',
            4=>'ناموفق',
        ];
        foreach ($orders as $order){
            $order->order_status_text=$orderStatus[$order->order_status];

            $order->paid_text=($order->paid==1)?'پرداخت شده':'ناموفق';
            $order->time_text=Helpers::convert_date_g_to_j($order->created_at,true).' - '.$order->created_at->format('H:i:s');

            $order->paid_amount_text=number_format($order->paid_amount).' تومان';


        }


        $data=[
            'activeTab'=>3,
            'orders'=>$orders,
        ];

        return view('landing.panel.ordersHistory',$data);


    }


    public function showOrderDetails(Request $request){

        if(!isset($request->order_id))abort(404);

        $user=Auth::Guard('user')->user();
        $order=$user->Orders()
                    ->with('Products')
                    ->findOrFail($request->order_id);


        $orderStatus=[
            0=>'جدید',
            1=>'بررسی شده',
            2=>'انجام شده',
            3=>'ارسال شده',
            4=>'ناموفق',
        ];


        $order->order_status_text=$orderStatus[$order->order_status];

        $order->paid_amount_text=number_format($order->paid_amount).' تومان';

        $order->delivery_price_text=number_format($order->delivery_price).' تومان';

        if($order->discount_amount!=Null)
            $order->discount_amount_text=number_format($order->discount_amount).' تومان';
        else{
            $order->discount_amount_text='-';
        }

        if($order->VAT!=Null)
            $order->VAT_text=number_format($order->VAT).' تومان';
        else{
            $order->VAT_text='-';
        }


        $data=[
            'activeTab'=>3,
            'order'=>$order,
        ];

        return view('landing.panel.orderDetails',$data);


    }



    public function showPayments(){

        $user=Auth::guard('user')->user();
        $payments=$user->Payments()
            ->where('paid_status',1)
            ->orderBy('pay_time','DESC')
            ->orderBy('created_at','DESC')
            ->get();

        foreach ($payments as $payment){
            $payment->time_text=Helpers::convert_date_g_to_j($payment->pay_time,true).' - '.$payment->pay_time->format('H:i:s');
            $payment->paid_status_text=($payment->paid_status==1)?'پرداخت شده':'ناموفق';
            $payment->paid_amount_text=number_format($payment->total_paid_amount).' تومان';
        }

        $data=[
            'activeTab'=>4,
            'payments'=>$payments,
        ];

        return view('landing.panel.paymentsList',$data);
    }








    public function addToCart(Request $request){
        $property_id=array();
        if(isset($request->selectables) && $request->selectables!=Null && count($request->selectables)>0){
            foreach($request->selectables as $key=>$sl){
                $property_id[]=$key;
                $property_id[]=$sl;
            }
        }

        if(isset($request->fillables) && $request->fillables!=Null && count($request->fillables)>0){
            foreach($request->fillables as $key=>$sl){
                $property_id[]=$key;
            }
        }


        $newRequest = new \Illuminate\Http\Request();

        $newRequest->replace([
            'product_id' => $request->product_id,
            'order_count' => $request->order_count,
            'properties'=>$property_id,
        ]);



        $validator=Validator::make($newRequest->all(),[
            'product_id'=>'required|exists:products,id',
            'order_count'=>'required|integer',
            'properties.*'=>'nullable|integer|exists:properties,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->with('messages',['ورودی های خود را بررسی کنید']);
        }

        $product=Product::findOrFail($newRequest->product_id);


        if($product->in_stock_count<$newRequest->order_count || $product->product_status==0){
            return redirect()->back()
                ->with('messages',['تعداد انتخاب شده بیشتر از موجودی فروشگاه است.']);
        }


        $user = Auth::guard('user')->user();
        $orderProduct= new \App\Models\OrderProduct();
        $orderProduct->user_id=$user->id;
        $orderProduct->product_id=$newRequest->product_id;
        $orderProduct->order_count=$newRequest->order_count;
        $orderProduct->save();



        if(isset($request->selectables) && $request->selectables!=Null && count($request->selectables)>0){
            $properties=array();
            foreach($request->selectables as $key=>$sl){
                $properties[]=['property_id'=>$key,'property_value_id'=>$sl];
            }
            $orderProduct->Property()->attach($properties);
        }


        if(isset($request->fillables) && $request->fillables!=Null && count($request->fillables)>0){
            $properties=array();
            foreach($request->fillables as $key=>$sl){
                $properties[]=['property_id'=>$key,'property_value_text'=>$sl];
            }
            $orderProduct->Property()->attach($properties);
        }



        return redirect(Route('showCart'))
            ->with('messages',['محصول مورد نظر با موفقیت به سبد خرید افزوده شد']);
    }



    public function showCart(){


        $user = Auth::guard('user')->user();

        $cartElements=$user->OrderProducts()
            ->with('Product')
            ->where('paid',0)->get();



        $sumOfPrices=0;
        foreach($cartElements as $uAR){
            $uAR->Product->finalPrice=$uAR->Product->product_price;
            if($uAR->Product->discount_percent!=0 && $uAR->Product->discount_percent!=Null){
                $tempPrice=($uAR->Product->product_price/100)*$uAR->Product->discount_percent;
                $uAR->Product->finalPrice=$uAR->Product->product_price-$tempPrice;
            }
            $uAR->Product->totalPrice=$uAR->Product->finalPrice*$uAR->order_count;
            $sumOfPrices+=$uAR->Product->totalPrice;

        }


        $deliveryPrice=10000; //read from config - delivery price
        $finalPrice=$sumOfPrices+$deliveryPrice;

        $data=[
            'cartElements'=>$cartElements,
            'sumOfPrices'=>$sumOfPrices,
            'finalPrice'=>$finalPrice,
            'deliveryPrice'=>$deliveryPrice,
        ];


        return view('landing.cart',$data);
    }




    public function updateCart(Request $request){

        $user=Auth::guard('user')->user();

        $messages=[];
        foreach ($request->order_count as $key=>$oc){
            $orderProduct=$user->OrderProducts()->findOrFail($key);
            if($orderProduct->Product->in_stock_count<$oc){
                $messages[]='تعداد محصول انتخاب شده('.$orderProduct->Product->product_title.') بیشتر از موجودی می باشد';
            }else{
                $orderProduct->order_count=$oc;
                $orderProduct->save();
            }

        }

        $messages[]='سبد خرید شما با موفقیت بروزرسانی شد';
        return back()->with('messages',$messages);


    }


    public function clearCart(Request $request){

        $user=Auth::guard('user')->user();

        $orderProduct=$user->OrderProducts()->delete();

        $messages[]='سبد خرید شما با موفقیت بروزرسانی شد';
        return back()->with('messages',$messages);


    }

    public function removeFromCart(Request $request){



        if(empty($request->cartItemId)) abort(404);


        $user=Auth::guard('user')->user();

        $user->OrderProducts()->findOrFail($request->cartItemId)->delete();

        $messages[]='سبد خرید شما با موفقیت بروزرسانی شد';
        return back()->with('messages',$messages);



    }



}

<?php

namespace App\Http\Controllers\Landing\Panel;

use App\Models\OrderPayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralController extends Controller
{
    public function showDashboard(){

        $data=[
            'activeTab'=>1,
        ];

        return view('landing.panel.dashboard',$data);
    }


    public function showUserInfoForm(){

        $user=\Auth::guard('user')->user();



        $data=[
            'user'=>$user,
            'activeTab'=>2,
        ];

        return view('landing.panel.userInfo',$data);
    }


    public function saveUserInfo(Request $request){

        $validator = \Validator::make($request->all(),[
            'fullname'=>'required|min:2|max:140',
            'address' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>['لطفا ورودیهای خود را بررسی کنید']]);;
        }

        $user=\Auth::guard('user')->user();
        $user->fullname=$request->fullname;
        $user->address=nl2br($request->address);
        $user->save();

        $messages=['ااطلاعات کاربری شما با موفقیت ویرایش شد'];

        return redirect(Route('showUserInfoForm'))->with('messages', $messages);
    }



    public function showChangePasswordForm(){


        $data=[
            'activeTab'=>5,
        ];

        return view('landing.panel.changePasword',$data);
    }


    public function changePassword(Request $request){

        $validator = \Validator::make($request->all(),[
            'password'=>'required|max:40',
            'repassword' => 'required|max:40',
        ]);

        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>['لطفا ورودیهای خود را بررسی کنید']]);;
        }


        if ($request->password!=$request->repassword) {
            return back()->with(['messages'=>['رمز و تکرار آن با یکدیگر برابر نیستند']]);;
        }

        $user=\Auth::guard('user')->user();
        $user->password=bcrypt($request->password);
        $user->save();

        $messages=['رمز عبور شما با موفقیت تغییر یافت'];

        return redirect(Route('showChangePasswordForm'))->with('messages', $messages);
    }



    public function getWishProductList(){


        $user=\Auth::guard('user')->user();

        $products=$user->ProductWish()
                        ->where('product_status',1)
                        ->get();


        $data=[
            'activeTab'=>6,
            'products'=>$products,
        ];

        return view('landing.panel.productWishList',$data);
    }




}

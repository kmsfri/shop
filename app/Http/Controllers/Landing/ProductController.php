<?php

namespace App\Http\Controllers\Landing;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Validator;
use Auth;
use App\Models\Brand;

class ProductController extends Controller
{
    public function showProductPage(Request $request){

        if(!isset($request->product_slug))abort(404);
        $randpost = Post::where('post_status',1)->inRandomOrder()->take(1)->get();
        $randProducts=Product::where('product_status',1)
            ->inRandomOrder()->take(3)->get();
        $product=Product::where('product_slug',$request->product_slug)
                        ->where('product_status',1)
                        ->with('ProductImages')
                        ->with('Comments')
                        ->with('Categories')
                        ->firstOrFail();

        $properties=array();
        foreach($product->Property()->where('select_status',0)->get() as $ap){ //unselectable by buyer

            $properties[]=[
                'propTitle'=>$ap->Prop()->first()->property_title,
                'propValue'=>$ap->property_title,
            ];
        }
        $cat_id = $product->Categories()->get();
        $related_products = (new \App\Models\Product)->related_products($cat_id->pluck('id')->toArray(),$product->id);

        $product->properties=$properties;




        $properties=array();
        foreach($product->Property()->where('select_status',1)->get() as $ap){ //selectable by buyer

            $propValues=$ap->propEnabledValues()
                ->select('id','property_title')
                ->get();

            $propValuesArray=array();
            foreach ($propValues as $prVal){
                $propValuesArray[]=[
                    'property_id'=>$prVal->id,
                    'property_title'=>$prVal->property_title,
                ];
            }
            $properties[]=[
                'propID'=>$ap->id,
                'propTitle'=>$ap->property_title,
                'propValue'=>$propValuesArray,
            ];
        }
        $product->selectables=$properties;

        $properties=array();
        foreach($product->Property()->where('select_status',2)->get() as $ap){ //fillable by buyer
            $properties[]=[
                'propID'=>$ap->id,
                'propTitle'=>$ap->property_title,
            ];
        }
        $product->fillables=$properties;


        $product->finalPrice=$product->product_price;
        if($product->discount_percent!=0 && $product->discount_percent!=Null){
            $tempPrice=($product->product_price/100)*$product->discount_percent;
            $product->finalPrice=$product->product_price-$tempPrice;
        }



        //begin set seo tags
        $meta=$request->meta;
        $meta->title=$product->product_title;
        $meta->description=$product->product_description;

        $canonical_url=urldecode(url(Route('showProductPage',$product->product_slug)));

        $openGraph=$request->openGraph;
        $openGraph->title=$product->product_title;
        $openGraph->description=$product->product_description;
        if($product->ProductImages()->count()>0){
            $openGraph->image=url($product->ProductImages()->first()->image_dir);
        }
        //end set seo tags


        return view('landing.product',[
            'product'=>$product,
            'randProducts'=>$randProducts,
            'randpost'=>$randpost,
            'related_products'=>$related_products,

            'meta'=>$meta,
            'openGraph'=>$openGraph,
            'canonical_url'=>$canonical_url,
        ]);
    }
    public function products(){
        $randpost = Post::where('post_status',1)->inRandomOrder()->take(1)->get();
        $randProducts=Product::where('product_status',1)
            ->inRandomOrder()->take(3)->get();
        $products = Product::where('product_status',1)
            ->orderBy('created_at','DESC')
            ->paginate(12);
        $cats = Category::where('category_status',1)
            ->orderBy('category_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();
        $brands = Brand::where('brand_status',1)
            ->orderBy('brand_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();

        return view('landing.list-product',['products'=>$products,
            'cats'=>$cats,
            'randProducts'=>$randProducts,
            'randpost'=>$randpost,
            'brands'=>$brands]);
    }
    public function productssearch(Request $request){
        $randpost = Post::where('post_status',1)->inRandomOrder()->take(1)->get();
        $randProducts=Product::where('product_status',1)
            ->inRandomOrder()->take(3)->get();
        $cats = Category::where('category_status',1)
            ->orderBy('category_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();
        $brands = Brand::where('brand_status',1)
            ->orderBy('brand_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();


        if ($request->brand_id == 0){
            $key='%'.$request->search.'%';
            $products=Product::orWhere('product_title','like',$key)
                    ->orWhere('product_description','like',$key)
                    ->orWhere('product_slug','like',$key)
                    ->where('product_status',1)
                    ->orderby('created_at','DESC')->paginate(12);
            return view('landing.list-product',['products'=>$products,
                'cats'=>$cats,
                'randProducts'=>$randProducts,
                'randpost'=>$randpost,
                'brands'=>$brands]);
        }
        elseif($request->brand_id > 0 ){
            $key='%'.$request->search.'%';
            $brand=Brand::findorfail($request->brand_id);
            $products = $brand->Products()
                ->where('product_title','like',$key)
                ->orWhere('product_description','like',$key)
                ->orWhere('product_slug','like',$key)
                ->where('product_status',1)
                ->orderby('created_at','DESC')->paginate(12);
            return view('landing.list-product',['products'=>$products,
                'cats'=>$cats,
                'randProducts'=>$randProducts,
                'randpost'=>$randpost,
                'brands'=>$brands]);
        }
        else{
            abort(404);
        }

    }
    public function productscat($category_slug){

        $cat = Category::where('category_slug',$category_slug)->first();
        if (!isset($cat))
            abort(404);
        $randpost = Post::where('post_status',1)->inRandomOrder()->take(1)->get();
        $randProducts=Product::where('product_status',1)
            ->inRandomOrder()->take(3)->get();
        $products = $cat->Products()
            ->where('product_status',1)
            ->orderBy('created_at','DESC')
            ->paginate(12);
        $cats = Category::where('category_status',1)
            ->orderBy('category_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();
        $brands = Brand::where('brand_status',1)
            ->orderBy('brand_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();

        return view('landing.list-product',['products'=>$products,
            'cats'=>$cats,
            'randProducts'=>$randProducts,
            'randpost'=>$randpost,
            'brands'=>$brands]);
    }
    public function productsbrand($category_slug){
        $cat = Brand::where('brand_slug',$category_slug)->first();
        if (!isset($cat))
            abort(404);
        $randpost = Post::where('post_status',1)->inRandomOrder()->take(1)->get();
        $randProducts=Product::where('product_status',1)
            ->inRandomOrder()->take(3)->get();
        $products = Product::where('brand_id',$cat->id)
            ->where('product_status',1)
            ->orderBy('created_at','DESC')
            ->paginate(12);
        $cats = Category::where('category_status',1)
            ->orderBy('category_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();
        $brands = Brand::where('brand_status',1)
            ->orderBy('brand_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();
        return view('landing.list-product',['products'=>$products,
            'cats'=>$cats,
            'randProducts'=>$randProducts,
            'randpost'=>$randpost,
            'brands'=>$brands]);
    }
    public function productsprice(Request $request){
        $randpost = Post::where('post_status',1)->inRandomOrder()->take(1)->get();
        $randProducts=Product::where('product_status',1)
            ->inRandomOrder()->take(3)->get();
        $products = Product::where('product_price','>=',$request->from)
            ->where('product_price','<=',$request->to)
            ->where('product_status',1)
            ->orderBy('created_at','DESC')
            ->paginate(12);
        $cats = Category::where('category_status',1)
            ->orderBy('category_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();
        $brands = Brand::where('brand_status',1)
            ->orderBy('brand_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();
        return view('landing.list-product',['products'=>$products,
            'cats'=>$cats,
            'randProducts'=>$randProducts,
            'randpost'=>$randpost,
            'brands'=>$brands]);
    }
    public function productsslug($slug){
        $randpost = Post::where('post_status',1)->inRandomOrder()->take(1)->get();
        $randProducts=Product::where('product_status',1)
            ->inRandomOrder()->take(3)->get();
        if ($slug == "discounted-time"){
            $products = Product::where('countdown_to','>',date('Y-m-d').' 00:00:00')
                ->where('product_status',1)
                ->orderBy('created_at','DESC')
                ->paginate(12);
            $cats = Category::where('category_status',1)
                ->orderBy('category_order','ASC')
                ->orderBy('created_at','DESC')
                ->get();
            $brands = Brand::where('brand_status',1)
                ->orderBy('brand_order','ASC')
                ->orderBy('created_at','DESC')
                ->get();
            return view('landing.list-product',['products'=>$products,
                'cats'=>$cats,
            'randProducts'=>$randProducts,
            'randpost'=>$randpost,
            'brands'=>$brands]);
        }
        elseif ($slug == "discounted-price"){
            $products = Product::where('discount_percent','>',0)
                ->where('product_status',1)
                ->orderBy('created_at','DESC')
                ->paginate(12);
            $cats = Category::where('category_status',1)
                ->orderBy('category_order','ASC')
                ->orderBy('created_at','DESC')
                ->get();
            $brands = Brand::where('brand_status',1)
                ->orderBy('brand_order','ASC')
                ->orderBy('created_at','DESC')
                ->get();
            return view('landing.list-product',['products'=>$products,
                'cats'=>$cats,
            'randProducts'=>$randProducts,
            'randpost'=>$randpost,
            'brands'=>$brands]);
        }
        elseif ($slug == "fixed-price"){
            $products = Product::where('discount_percent',0)
                ->where('product_status',1)
                ->orderBy('created_at','DESC')
                ->paginate(12);
            $cats = Category::where('category_status',1)
                ->orderBy('category_order','ASC')
                ->orderBy('created_at','DESC')
                ->get();
            $brands = Brand::where('brand_status',1)
                ->orderBy('brand_order','ASC')
                ->orderBy('created_at','DESC')
                ->get();
            return view('landing.list-product',['products'=>$products,
                'cats'=>$cats,
            'randProducts'=>$randProducts,
            'randpost'=>$randpost,
            'brands'=>$brands]);
        }
        else{
            abort(404);
        }
    }
    public function submit_comment(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'comment_text' => 'required|max:255',

            ]
        );

        if ($validator->fails()) {
            validator_fails:
            return redirect()->back()
                ->withInput($request->input())
                ->withErrors($validator->errors());
        }

        if (Auth::guard('user')->check()) {
            $attach_data[$id] = [
                'product_id' => $id,
                'user_id' => Auth::guard('user')->user()->id,
                'comment_text' => $request->comment_text

            ];
            $product = Product::findorfail($id);
            $product->Comments()->attach($attach_data);
            return redirect()->back()
                ->with('data', 'نظر شما با موفقیت ثبت شد، بعد از تایید مدیر نمایش داده می شود.');
        }
        else{
            abort(404);
        }
    }
    public function gotocompare(){
        $products = Product::where('product_status',1)->get();
        return view('landing.gotocompare',[
            'products'=>$products
        ]);
    }
    public function productscompare(Request $request){
        $product1=Product::where('id',$request->p1)
            ->where('product_status',1)->firstOrFail();
        $properties=array();
        foreach($product1->Property()->where('select_status',0)->get() as $ap){ //unselectable by buyer

            $properties[]=[
                'propTitle'=>$ap->Prop()->first()->property_title,
                'propValue'=>$ap->property_title,
            ];
        }
        $product1->properties=$properties;
        $product2=Product::where('id',$request->p2)
            ->where('product_status',1)->firstOrFail();
        $properties=array();
        foreach($product2->Property()->where('select_status',0)->get() as $ap){ //unselectable by buyer

            $properties[]=[
                'propTitle'=>$ap->Prop()->first()->property_title,
                'propValue'=>$ap->property_title,
            ];
        }
        $product2->properties=$properties;
        return view('landing.Compare',[
            'product1'=>$product1,
            'product2'=>$product2
        ]);
    }



}

<?php

namespace App\Http\Controllers\Landing;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Brand;
use Validator;
use Auth;
class GeneralController extends Controller
{
    public function showMainPage(){
        $brands = Brand::where('brand_status',1)->orderBy('brand_order','ASC')->orderBy('created_at','DESC')->get();
        $randProducts1=\App\Models\Product::where('product_status',1)
                                    ->withCount('selectableProperties')
                                    ->withCount('fillableProperties')
                                    ->inRandomOrder()->take(6)->get();


        $randProducts2=\App\Models\Product::where('product_status',1)
                                            ->withCount('selectableProperties')
                                            ->withCount('fillableProperties')
                                            ->inRandomOrder()->take(4)->get();

        $randProducts3=\App\Models\Product::where('product_status',1)
                                            ->withCount('selectableProperties')
                                            ->withCount('fillableProperties')
                                            ->inRandomOrder()->take(4)->get();


        $oneRandomDiscount=\App\Models\Discount::where('discount_status',1)
                           ->inRandomOrder()->take(1)->first();
        $posts = Post::where('post_status',1)->take(3)->get();

        $data=[
            'randomProducts1'=>$randProducts1,
            'randProducts2'=>$randProducts2,
            'randProducts3'=>$randProducts3,
            'oneRandomDiscount'=>$oneRandomDiscount,
            'posts'=>$posts,
            'brands'=>$brands
        ];

        return view('landing.index',$data);

    }
    public function rate_product(Request $request,$id){
        $validator = Validator::make(
            $request->all(),
            [
                's_value'=>'required|integer|between:1,5',
            ]
        );
        if($validator->fails()){

            echo "error";exit();
        }

        if (Auth::guard('user')->check()){
            $product = Product::findorfail($id);
            $attach_data[$id] = [
                'product_id' => $id,
                'user_id' => Auth::guard('user')->user()->id,
                'score_value' => $request->s_value,

            ];

            $product->Ratings()->wherePivot('user_id',Auth::guard('user')->user()->id)->sync($attach_data);

            echo "ok";
        }
        else{
            redirect()->route('user-login');
        }
    }
    public function add_to_wish($id){


        if (Auth::guard('user')->check()){
            $product = Product::findorfail($id);
            $attach_data[$id] = [
                'product_id' => $id,
                'user_id' => Auth::guard('user')->user()->id,

            ];

            $product->IsWished()->wherePivot('user_id',Auth::guard('user')->user()->id)->sync($attach_data);

            return redirect()->back()->with('wish','<script>alert("با موفقیت به لیست علاقه مندی افزوده شد")</script>');
        }
        else{
            redirect()->route('user-login');
        }
    }

    public function showNotePage(Request $request){
        $note=\App\Models\Note::where('note_slug',$request->note_slug)
            ->where('note_status',1)
            ->firstOrFail();


        //begin set seo tags
        $meta=$request->meta;
        $meta->title=$note->note_title;
        $meta->description=$note->note_short_desc;
        $canonical_url=urldecode(url(Route('showNotePage',$note->note_slug)));
        $openGraph=$request->openGraph;
        $openGraph->title=$note->note_title;
        $openGraph->description=$note->note_short_desc;
        $openGraph->image=Null;
        //end set seo tags

        $data=[
            'note'=>$note,
            'meta'=>$meta,
            'openGraph'=>$openGraph,
            'canonical_url'=>$canonical_url,
        ];

        return view('landing.noteDetails',$data);

    }




    public function showContactUsForm(){
        return view('landing.contact_us');
    }

    public function saveContactUs(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'phone_number'=>'required|max:50',
                'contact_title'=>'required|max:140',
                'contact_text'=>'required|max:500',
            ]
        );
        if($validator->fails()){
            return redirect()->back()
                ->withInput($request->input())
                ->withErrors($validator->errors())
                ->with('messages',['ورودی های خود را بررسی کنید']);
        }


        $contact= new \App\Models\Contact();
        $contact->phone_number=$request->phone_number;
        $contact->contact_title=$request->contact_title;
        $contact->contact_text=nl2br($request->contact_text);
        $contact->user_ip=$request->ip();
        $contact->save();

        $messages=['پیام شما با موفقیت ارسال شد.'];
        return redirect()->back()->with('messages', $messages);


    }
}

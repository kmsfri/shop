<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Validator;
use Hash;
use Illuminate\Http\Request;
class Login_ResgisterController extends Controller
{
    use AuthenticatesUsers;

    protected $guard='user';

    protected $table='users';
    protected $redirectTo = 'user/dashboard';
    protected $redirectPathAfterLogout = 'user/login';
    public function showUserLoginForm(){
        return view('landing.login');
    }
    public function showUserRegisterForm(){
        return view('landing.register');
    }
    public function showUserForgetForm(){
        return view('landing.password');
    }

    protected function guard()
    {
        return Auth::guard('user');
    }

    public function Login(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'mobile'=>'required|digits:11',
                'password'=>'required',
            ]
        );
        if($validator->fails()){
            return redirect(url('user/login'))
                ->withInput($request->input())
                ->withErrors($validator->errors())
                ->with('data','فیلد های مورد نظر را بررسی کنید');

        }
        if(Auth::guard('user')->attempt(['mobile_number' => $request->mobile, 'password' => $request->password,'user_status' => 1])){
            return redirect(url('/user/dashboard'));
        }else{
            $data="مقادیر وارد شده اشتباه می باشند";
            return redirect(url('user/login'))
                ->withInput($request->input())->with('data', $data);
        }

    }
    public function Register(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'mobile'=>'required|digits:11|unique:users,mobile_number',
            ]
        );

        if($validator->fails()){
            return redirect(url('user/register'))
                ->withInput($request->input())
                ->withErrors($validator->errors())
                ->with('messages', ['شماره موبایل وارد شده معتبر نمیباشد و یا اینکه حسابی با این شماره در سیستم موجود است']);

        }
        //$randompass = rand(9999,9999999);
        $randompass = 1111;

        try{
            $user = new User();
            $user->mobile_number = $request->mobile;
            $user->user_status = 1;
            $user->password = bcrypt($randompass);

            $number = ltrim($request->mobile, 0);
            $user->save();
            return redirect(Route('user-login'))
                ->with('messages', ['رمز عبور جدید برای شماره ی وارد شده ارسال شد، بعد از دریافت می توانید وارد شوید']);
            /*if(\Helpers::sendsms($number,$randompass) == 0){
                $user->save();
                return redirect(url('User/Login'))
                    ->with('data', 'رمز عبور جدید برای شماره ی وارد شده ارسال شد، بعد از دریافت می توانید وارد شوید');
            }
            else {
                goto catch_block;
            }*/

        }
        catch(\Exception $e) {
            catch_block:
            return redirect()->back()->with('messages',['مشکلی در عملیات ثبت نام به وجود آمد، دوباره تلاش کنید']);
        }

    }
    public function Forget(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'mobile'=>'required|digits:11',
            ]
        );

        if($validator->fails()){
            return redirect(url('user/password'))
                ->withInput($request->input())
                ->withErrors($validator->errors());

        }


        //sms to user

        $user = User::where('mobile_number',$request->mobile)->where('user_status',1)->first();
        if(!isset($user)){
            //error user not exist
            return redirect()->back()
                ->withInput($request->input())
                ->with('messages',['این شماره در سیستم موجود نمی باشد']);
        }
        $randompass = rand(9999,9999999);

        try{
            $user->password = bcrypt($randompass);

            $number = ltrim($request->mobile, 0);
            if(\Helpers::sendsms($number,$randompass) == 0){
                $user->save();
                return redirect(url('user/login'))
                    ->with('messages', ['رمز عبور جدید برای شماره ی وارد شده ارسال شد، بعد از دریافت می توانید وارد شوید']);
            }
            else {
                goto catch_block;
            }

        }
        catch(\Exception $e) {
            catch_block:
            return redirect()->back()->with('messages',['مشکلی در عملیات ثبت نام به وجود آمد، دوباره تلاش کنید']);
        }

    }



    public function __construct()
    {
        $this->middleware('guest:user')->except('logout');
    }
    public function logout(Request $request)
    {
        $this->guard('user')->logout();
        $request->session()->invalidate();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect($this->redirectPathAfterLogout);
    }
}

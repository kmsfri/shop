<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostImages;
use Validator;
use App\Models\PCategory;
use App\User;
use Auth;
class BlogController extends Controller
{
    public function showblog()
    {
        $posts = Post::where('post_status', 1)->orderBy('created_at', 'DESC')->paginate(12);
        $recentposts = Post::where('post_status', 1)->inRandomOrder()->take(6)->get();
        $cats = PCategory::where('category_status', 1)->orderBy('category_order', 'ASC')->orderBy('created_at', 'DESC')->get();
        $meta = (object)[
            'title' => 'مطالب وبسایت', //Config--
            'keywords' => 'کلمات کلیدی وبسایت', //Config--
            'description' => 'توضیحات وبسایت', //Config--
        ];
        $canonical_url = urldecode(url(Route('showblog')));
        $openGraph = (object)[
            'locale' => 'fa_IR',
            'type' => 'website',
            'title' => 'مطالب وبسایت',//Config--
            'description' => 'توضیحات', //Config--
            'url' => urldecode(\URL::full()),
            'site_name' => 'نام سایت', //Config--
        ];
        return view('landing.listpost', [
            'posts' => $posts,
            'meta' => $meta,
            'openGraph' => $openGraph,
            'canonical_url' => $canonical_url,
            'cats' => $cats,
            'recentposts' => $recentposts,
        ]);
    }

    public function showsingleblog($slug)
    {
        $post = Post::where('post_slug', $slug)->where('post_status', 1)->first();
        if (!isset($post)) abort(404);
        $recentposts = Post::where('post_status', 1)->inRandomOrder()->take(2)->get();
        $meta = (object)[
            'title' => $post->post_title, //Config--
            'keywords' => $post->meta_keywords, //Config--
            'description' => $post->short_text, //Config--
        ];
        $canonical_url = urldecode(url(Route('showsingleblog', $post->post_slug)));
        $openGraph = (object)[
            'locale' => 'fa_IR',
            'type' => 'website',
            'title' => $post->post_title,//Config--
            'description' => $post->short_text, //Config--
            'url' => urldecode(\URL::full()),
            'site_name' => 'نام سایت', //Config--
        ];
        return view('landing.singlepost', ['post' => $post,
            'recentposts' => $recentposts
        ]);
    }
    public function blogcat($slug){
        $cat = PCategory::where('category_slug',$slug)->first();
        if (!isset($cat))
            abort(404);
        $recentposts = Post::where('post_status', 1)->inRandomOrder()->take(2)->get();
        $posts = $cat->Posts()
            ->where('post_status',1)
            ->orderBy('created_at','DESC')
            ->paginate(12);
        $cats = PCategory::where('category_status',1)
            ->orderBy('category_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();
        return view('landing.listpost',['posts'=>$posts,
            'recentposts' => $recentposts,
            'cats'=>$cats,]);
    }
    public function submit_comment(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'comment_text' => 'required',

            ]
        );

        if ($validator->fails()) {
            validator_fails:
            return redirect()->back()
                ->withInput($request->input())
                ->withErrors($validator->errors());
        }

        if (Auth::guard('user')->check()) {
            $attach_data[$id] = [
                'post_id' => $id,
                'user_id' => Auth::guard('user')->user()->id,
                'comment_text' => $request->comment_text

            ];
            $post = Post::findorfail($id);
            $post->Comments()->attach($attach_data);
            return redirect()->back()
                ->with('data', 'نظر شما با موفقیت ثبت شد، بعد از تایید مدیر نمایش داده می شود.');
        }
        else{
            abort(404);
        }
    }
    public function search(Request $request){
        $key='%'.$request->search.'%';
        $posts=Post::orWhere('post_title','like',$key)
            ->orWhere('short_text','like',$key)
            ->orWhere('main_text','like',$key)
            ->orWhere('post_tags','like',$key)->where('post_status',1)->orderby('created_at','DESC')->paginate(12);
        $cats = PCategory::all();
        $recentposts = Post::where('post_status', 1)->inRandomOrder()->take(2)->get();
        return view('landing.listpost',['posts'=>$posts,
            'recentposts' => $recentposts,
            'cats'=>$cats,]);
    }
}

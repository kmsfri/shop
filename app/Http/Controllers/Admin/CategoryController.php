<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CategoryController extends Controller
{
    public function categories(Request $request){

        if($request->parent_id!=Null){
            $parent_ctg=\App\Models\Category::find($request->parent_id);
            $parent_ctg_level2=\App\Models\Category::find($parent_ctg->parent_id);
            if($parent_ctg_level2==Null){
                $title="زیرمجموعه های دسته بندی: ". $parent_ctg->category_title;
                $backward_url=Route('adminCategories').'/'.$parent_ctg->id;
                $add_url=Route('add-category-form',$parent_ctg->id);
                $canHasSubCategory=true;
            }else{
                $title="زیرمجموعه های دسته بندی: ". $parent_ctg->category_title;
                $backward_url=Route('adminCategories');
                $add_url=Route('add-category-form',$parent_ctg->id);
                $canHasSubCategory=false;
            }

        }else{
            $title="لیست دسته بندیها";
            $backward_url=Route('adminDashboard');
            $add_url=Route('add-category-form');
            $canHasSubCategory=true;
        }

        $categories=\App\Models\Category::select('*')->where('parent_id','=',$request->parent_id)->orderBy('created_at')->get();

        $del_url=Route('do-delete-category',$request->parent_id);

        $resp=[
            'title'=>$title,
            'backward_url'=>$backward_url,
            'add_url'=>$add_url,
            'del_url'=>$del_url,
            'categories'=>$categories,
            'parent_id'=>$request->parent_id,
            'canHasSubCategory'=>$canHasSubCategory,
            'delete_url'=>Route('do-delete-category'),
        ];


        return view('admin.pages.lists.categories' ,$resp);
    }

    public function showAddCategoryForm(Request $request){

        if($request->parent_id!=Null){
            $parent_ctg=\App\Models\Category::find($request->parent_id);
            $parent_ctg_level2=\App\Models\Category::find($parent_ctg->parent_id);
            if($parent_ctg_level2==Null){
                $title="افزودن دسته بندی مطالب گردشگری جدید به: ". $parent_ctg->category_title;
            }else{
                $title="افزودن دسته بندی مطالب گردشگری جدید به: ". $parent_ctg->category_title." - متعلق به: ".$parent_ctg_level2->category_title;
            }

        }else{
            $title="افزودن دسته بندی مربوط به مطالب گردشگری";
        }
        $backward_url=Route('adminCategories',$request->parent_id);

        $resp=[
            'title'=>$title,
            'post_add_url'=>Route('do-add-category'),
            'request_type'=>'add',
            'parent_id'=>$request->parent_id,
            'backward_url'=>$backward_url,
            'ctg_type'=>'category',
        ];
        return view('admin.pages.forms.add_category' ,$resp);
    }

    var $img_upload_error_msg='';
    var $destinationPath = '/uploads/categories';
    private function changeCategory(Request $request){
        $uploaded_file_dir="";
        $this->img_upload_error_msg=array();
        $to_remove_dir="";
        try {
            if($request->category_img!=Null) {
                $file = $request->category_img;
                $fileName = "";
                if ($file == null) {
                    $fileName = "";
                } else {

                    if ($file->isValid()) {
                        $fileName = time() . '_' . $file->getClientOriginalName();

                        $file->move(public_path().$this->destinationPath, $fileName);
                        $uploaded_file_dir = $this->destinationPath.'/'.$fileName;
                        if($request->edit_id!=Null) {
                            $to_remove_dir = Category::find($request->edit_id)->category_img;
                        }
                    } else {
                        $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
                        goto catch_block;
                    }
                }

            }

            DB::transaction(function() use($request,$uploaded_file_dir,$to_remove_dir){
                if($request->edit_id!=Null) {
                    $ctg=Category::find($request->edit_id);
                }else{
                    $ctg = new Category();
                }

                $ctg->category_title=$request->category_title;
                if($uploaded_file_dir!=""){
                    $ctg->category_img=$uploaded_file_dir;
                }
                $ctg->category_order=$request->category_order;
                $ctg->category_status=$request->category_status;
                $ctg->category_slug=$request->category_slug_corrected;
                $ctg->save();

                if($request->edit_id!=Null && $to_remove_dir!=""){
                    if(file_exists(public_path().'/'.$to_remove_dir)){
                        unlink(public_path().'/'.$to_remove_dir);
                    }
                }


            });

        }
        catch(Exception $e) {
            catch_block:
            if(file_exists(public_path().'/'.$uploaded_file_dir)){
                unlink(public_path().'/'.$uploaded_file_dir);
            }
            return false;
        }

        return true;
    }

    public function saveCategory(Request $request){

        $category_slug_corrected='';
        if(isset($request->category_slug) && $request->category_slug!=null){
            $category_slug_corrected=\Helpers::make_slug($request->category_slug);
        }



        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'category_title' => $request->category_title,
            'category_img' => $request->category_img,
            'category_order'=>$request->category_order,
            'category_status'=>$request->category_status,
            'category_slug_corrected'=>$category_slug_corrected,
            'parent_id' => $request->parent_id,
        ]);

        $this->validate($newRequest, [
            'category_title' => 'required|min:2|max:255|unique:categories',
            'category_img' => 'nullable|image|max:2048',
            'category_order'=>'required|integer',
            'category_status'=>'required|integer',
            'category_slug_corrected'=>'required|unique:categories,category_slug',
            'parent_id' => 'nullable|exists:categories,id',
        ]);


        if($this->changeCategory($newRequest)){
            $msg=['دسته بندی جدید با موفقیت اضافه شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);;
        }



        return redirect(url(Route('adminCategories',$request->parent_id)))->with('messages', $msg);


    }

    public function editCategory(Request $request){

        $ctg=\App\Models\Category::where('id','=',$request->id)->first();
        if($ctg==Null){
            die('invalid request!');
        }

        $parent=$ctg->ParentCategory()->first();
        if($parent==Null){
            $parent_id=Null;
            $title="ویرایش  ".$ctg->category_title;
        }else{

            $parent_ctg_level2=$parent->ParentCategory()->first();
            if($parent_ctg_level2==Null){
                $title="ویرایش  ".$ctg->category_title." متعلق به: ". $parent->category_title;
            }else{
                $title="ویرایش  ".$ctg->category_title." متعلق به: ". $parent->category_title." - ".$parent_ctg_level2->category_title;
            }
            $parent_id=$parent->id;
        }

        $backward_url=Route('adminCategories',$request->parent_id);


        $resp=[
            'category'=>$ctg,
            'request_type'=>'edit',
            'post_edit_url'=>Route('do-edit-category'),
            'parent_id'=>$parent_id,
            'edit_id'=>$ctg->id,
            'title'=>$title,
            'backward_url'=>$backward_url,
            'ctg_type'=>'category',
        ];


        return view('admin.pages.forms.add_category' ,$resp);
    }

    public function doEditCategory(Request $request){




        $category_slug_corrected='';
        if(isset($request->category_slug) && $request->category_slug!=null){
            $category_slug_corrected=\Helpers::make_slug($request->category_slug);
        }

        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'category_title' => $request->category_title,
            'category_img' =>  $request->category_img,
            'category_order'=>$request->category_order,
            'category_status'=>$request->category_status,
            'category_slug_corrected'=>$category_slug_corrected,
            'edit_id' => $request->edit_id,
        ]);




        $this->validate($newRequest, [
            'category_title' => 'required|min:2|max:255|unique:categories,category_title,'.$newRequest->edit_id,
            'category_img' => 'nullable|image|max:2048',
            'category_order'=>'required|integer',
            'category_status'=>'required|integer',
            'category_slug_corrected'=>'required|unique:categories,category_slug,'.$newRequest->edit_id,
            'edit_id' => 'required|exists:categories,id',
        ]);



        if($this->changeCategory($newRequest)){
            $msg=['دسته بندی مورد نظر با موفقیت ویرایش شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);;
        }

        return redirect(url(Route('adminCategories')))->with('messages', $msg);

    }

    public function deleteCategory(Request $request){



        if(isset($request->remove_val)){
            foreach($request->remove_val as $c_id){
                $u=Category::find($c_id);
                if($u!=Null && $u->category_img!=null && trim($u->category_img)!=''){
                    if(file_exists(public_path().$u->category_img)){
                        unlink(public_path().$u->category_img);
                    }
                }
                unset($u);
            }

            Category::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(url(Route('adminCategories',$request->parent_id)))->with('messages', $msg);


    }
}

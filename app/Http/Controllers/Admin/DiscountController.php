<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use Helpers;


class DiscountController extends Controller{


    public function discounts(Request $request){


        $discounts=\App\Models\Discount::select('*')
            ->orderBy('created_at')
            ->get();



        $resp=[
            'title'=>'کدهای تخفیف',
            'backward_url'=>Route('adminDashboard'),
            'add_url'=>Route('adminAddDiscountForm'),
            'del_url'=>Route('adminDoDeleteDiscount'),
            'discounts'=>$discounts,
            'delete_url'=>Route('adminDoDeleteDiscount'),
        ];


        return view('admin.pages.lists.discounts' ,$resp);
    }

    public function showAddDiscountForm(Request $request){



        $resp=[
            'title'=>'افزودن کد تخفیف جدید',
            'post_add_url'=>Route('adminDoAddDiscount'),
            'request_type'=>'add',
            'backward_url'=>Route('adminDiscounts'),
        ];
        return view('admin.pages.forms.add_discount' ,$resp);
    }


    public function saveDiscount(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'discount_code'=>'required|max:50|unique:product_discounts,discount_code',
                'discount_percent'=>'required|integer',
                'discount_title1'=>'required|max:255',
                'discount_title2'=>'nullable|max:255',
                'is_valid_to'=>'nullable|max:12',
                'total_count'=>'required|integer',
                'used_count'=>'required|integer',
                'up_to'=>'required|integer',
                'discount_status'=>'required|integer',
            ]
        );


        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput();
        }


        if($this->changeDiscounts($request)){
            $msg=['کد تخفیف مورد نظر با موفقیت افزوده شد'];
        }else{
            goto validator_fails;
        }


        return redirect(Route('adminDiscounts'))->with('messages', $msg);

    }






    public function editDiscount(Request $request){

        $discount=\App\Models\Discount::findOrFail($request->id);

        $resp=[
            'discount'=>$discount,
            'request_type'=>'edit',
            'post_edit_url'=>Route('adminDoEditDiscount'),
            'edit_id'=>$discount->id,
            'title'=>'ویرایش لیست',
            'backward_url'=>Route('adminDiscounts'),
        ];


        $r_date=Helpers::convert_date_g_to_j(trim((string)$discount->is_valid_to),true);
        $discount->is_valid_to_j=$r_date;


        return view('admin.pages.forms.add_discount' ,$resp);
    }


    public function doEditDiscount(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'edit_id'=>'required|integer|exists:product_discounts,id',
                'discount_code'=>'required|max:50|unique:product_discounts,discount_code,'.$request->edit_id,
                'discount_percent'=>'required|integer',
                'discount_title1'=>'required|max:255',
                'discount_title2'=>'nullable|max:255',
                'is_valid_to'=>'nullable|max:12',
                'total_count'=>'required|integer',
                'used_count'=>'required|integer',
                'up_to'=>'required|integer',
                'discount_status'=>'required|integer',
            ]
        );


        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput();
        }


        if($this->changeDiscounts($request)){
            $msg=['کد تخفیف مورد نظر با موفقیت ویرایش شد'];
        }else{
            goto validator_fails;
        }


        return redirect(Route('adminDiscounts'))->with('messages', $msg);

    }




    public function deleteDiscount(Request $request){

        if(isset($request->remove_val)){
            \App\Models\Discount::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];

        return redirect(url(Route('adminDiscounts')))->with('messages', $msg);


    }





    private function changeDiscounts($request){


        if($request->edit_id!=Null) {
            $dc=\App\Models\Discount::find($request->edit_id);
        }else{
            $dc=new \App\Models\Discount();
        }



        $dc->discount_code = $request->discount_code;
        $dc->discount_percent = $request->discount_percent;
        $dc->discount_title1 = $request->discount_title1;
        $dc->discount_title2 = $request->discount_title2;
        $dc->is_valid_to = Helpers::convert_date_j_to_g(trim($request->is_valid_to),true);
        $dc->total_count = $request->total_count;
        $dc->used_count = $request->used_count;
        $dc->up_to = $request->up_to;
        $dc->discount_status = $request->discount_status;

        $dc->save();


        return true;
    }







}
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Contact;
use DB;
use Validator;

class ContactController extends Controller
{
    public function contacts(Request $request){

        $title="لیست پیامهای تماس با ما";

        $contacts=Contact::orderBy('created_at','DESC')->get();

        $add_url=Null;

        $backward_url=url()->previous();

        $del_url=Route('do-delete-contact');

        $resp=[
            'title'=>$title,
            'backward_url'=>$backward_url,
            'add_url'=>$add_url,
            'del_url'=>$del_url,
            'contacts'=>$contacts,
            'delete_url'=>Route('do-delete-contact'),
        ];


        return view('admin.pages.lists.contacts' ,$resp);
    }

    public function showAddContactForm(Request $request){

        $title="افزودن پیام جدید";

        $backward_url=url()->previous();

        $resp=[
            'title'=>$title,
            'post_add_url'=>Route('do-add-contact'),
            'request_type'=>'add',
            'backward_url'=>$backward_url,
        ];
        return view('admin.pages.forms.add_contact' ,$resp);
    }


    private function changeContact(Request $request){

        try {
            DB::transaction(function() use($request){
                if($request->edit_id!=Null) {
                    $contact=Contact::find($request->edit_id);
                }else{
                    $contact = new Contact();
                }
                $contact->phone_number=$request->phone_number;
                $contact->contact_title=$request->contact_title;
                $contact->contact_text=nl2br($request->contact_text);
                $contact->viewed=$request->viewed;
                $contact->Save();

            });

        }
        catch(Exception $e) {
            catch_block:

            return false;
        }

        return true;
    }

    public function saveContact(Request $request){

        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'phone_number' => $request->phone_number,
            'contact_title'=>$request->contact_title,
            'contact_text'=>$request->contact_text,
            'viewed'=>$request->viewed,
        ]);

        $this->validate($newRequest, [
            'phone_number'=>'required|max:50',
            'contact_title'=>'required|max:255',
            'contact_text'=>'required|max:500',
            'viewed'=>'required|integer|between:0,1',
        ]);


        if($this->changeContact($newRequest)){
            $msg=['پیام جدید با موفقیت اضافه شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);
        }



        return redirect(url(Route('adminContacts')))->with('messages', $msg);


    }

    public function editContact(Request $request){

        $contact=Contact::findOrFail($request->id);

        $title="ویرایش پیام";

        $backward_url=Route('adminContacts');


        $resp=[
            'contact'=>$contact,
            'request_type'=>'edit',
            'post_edit_url'=>Route('do-edit-contact'),
            'edit_id'=>$contact->id,
            'title'=>$title,
            'backward_url'=>$backward_url,
        ];


        return view('admin.pages.forms.add_contact' ,$resp);
    }

    public function doEditContact(Request $request){


        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'edit_id' => $request->edit_id,
            'phone_number'=>$request->phone_number,
            'contact_title'=>$request->contact_title,
            'contact_text'=>$request->contact_text,
            'viewed'=>$request->viewed,
        ]);

        $this->validate($newRequest, [
            'edit_id' => 'required|exists:contacts,id',
            'phone_number'=>'required|max:50',
            'contact_title'=>'required|max:255',
            'contact_text'=>'required|max:500',
            'viewed'=>'required|integer|between:0,1',
        ]);



        if($this->changeContact($newRequest)){
            $msg=['پیام مورد نظر با موفقیت ویرایش شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);
        }

        return redirect(url(Route('adminContacts')))->with('messages', $msg);

    }

    public function deleteContact(Request $request){



        if(isset($request->remove_val)){
            Contact::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(url(Route('adminContacts')))->with('messages', $msg);


    }
}

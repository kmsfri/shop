<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;

class UserController extends Controller
{
    var $img_upload_error_msg='';
    var $destinationPath = '/uploads/users/junior';

    public function showUsers(Request $request){

        $users=\App\Models\User::select('*')
            ->orderBy('user_status','DESC')
            ->orderBy('created_at','ASC')
            ->get();

        $title="لیست کاربران وبسایت";
        $backward_url=Route('adminDashboard');
        $add_url=Route('addUser');

        $resp=[
            'users'=>$users,
            'title'=>$title,
            'backward_url'=>$backward_url,
            'add_url'=>$add_url,
            'delete_url'=>Route('deleteUser'),
        ];



        return view('admin.pages.lists.junior_users' ,$resp);
    }



    public function showAddUserForm(Request $request){

        $title="افزودن کاربر جدید";
        $backward_url=Route('userList');


        $data=[
            'request_type'=>'add',
            'title'=>$title,
            'backward_url'=>$backward_url,
            'post_add_url'=>Route('addUser'),

        ];
        return view('admin.pages.forms.add_junior' ,$data);
    }



    public function saveUser(Request $request){


        $validator = Validator::make($request->all(),[
            'mobile_number' => 'required|regex:/(0)[0-9]{10}/|unique:users',
            'fullname'=>'required|min:2|max:140',
            'password'=>'required|min:4|max:40',
            'avatar_dir' => 'mimes:png,jpg,jpeg|max:1048',
            'user_status'=>'required|integer',
        ]);

        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>$this->img_upload_error_msg]);;
        }

        if($this->changeUser($request)){
            $msg=['کاربر جدید با موفقیت افزوده شد'];
        }else{
            goto validator_fails;
        }

        return redirect(Route('userList'))->with('messages', $msg);

    }


    private function changeUser($request){
        $uploaded_file_dir="";
        $this->img_upload_error_msg=array();
        $to_remove_dir="";
        try {
            if($request->avatar_dir!=Null) {
                $file = $request->file('avatar_dir');
                $fileName = "";
                if ($file == null) {
                    $fileName = "";
                } else {

                    if ($file->isValid()) {
                        $fileName = time() . '_' . $file->getClientOriginalName();

                        $file->move(public_path().$this->destinationPath, $fileName);
                        $uploaded_file_dir = $this->destinationPath.'/'.$fileName;
                        if($request->edit_id!=Null) {
                            $to_remove_dir = \App\Models\User::find($request->edit_id)->avatar_dir;
                        }
                    } else {
                        $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
                        goto catch_block;
                    }
                }

            }

            DB::transaction(function() use($request,$uploaded_file_dir,$to_remove_dir){
                if($request->edit_id!=Null) {
                    $u=\App\Models\User::find($request->edit_id);
                }else{
                    $u=new \App\Models\User();
                }

                $u->mobile_number=$request->mobile_number;
                $u->fullname=$request->fullname;
                if(trim($request->password)!="**||password-no-changed") {
                    $u->password = bcrypt($request->password);
                }
                if($uploaded_file_dir!=""){
                    $u->avatar_dir=$uploaded_file_dir;
                }
                $u->user_status=(int)$request->user_status;
                $u->save();

                if($request->edit_id!=Null && $to_remove_dir!=""){
                    if(file_exists(public_path().'/'.$to_remove_dir)){
                        unlink(public_path().'/'.$to_remove_dir);
                    }
                }


            });

        }
        catch(Exception $e) {
            catch_block:
            if(file_exists(public_path().'/'.$uploaded_file_dir)){
                unlink(public_path().'/'.$uploaded_file_dir);
            }
            return false;
        }

        return true;
    }



    public function deleteUser(Request $request){

        if(isset($request->remove_val)){
            foreach($request->remove_val as $c_id){
                $u=\App\Models\User::find($c_id);
                if($u!=Null && $u->avatar_dir!=null && trim($u->avatar_dir)!=''){
                    if(file_exists(public_path().'/uploads/users/junior/'.$u->avatar_dir)){
                        unlink(public_path().'/uploads/users/junior/'.$u->avatar_dir);
                    }
                }
                unset($u);
            }

            \App\Models\User::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(Route('userList'))->with('messages', $msg);
    }



    public function editUser(Request $request){

        if($request->id!=Null){
            $u=\App\Models\User::find($request->id);
            if($u==Null){ die('invalid request');}

            $title="ویرایش اطلاعات کاربر: ".$u->mobile_number;
            $backward_url=Route('userList');
        }else{
            die('invalid request!');
        }


        $data=[
            'request_type'=>'edit',
            'u'=>$u,
            'title'=>$title,
            'backward_url'=>$backward_url,
            'post_edit_url'=>Route('doEditUser'),
            'edit_id'=>$u->id,
        ];


        return view('admin.pages.forms.add_junior' ,$data);

    }








    public function doEditUser(Request $request){


        $validator = Validator::make(
            $request->all(),
            [
                'edit_id'=>'required|integer|exists:users,id',
                'mobile_number' => 'required|regex:/(0)[0-9]{10}/|unique:users,mobile_number,'.$request->edit_id,
                'fullname'=>'required|min:2|max:140',
                'password'=>'required|min:4|max:40',
                'avatar_dir' => 'mimes:png,jpg,jpeg|max:1048',
                'user_status'=>'required|integer',
            ]
        );


        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>$this->img_upload_error_msg]);;
        }


        if($this->changeUser($request)){
            $msg=['کاربر مورد نظر با موفقیت ویرایش شد'];
        }else{

            goto validator_fails;
        }

        return redirect(Route('userList'))->with('messages', $msg);

    }

}

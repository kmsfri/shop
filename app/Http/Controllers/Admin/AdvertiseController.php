<?php

namespace App\Http\Controllers\Admin;


use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Product;
use Validator;
use DB;
use Helpers;
use Auth;
class AdvertiseController extends Controller
{
    public function showAdvertises(Request $request){


        $advertises=Product::OrderBy('created_at')->orderBy('updated_at')->get();
        $backward_url=Route('adminDashboard');
        $add_url=Route('addAdvertise');


        $data=[
            'title'=>'لیست محصولات ثبت شده',
            'add_url'=>$add_url,
            'del_url'=>Route('deleteAdvertise'),
            'advertises'=>$advertises,
            'backward_url'=>$backward_url,
            'delete_url'=>Route('deleteAdvertise'),
        ];

        return view('admin.pages.lists.advertises',$data);

    }



    public function showAddAdvertiseForm(Request $request){

        $props=\App\Models\Property::select('id','property_title')
            ->where('property_status','=',1)
            ->where('parent_id','=',Null)
            ->orderBy('property_order','ASC')
            ->get();

        $prop_values=array();

        $brands=Brand::where('brand_status',1)
                        ->orderBy('brand_order','ASC')
                        ->orderBy('created_at','DESC')
                        ->get();


        $data=[
            'request_type'=>'add',
            'brands'=>$brands,
            'title'=>'افزودن محصول جدید',
            'backward_url'=>Route('advertisesList'),
            'post_add_url'=>Route('doAddAdvertise'),
            'props'=>$props,
            'prop_values'=>$prop_values,
        ];
        return view('admin.pages.forms.add_advertise' ,$data);

    }




    public function editAdvertise(Request $request){

        $advertise = Product::where('id',$request->id)->first();
        if(empty($advertise))die('invalid_request!');



        $r_date=Helpers::convert_date_g_to_j(trim((string)$advertise->countdown_to),true);
        $advertise->countdownTo=$r_date;


        $props=\App\Models\Property::select('id','property_title')
            ->where('property_status','=',1)
            ->where('parent_id','=',Null)
            ->orderBy('property_order','ASC')
            ->get();


        $brands=Brand::where('brand_status',1)
            ->orderBy('brand_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();


        $data=[
            'title'=>'ویرایش محصول',
            'brands'=>$brands,
            'request_type'=>'edit',
            'post_edit_url'=>Route('doEditAdvertise'),
            'advertise'=>$advertise,
            'edit_id'=>$advertise->id,
            'backward_url'=>Route('advertisesList'),
            'props'=>$props,
        ];

        return view('admin.pages.forms.add_advertise' ,$data);

    }


    var $upload_dir='/uploads/products/';
    public function saveAdvertise(Request $request){

        $newRequest=[
            'newImg.*' => $request->newImg,
            'oldImg.*' => $request->oldImg,
            'product_title'=>$request->product_title,
            'brand_id'=>$request->brand_id,
            'countdown'=>$request->countdown,
            'product_slug'=>\Helpers::make_slug($request->product_slug),
            'product_description'=>nl2br($request->product_description),
            'product_price'=>$request->product_price,
            'discount_percent'=>$request->discount_percent,
            'product_status'=>$request->product_status,
            'in_stock_count'=>$request->in_stock_count,
            'props'=>$request->props,
        ];



        $validator = Validator::make(
            $newRequest,
            [
                'product_slug'=>'required|max:100|unique:products,product_slug'.((isset($request->edit_id)?','.$request->edit_id:Null)),
                'newImg.*' => 'mimes:png,jpg,jpeg|max:2048',
                'oldImg.*' => 'integer|exists:product_images,id',
                'product_title'=>'required|max:140',
                'brand_id'=>'nullable|max:140|exists:brands,id',
                'countdown'=>'required|max:15',
                'product_description'=>'required|max:1000',
                'product_price'=>'required|numeric|max:200000000',
                'discount_percent'=>'nullable|numeric|min:0|max:100',
                'product_status'=>'required|integer',
                'in_stock_count'=>'required|integer',
                'props.*' => 'nullable',


            ]
        );




        if($validator->fails()){
            return redirect()->back()
                ->withInput($request->input())
                ->withErrors($validator->errors())
                ->with('messages',['ورودی های خود را بررسی کنید']);
        }


        $uploaded_files_dir=array();
        $main_img_dir=Null;
        $this->img_upload_error_msg=array();
        try {

            if($request->newImg!=Null) {
                foreach ($request->newImg as $key => $uimg) {
                    $file = $request->file('newImg.' . $key);
                    $fileName = "";
                    if ($file == null) {
                        $fileName = "";
                    } else {
                        if ($file->isValid()) {
                            $fileName = Auth::guard('admin')->user()->id.str_replace(' ', '', time()).rand(1000,9999). '.' . $file->guessClientExtension();
                            $destinationPath = $this->upload_dir;
                            $imgConf=[
                                'imgType'=>'productImage',
                                'imgObject'=>$file,
                                'resultDir'=>public_path().$destinationPath.'/'.$fileName,
                            ];
                            \Helpers::save_img($imgConf['imgType'],$imgConf['imgObject'],$imgConf['resultDir']);
                            //$file->move(public_path().$destinationPath, $fileName);
                            $uploaded_files_dir[] = $destinationPath.'/'.$fileName;

                            if($main_img_dir==Null){
                                $destinationPath = $this->upload_dir;
                                $imgConf=[
                                    'imgType'=>'productImageThumbnail',
                                    'imgObject'=>$file,
                                    'resultDir'=>public_path().$destinationPath.'/'.'thumb'.$fileName,
                                ];
                                \Helpers::save_img($imgConf['imgType'],$imgConf['imgObject'],$imgConf['resultDir']);
                                $main_img_dir = $destinationPath.'/'.'thumb'.$fileName;
                            }


                        } else {
                            $this->img_upload_error_msg = 'آپلود یکی از تصاویر ناموفق بود';
                            goto catch_block;
                        }
                    }
                }
            }

            $removableOldImgID=array();
            $removableOldImgDir=array();

            if(isset($request->edit_id) && $request->edit_id!=Null){

                $ad=Product::where('id',$request->edit_id)->first();
                if($ad==Null)die('invalid_request!');

                foreach($ad->ProductImages()->get() as $cimg){
                    $exists=false;
                    if($request->oldImg!=Null){
                        foreach($request->oldImg as $oimg){
                            if($oimg==$cimg->id){
                                $exists=True;
                                break;
                            }
                        }
                    }
                    if(!$exists){
                        $removableOldImgID[]=$cimg->id;
                        $removableOldImgDir[]=$cimg->image_dir;
                    }
                }

                if($main_img_dir!=Null && $ad->main_img_dir!=Null){
                    $removableOldImgDir[]=$ad->main_img_dir;
                }

            }else{
                $ad=new Product();
            }


            DB::transaction(function() use($request,$newRequest,$uploaded_files_dir,$main_img_dir,$removableOldImgID,$removableOldImgDir,$ad){

                $ad->product_slug=$newRequest['product_slug'];
                $ad->product_title=$request->product_title;
                $ad->brand_id=$request->brand_id;
                if($main_img_dir!=Null){
                    $ad->main_img_dir=$main_img_dir;
                }

                if($request->countdown!=Null){
                    $ad->countdown_to=Helpers::convert_date_j_to_g(trim($request->countdown),true);
                }else{
                    $ad->countdown_to=Null;
                }

                $ad->product_description=$request->product_description;
                $ad->product_price=$request->product_price;
                $ad->discount_percent=$request->discount_percent;
                $ad->product_status=$request->product_status;
                $ad->in_stock_count=$request->in_stock_count;
                $ad->save();


                $props=array();
                foreach($request->props as $key=>$p){

                    if($p!=Null){
                        if($p=='selectable_by_buyer'){
                            $props[$key]=['select_status'=>1];
                        }else if($p=='fillable_by_buyer'){
                            $props[$key]=['select_status'=>2];
                        }else if($p=='select_color_by_buyer'){
                            $props[$key]=['select_status'=>3];
                        }else{
                            $props[$p]=['select_status'=>0];
                        }

                    }
                }
                $ad->property()->sync($props);



                $img_data=array();
                foreach($uploaded_files_dir as $key=>$ufd){
                    $img_data[]=new \App\Models\ProductImages  ([
                        'product_id'=>$ad->id,
                        'image_dir'=>$ufd,
                        'image_order'=>$key,
                    ]);
                }

                $ad->ProductImages()->whereIn('id',$removableOldImgID)->delete();

                $ad->ProductImages()->saveMany($img_data);

                if($request->edit_id!=Null){
                    foreach($removableOldImgDir as $uimg_dir){
                        if(file_exists(public_path().'/'.$uimg_dir)){
                            unlink(public_path().'/'.$uimg_dir);
                        }
                    }
                }

            });

        }
        catch(Exception $e) {
            catch_block:
            foreach($uploaded_files_dir as $uimg_dir){
                if(file_exists(public_path().'/'.$uimg_dir)){
                    unlink(public_path().'/'.$uimg_dir);
                }
            }
            if(file_exists(public_path().'/'.$main_img_dir)){
                unlink(public_path().'/'.$main_img_dir);
            }

            return redirect()->back()
                ->withInput($request->input())
                ->with('messages',['عملیات با شکست مواجه شد، دوباره تلاش کنید']);

        }
        if(isset($request->edit_id) && $request->edit_id!=Null){
            $msg=["محصول مورد نظر با موفقیت ویرایش شد"];
        }else{
            $msg=["محصول جدید با موفقیت افزوده شد"];
        }
        return redirect(url(Route('adminEditAdvertiseCategory',$ad->id)))->with('messages', $msg);
    }




    public function deleteAdvertise(Request $request){

        if(isset($request->remove_val)){
            foreach($request->remove_val as $ads_id){
                $ads=Product::find($ads_id);
                if($ads!=Null && $ads->ProductImages()->get()!=null){
                    foreach ($ads->ProductImages()->get() as $aimg){
                        if(trim($aimg->img_dir)!=''){
                            if(file_exists(public_path().$aimg->img_dir)){
                                unlink(public_path().$aimg->img_dir);
                            }
                        }
                    }


                    if($ads->main_img_dir!=Null && file_exists(public_path().$ads->main_img_dir)){
                        unlink(public_path().$ads->main_img_dir);
                    }



                }
                unset($ads);
            }

            Product::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(Route('advertisesList'))->with('messages', $msg);
    }


    public function editAdvertiseCategory(Request $request){

        $product=Product::where('id',$request->product_id)
            ->first();
        if($product==Null)die('invalid request!');

        $masterCtgs=\App\Models\Category::where('category_status',1)
            ->where('parent_id',Null)
            ->orderBy('category_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();

        $data=[
            'title'=>'انتخاب دسته بندی',
            'request_type'=>'edit',
            'post_edit_url'=>Route('adminDoEditAdvertiseCategory'),
            'masterCtgs'=>$masterCtgs,
            'product'=>$product,
            'edit_id'=>$product->id,
            'backward_url'=>Route('editAdvertise',$product->id),
        ];

        return view('admin.pages.forms.SelectAdvertiseCategory',$data);


    }


    public function doEditAdvertiseCategory(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'ctg.*' => 'integer|exists:categories,id',
                'edit_id'=>'required|exists:products,id',
            ]
        );



        if($validator->fails()){
            return redirect()->back()
                ->withInput($request->input())
                ->withErrors($validator->errors())
                ->with('messages',['ورودی های خود را بررسی کنید']);
        }





        $product=Product::where('id',$request->edit_id)
            ->first();
        if($product==Null)die('invalid request!');


        $product->Categories()->sync($request->ctg);


        $msg=['دسته بندی محصول مورد نظر با موفقیت بروزرسانی شد'];
        return redirect(url(Route('advertisesList')))->with('messages', $msg);


    }



}

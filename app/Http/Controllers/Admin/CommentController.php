<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use App\Models\PostComment;
use App\Models\Product;
use App\Models\ProductComment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;

class CommentController extends Controller
{
    public function comments(Request $request){


        if(isset($request->product_id)){
            $product=Product::findOrFail($request->product_id);
            $title="لیست نظرات محصول: ".$product->product_title;

            $add_url=Route('add-comment-form',$product->id);

            $comments=ProductComment::where('product_id',$request->product_id)
                                    ->orderBy('created_at','DESC')->get();
        }else{
            $title="لیست نظرات محصولات";

            $comments=ProductComment::orderBy('created_at','DESC')->get();

            $add_url=Null;

        }
        $backward_url=url()->previous();


        $del_url=Route('do-delete-comment');

        $resp=[
            'title'=>$title,
            'backward_url'=>$backward_url,
            'add_url'=>$add_url,
            'del_url'=>$del_url,
            'comments'=>$comments,
            'delete_url'=>Route('do-delete-comment'),
        ];


        return view('admin.pages.lists.comments' ,$resp);
    }

    public function showAddCommentForm(Request $request){

        if(!isset($request->product_id)) abort(404);

        $product=Product::findOrFail($request->product_id);

        $title="افزودن نظر جدید به محصول: ".$product->product_title;

        $backward_url=url()->previous();

        $resp=[
            'product_id'=>$request->product_id,
            'title'=>$title,
            'post_add_url'=>Route('do-add-comment'),
            'request_type'=>'add',
            'backward_url'=>$backward_url,
        ];
        return view('admin.pages.forms.add_comment' ,$resp);
    }


    private function changeComment(Request $request){


        try {

            DB::transaction(function() use($request){
                if($request->edit_id!=Null) {
                    $comment=ProductComment::find($request->edit_id);
                }else{
                    $comment = new ProductComment();
                    $comment->product_id = $request->product_id;
                    $comment->user_id = Auth::guard('user')->user()->id;
                }

                $comment->comment_text=nl2br($request->comment_text);
                $comment->comment_status=$request->comment_status;
                $comment->Save();

            });

        }
        catch(Exception $e) {
            catch_block:

            return false;
        }

        return true;
    }

    public function saveComment(Request $request){


        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'product_id' => $request->product_id,
            'comment_text' => $request->comment_text,
            'comment_status'=>$request->comment_status,
        ]);

        $this->validate($newRequest, [
            'product_id' => 'required|integer|exists:products,id',
            'comment_text' => 'required|max:255',
            'comment_status'=>'required|integer',
        ]);


        if($this->changeComment($newRequest)){
            $msg=['نظر جدید با موفقیت اضافه شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);;
        }



        return redirect(url(Route('adminComments')))->with('messages', $msg);


    }

    public function editComment(Request $request){

        $comment=ProductComment::findOrFail($request->id);

        $title="ویرایش نظر";

        $backward_url=Route('adminComments');


        $resp=[
            'comment'=>$comment,
            'request_type'=>'edit',
            'post_edit_url'=>Route('do-edit-comment'),
            'edit_id'=>$comment->id,
            'title'=>$title,
            'backward_url'=>$backward_url,
        ];


        return view('admin.pages.forms.add_comment' ,$resp);
    }

    public function doEditComment(Request $request){


        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'comment_text' => $request->comment_text,
            'comment_status'=>$request->comment_status,
            'edit_id' => $request->edit_id,
        ]);




        $this->validate($newRequest, [
            'comment_text' => 'required|max:255',
            'comment_status'=>'required|integer',
            'edit_id' => 'required|exists:user_product_comments,id',
        ]);



        if($this->changeComment($newRequest)){
            $msg=['نظر مورد نظر با موفقیت ویرایش شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);;
        }

        return redirect(url(Route('adminComments')))->with('messages', $msg);

    }

    public function deleteComment(Request $request){



        if(isset($request->remove_val)){
            ProductComment::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(url(Route('adminComments')))->with('messages', $msg);


    }



























    public function postComments(Request $request){


        if(isset($request->post_id)){
            $post=Post::findOrFail($request->post_id);
            $title="لیست نظرات مطلب: ".$post->post_title;

            $add_url=Route('add-post-comment-form',$post->id);

            $comments=PostComment::where('post_id',$request->post_id)
                ->orderBy('created_at','DESC')->get();
        }else{
            $title="لیست نظرات مطالب";

            $comments=PostComment::orderBy('created_at','DESC')->get();

            $add_url=Null;

        }
        $backward_url=url()->previous();





        $del_url=Route('do-delete-post_comment');


        $resp=[
            'title'=>$title,
            'backward_url'=>$backward_url,
            'add_url'=>$add_url,
            'del_url'=>$del_url,
            'comments'=>$comments,
            'delete_url'=>Route('do-delete-post_comment'),
        ];


        return view('admin.pages.lists.postComments' ,$resp);
    }

    public function showAddPostCommentForm(Request $request){

        if(!isset($request->post_id)) abort(404);

        $post=Post::findOrFail($request->post_id);

        $title="افزودن نظر جدید به مطلب: ".$post->post_title;

        $backward_url=url()->previous();

        $resp=[
            'post_id'=>$request->post_id,
            'title'=>$title,
            'post_add_url'=>Route('do-add-post-comment'),
            'request_type'=>'add',
            'backward_url'=>$backward_url,
        ];
        return view('admin.pages.forms.add_post_comment' ,$resp);
    }


    private function changePostComment(Request $request){
        try {

            DB::transaction(function() use($request){
                if($request->edit_id!=Null) {
                    $comment=PostComment::find($request->edit_id);
                }else{
                    $comment = new PostComment();
                    $comment->post_id = $request->post_id;
                    $comment->user_id = Auth::guard('user')->user()->id;
                }

                $comment->comment_text=nl2br($request->comment_text);
                $comment->comment_status=$request->comment_status;
                $comment->Save();

            });

        }
        catch(Exception $e) {
            catch_block:

            return false;
        }

        return true;
    }

    public function savePostComment(Request $request){


        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'post_id' => $request->post_id,
            'comment_text' => $request->comment_text,
            'comment_status'=>$request->comment_status,
        ]);

        $this->validate($newRequest, [
            'post_id' => 'required|integer|exists:posts,id',
            'comment_text' => 'required|max:255',
            'comment_status'=>'required|integer',
        ]);


        if($this->changePostComment($newRequest)){
            $msg=['نظر جدید با موفقیت اضافه شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);;
        }



        return redirect(url(Route('adminPostComments')))->with('messages', $msg);


    }

    public function editPostComment(Request $request){

        $comment=PostComment::findOrFail($request->id);

        $title="ویرایش نظر";

        $backward_url=Route('adminPostComments');


        $resp=[
            'comment'=>$comment,
            'request_type'=>'edit',
            'post_edit_url'=>Route('do-edit-post_comment'),
            'edit_id'=>$comment->id,
            'title'=>$title,
            'backward_url'=>$backward_url,
        ];


        return view('admin.pages.forms.add_post_comment' ,$resp);
    }

    public function doEditPostComment(Request $request){


        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'comment_text' => $request->comment_text,
            'comment_status'=>$request->comment_status,
            'edit_id' => $request->edit_id,
        ]);




        $this->validate($newRequest, [
            'comment_text' => 'required|max:255',
            'comment_status'=>'required|integer',
            'edit_id' => 'required|exists:post_comments,id',
        ]);



        if($this->changePostComment($newRequest)){
            $msg=['نظر مورد نظر با موفقیت ویرایش شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);;
        }

        return redirect(url(Route('adminPostComments')))->with('messages', $msg);

    }

    public function deletePostComment(Request $request){



        if(isset($request->remove_val)){
            PostComment::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(url(Route('adminPostComments')))->with('messages', $msg);


    }
}
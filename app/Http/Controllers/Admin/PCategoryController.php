<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PCategory;
use DB;
class PCategoryController extends Controller
{
    public function categories(Request $request){

        if($request->parent_id!=Null){
            $parent_ctg=PCategory::find($request->parent_id);
            $parent_ctg_level2=PCategory::find($parent_ctg->parent_id);
            if($parent_ctg_level2==Null){
                $title="زیرمجموعه های دسته بندی: ". $parent_ctg->category_title;
                $backward_url=Route('adminpCategories').'/'.$parent_ctg->id;
                $add_url=Route('add-pcategory-form',$parent_ctg->id);
                $canHasSubCategory=true;
            }else{
                $title="زیرمجموعه های دسته بندی: ". $parent_ctg->category_title;
                $backward_url=Route('adminpCategories');
                $add_url=Route('add-pcategory-form',$parent_ctg->id);
                $canHasSubCategory=false;
            }

        }else{
            $title="لیست دسته بندیهای مطالب";
            $backward_url=Route('adminDashboard');
            $add_url=Route('add-pcategory-form');
            $canHasSubCategory=true;
        }

        $categories=PCategory::select('*')->where('parent_id','=',$request->parent_id)->orderBy('created_at')->get();

        $del_url=Route('do-delete-pcategory',$request->parent_id);

        $resp=[
            'title'=>$title,
            'backward_url'=>$backward_url,
            'add_url'=>$add_url,
            'del_url'=>$del_url,
            'categories'=>$categories,
            'parent_id'=>$request->parent_id,
            'canHasSubCategory'=>$canHasSubCategory,
            'delete_url'=>Route('do-delete-pcategory'),
        ];


        return view('admin.pages.lists.pcategories' ,$resp);
    }

    public function showAddCategoryForm(Request $request){

        if($request->parent_id!=Null){
            $parent_ctg=PCategory::find($request->parent_id);
            $parent_ctg_level2=PCategory::find($parent_ctg->parent_id);
            if($parent_ctg_level2==Null){
                $title="افزودن دسته بندی جدید به: ". $parent_ctg->category_title;
            }else{
                $title="افزودن دسته بندی جدید به: ". $parent_ctg->category_title." - متعلق به: ".$parent_ctg_level2->category_title;
            }

        }else{
            $title="افزودن دسته بندی";
        }
        $backward_url=Route('adminpCategories',$request->parent_id);

        $resp=[
            'title'=>$title,
            'post_add_url'=>Route('do-add-pcategory'),
            'request_type'=>'add',
            'parent_id'=>$request->parent_id,
            'backward_url'=>$backward_url,
            'ctg_type'=>'pcategory',
        ];
        return view('admin.pages.forms.add_pcategory' ,$resp);
    }

    private function changeCategory(Request $request){
            DB::transaction(function() use($request){
                if($request->edit_id!=Null) {
                    $ctg=PCategory::find($request->edit_id);
                }else{
                    $ctg = new PCategory();
                }

                $ctg->category_title=$request->category_title;
                $ctg->category_order=$request->category_order;
                $ctg->category_status=$request->category_status;
                $ctg->parent_id=$request->parent_id;
                $ctg->category_slug=$request->category_slug_corrected;
                $ctg->save();

            });
        return true;
    }

    public function saveCategory(Request $request){
        $category_slug_corrected='';
        if(isset($request->category_slug) && $request->category_slug!=null){
            $category_slug_corrected=\Helpers::make_slug($request->category_slug);
        }
        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'category_title' => $request->category_title,
            'category_order'=>$request->category_order,
            'category_status'=>$request->category_status,
            'category_slug_corrected'=>$category_slug_corrected,
            'parent_id' => $request->parent_id,
        ]);

        $this->validate($newRequest, [
            'category_title' => 'required|min:2|max:255|unique:categories',
            'category_order'=>'required|integer',
            'category_status'=>'required|integer',
            'category_slug_corrected'=>'required|unique:pcategories,category_slug',
            'parent_id' => 'nullable|exists:pcategories,id',
        ]);


        if($this->changeCategory($newRequest)){
            $msg=['دسته بندی جدید با موفقیت اضافه شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);;
        }

        return redirect(url(Route('adminpCategories',$request->parent_id)))->with('messages', $msg);


    }

    public function editCategory(Request $request){

        $ctg=PCategory::where('id','=',$request->id)->first();
        if($ctg==Null){
            die('invalid request!');
        }

        $parent=$ctg->ParentCategory()->first();
        if($parent==Null){
            $parent_id=Null;
            $title="ویرایش  ".$ctg->category_title;
        }else{

            $parent_ctg_level2=$parent->ParentCategory()->first();
            if($parent_ctg_level2==Null){
                $title="ویرایش  ".$ctg->category_title." متعلق به: ". $parent->category_title;
            }else{
                $title="ویرایش  ".$ctg->category_title." متعلق به: ". $parent->category_title." - ".$parent_ctg_level2->category_title;
            }
            $parent_id=$parent->id;
        }

        $backward_url=Route('adminpCategories',$request->parent_id);


        $resp=[
            'category'=>$ctg,
            'request_type'=>'edit',
            'post_edit_url'=>Route('do-edit-pcategory'),
            'parent_id'=>$parent_id,
            'edit_id'=>$ctg->id,
            'title'=>$title,
            'backward_url'=>$backward_url,
            'ctg_type'=>'pcategory',
        ];


        return view('admin.pages.forms.add_pcategory' ,$resp);
    }

    public function doEditCategory(Request $request){

        $category_slug_corrected='';
        if(isset($request->category_slug) && $request->category_slug!=null){
            $category_slug_corrected=\Helpers::make_slug($request->category_slug);
        }

        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'category_title' => $request->category_title,
            'category_order'=>$request->category_order,
            'category_status'=>$request->category_status,
            'category_slug_corrected'=>$category_slug_corrected,
            'edit_id' => $request->edit_id,
        ]);


        $this->validate($newRequest, [
            'category_title' => 'required|min:2|max:255|unique:categories,category_title,'.$newRequest->edit_id,
            'category_order'=>'required|integer',
            'category_status'=>'required|integer',
            'category_slug_corrected'=>'required|unique:pcategories,category_slug,'.$newRequest->edit_id,
            'edit_id' => 'required|exists:pcategories,id',
        ]);



        if($this->changeCategory($newRequest)){
            $msg=['دسته بندی مورد نظر با موفقیت ویرایش شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);;
        }

        return redirect(url(Route('adminpCategories')))->with('messages', $msg);

    }

    public function deleteCategory(Request $request){



        if(isset($request->remove_val)){


            PCategory::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(url(Route('adminpCategories',$request->parent_id)))->with('messages', $msg);


    }
}

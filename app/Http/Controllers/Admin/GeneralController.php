<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use Helpers;


class GeneralController extends Controller{


    var $img_upload_error_msg='';

    public function slides(Request $request){

        $slides=\App\Models\Slider::select('*')
                ->with('Product')
                ->orderBy('img_order','ASC')
                ->orderBy('img_status','DESC')
                ->orderBy('created_at','DESC')
                ->get();


        $data=[
            'title'=>'لیست اسلایدها',
            'add_url'=>Route('adminAddSlideFrm'),
            'del_url'=>Route('adminDeleteSlide'),
            'slides'=>$slides,
            'backward_url'=>Route('adminDashboard'),
            'delete_url'=>Route('adminDeleteSlide'),
        ];

        return view('admin.pages.lists.slides',$data);
        
    }

    public function showAddSlideForm(Request $request){

        $data=[
            'request_type'=>'add',
            'title'=>'افزودن اسلاید جدید',
            'backward_url'=>Route('adminDashboard'),
            'post_add_url'=>Route('adminSaveSlide'),
            'product_id'=>isset($request->product_id)?$request->product_id:Null
        ];



        return view('admin.pages.forms.add_slide' ,$data);
    }

    public function saveSlide(Request $request){

        $validator = Validator::make($request->all(),[
            'title1'=>'nullable|max:255',
            'product_id'=>'nullable|integer|exists:products,id',
            'img_dir'=>'nullable|mimes:png,jpg,jpeg|max:2048',
            'img_link'=>'nullable|max:255',
            'img_order'=>'required|integer',
            'img_status'=>'required|integer',
        ]);



        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>$this->img_upload_error_msg]);
        }


        if($this->changeSlider($request)){
            $msg=['اسلاید جدید با موفقیت به سیستم اضافه شد'];
        }else{

            goto validator_fails;
        }

        return redirect(Route('adminSlides'))->with('messages', $msg);

    }





    var $slidesPath='/uploads/slider';
    private function changeSlider($request){



        $uploaded_file_dir="";
        $this->img_upload_error_msg=array();
        $to_remove_dir="";
        try {
            if($request->img_dir!=Null) {

                $file = $request->file('img_dir');
                $fileName = "";

                if ($file == null) {
                    $fileName = "";
                } else {

                    if ($file->isValid()) {
                        $fileName = time() . '_' . $file->getClientOriginalName();
                        $destinationPath = public_path() . $this->slidesPath;
                        $file->move($destinationPath, $fileName);
                        $uploaded_file_dir = $this->slidesPath.'/'.$fileName;
                        if($request->edit_id!=Null) {
                            $to_remove_dir = \App\Models\Slider::find($request->edit_id)->img_dir;
                        }
                    } else {
                        $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
                        goto catch_block;
                    }
                }

            }

            DB::transaction(function() use($request,$uploaded_file_dir,$to_remove_dir){

                if($request->edit_id!=Null) {
                    $slider=\App\Models\Slider::find($request->edit_id);
                }else{
                    $slider=new \App\Models\Slider();
                    if($request->product_id!=Null){
                        $slider->product_id=$request->product_id;
                    }
                }
                $slider->title1=$request->title1;
                if($uploaded_file_dir!=""){
                    $slider->img_dir=$uploaded_file_dir;
                }
                $slider->img_link=$request->img_link;
                $slider->img_order=$request->img_order;
                $slider->img_status=$request->img_status;

                $slider->save();


                if($request->edit_id!=Null && $to_remove_dir!=""){
                    if(file_exists(public_path().'/'.$to_remove_dir)){
                    unlink(public_path().'/'.$to_remove_dir);
                    }
                }

            });

        }
        catch(Exception $e) {
            catch_block:
            if(file_exists(public_path().'/'.$uploaded_file_dir)){
                unlink(public_path().'/'.$uploaded_file_dir);
            }

            return false;

        }

        return true;
    }


















    public function deleteSlide(Request $request){

        if(isset($request->remove_val)){
            foreach($request->remove_val as $c_id){
                $u=\App\Models\Slider::find($c_id);
                if($u!=Null && $u->img_dir!=null && trim($u->img_dir)!=''){
                    if(file_exists(public_path().'/'.$u->img_dir)){
                    unlink(public_path().'/'.$u->img_dir);
                    }
                }
                unset($u);
            }

            \App\Models\Slider::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(Route('adminSlides'))->with('messages', $msg);


    }



    public function editSlide(Request $request){

        if($request->id!=Null){
            $slide=\App\Models\Slider::find($request->id);
            if($slide==Null){ die('invalid request');}
            $title="ویرایش اسلاید";
            $backward_url=Route('adminSlides');
        }else{
            die('invalid request!');
        }


        $data=[
            'request_type'=>'edit',
            'slide'=>$slide,
            'title'=>$title,
            'post_edit_url'=>Route('adminDoEditSlide'),
            'edit_id'=>$slide->id,
            'backward_url'=>$backward_url,
        ];


        return view('admin.pages.forms.add_slide' ,$data);


    }


    public function doEditSlide(Request $request){



        $validator = Validator::make(
            $request->all(),
            [
                'edit_id'=>'required|integer|exists:slider,id',
                'title1'=>'nullable|max:255',
                'product_id'=>'nullable|integer|exists:products,id',
                'img_dir'=>'nullable|mimes:png,jpg,jpeg|max:2048',
                'img_link'=>'nullable|max:255',
                'img_order'=>'required|integer',
                'img_status'=>'required|integer',
            ]
        );


        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>$this->img_upload_error_msg]);
        }


        if($this->changeSlider($request)){
            $msg=['اسلایدر مورد نظر با موفقیت ویرایش شد'];
        }else{
            goto validator_fails;
        }




        return redirect(Route('adminSlides'))->with('messages', $msg);



    }










    public function slides2(Request $request){

        $slides=\App\Models\Slider2::select('*')
            ->with('Product')
            ->orderBy('img_order','ASC')
            ->orderBy('img_status','DESC')
            ->orderBy('created_at','DESC')
            ->get();


        $data=[
            'title'=>'لیست اسلایدها',
            'add_url'=>Route('adminAddSlide2Frm'),
            'del_url'=>Route('adminDeleteSlide2'),
            'slides'=>$slides,
            'backward_url'=>Route('adminDashboard'),
            'delete_url'=>Route('adminDeleteSlide2'),
        ];

        return view('admin.pages.lists.slides2',$data);

    }

    public function showAddSlide2Form(Request $request){

        $data=[
            'request_type'=>'add',
            'title'=>'افزودن اسلاید جدید',
            'backward_url'=>Route('adminDashboard'),
            'post_add_url'=>Route('adminSaveSlide2'),
            'product_id'=>isset($request->product_id)?$request->product_id:Null
        ];



        return view('admin.pages.forms.add_slide2' ,$data);
    }

    public function saveSlide2(Request $request){

        $validator = Validator::make($request->all(),[
            'title1'=>'nullable|max:255',
            'slide2_text'=>'nullable|max:1000',
            'slide2_color'=>'nullable|max:20',
            'product_id'=>'nullable|integer|exists:products,id',
            'img_dir'=>'nullable|mimes:png,jpg,jpeg|max:2048',
            'img_link'=>'nullable|max:255',
            'img_order'=>'required|integer',
            'img_status'=>'required|integer',
        ]);



        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>$this->img_upload_error_msg]);
        }


        if($this->changeSlider2($request)){
            $msg=['اسلاید جدید با موفقیت به سیستم اضافه شد'];
        }else{

            goto validator_fails;
        }

        return redirect(Route('adminSlides2'))->with('messages', $msg);

    }



    private function changeSlider2($request){



        $uploaded_file_dir="";
        $this->img_upload_error_msg=array();
        $to_remove_dir="";
        try {
            if($request->img_dir!=Null) {

                $file = $request->file('img_dir');
                $fileName = "";

                if ($file == null) {
                    $fileName = "";
                } else {

                    if ($file->isValid()) {
                        $fileName = time() . '_' . $file->getClientOriginalName();
                        $destinationPath = public_path() . $this->slidesPath;
                        $file->move($destinationPath, $fileName);
                        $uploaded_file_dir = $this->slidesPath.'/'.$fileName;
                        if($request->edit_id!=Null) {
                            $to_remove_dir = \App\Models\Slider2::find($request->edit_id)->img_dir;
                        }
                    } else {
                        $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
                        goto catch_block;
                    }
                }

            }

            DB::transaction(function() use($request,$uploaded_file_dir,$to_remove_dir){

                if($request->edit_id!=Null) {
                    $slider=\App\Models\Slider2::find($request->edit_id);
                }else{
                    $slider=new \App\Models\Slider2();
                    if($request->product_id!=Null){
                        $slider->product_id=$request->product_id;
                    }
                }
                $slider->title1=$request->title1;
                $slider->slide2_text=nl2br($request->slide2_text);
                $slider->slide2_color=$request->slide2_color;
                if($uploaded_file_dir!=""){
                    $slider->img_dir=$uploaded_file_dir;
                }
                $slider->img_link=$request->img_link;
                $slider->img_order=$request->img_order;
                $slider->img_status=$request->img_status;

                $slider->save();


                if($request->edit_id!=Null && $to_remove_dir!=""){
                    if(file_exists(public_path().'/'.$to_remove_dir)){
                        unlink(public_path().'/'.$to_remove_dir);
                    }
                }

            });

        }
        catch(Exception $e) {
            catch_block:
            if(file_exists(public_path().'/'.$uploaded_file_dir)){
                unlink(public_path().'/'.$uploaded_file_dir);
            }

            return false;

        }

        return true;
    }


    public function deleteSlide2(Request $request){

        if(isset($request->remove_val)){
            foreach($request->remove_val as $c_id){
                $u=\App\Models\Slider2::find($c_id);
                if($u!=Null && $u->img_dir!=null && trim($u->img_dir)!=''){
                    if(file_exists(public_path().'/'.$u->img_dir)){
                        unlink(public_path().'/'.$u->img_dir);
                    }
                }
                unset($u);
            }

            \App\Models\Slider2::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(Route('adminSlides2'))->with('messages', $msg);


    }



    public function editSlide2(Request $request){

        if($request->id!=Null){
            $slide=\App\Models\Slider2::find($request->id);
            if($slide==Null){ die('invalid request');}
            $title="ویرایش اسلاید";
            $backward_url=Route('adminSlides2');
        }else{
            die('invalid request!');
        }


        $data=[
            'request_type'=>'edit',
            'slide'=>$slide,
            'title'=>$title,
            'post_edit_url'=>Route('adminDoEditSlide2'),
            'edit_id'=>$slide->id,
            'backward_url'=>$backward_url,
        ];


        return view('admin.pages.forms.add_slide2' ,$data);


    }


    public function doEditSlide2(Request $request){



        $validator = Validator::make(
            $request->all(),
            [
                'edit_id'=>'required|integer|exists:slider2,id',
                'title1'=>'nullable|max:255',
                'slide2_text'=>'nullable|max:1000',
                'slide2_color'=>'nullable|max:20',
                'product_id'=>'nullable|integer|exists:products,id',
                'img_dir'=>'nullable|mimes:png,jpg,jpeg|max:2048',
                'img_link'=>'nullable|max:255',
                'img_order'=>'required|integer',
                'img_status'=>'required|integer',
            ]
        );


        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>$this->img_upload_error_msg]);
        }


        if($this->changeSlider2($request)){
            $msg=['اسلایدر مورد نظر با موفقیت ویرایش شد'];
        }else{
            goto validator_fails;
        }




        return redirect(Route('adminSlides2'))->with('messages', $msg);



    }



















    public function lists(Request $request){





        $lists=\App\Models\Lists::select('*')
                ->orderBy('list_order')
                ->orderBy('created_at')
                ->get();



        $resp=[
            'title'=>'لیستها',
            'backward_url'=>Route('adminDashboard'),
            'add_url'=>Route('adminAddListForm'),
            'del_url'=>Route('adminDoDeleteList'),
            'lists'=>$lists,
            'delete_url'=>Route('adminDoDeleteList'),
        ];


        return view('admin.pages.lists.lists' ,$resp);
    }

    public function showAddListForm(Request $request){



        $resp=[
            'title'=>'افزودن لیست جدید',
            'post_add_url'=>Route('adminDoAddList'),
            'request_type'=>'add',
            'backward_url'=>Route('adminLists'),
        ];
        return view('admin.pages.forms.add_list' ,$resp);
    }


    public function saveList(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'list_title'=>'nullable|max:255',
                'list_description'=>'nullable|max:1000',
                'list_order'=>'required|integer',
                'list_status'=>'required|integer',
            ]
        );


        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput();
        }


        if($this->changeLists($request)){
            $msg=['لیست مورد نظر با موفقیت افزوده شد'];
        }else{
            goto validator_fails;
        }


        return redirect(Route('adminLists'))->with('messages', $msg);

    }






    public function editList(Request $request){

        $list=\App\Models\Lists::findOrFail($request->id);

        $resp=[
            'list'=>$list,
            'request_type'=>'edit',
            'post_edit_url'=>Route('adminDoEditList'),
            'edit_id'=>$list->id,
            'title'=>'ویرایش لیست',
            'backward_url'=>Route('adminLists'),
        ];


        return view('admin.pages.forms.add_list' ,$resp);
    }


    public function doEditList(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'edit_id'=>'required|integer|exists:lists,id',
                'list_title'=>'nullable|max:255',
                'list_description'=>'nullable|max:1000',
                'list_order'=>'required|integer',
                'list_status'=>'required|integer',
            ]
        );


        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput();
        }


        if($this->changeLists($request)){
            $msg=['لیست مورد نظر با موفقیت ویرایش شد'];
        }else{
            goto validator_fails;
        }


        return redirect(Route('adminLists'))->with('messages', $msg);

    }




    public function deleteList(Request $request){

        if(isset($request->remove_val)){
            \App\Models\Lists::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];

        return redirect(url(Route('adminLists')))->with('messages', $msg);


    }





    private function changeLists($request){


        if($request->edit_id!=Null) {
            $list=\App\Models\Lists::find($request->edit_id);
        }else{
            $list=new \App\Models\Lists();
        }

        $list->list_title=$request->list_title;
        $list->list_description=nl2br($request->list_description);
        $list->list_order=$request->list_order;
        $list->list_status=$request->list_status;
        $list->save();

        return true;
    }




    public function editAdvertiseList(Request $request){

        $advertise=\App\Models\Product::findOrFail($request->product_id);

        $lists=\App\Models\Lists::where('list_status',1)
            ->orderBy('list_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();

        $data=[
            'title'=>'انتخاب لیست برای محصول',
            'request_type'=>'edit',
            'post_edit_url'=>Route('adminDoEditAdvertiseList'),
            'lists'=>$lists,
            'advertise'=>$advertise,
            'edit_id'=>$advertise->id,
            'backward_url'=>Route('adminDashboard'),
        ];

        return view('admin.pages.forms.SelectAdvertiseList',$data);


    }


    public function doEditAdvertiseList(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'lists.*' => 'integer|exists:lists,id',
                'edit_id'=>'required|exists:products,id',
            ]
        );



        if($validator->fails()){
            return redirect()->back()
                ->withInput($request->input())
                ->withErrors($validator->errors())
                ->with('messages',['ورودی های خود را بررسی کنید']);
        }



        $advertise=\App\Models\Product::findOrFail($request->edit_id);


        $advertise->Lists()->sync($request->lists);


        $msg=['لیست های مورد نظر با موفقیت بروزرسانی شدند'];
        return redirect(url(Route('adminLists')))->with('messages', $msg);


    }




    public function getListAdvertises(Request $request){
        $list=\App\Models\Lists::findOrFail($request->list_id);


        $advertises=$list->Products()
                        ->OrderBy('created_at')
                        ->orderBy('updated_at')
                        ->get();


        $resp=[
            'title'=>'محصولات متعلق به لیست '.$list->list_title,
            'backward_url'=>Route('adminLists'),
            'add_url'=>Null,
            'del_url'=>Null,
            'advertises'=>$advertises,
            'delete_url'=>Null,
        ];


        return view('admin.pages.lists.advertises' ,$resp);

    }





    public function editConfig(){

        $master_title=\App\Models\Configuration::first()->MasterTitle()->first()->value1;
        $master_website_logo=\App\Models\Configuration::first()->WebsiteLogo()->first()->value1;
        $footer_about_us=\App\Models\Configuration::first()->FooterAboutUs()->first()->value1;
        $footer_support_text=\App\Models\Configuration::first()->footerSupportText()->first()->value1;
        $footer_contact_us_title=\App\Models\Configuration::first()->FooterContactTitle()->first()->value1;
        $delivery_price=\App\Models\Configuration::first()->DeliveryPrice()->first()->value1;
        $website_description=\App\Models\Configuration::first()->WebsiteDescription()->first()->value1;
        $website_keywords=\App\Models\Configuration::first()->WebsiteKeywords()->first()->value1;




        $backward_url=url()->previous();
        $title="تنظیمات وبسایت";



        $resp=[
            'm_title'=>$master_title,
            'm_website_logo'=>$master_website_logo,
            'footer_about_us'=>$footer_about_us,
            'footer_support_text'=>$footer_support_text,
            'footer_contact_us_title'=>$footer_contact_us_title,
            'delivery_price'=>$delivery_price,
            'website_description'=>$website_description,
            'website_keywords'=>$website_keywords,
            'backward_url'=>$backward_url,
            'title'=>$title,
            'post_edit_url'=>Route('doEditConfig'),
            'request_type'=>'edit',
        ];

        return view('admin.pages.forms.configuration' ,$resp);
    }


    public function doEditConfig(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'm_title'=>'required|min:4|max:80',
                'm_website_logo'=>'nullable|max:200',
                'footer_about_us'=>'nullable|max:255',
                'footer_support_text'=>'nullable|max:255',
                'footer_contact_us_title'=>'nullable|max:255',
                'delivery_price'=>'required|integer',
                'website_description'=>'required|max:255',
                'website_keywords'=>'required|max:255',
            ]
        );


        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>$this->img_upload_error_msg]);;
        }


        if($this->changeConfig($request)){
            $msg=['پیکربندی وبسایت با موفقیت ویرایش شد'];
        }else{
            goto validator_fails;
        }


        return redirect(Route('editConfig'))->with('messages', $msg);
    }




    private function changeConfig($request){
        $uploaded_file_dir="";
        $this->img_upload_error_msg=array();
        $to_remove_dir="";
        try {
            if($request->m_website_logo!=Null) {

                $file = $request->file('m_website_logo');
                $fileName = "";

                if ($file == null) {
                    $fileName = "";
                } else {

                    if ($file->isValid()) {
                        $fileName = time() . '_' . $file->getClientOriginalName();
                        $destinationPath =  '/uploads/static';
                        $file->move(public_path() .$destinationPath, $fileName);
                        $uploaded_file_dir = $destinationPath.'/'.$fileName;

                        $to_remove_dir = \App\Models\Configuration::first()->WebsiteLogo()->first()->value1;

                    } else {
                        $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
                        goto catch_block;
                    }
                }

            }

            DB::transaction(function() use($request,$uploaded_file_dir,$to_remove_dir){


                $master_title=\App\Models\Configuration::first()->MasterTitle()->first();
                $master_title->value1=$request->m_title;
                $master_title->save();

                if($uploaded_file_dir!="") {
                    $master_website_logo = \App\Models\Configuration::first()->WebsiteLogo()->first();
                    $master_website_logo->value1 = $uploaded_file_dir;
                    $master_website_logo->save();
                }

                $FooterAboutUs=\App\Models\Configuration::first()->FooterAboutUs()->first();
                $FooterAboutUs->value1=$request->footer_about_us;
                $FooterAboutUs->save();

                $footer_support_text=\App\Models\Configuration::first()->footerSupportText()->first();
                $footer_support_text->value1=$request->footer_support_text;
                $footer_support_text->save();

                $footer_contact_us_title=\App\Models\Configuration::first()->FooterContactTitle()->first();
                $footer_contact_us_title->value1=$request->footer_contact_us_title;
                $footer_contact_us_title->save();

                $delivery_price=\App\Models\Configuration::first()->DeliveryPrice()->first();
                $delivery_price->value1=$request->delivery_price;
                $delivery_price->save();

                $website_description=\App\Models\Configuration::first()->WebsiteDescription()->first();
                $website_description->value1=nl2br($request->website_description);
                $website_description->save();

                $website_keywords=\App\Models\Configuration::first()->WebsiteKeywords()->first();
                $website_keywords->value1=$request->website_keywords;
                $website_keywords->save();


                if($to_remove_dir!=""){
                    if(file_exists(public_path().'/uploads/static/'.$to_remove_dir)){
                        unlink(public_path().'/uploads/static/'.$to_remove_dir);
                    }
                }

            });





        }
        catch(Exception $e) {
            catch_block:
            if(file_exists(public_path().$uploaded_file_dir)){
                unlink(public_path().$uploaded_file_dir);
            }

            return false;

        }

        return true;
    }





}
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertyController extends Controller
{
    public function properties(Request $request){

        if($request->parent_id==Null){
            $title="لیست خصوصیات موجود در وب سایت";

            $backward_url=Route('properties');
            $add_url=Route('showAddPropertyForm');

        }else{
            $prop_val=\App\Models\Property::find($request->parent_id);
            if($prop_val==Null){die('invalid request');}
            $title="مقادیر تعریف شده برای خصوصیت: ".$prop_val->property_title;

            $backward_url=Route('properties');
            $add_url=Route('showAddPropertyForm',$request->parent_id);
        }

        $properties=\App\Models\Property::select('*')
            ->where('parent_id','=',$request->parent_id)
            ->orderBy('property_order')
            ->orderBy('created_at')->get();

        return view('admin.pages.lists.properties' ,[
            'properties'=>$properties ,
            'parent_id'=>$request->parent_id,
            'title'=>$title,
            'backward_url'=>$backward_url,
            'add_url'=>$add_url,
            'delete_url'=>Route('deleteProperty'),

        ]);
    }

    public function showAddPropertyForm(Request $request){

        if($request->parent_id!=Null){
            $parent_prop=\App\Models\Property::find($request->parent_id);
            $title="افزودن مقدار جدید به خصوصیت ". $parent_prop->property_title;
        }else{
            $title="افزودن خصوصیت جدید به سیستم";
        }
        return view('admin.pages.forms.add_property' ,[
            'request_type'=>'add' ,
            'parent_id'=>$request->parent_id ,
            'title'=>$title,
            'backward_url'=>Route('properties',$request->parent_id),
            'post_add_url'=>Route('saveProperty'),
        ]);
    }

    public function saveProperty(Request $request){



        $this->validate($request, [
            'property_title' => 'required|min:1|max:20|unique:properties,property_title',
            'property_order'=>'required|integer',
            'property_status'=>'required|integer',
            'parent_id' => 'nullable|exists:properties,id',
        ]);


        $prop = new \App\Models\Property;
        $prop->parent_id=$request->parent_id;
        $prop->property_title=$request->property_title;
        $prop->property_status=$request->property_status;
        $prop->property_order=$request->property_order;
        $prop->save();


        if($request->parent_id==Null){
            $msg=['خصوصیت جدید با موفقیت به سیستم اضافه شد'];
        }else{
            $msg=['مقدار جدید با موفقیت اضافه شد'];
        }

        return redirect(url('admin/property/'.$request->parent_id))->with('messages', $msg);




    }


    public function deleteProperty(Request $request){

        if(isset($request->remove_val)){
            \App\Models\Property::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(Route('properties',$request->parent_id))->with('messages', $msg);
    }



    public function editProperty(Request $request){



        $prop=\App\Models\Property::where('id','=',$request->id)->first();
        if($prop==Null){
            die('invalid request!');
        }

        $parent=$prop->prop()->first();
        if($parent==Null){
            $parent_id=Null;
            $title="ویرایش خصوصیت ".$prop->property_title;
        }else{
            $parent_id=$parent->id;
            $title="ویرایش مقدار ".$prop->property_title." متعلق به خصوصیت ".$parent->property_title;
        }


        $resp=[
            'property'=>$prop,
            'edit_id'=>$prop->id,
            'request_type'=>'edit',
            'parent_id'=>$parent_id,
            'title'=>$title,
            'backward_url'=>Route('properties',$request->parent_id),
            'post_edit_url'=>Route('doEditProperty'),
        ];

        return view('admin.pages.forms.add_property' ,$resp);
    }


    public function doEditProperty(Request $request){



        $this->validate($request, [
            'edit_id' => 'required|exists:properties,id',
            'property_title' => 'required|min:1|max:20|unique:properties,property_title,'.$request->edit_id,
            'property_order'=>'required|integer',
            'property_status'=>'required|integer',
            'parent_id' => 'nullable|exists:properties,id',
        ]);



        $prop=\App\Models\Property::find($request->edit_id);

        $prop->parent_id=$request->parent_id;
        $prop->property_title=$request->property_title;
        $prop->property_status=$request->property_status;
        $prop->property_order=$request->property_order;
        $prop->save();


        $msg=[
            $prop->property_title.' با موفقیت ویرایش شد'
        ];
        return redirect(url('admin/property'))->with('messages', $msg);

    }
}

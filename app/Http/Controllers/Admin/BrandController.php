<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class BrandController extends Controller
{
    public function brands(Request $request){

        $title="لیست برندها";
        $backward_url=Route('adminDashboard');
        $add_url=Route('add-brand-form');

        $brands=\App\Models\Brand::select('*')->orderBy('created_at')->get();

        $del_url=Route('do-delete-brand');

        $resp=[
            'title'=>$title,
            'backward_url'=>$backward_url,
            'add_url'=>$add_url,
            'del_url'=>$del_url,
            'brands'=>$brands,
            'delete_url'=>Route('do-delete-brand'),
        ];


        return view('admin.pages.lists.brands' ,$resp);
    }

    public function showAddBrandForm(Request $request){

        $title="افزودن برند جدید";

        $backward_url=Route('adminBrands');

        $resp=[
            'title'=>$title,
            'post_add_url'=>Route('do-add-brand'),
            'request_type'=>'add',
            'backward_url'=>$backward_url,
        ];
        return view('admin.pages.forms.add_brand' ,$resp);
    }

    var $img_upload_error_msg='';
    var $destinationPath = '/uploads/brands';
    private function changeBrand(Request $request){
        $uploaded_file_dir="";
        $this->img_upload_error_msg=array();
        $to_remove_dir="";
        try {
            if($request->brand_img!=Null) {
                $file = $request->brand_img;
                $fileName = "";
                if ($file == null) {
                    $fileName = "";
                } else {

                    if ($file->isValid()) {
                        $fileName = time() . '_' . $file->getClientOriginalName();

                        $file->move(public_path().$this->destinationPath, $fileName);
                        $uploaded_file_dir = $this->destinationPath.'/'.$fileName;
                        if($request->edit_id!=Null) {
                            $to_remove_dir = Brand::find($request->edit_id)->brand_img;
                        }
                    } else {
                        $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
                        goto catch_block;
                    }
                }

            }

            DB::transaction(function() use($request,$uploaded_file_dir,$to_remove_dir){
                if($request->edit_id!=Null) {
                    $brand=Brand::find($request->edit_id);
                }else{
                    $brand = new Brand();
                }

                $brand->brand_title=$request->brand_title;
                if($uploaded_file_dir!=""){
                    $brand->brand_img=$uploaded_file_dir;
                }
                $brand->brand_order=$request->brand_order;
                $brand->brand_status=$request->brand_status;
                $brand->brand_slug=$request->brand_slug_corrected;
                $brand->save();

                if($request->edit_id!=Null && $to_remove_dir!=""){
                    if(file_exists(public_path().'/'.$to_remove_dir)){
                        unlink(public_path().'/'.$to_remove_dir);
                    }
                }


            });

        }
        catch(Exception $e) {
            catch_block:
            if(file_exists(public_path().'/'.$uploaded_file_dir)){
                unlink(public_path().'/'.$uploaded_file_dir);
            }
            return false;
        }

        return true;
    }

    public function saveBrand(Request $request){

        $brand_slug_corrected='';
        if(isset($request->brand_slug) && $request->brand_slug!=null){
            $brand_slug_corrected=\Helpers::make_slug($request->brand_slug);
        }



        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'brand_title' => $request->brand_title,
            'brand_img' => $request->brand_img,
            'brand_order'=>$request->brand_order,
            'brand_status'=>$request->brand_status,
            'brand_slug_corrected'=>$brand_slug_corrected,
        ]);

        $this->validate($newRequest, [
            'brand_title' => 'required|min:2|max:255|unique:brands',
            'brand_img' => 'nullable|image|max:2048',
            'brand_order'=>'required|integer',
            'brand_status'=>'required|integer',
            'brand_slug_corrected'=>'required|unique:brands,brand_slug',
        ]);


        if($this->changeBrand($newRequest)){
            $msg=['برند جدید با موفقیت اضافه شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);;
        }



        return redirect(url(Route('adminBrands')))->with('messages', $msg);


    }

    public function editBrand(Request $request){

        $brand=\App\Models\Brand::where('id','=',$request->id)->first();
        if($brand==Null){
            die('invalid request!');
        }

        $title="ویرایش  ".$brand->brand_title;

        $backward_url=Route('adminBrands');


        $resp=[
            'brand'=>$brand,
            'request_type'=>'edit',
            'post_edit_url'=>Route('do-edit-brand'),
            'edit_id'=>$brand->id,
            'title'=>$title,
            'backward_url'=>$backward_url,
        ];


        return view('admin.pages.forms.add_brand' ,$resp);
    }

    public function doEditBrand(Request $request){

        $brand_slug_corrected='';
        if(isset($request->brand_slug) && $request->brand_slug!=null){
            $brand_slug_corrected=\Helpers::make_slug($request->brand_slug);
        }

        $newRequest = new \Illuminate\Http\Request();
        $newRequest->replace([
            'brand_title' => $request->brand_title,
            'brand_img' =>  $request->brand_img,
            'brand_order'=>$request->brand_order,
            'brand_status'=>$request->brand_status,
            'brand_slug_corrected'=>$brand_slug_corrected,
            'edit_id' => $request->edit_id,
        ]);




        $this->validate($newRequest, [
            'brand_title' => 'required|min:2|max:255|unique:brands,brand_title,'.$newRequest->edit_id,
            'brand_img' => 'nullable|image|max:2048',
            'brand_order'=>'required|integer',
            'brand_status'=>'required|integer',
            'brand_slug_corrected'=>'required|unique:brands,brand_slug,'.$newRequest->edit_id,
            'edit_id' => 'required|exists:brands,id',
        ]);



        if($this->changeBrand($newRequest)){
            $msg=['برند مورد نظر با موفقیت ویرایش شد'];
        }else{
            return back()
                ->withInput()
                ->with(['messages'=>["عملیات با خطا مواجه شد"]]);;
        }

        return redirect(url(Route('adminBrands')))->with('messages', $msg);

    }

    public function deleteBrand(Request $request){



        if(isset($request->remove_val)){
            foreach($request->remove_val as $c_id){
                $u=Brand::find($c_id);
                if($u!=Null && $u->brand_img!=null && trim($u->brand_img)!=''){
                    if(file_exists(public_path().$u->brand_img)){
                        unlink(public_path().$u->brand_img);
                    }
                }
                unset($u);
            }

            Brand::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(url(Route('adminBrands')))->with('messages', $msg);


    }
}

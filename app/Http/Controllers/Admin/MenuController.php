<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    public function menus(Request $request){

        $menus=\App\Models\Menu::select('*')
            ->where('parent_id','=',$request->parent_id)
            ->where('menu_location',1)
            ->orderBy('c_order','ASC')
            ->orderBy('created_at')->get();

        if($request->parent_id==Null){
            $title="منوها";
            $add_url=url('/admin/menu/add');
            $canHasSubmenu=true;
        }else{
            $menu=\App\Models\Menu::find($request->parent_id);
            if($menu==Null){die('invalid request');}
            $title="لیست زیرمنوهای متعلق به منوی: ".$menu->m_title;
            $add_url=url('/admin/menu/add/'.$request->parent_id);
            $canHasSubmenu=false;
        }


        $backward_url=url('dashboard');
        $del_url=url('/admin/menu/delete');
        return view('admin.pages.lists.menus' ,[
            'canHasSubmenu'=>$canHasSubmenu,
            'data'=>$menus ,
            'parent_id'=>$request->parent_id,
            'title'=>$title ,
            'delete_url'=>$del_url,
            'add_url'=>$add_url,
            'backward_url'=>$backward_url
        ]);
    }

    public function showAddMenuForm(Request $request){

        if($request->parent_id!=Null){
            $parent_menu=\App\Models\Menu::find($request->parent_id);
            $title="افزودن زیر منوی جدید به: ". $parent_menu->m_title;
        }else{
            $title="افزودن منوی جدید به سیستم";
        }

        $add_url=Route('saveMenu');

        $backward_url=url('admin/menu');
        return view('admin.pages.forms.add_menu' ,[
            'request_type'=>'add' ,
            'parent_id'=>$request->parent_id , 'title'=>$title, 'post_add_url'=>$add_url , "backward_url"=>$backward_url]);
    }

    public function saveMenu(Request $request){

        $this->validate($request, [
            'm_title'=>'required|min:2|max:50',
            'parent_id'=>'nullable|exists:menus,id',
            'c_order'=>'required|integer',
            'c_status'=>'required|integer',
            'm_link'=>'nullable|max:200',
        ]);


        $menu = new \App\Models\Menu;
        $menu->m_title=$request->m_title;
        $menu->parent_id=$request->parent_id;
        $menu->c_order=$request->c_order;
        $menu->c_status=$request->c_status;
        $menu->m_link=$request->m_link;
        $menu->menu_location=1;
        $menu->save();


        if($request->parent_id==Null){
            $msg=['زیرمنوی جدید با موفقیت اضافه شد'];
        }else{
            $msg=['منوی جدید با موفقیت اضافه شد'];
        }

        return redirect(url('admin/menu/'.$request->parent_id))->with('messages', $msg);


    }


    public function deleteMenu(Request $request){

        if(isset($request->remove_val)){
            \App\Models\Menu::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(url('admin/menu/'.$request->parent_id))->with('messages', $msg);
    }



    public function editMenu(Request $request){



        $menu=\App\Models\Menu::where('id','=',$request->id)->first();
        if($menu==Null){
            die('invalid request!');
        }

        $parent=$menu->menu()->first();
        if($parent==Null){
            $parent_id=Null;
            $title="ویرایش منوی ".$menu->m_title;
        }else{
            $parent_id=$parent->id;
            $title="ویرایش زیرمنوی ".$menu->m_title." متعلق به منوی ".$parent->m_title;
        }

        $edit_url=Route('doEditMenu');
        $backward_url=url('admin/menu');
        $resp=[
            'data'=>$menu,
            'request_type'=>'edit',
            'parent_id'=>$parent_id,
            'title'=>$title,
            'edit_id'=>$request->id,
            'post_edit_url'=>$edit_url,
            "backward_url"=>$backward_url,
        ];

        return view('admin.pages.forms.add_menu' ,$resp);
    }


    public function doEditMenu(Request $request){



        $this->validate($request, [
            'edit_id' => 'required|exists:menus,id',
            'm_title'=>'required|min:2|max:50',
            'parent_id'=>'nullable|exists:menus,id',
            'c_order'=>'required|integer',
            'c_status'=>'required|integer',
            'm_link'=>'nullable|max:200',
        ]);



        $menu=\App\Models\Menu::find($request->edit_id);

        $menu->m_title=$request->m_title;
        $menu->c_order=$request->c_order;
        $menu->c_status=$request->c_status;
        $menu->m_link=$request->m_link;
        $menu->save();


        $msg=[
            $menu->m_title.' با موفقیت ویرایش شد'
        ];
        return redirect(url('admin/menu/'.$menu->parent_id))->with('messages', $msg);

    }





    public function footerLinks(Request $request){

        $menus=\App\Models\Menu::select('*')
            ->where('parent_id','=',$request->parent_id)
            ->where('menu_location',2)
            ->orderBy('c_order','ASC')
            ->orderBy('created_at')->get();

        if($request->parent_id==Null){
            $title="لینک های فوتر";
            $add_url=Route('showAddFooterLinkForm');
            $canHasSubmenu=true;
        }else{
            $menu=\App\Models\Menu::find($request->parent_id);
            if($menu==Null){die('invalid request');}
            $title="لیست زیرمنوهای متعلق به منوی: ".$menu->m_title;
            $add_url=Route('showAddFooterLinkForm',$request->parent_id);
            $canHasSubmenu=false;
        }


        $backward_url=url('dashboard');
        $del_url=Route('deleteFooterLink');
        return view('admin.pages.lists.menus' ,[
            'canHasSubmenu'=>$canHasSubmenu,
            'data'=>$menus ,
            'parent_id'=>$request->parent_id,
            'title'=>$title ,
            'delete_url'=>$del_url,
            'add_url'=>$add_url,
            'backward_url'=>$backward_url
        ]);
    }

    public function showAddFooterLinkForm(Request $request){

        if($request->parent_id!=Null){
            $parent_menu=\App\Models\Menu::find($request->parent_id);
            $title="افزودن زیر منوی جدید به: ". $parent_menu->m_title;
        }else{
            $title="افزودن لینک جدید به فوتر";
        }

        $add_url=Route('saveFooterLink');

        $backward_url=url()->previous();
        return view('admin.pages.forms.add_menu' ,[
            'request_type'=>'add' ,
            'parent_id'=>$request->parent_id , 'title'=>$title, 'post_add_url'=>$add_url , "backward_url"=>$backward_url]);
    }

    public function saveFooterLink(Request $request){

        $this->validate($request, [
            'm_title'=>'required|min:2|max:50',
            'parent_id'=>'nullable|exists:menus,id',
            'c_order'=>'required|integer',
            'c_status'=>'required|integer',
            'm_link'=>'nullable|max:200',
        ]);


        $menu = new \App\Models\Menu;
        $menu->m_title=$request->m_title;
        $menu->parent_id=$request->parent_id;
        $menu->c_order=$request->c_order;
        $menu->c_status=$request->c_status;
        $menu->m_link=$request->m_link;
        $menu->menu_location=2;
        $menu->save();

        if($request->parent_id==Null){
            $msg=['زیرمنوی جدید با موفقیت به فوتر اضافه شد'];
        }else{
            $msg=['منوی جدید با موفقیت به فوتر اضافه شد'];
        }
        return redirect(Route('footerLinks',$request->parent_id))->with('messages', $msg);

    }


    public function deleteFooterLink(Request $request){

        if(isset($request->remove_val)){
            \App\Models\Menu::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(Route('footerLinks'.$request->parent_id))->with('messages', $msg);
    }



    public function editFooterLink(Request $request){

        $menu=\App\Models\Menu::findOrFail($request->id);
        $parent=$menu->menu()->first();
        if($parent==Null){
            $parent_id=Null;
            $title="ویرایش منوی ".$menu->m_title;
        }else{
            $parent_id=$parent->id;
            $title="ویرایش زیرمنوی ".$menu->m_title." متعلق به منوی ".$parent->m_title;
        }

        $edit_url=Route('doEditFooterLink');
        $backward_url=url()->previous();
        $resp=[
            'data'=>$menu,
            'request_type'=>'edit',
            'parent_id'=>$parent_id,
            'title'=>$title,
            'edit_id'=>$request->id,
            'post_edit_url'=>$edit_url,
            "backward_url"=>$backward_url,
        ];

        return view('admin.pages.forms.add_menu' ,$resp);
    }


    public function doEditFooterLink(Request $request){

        $this->validate($request, [
            'edit_id' => 'required|exists:menus,id',
            'm_title'=>'required|min:2|max:50',
            'parent_id'=>'nullable|exists:menus,id',
            'c_order'=>'required|integer',
            'c_status'=>'required|integer',
            'm_link'=>'nullable|max:200',
        ]);



        $menu=\App\Models\Menu::find($request->edit_id);

        $menu->m_title=$request->m_title;
        $menu->c_order=$request->c_order;
        $menu->c_status=$request->c_status;
        $menu->m_link=$request->m_link;
        $menu->save();


        $msg=[
            $menu->m_title.' با موفقیت ویرایش شد'
        ];
        return redirect(Route('footerLinks',$menu->parent_id))->with('messages', $msg);

    }


}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostImages;
use App\Models\PCategory;
use Validator;
use Auth;
use DB;
use \App\Models\Note;

class PostController extends Controller
{
    public function showPosts(){
        $posts=Post::OrderBy('created_at')->orderBy('updated_at')->get();
        $backward_url=Route('adminDashboard');
        $add_url=Route('addpost');


        $data=[
            'title'=>'لیست مطالب ثبت شده',
            'add_url'=>$add_url,
            'del_url'=>Route('deletePost'),
            'posts'=>$posts,
            'backward_url'=>$backward_url,
            'delete_url'=>Route('deletePost'),
        ];

        return view('admin.pages.lists.posts',$data);
    }
    public function showaddpostform(){



        $data=[
            'request_type'=>'add',
            'title'=>'افزودن محصول جدید',
            'backward_url'=>Route('PostsList'),
            'post_add_url'=>Route('doAddPost'),
        ];
        return view('admin.pages.forms.add_post' ,$data);

    }
    var $upload_dir='/uploads/blog/';
    public function savePost(Request $request){
        if(isset($request->post_slug) && $request->post_slug!=null){
            $request->post_slug = \Helpers::make_slug($request->post_slug);
        }
        $newRequest=[
            'newImg.*' => $request->newImg,
            'oldImg.*' => $request->oldImg,
            'post_title'=>$request->post_title,
            'short_text'=>nl2br($request->short_text),
            'main_text'=>nl2br($request->main_text),
            'post_status'=>$request->post_status,
            'post_tags'=>$request->post_tags,
            'post_slug'=>$request->post_slug,
            'meta_keywords'=>$request->meta_keywords,
        ];



        $validator = Validator::make(
            $newRequest,
            [
                'newImg.*' => 'mimes:png,jpg,jpeg|max:2048',
                'oldImg.*' => 'integer|exists:product_images,id',
                'post_title'=>'required|max:140',
                'short_text'=>'required|max:255',
                'post_tags'=>'required|max:255',
                'meta_keywords'=>'required|max:255',
                'post_slug'=>'required|max:255|unique:posts,post_slug,'.$request->edit_id,
                'main_text'=>'required',
                'post_status'=>'required|integer',
            ]
        );

        if($validator->fails()){
            return redirect()->back()
                ->withInput($request->input())
                ->withErrors($validator->errors())
                ->with('messages',['ورودی های خود را بررسی کنید']);
        }


        $uploaded_files_dir=array();
        $main_img_dir=Null;
        $this->img_upload_error_msg=array();
        try {

            if($request->newImg!=Null) {
                foreach ($request->newImg as $key => $uimg) {
                    $file = $request->file('newImg.' . $key);
                    $fileName = "";
                    if ($file == null) {
                        $fileName = "";
                    } else {
                        if ($file->isValid()) {
                            $fileName = Auth::guard('admin')->user()->id.str_replace(' ', '', time()).rand(1000,9999). '.' . $file->guessClientExtension();
                            $destinationPath = $this->upload_dir;
                            $imgConf=[
                                'imgType'=>'postImage',
                                'imgObject'=>$file,
                                'resultDir'=>public_path().$destinationPath.'/'.$fileName,
                            ];
                            \Helpers::save_img($imgConf['imgType'],$imgConf['imgObject'],$imgConf['resultDir']);
                            //$file->move(public_path().$destinationPath, $fileName);
                            $uploaded_files_dir[] = $destinationPath.'/'.$fileName;

                            if($main_img_dir==Null){
                                $destinationPath = $this->upload_dir;
                                $imgConf=[
                                    'imgType'=>'postImageThumbnail',
                                    'imgObject'=>$file,
                                    'resultDir'=>public_path().$destinationPath.'/'.'thumb'.$fileName,
                                ];
                                \Helpers::save_img($imgConf['imgType'],$imgConf['imgObject'],$imgConf['resultDir']);
                                $main_img_dir = $destinationPath.'/'.'thumb'.$fileName;
                            }


                        } else {
                            $this->img_upload_error_msg = 'آپلود یکی از تصاویر ناموفق بود';
                            goto catch_block;
                        }
                    }
                }
            }

            $removableOldImgID=array();
            $removableOldImgDir=array();

            if(isset($request->edit_id) && $request->edit_id!=Null){

                $ad=Post::where('id',$request->edit_id)->first();
                if($ad==Null)die('invalid_request!');

                foreach($ad->PostImages()->get() as $cimg){
                    $exists=false;
                    if($request->oldImg!=Null){
                        foreach($request->oldImg as $oimg){
                            if($oimg==$cimg->id){
                                $exists=True;
                                break;
                            }
                        }
                    }
                    if(!$exists){
                        $removableOldImgID[]=$cimg->id;
                        $removableOldImgDir[]=$cimg->image_dir;
                    }
                }

                if($main_img_dir!=Null && $ad->main_img_dir!=Null){
                    $removableOldImgDir[]=$ad->main_img_dir;
                }

            }else{
                $ad=new Post();
            }


            DB::transaction(function() use($request,$uploaded_files_dir,$main_img_dir,$removableOldImgID,$removableOldImgDir,$ad){

                $ad->post_title=$request->post_title;
                if($main_img_dir!=Null){
                    $ad->main_img_dir=$main_img_dir;
                }
                $ad->main_text=$request->main_text;
                $ad->post_status=$request->post_status;
                $ad->short_text=$request->short_text;
                $ad->meta_keywords=$request->meta_keywords;
                $ad->post_slug=$request->post_slug;
                $ad->post_tags=$request->post_tags;

                if(!isset($request->edit_id) || $request->edit_id==Null){
                    $ad->created_by = Auth::guard('admin')->user()->id;
                }
                $ad->save();

                $img_data=array();
                foreach($uploaded_files_dir as $key=>$ufd){
                    $img_data[]=new \App\Models\PostImages  ([
                        'post_id'=>$ad->id,
                        'image_dir'=>$ufd,
                        'image_order'=>$key,
                    ]);
                }

                $ad->PostImages()->whereIn('id',$removableOldImgID)->delete();

                $ad->PostImages()->saveMany($img_data);

                if($request->edit_id!=Null){
                    foreach($removableOldImgDir as $uimg_dir){
                        if(file_exists(public_path().'/'.$uimg_dir)){
                            unlink(public_path().'/'.$uimg_dir);
                        }
                    }
                }

            });

        }
        catch(Exception $e) {
            catch_block:
            foreach($uploaded_files_dir as $uimg_dir){
                if(file_exists(public_path().'/'.$uimg_dir)){
                    unlink(public_path().'/'.$uimg_dir);
                }
            }
            if(file_exists(public_path().'/'.$main_img_dir)){
                unlink(public_path().'/'.$main_img_dir);
            }

            return redirect()->back()
                ->withInput($request->input())
                ->with('messages',['عملیات با شکست مواجه شد، دوباره تلاش کنید']);

        }
        if(isset($request->edit_id) && $request->edit_id!=Null){
            $msg=["محصول مورد نظر با موفقیت ویرایش شد"];
        }else{
            $msg=["محصول جدید با موفقیت افزوده شد"];
        }
        return redirect(url(Route('adminEditPostCategory',$ad->id)))->with('messages', $msg);
    }
    public function editPost(Request $request){

        $post = Post::where('id',$request->id)->first();
        if(empty($post))die('invalid_request!');
        $data=[
            'title'=>'ویرایش مطلب',
            'request_type'=>'edit',
            'post_edit_url'=>Route('doEditPost'),
            'post'=>$post,
            'edit_id'=>$post->id,
            'backward_url'=>Route('PostsList'),
        ];

        return view('admin.pages.forms.add_post' ,$data);

    }
    public function deletePost(Request $request){

        if(isset($request->remove_val)){
            foreach($request->remove_val as $ads_id){
                $ads=Post::find($ads_id);
                if($ads!=Null && $ads->PostImages()->get()!=null){
                    foreach ($ads->PostImages()->get() as $aimg){
                        if(trim($aimg->img_dir)!=''){
                            if(file_exists(public_path().$aimg->img_dir)){
                                unlink(public_path().$aimg->img_dir);
                            }
                        }
                    }


                    if($ads->main_img_dir!=Null && file_exists(public_path().$ads->main_img_dir)){
                        unlink(public_path().$ads->main_img_dir);
                    }



                }
                unset($ads);
            }

            Product::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(Route('PostsList'))->with('messages', $msg);
    }


    public function editPostCategory(Request $request){

        $post=Post::where('id',$request->post_id)
            ->first();
        if($post==Null)die('invalid request!');

        $masterCtgs=\App\Models\PCategory::where('category_status',1)
            ->where('parent_id',Null)
            ->orderBy('category_order','ASC')
            ->orderBy('created_at','DESC')
            ->get();

        $data=[
            'title'=>'انتخاب دسته بندی',
            'request_type'=>'edit',
            'post_edit_url'=>Route('adminDoEditPostCategory'),
            'masterCtgs'=>$masterCtgs,
            'post'=>$post,
            'edit_id'=>$post->id,
            'backward_url'=>Route('editPost',$post->id),
        ];

        return view('admin.pages.forms.SelectPostCategory',$data);


    }


    public function doEditPostCategory(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'ctg.*' => 'integer|exists:pcategories,id',
                'edit_id'=>'required|exists:posts,id',
            ]
        );



        if($validator->fails()){
            return redirect()->back()
                ->withInput($request->input())
                ->withErrors($validator->errors())
                ->with('messages',['ورودی های خود را بررسی کنید']);
        }





        $post=Post::where('id',$request->edit_id)
            ->first();
        if($post==Null)die('invalid request!');


        $post->Categories()->sync($request->ctg);


        $msg=['دسته بندی محصول مورد نظر با موفقیت بروزرسانی شد'];
        return redirect(url(Route('PostsList')))->with('messages', $msg);


    }




    public function showNotes(Request $request){

        $notes=Note::OrderBy('created_at')->orderBy('updated_at')->get();
        $backward_url=url()->previous();
        $add_url=Route('addNote');


        $data=[
            'title'=>'لیست مطالب وبسایت',
            'add_url'=>$add_url,
            'del_url'=>Route('deleteNote'),
            'notes'=>$notes,
            'backward_url'=>$backward_url,
            'delete_url'=>Route('deleteNote'),
        ];

        return view('admin.pages.lists.notes',$data);

    }



    public function showAddNoteForm(Request $request){

        $data=[
            'request_type'=>'add',
            'title'=>'افزودن مطلب جدید',
            'backward_url'=>url()->previous(),
            'post_add_url'=>Route('doAddNote'),
        ];
        return view('admin.pages.forms.add_note' ,$data);

    }




    public function editNote(Request $request){

        $note = Note::where('id',$request->id)->first();
        if(empty($note))die('invalid_request!');


        $data=[
            'title'=>'ویرایش مطلب',
            'request_type'=>'edit',
            'post_edit_url'=>Route('doEditNote'),
            'note'=>$note,
            'edit_id'=>$note->id,
            'backward_url'=>url()->previous(),
        ];
        return view('admin.pages.forms.add_note' ,$data);
    }

    public function saveNote(Request $request){

        $newRequest=[
            'note_title'=>$request->note_title,
            'note_slug'=>\Helpers::make_slug($request->note_slug),
            'note_body'=> $request->note_body,
            'note_short_desc'=>$request->note_short_desc,
            'note_order'=>$request->note_order,
            'note_status'=>$request->note_status,
        ];




        $validator = Validator::make(
            $newRequest,
            [
                'note_title'=>'required|max:255',
                'note_slug'=>'required|max:255|unique:notes,note_slug'.(((isset($request->edit_id) && $request->edit_id!=Null))?(','.$request->edit_id):Null) ,
                'note_body'=>'required',
                'note_short_desc'=>'required|max:255',
                'note_order'=>'required|integer',
                'note_status'=>'required|integer|between:0,1',
            ]
        );



        if($validator->fails()){
            return redirect()->back()
                ->withInput($request->input())
                ->withErrors($validator->errors())
                ->with('messages',['ورودی های خود را بررسی کنید']);
        }

        try {
            if(isset($request->edit_id) && $request->edit_id!=Null){
                $note=Note::where('id',$request->edit_id)->first();
                if($note==Null)die('invalid_request!');
            }else{
                $note=new Note();
            }


            DB::transaction(function() use($request,$newRequest,$note){


                $note->note_body=nl2br($request->note_body);
                $note->note_short_desc=nl2br($request->note_short_desc);
                $note->note_title=$request->note_title;
                $note->note_slug=$newRequest['note_slug'];
                $note->note_order=$request->note_order;
                $note->note_status=$request->note_status;
                $note->save();

            });

        }
        catch(Exception $e) {
            catch_block:

            return redirect()->back()
                ->withInput($request->input())
                ->with('messages',['عملیات با شکست مواجه شد، دوباره تلاش کنید']);

        }
        if(isset($request->edit_id) && $request->edit_id!=Null){
            $msg=["مطلب مورد نظر با موفقیت ویرایش شد"];
        }else{
            $msg=["مطلب جدید با موفقیت افزوده شد"];
        }
        return redirect(url(Route('notesList')))->with('messages', $msg);
    }




    public function deleteNote(Request $request){

        if(isset($request->remove_val)){
            Note::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(Route('notesList'))->with('messages', $msg);
    }


}

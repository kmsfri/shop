<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;


class AdminController extends Controller
{
    var $img_upload_error_msg='';

    public function showAdmins(Request $request){


        $admins=\App\Models\AdminUser::select('*')
            ->orderBy('user_status','DESC')
            ->orderBy('created_at','ASC')
            ->get();

        $title="لیست کاربران بخش مدیریت وب سایت";
        $backward_url=url('/admin/dashboard');
        $add_url=url('/admin/user/admin/add');

        $resp=[
            'admins'=>$admins,
            'title'=>$title,
            'backward_url'=>$backward_url,
            'add_url'=>$add_url,
            'delete_url'=>Route('deleteAdmin'),
        ];



        return view('admin.pages.lists.admin_users' ,$resp);
    }



    public function showAddAdminForm(Request $request){

        $title="افزودن کاربر جدید به بخش مدیریت وب سایت";
        $backward_url=url('/admin/user/admin');


        $data=[
            'request_type'=>'add',
            'title'=>$title,
            'backward_url'=>$backward_url,
            'post_add_url'=>Route('editAdmin'),

        ];
        return view('admin.pages.forms.add_admin' ,$data);
    }



    public function saveAdmin(Request $request){


        $validator = Validator::make($request->all(),[
            'user_name'=>'required|min:3|max:100|unique:admin_users',
            'email'=>'nullable|email|max:100',
            'password'=>'required|min:4|max:40',
            'user_title'=>'required|min:3|max:40',
            'description'=>'max:1000',
            'user_status'=>'required|integer',
            'avatar_dir' => 'mimes:png,jpg,jpeg|max:1048',
        ]);



        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>$this->img_upload_error_msg]);;
        }



        if($this->changeAdminUser($request)){
            $msg=['کاربر جدید با موفقیت به بخش مدیریت وب سایت اضافه شد'];
        }else{

            goto validator_fails;
        }

        return redirect(url('admin/user/admin'))->with('messages', $msg);




    }




    private function changeAdminUser($request){
        $uploaded_file_dir="";
        $this->img_upload_error_msg=array();
        $to_remove_dir="";
        try {
            if($request->avatar_dir!=Null) {

                $file = $request->file('avatar_dir');
                $fileName = "";

                if ($file == null) {
                    $fileName = "";
                } else {

                    if ($file->isValid()) {
                        $fileName = time() . '_' . $file->getClientOriginalName();
                        $destinationPath = public_path() . '/uploads/users/admin';

                        $file->move($destinationPath, $fileName);
                        $uploaded_file_dir = $fileName;
                        if($request->edit_id!=Null) {
                            $to_remove_dir = \App\Models\AdminUser::find($request->edit_id)->avatar_dir;
                        }
                    } else {
                        $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
                        goto catch_block;
                    }
                }

            }

            DB::transaction(function() use($request,$uploaded_file_dir,$to_remove_dir){


                if($request->edit_id!=Null) {

                    $u=\App\Models\AdminUser::find($request->edit_id);
                }else{
                    $u=new \App\Models\AdminUser();
                }



                $u->user_name=$request->user_name;
                $u->email=$request->email;
                if(trim($request->password)!="**||password-no-changed") {
                    $u->password = bcrypt($request->password);
                }
                $u->user_title=$request->user_title;
                if($uploaded_file_dir!=""){
                    $u->avatar_dir=$uploaded_file_dir;
                }
                $u->description=nl2br($request->description);
                $u->user_status=(int)$request->user_status;
                $u->save();


                if($request->edit_id!=Null && $to_remove_dir!=""){
                    if(file_exists(public_path().'/uploads/users/admin/'.$to_remove_dir)){
                        unlink(public_path().'/uploads/users/admin/'.$to_remove_dir);
                    }
                }

            });





        }
        catch(Exception $e) {
            catch_block:
            if(file_exists(public_path().'/uploads/users/admin/'.$uploaded_file_dir)){
                unlink(public_path().'/uploads/users/admin/'.$uploaded_file_dir);
            }

            return false;

        }

        return true;
    }






    public function deleteAdmin(Request $request){

        if(isset($request->remove_val)){
            foreach($request->remove_val as $c_id){
                $u=\App\Models\AdminUser::find($c_id);
                if($u!=Null && $u->avatar_dir!=null && trim($u->avatar_dir)!=''){
                    if(file_exists(public_path().'/uploads/users/admin/'.$u->avatar_dir)){
                        unlink(public_path().'/uploads/users/admin/'.$u->avatar_dir);
                    }
                }
                unset($$u);
            }

            \App\Models\AdminUser::destroy($request->remove_val);
        }
        $msg=['موارد انتخاب شده با موفقیت حذف شدند'];
        return redirect(url('admin/user/admin'))->with('messages', $msg);
    }






    public function editAdmin(Request $request){

        if($request->id!=Null){
            $u=\App\Models\AdminUser::find($request->id);
            if($u==Null){ die('invalid request');}

            $title="ویرایش اطلاعات کاربر بخش مدیریت: ".$u->user_title;
            $backward_url=url('/admin/user/admin');
        }else{
            die('invalid request!');
        }


        $data=[
            'request_type'=>'edit',
            'u'=>$u,
            'title'=>$title,
            'backward_url'=>$backward_url,
            'post_edit_url'=>Route('doEditAdmin'),
            'edit_id'=>$u->id,
        ];


        return view('admin.pages.forms.add_admin' ,$data);

    }








    public function doEditAdmin(Request $request){



        $validator = Validator::make(
            $request->all(),
            [
                'edit_id'=>'required|integer|exists:admin_users,id',
                'user_name'=>'required|min:2|max:20|unique:admin_users,user_name,'.$request->edit_id,
                'email'=>'nullable|email|max:100',
                'password'=>'required|min:4|max:40',
                'user_title'=>'required|min:3|max:40',
                'description'=>'max:1000',
                'user_status'=>'required|integer',
                'avatar_dir' => 'mimes:png,jpg,jpeg|max:1048',
            ]
        );


        if ($validator->fails()) {
            validator_fails:
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['messages'=>$this->img_upload_error_msg]);;
        }


        if($this->changeAdminUser($request)){
            $msg=['کاربر مورد نظر با موفقیت ویرایش شد'];
        }else{

            goto validator_fails;
        }




        return redirect(url('admin/user/admin'))->with('messages', $msg);

    }























    public function editSectionPermit(Request $request){


        $user=\App\Models\AdminUser::find($request->id);
        if($user==Null){
            return "invalid";
        }

        $route_g=\App\Models\AdminRoutes::orderBy('r_order','ASC')->orderBy('created_at','ASC')->orderBy('updated_at')->get();
        $routes_g_user=clone $user;
        $routes_g_user=$routes_g_user->route;
        foreach ($route_g as $key => $c){
            if($routes_g_user!=null && $routes_g_user->contains($c->id)){
                $route_g[$key]->permited=1;
            }else{
                $route_g[$key]->permited=0;
            }
        }

        $route_g = $route_g->sortByDesc(function($route)
        {
            return $route->permited;
        });

        $backward_url=url('/admin/user/admin');

        $resp=[
            'routes'=>$route_g,
            'request_type'=>'edit',
            'title'=>'ویرایش مجوزهای کاربر: '.$user->user_name,
            'u_id'=>$user->id,
            'backward_url'=>$backward_url,
        ];

        return view('admin.pages.add_admin_section_permission' ,$resp);
    }






    public function doEditSectionPermit(Request $request){

        $validator = Validator::make($request->all(),[
            'u_id'=>'required|integer|exists:admin_users,id',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $user=\App\Models\AdminUser::find($request->u_id);

        (count($request->routes_assign_to_user)>0)? $assign_routes=$request->routes_assign_to_user : $assign_routes=array();
        $user->route()->sync($assign_routes);


        $msg=[
            'مجوزهای کاربر: '.$user->user_name.' با موفقیت ویرایش شد.'
        ];


        return redirect(url('admin/user/admin'))->with('messages', $msg);

    }
















    public function dashboard(Request $request){




        $now_datetime= \Carbon\Carbon::now()->toDateTimeString();
        $now_datetime_arr=explode(' ',$now_datetime);

        $dashData['nowTime']=$now_datetime_arr[1];

        $t_date=\Helpers::convert_date_g_to_j($now_datetime_arr[0],true);
        $t_date=explode('/',$t_date);
        $month=\Helpers::get_equal_str_month($t_date[1]);
        $dashData['nowDate']=$t_date[2].' '.$month.' '.$t_date[0];


        return view('admin.pages.dashboard' ,$dashData);

    }
}

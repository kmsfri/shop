$(document).ready(function () {
	'use strict';
	$('.overlay').on('click', function () {
		$('#sidebar').removeClass('active');
		$('.overlay').fadeOut();
	});
	$('#sidebarCollapse').on('click', function () {
		$('#sidebar').addClass('active');
		$(".overlay").fadeIn();
		$('.collapse.in').toggleClass('in');
		$('a[aria-expanded=true]').attr('aria-expanded', 'false');
	});
});

$(document).ready(function () {
	'use strict';
	$('.dropdown-toggle').dropdown();
});

function readURL(input) {
	'use strict';
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
	'use strict';
    readURL(this);
});

$(document).ready(function() {
	'use strict';
	$(".carousel").swiperight(function() {
		$(this).carousel('prev');
	});
	$(".carousel").swipeleft(function() {
		$(this).carousel('next');
	});
	
	$("#carouselItem").swiperight(function() {
		$(this).carousel('prev');
	});
	$("#carouselItem").swipeleft(function() {
		$(this).carousel('next');
	});
});

function myFunction(x) {
	'use strict';
    x.classList.toggle("mdi-heart-outline");
}

$('#carouselItem').carousel({
    pause: true,
    interval: false
});



$(document).ready(function() {
	'use strict';
  $('select').niceSelect();
});





$(document).ready(function() {
	'use strict';
	var descMinHeight = 120;
	var desc = $('.desc');
	var descWrapper = $('.desc-wrapper');
	if (desc.height() > descWrapper.height()) {
		$('.more-info').show();
	}
	$('.more-info').click(function() {
		var fullHeight = $('.desc').height();
		if ($(this).hasClass('expand')) {
			$('.desc-wrapper').animate({'height': descMinHeight}, 'slow');
		}
		else {
			$('.desc-wrapper').css({'height': descMinHeight, 'max-height': 'none'}).animate({'height': fullHeight}, 'slow');
		}
		$(this).toggleClass('expand');
		return false;
	});
});





jQuery('.quantity').each(function() {
	'use strict';
  var spinner = jQuery(this),
	input = spinner.find('input[type="number"]'),
	btnUp = spinner.find('.quantity-up'),
	btnDown = spinner.find('.quantity-down'),
	min = input.attr('min'),
	max = input.attr('max');

  btnUp.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue >= max) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue + 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");
  });

  btnDown.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue <= min) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue - 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");
  });

});


$(document).ready(function(){
	'use strict';
    $(".btn-del").click(function(){
        $(".cardidel").remove();
    });
});


$('.btn-next').on('click', function () {
	'use strict';
	$('.flip-container').addClass('flipper');
	$('.pc-log').addClass('bg-ver');
});
$('.btn-pre').on('click', function () {
	'use strict';
	$('.flip-container').removeClass('flipper');
	$('.pc-log').removeClass('bg-ver');
});

















document.querySelector('.inum').addEventListener("keypress", function(evt) {
	'use strict';
    if (evt.which < 48 || evt.which > 57) {
        evt.preventDefault();
    }
});
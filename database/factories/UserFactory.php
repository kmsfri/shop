<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\ShoperUser::class, function (Faker $faker) {
    return [
        'mobile_number' => $faker->unique()->e164PhoneNumber,
        'password' => bcrypt('1111'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Shop::class, function (Faker $faker){
    return [
        //'shoper_user_id' => random_int(\App\Models\ShoperUser::min('id'), \App\Models\ShoperUser::max('id')),
        'shoper_user_id' => App\Models\ShoperUser::all(['id'])->random(),
        'shop_title' => $faker->realText(140),
        'shop_address' => $faker->address,
        'shop_description' => $faker->paragraph,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'mobile_number' => $faker->unique()->e164PhoneNumber,
        'password' => bcrypt('1111'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Models\ShopCategory::class, function (Faker $faker){
    return [
        'shop_id' => App\Models\Shop::all(['id'])->random(),
        'category_id' => App\Models\Category::all(['id'])->random(),
    ];
});



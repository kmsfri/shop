<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoperAdminTicketMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoper_admin_ticket_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shoper_admin_ticket_id')->unsigned();
            $table->foreign('shoper_admin_ticket_id')->references('id')->on('shoper_admin_tickets')->onDelete('cascade')->onUpdate('cascade');
            $table->smallInteger('sender')->unsigned()->comment='0:shoper_user - 1:user';
            $table->string('messsage_text',280);
            $table->boolean('seen')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoper_admin_ticket_messages');
    }
}

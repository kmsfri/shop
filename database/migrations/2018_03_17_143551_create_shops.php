<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shoper_user_id')->unsigned();
            $table->foreign('shoper_user_id')->references('id')->on('shoper_users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('shop_title');
            $table->string('shop_address',500);
            $table->text('shop_description');
            $table->boolean('shop_status')->default(1)->comment='0:disable - 1:active';
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}

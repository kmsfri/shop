<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvertiseImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertise_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('advertise_id')->unsigned();
            $table->foreign('advertise_id')->references('id')->on('advertises')->onDelete('cascade')->onUpdate('cascade');
            $table->string('image_dir');
            $table->string('image_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertise_images');
    }
}

<?php

Route::group(['middleware'=>['initCommonData']],function() {



    Route::group(['prefix'=>'user'],function() {
        // Authentication Routes...
        Route::get('login', 'User\Auth\Login_ResgisterController@showUserLoginForm')->name('user-login');
        Route::post('login', 'User\Auth\Login_ResgisterController@Login')->name('do-user-login');
        Route::get('register', 'User\Auth\Login_ResgisterController@showUserRegisterForm')->name('user-register');
        Route::post('register', 'User\Auth\Login_ResgisterController@Register')->name('do-user-register');
        Route::get('forget', 'User\Auth\Login_ResgisterController@showUserForgetForm')->name('user-forget');
        Route::post('forget', 'User\Auth\Login_ResgisterController@Forget')->name('do-user-forget');
        Route::get('logout', 'User\Auth\Login_ResgisterController@logout')->name('do-user-logout');

        Route::group(['middleware'=>['auth:user']],function() {


            Route::post('/cart/add', 'Landing\Panel\OrderController@addToCart')->name('addProductToCart');
            Route::get('/cart/show', 'Landing\Panel\OrderController@showCart')->name('showCart');

            Route::post('/cart/update', 'Landing\Panel\OrderController@updateCart')->name('updateCart');
            Route::post('/cart/clear', 'Landing\Panel\OrderController@clearCart')->name('clearCart');
            Route::get('/cart/remove/{cartItemId}', 'Landing\Panel\OrderController@removeFromCart')->name('removeFromCart');


            Route::get('dashboard', 'Landing\Panel\GeneralController@showDashboard')->name('showUserDashboard');
            Route::post('rate/product/{id}', 'Landing\GeneralController@rate_product')->name('rate_product');
            Route::get('wish/product/{id}', 'Landing\GeneralController@add_to_wish')->name('add_to_wish');
            Route::get('wish/list', 'Landing\Panel\GeneralController@getWishProductList')->name('showWishProductList');

            Route::get('info', 'Landing\Panel\GeneralController@showUserInfoForm')->name('showUserInfoForm');
            Route::post('info', 'Landing\Panel\GeneralController@saveUserInfo')->name('saveUserInfo');

            Route::get('password', 'Landing\Panel\GeneralController@showChangePasswordForm')->name('showChangePasswordForm');
            Route::post('password', 'Landing\Panel\GeneralController@changePassword')->name('changePassword');

            Route::get('payments', 'Landing\Panel\OrderController@showPayments')->name('showUserPayments');

            Route::get('order/history', 'Landing\Panel\OrderController@showOrderHistory')->name('showUserOrderHistory');
            Route::get('order/details/{order_id}', 'Landing\Panel\OrderController@showOrderDetails')->name('showUserOrderDetails');
        });





    });


    //Blog Website


        Route::get('/blogs', 'Web\BlogController@showblog')->name('showblog');
        Route::get('/blog/{slug}', 'Web\BlogController@showsingleblog')->name('showsingleblog');
        Route::get('/blogs/category/{slug}', 'Web\BlogController@blogcat')->name('blogcat');

        Route::get('/note/{note_slug}', 'Landing\GeneralController@showNotePage')->name('showNotePage');

    Route::get('/Search', 'Web\BlogController@search')->name('searchblog');


    Route::get('/', 'Landing\GeneralController@showMainPage')->name('mainPage');
    Route::get('/product/{product_slug}', 'Landing\ProductController@showProductPage')->name('showProductPage');

    Route::get('/contact', 'Landing\GeneralController@showContactUsForm')->name('showContactUsForm');
    Route::post('/contact', 'Landing\GeneralController@saveContactUs')->name('saveContactUs');

    Route::get('/products', 'Landing\ProductController@products')->name('products');
    Route::get('/products/category/{category_slug}', 'Landing\ProductController@productscat')->name('productscat');
    Route::get('/products/brand/{category_slug}', 'Landing\ProductController@productsbrand')->name('productsbrand');
    Route::get('/products/search', 'Landing\ProductController@productssearch')->name('productssearch');
    Route::get('/products/price', 'Landing\ProductController@productsprice')->name('productsprice');
    Route::get('/products/status/{slug}', 'Landing\ProductController@productsslug')->name('productsslug');
    Route::get('/products/compare/', 'Landing\ProductController@productscompare')->name('productscompare');
    Route::get('/products/begin/compare/', 'Landing\ProductController@gotocompare')->name('gotocompare');



    Route::get('/listPr', function(){
        return view('landing.list-product');
    });
    Route::get('/listPs', function(){
        return view('landing.listpost');
    });

    Route::get('/login', function(){
        return view('landing.login');
    });



    Route::get('/singlepost', function(){
        return view('landing.singlepost');
    });
});

Route::group(['middleware'=>['auth:user']],function(){

    Route::post('submit_comment/{id}', 'Web\BlogController@submit_comment')->name('submit_comment');
    Route::post('submit_comment/product/{id}', 'Landing\ProductController@submit_comment')->name('submit_comment_product');


});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/










Route::group(['prefix'=>'admin'],function(){
    // Authentication Routes...
    Route::get('login', 'Admin\AuthAdmin\LoginController@showLoginForm')->name('admin-login');
    Route::post('login', 'Admin\AuthAdmin\LoginController@login')->name('do-admin-login');
    Route::get('logout', 'Admin\AuthAdmin\LoginController@logout')->name('do-admin-logout');



    Route::group(['middleware'=>['auth:admin',/*'init_admin_common_data'*/]],function(){

        Route::group(['middleware'=>[/*'route_permission'*/]],function(){


            Route::get('/', function(){
                return redirect('admin/dashboard');
            });
            Route::get('dashboard', 'Admin\AdminController@dashboard')->name('adminDashboard');

            Route::get('/user/admin', 'Admin\AdminController@showAdmins')->name('adminList');
            Route::get('/user/admin/add', 'Admin\AdminController@showAddAdminForm')->name('addAdmin');
            Route::post('/user/admin/add', 'Admin\AdminController@saveAdmin')->name('doAddAdmin');
            Route::post('/user/admin/delete', 'Admin\AdminController@deleteAdmin')->name('deleteAdmin');
            Route::get('/user/admin/edit/{id}', 'Admin\AdminController@editAdmin')->name('editAdmin');
            Route::post('/user/admin/edit', 'Admin\AdminController@doEditAdmin')->name('doEditAdmin');

            Route::get('/user/admin/edit_sec_permit/{id}', 'Admin\AdminController@editSectionPermit');
            Route::post('/user/admin/edit_sec_permit', 'Admin\AdminController@doEditSectionPermit');


            Route::get('/category/add/{parent_id?}', 'Admin\CategoryController@showAddCategoryForm')->name('add-category-form');
            Route::post('/category/add', 'Admin\CategoryController@saveCategory')->name('do-add-category');
            Route::post('/category/delete/{parent_id?}', 'Admin\CategoryController@deleteCategory')->name('do-delete-category');
            Route::get('/category/edit/{id}', 'Admin\CategoryController@editCategory')->name('edit-category-form');
            Route::post('/category/edit', 'Admin\CategoryController@doEditCategory')->name('do-edit-category');
            Route::get('/category/{parent_id?}', 'Admin\CategoryController@categories')->name('adminCategories');


            Route::get('/brand/add', 'Admin\BrandController@showAddBrandForm')->name('add-brand-form');
            Route::post('/brand/add', 'Admin\BrandController@saveBrand')->name('do-add-brand');
            Route::post('/brand/delete', 'Admin\BrandController@deleteBrand')->name('do-delete-brand');
            Route::get('/brand/edit/{id}', 'Admin\BrandController@editBrand')->name('edit-brand-form');
            Route::post('/brand/edit', 'Admin\BrandController@doEditBrand')->name('do-edit-brand');
            Route::get('/brand', 'Admin\BrandController@brands')->name('adminBrands');


            Route::get('/comment/add/{product_id}', 'Admin\CommentController@showAddCommentForm')->name('add-comment-form');
            Route::post('/comment/add', 'Admin\CommentController@saveComment')->name('do-add-comment');
            Route::get('/comment/edit/{id}', 'Admin\CommentController@editComment')->name('edit-comment-form');
            Route::post('/comment/edit', 'Admin\CommentController@doEditComment')->name('do-edit-comment');
            Route::post('/comment/delete', 'Admin\CommentController@deleteComment')->name('do-delete-comment');
            Route::get('/comments/{product_id?}', 'Admin\CommentController@comments')->name('adminComments');

            Route::get('/post/comment/add/{post_id}', 'Admin\CommentController@showAddPostCommentForm')->name('add-post-comment-form');
            Route::post('/post/comment/add', 'Admin\CommentController@savePostComment')->name('do-add-post-comment');
            Route::get('/post/comment/edit/{id}', 'Admin\CommentController@editPostComment')->name('edit-post-comment-form');
            Route::post('/post/comment/edit', 'Admin\CommentController@doEditPostComment')->name('do-edit-post_comment');
            Route::post('/post/comment/delete', 'Admin\CommentController@deletePostComment')->name('do-delete-post_comment');
            Route::get('/post/comments/{post_id?}', 'Admin\CommentController@postComments')->name('adminPostComments');


            Route::get('/contact/add/{product_id}', 'Admin\ContactController@showAddContactForm')->name('add-contact-form');
            Route::post('/contact/add', 'Admin\ContactController@saveContact')->name('do-add-contact');
            Route::get('/contact/edit/{id}', 'Admin\ContactController@editContact')->name('edit-contact-form');
            Route::post('/contact/edit', 'Admin\ContactController@doEditContact')->name('do-edit-contact');
            Route::post('/contact/delete', 'Admin\ContactController@deleteContact')->name('do-delete-contact');
            Route::get('/contacts/{product_id?}', 'Admin\ContactController@contacts')->name('adminContacts');



            Route::get('/user/junior', 'Admin\UserController@showUsers')->name('userList');
            Route::get('/user/junior/add', 'Admin\UserController@showAddUserForm')->name('addUser');
            Route::post('/user/junior/add', 'Admin\UserController@saveUser')->name('doAddUser');
            Route::post('/user/junior/delete', 'Admin\UserController@deleteUser')->name('deleteUser');
            Route::get('/user/junior/edit/{id}', 'Admin\UserController@editUser')->name('editUser');
            Route::post('/user/junior/edit', 'Admin\UserController@doEditUser')->name('doEditUser');



            Route::get('/property/add/{parent_id?}', 'Admin\PropertyController@showAddPropertyForm')->name('showAddPropertyForm');
            Route::post('/property/add', 'Admin\PropertyController@saveProperty')->name('saveProperty');
            Route::post('/property/delete', 'Admin\PropertyController@deleteProperty')->name('deleteProperty');
            Route::get('/property/edit/{id}', 'Admin\PropertyController@editProperty')->name('editProperty');
            Route::post('/property/edit', 'Admin\PropertyController@doEditProperty')->name('doEditProperty');
            Route::get('/property/{parent_id?}', 'Admin\PropertyController@properties')->name('properties');



            Route::get('/menu/add/{parent_id?}', 'Admin\MenuController@showAddMenuForm')->name('showAddMenuForm');
            Route::post('/menu/add', 'Admin\MenuController@saveMenu')->name('saveMenu');
            Route::post('/menu/delete', 'Admin\MenuController@deleteMenu')->name('deleteMenu');
            Route::get('/menu/edit/{id}', 'Admin\MenuController@editMenu')->name('editMenu');
            Route::post('/menu/edit', 'Admin\MenuController@doEditMenu')->name('doEditMenu');
            Route::get('/menu/{parent_id?}', 'Admin\MenuController@menus')->name('menus');

            Route::get('/footerLinks/add/{parent_id?}', 'Admin\MenuController@showAddFooterLinkForm')->name('showAddFooterLinkForm');
            Route::post('/footerLinks/add', 'Admin\MenuController@saveFooterLink')->name('saveFooterLink');
            Route::post('/footerLinks/delete', 'Admin\MenuController@deleteFooterLink')->name('deleteFooterLink');
            Route::get('/footerLinks/edit/{id}', 'Admin\MenuController@editFooterLink')->name('editFooterLink');
            Route::post('/footerLinks/edit', 'Admin\MenuController@doEditFooterLink')->name('doEditFooterLink');
            Route::get('/footerLinks/{parent_id?}', 'Admin\MenuController@footerLinks')->name('footerLinks');


            Route::get('/advertise/add', 'Admin\AdvertiseController@showAddAdvertiseForm')->name('addAdvertise');
            Route::post('/advertise/add', 'Admin\AdvertiseController@saveAdvertise')->name('doAddAdvertise');

            Route::get('/advertise/edit/{id}', 'Admin\AdvertiseController@editAdvertise')->name('editAdvertise');
            Route::post('/advertise/edit', 'Admin\AdvertiseController@saveAdvertise')->name('doEditAdvertise');
            Route::get('/advertises', 'Admin\AdvertiseController@showAdvertises')->name('advertisesList');
            Route::post('/advertise/delete', 'Admin\AdvertiseController@deleteAdvertise')->name('deleteAdvertise');

            Route::get('advertise/category/{product_id}','Admin\AdvertiseController@editAdvertiseCategory')->name('adminEditAdvertiseCategory');
            Route::post('advertise/category','Admin\AdvertiseController@doEditAdvertiseCategory')->name('adminDoEditAdvertiseCategory');
            
            //post

            Route::get('/posts', 'Admin\PostController@showPosts')->name('PostsList');
            Route::get('/post/add', 'Admin\PostController@showaddpostform')->name('addpost');
            Route::post('/post/add', 'Admin\PostController@savePost')->name('doAddPost');
            Route::get('/post/edit/{id}', 'Admin\PostController@editPost')->name('editPost');
            Route::post('/post/edit', 'Admin\PostController@savePost')->name('doEditPost');
            Route::post('/post/delete', 'Admin\PostController@deletePost')->name('deletePost');

            Route::get('post/category/{post_id}','Admin\PostController@editPostCategory')->name('adminEditPostCategory');
            Route::post('post/category','Admin\PostController@doEditPostCategory')->name('adminDoEditPostCategory');
            //endpost


            //Post Category

            Route::get('/pcategory/add/{parent_id?}', 'Admin\PCategoryController@showAddCategoryForm')->name('add-pcategory-form');
            Route::post('/pcategory/add/{parent_id?}', 'Admin\PCategoryController@saveCategory')->name('do-add-pcategory');
            Route::post('/pcategory/delete/{parent_id?}', 'Admin\PCategoryController@deleteCategory')->name('do-delete-pcategory');
            Route::get('/pcategory/edit/{id}', 'Admin\PCategoryController@editCategory')->name('edit-pcategory-form');
            Route::post('/pcategory/edit', 'Admin\PCategoryController@doEditCategory')->name('do-edit-pcategory');
            Route::get('/pcategory/{parent_id?}', 'Admin\PCategoryController@categories')->name('adminpCategories');



            //end post category




            Route::get('/slider', 'Admin\GeneralController@slides')->name('adminSlides');
            Route::get('/slider/add/{product_id?}', 'Admin\GeneralController@showAddSlideForm')->name('adminAddSlideFrm');
            Route::post('/slider/add', 'Admin\GeneralController@saveSlide')->name('adminSaveSlide');
            Route::post('/slider/delete', 'Admin\GeneralController@deleteSlide')->name('adminDeleteSlide');
            Route::get('/slider/edit/{id}', 'Admin\GeneralController@editSlide')->name('adminEditSlide');
            Route::post('/slider/edit', 'Admin\GeneralController@doEditSlide')->name('adminDoEditSlide');


            Route::get('/slider2', 'Admin\GeneralController@slides2')->name('adminSlides2');
            Route::get('/slider2/add/{product_id?}', 'Admin\GeneralController@showAddSlide2Form')->name('adminAddSlide2Frm');
            Route::post('/slider2/add', 'Admin\GeneralController@saveSlide2')->name('adminSaveSlide2');
            Route::post('/slider2/delete', 'Admin\GeneralController@deleteSlide2')->name('adminDeleteSlide2');
            Route::get('/slider2/edit/{id}', 'Admin\GeneralController@editSlide2')->name('adminEditSlide2');
            Route::post('/slider2/edit', 'Admin\GeneralController@doEditSlide2')->name('adminDoEditSlide2');



            Route::get('/lists/add', 'Admin\GeneralController@showAddListForm')->name('adminAddListForm');
            Route::post('/lists/add', 'Admin\GeneralController@saveList')->name('adminDoAddList');
            Route::post('/lists/delete', 'Admin\GeneralController@deleteList')->name('adminDoDeleteList');
            Route::get('/lists/edit/{id}', 'Admin\GeneralController@editList')->name('adminEditList');
            Route::post('/lists/edit', 'Admin\GeneralController@doEditList')->name('adminDoEditList');
            Route::get('/lists', 'Admin\GeneralController@lists')->name('adminLists');


            Route::get('advertise/list/{product_id}','Admin\GeneralController@editAdvertiseList')->name('adminEditAdvertiseList');
            Route::post('advertise/list','Admin\GeneralController@doEditAdvertiseList')->name('adminDoEditAdvertiseList');
            Route::get('list/getAdvertises/{list_id}','Admin\GeneralController@getListAdvertises')->name('adminGetListAdvertises');

            Route::get('/discount/add', 'Admin\DiscountController@showAddDiscountForm')->name('adminAddDiscountForm');
            Route::post('/discount/add', 'Admin\DiscountController@saveDiscount')->name('adminDoAddDiscount');
            Route::post('/discount/delete', 'Admin\DiscountController@deleteDiscount')->name('adminDoDeleteDiscount');
            Route::get('/discount/edit/{id}', 'Admin\DiscountController@editDiscount')->name('adminEditDiscount');
            Route::post('/discount/edit', 'Admin\DiscountController@doEditDiscount')->name('adminDoEditDiscount');
            Route::get('/discounts', 'Admin\DiscountController@discounts')->name('adminDiscounts');


            Route::get('/config', 'Admin\GeneralController@editConfig')->name('editConfig');
            Route::post('/config', 'Admin\GeneralController@doEditConfig')->name('doEditConfig');


            Route::get('/note/add', 'Admin\PostController@showAddNoteForm')->name('addNote');
            Route::post('/note/add', 'Admin\PostController@saveNote')->name('doAddNote');
            Route::get('/note/edit/{id}', 'Admin\PostController@editNote')->name('editNote');
            Route::post('/note/edit', 'Admin\PostController@saveNote')->name('doEditNote');
            Route::get('/notes', 'Admin\PostController@showNotes')->name('notesList');
            Route::post('/note/delete', 'Admin\PostController@deleteNote')->name('deleteNote');

        });
    });


});



